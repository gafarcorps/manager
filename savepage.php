<?php 
	
    require_once("includes/initialize.php");	
	
					
		if(isset($_POST['pid']))			{$pid      	     	=	$_POST['pid'];	   		} //$pid is the id of current page
		if(isset($_POST['parent_id']))	    {$parent_id         =	$_POST['parent_id'];	}
		if(isset($_POST['title']))	 		{$page_title		=	$_POST['title'];		}
	 	if(isset($_POST['desc']))			{$page_desc			=	$_POST['desc'];			}
		 if(isset($_POST['desc1']))			{$page_desc1			=	$_POST['desc1'];			}
		if(isset($_POST['position']))		{$position		    =	$_POST['position']; 	}
		if(isset($_POST['active']))			{$page_active		=	$_POST['active'];		}
		if(isset($_POST['home']))			{$page_home		=	$_POST['home'];		}	
		// Add By Boris
		if(isset($_POST['link']))			{$page_link			=	$_POST['link'];}
		if(isset($_POST['only_desc']))			{
			$page_only_desc			=	($_POST['only_desc']) ? true : false;
			$page_link = cleanInput($page_title);
			$page_link = sanitize_url($page_link);
			$page_link = replace_accents($page_link);
		}
		if(empty($page_only_desc))
			$page_only_desc = 0;
		
		if (!empty($_FILES['file_upload']['name'])) {
			$repository = 'media/files/';
			$file_upload = basename($_FILES['file_upload']['name']);
		}
		//
		if(!empty($page_active))        	{$page_active = 1; } 
		else {$page_active = 0;	}

		if(!empty($page_home))        	{$page_home = 1; } 
		else {$page_home = 0;	}
	 
 	$page_date = date('Y-m-d H:i:s');

   if(isset($_POST['new']) && !empty($session->user_account_id))	{	// New Page menu 
      
	 		
									$page->page_parent_id  				 = $parent_id;
									$page->page_uid 					 = $session->user_account_id;
									$page->page_title   				 = $page_title;
									$page->page_desc     				 = $page_desc;
									$page->page_desc_mini     				 = $page_desc1;
									$page->page_link                	 = $page_link;
									$page->page_position 				 = $position;
									$page->page_date				     = $page_date;
									$page->page_active  				 = $page_active;
									$page->page_home  				     = $page_home;
									$page->page_only_desc  				 = $page_only_desc;
									$page->page_section                  =0;

	   if (!empty($_FILES['file_upload'])) {
		   move_uploaded_file($_FILES['file_upload']['tmp_name'], $repository . $file_upload);
		   $page->page_image_path = $file_upload;
	   }
									
		$page->save();

		if(!empty($_POST['section_id'][0])){
					foreach($_POST['section_id'] as $section_id){
					 
						$page_section->page_section_pid = $page->page_id;
						$page_section->page_section_sid = $section_id;
						$page_section->save();
					}
				}
								 	redirect_to('app.php?item=listpages&msg=success');									
		} // END IF NEW ENTRY  
		





	if(isset($_POST['submit'])  && !empty($pid) && !empty($session->user_account_id))	{	// Updating a page			   	
			  
			  	$found_page = $page->find_by_id($pid); 
				
				$page->page_id       =  $found_page->page_id;	  
				
				if(!empty($parent_id)){ // UPDATE IF THERE ANY POSTED PARENT_ID VARIABLE
				$page->page_parent_id     =  $parent_id; 
				} 
				else{  // REPLACE THE ASSET VALUE
				$page->page_parent_id     = $found_page->page_parent_id;  				 
				} 

				$page->page_uid =  $session->user_account_id; // UPDATE THE PAGE_UID FIELD


				if(!empty($page_title)){  // UPDATE IF THERE ANY POSTED TITLE VARIABLE
				$page->page_title =  $page_title; 
				} 
				else{ // REPLACE THE ASSET VALUE
				$page->page_title  = $found_page->page_title;  				 
				}

				if(!empty($page_desc)){ // UPDATE IF THERE ANY POSTED DESC VARIABLE
				$page->page_desc =  $page_desc; 
				} 
				else{ // REPLACE THE ASSET VALUE
				$page->page_desc  = $found_page->page_desc;  
				}

				if(!empty($page_desc1)){ // UPDATE IF THERE ANY POSTED DESC VARIABLE
					$page->page_desc_mini =  $page_desc1; 
					} 
					else{ // REPLACE THE ASSET VALUE
					$page->page_desc_mini  = $found_page->page_desc_mini;  
					}

				//$page->page_link     =  urlencode($found_page->page_link); 
				$page->page_link     =  urlencode($page_link); 

				if(!empty($position)){ // UPDATE IF THERE ANY POSTED POSITION VARIABLE
				$page->page_position =  $position; 
				} 
				else{ // REPLACE THE ASSET VALUE
				$page->page_position  = $position; 
				} 
				$page->page_date   	 =  $page_date; 
				$page->page_active   =  $page_active;
				$page->page_home   =  $page_home;
				$page->page_only_desc   =  $page_only_desc;

				if (!empty($file_upload))  {
					move_uploaded_file($_FILES['file_upload']['tmp_name'], $repository . $file_upload);
					$page->page_image_path = $file_upload;
				}else {
					$page->page_image_path = $found_page->page_image_path;
				}
 
				 $page->save();
				
				if(!empty($_POST['section_id'][0])){

					foreach($_POST['section_id'] as $section_id){

						$found_page_section = $page_section->find_by_ids($pid, $section_id);
						//echo $found_page_section->page_section_id.'<br/>'; 
					 	if(empty($found_page_section)){ 
							$page_section->page_section_id  = null;
							$page_section->page_section_pid = $pid;
							$page_section->page_section_sid = $section_id;
							$page_section->save();
							}// end if
							else{
							    $page_section->page_section_id  = $found_page_section->page_section_id; 
								$page_section->page_section_pid = $pid;
								$page_section->page_section_sid = $section_id;
								$page_section->save();
								}// end else							
							
						
					} // end foreach
				}// end if
				 
			 	if($found_page->page_parent_id == 11){
			 	 	redirect_to('app.php?item=listpages&msg=success');
				}
				 else{ redirect_to('app.php?item=listpages&pid='.$found_page->page_parent_id.'&msg=success');
			}  
		}  // END IF UPDATE 	
						
 

 

	if (!empty($_GET['pid'])  && !empty($session->user_account_id) && $_GET['option']=='delete'){  // DELETE ENTRIE
											
															$pid  = $_GET['pid'];												
															$page = $page->find_by_id($pid);																
															$del  = $page->unable();
															redirect_to('app.php?item=formpage&pid='.$pid.'&option=edit&r=0');
	}  // END IF DELETE 
	 
	if (!empty($_GET['pid'])  && !empty($session->user_account_id) && $_GET['option']=='recover'){  // DELETE ENTRIE
											
															$pid  = $_GET['pid'];												
															$page = $page->find_by_id($pid);																
															$del  = $page->recover();
															redirect_to('app.php?item=formpage&pid='.$pid.'&option=edit&r=1');
	}  // END IF DELETE 
	 

?>
 