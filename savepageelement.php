<?php 
	 require_once("includes/initialize.php");	

	if(isset($_POST['submit']))   		{
	
		if(isset($_POST['page_id']))		{ $page_id  	    =	$_POST['page_id'];	    	}
		if(isset($_POST['page_element_id']))     	{ $page_element_id			= 	$_POST['page_element_id'];				}
		if(isset($_POST['title']))		{ $title 		= 	addslashes($_POST['title']);			}
		if(isset($_POST['desc']))		{ $desc 		= 	addslashes($_POST['desc']);   	        }
		if(isset($_POST['icon']))		{ $icon 		= 	$_POST['icon'];   	        }
		if(isset($_POST['url']))		{ $url 	    	= 	$_POST['url'];   	        }
		$date 	= 	date('Y-m-d');
		if(isset($_POST['active']))		{ $active		= 	($_POST['active']) ? true : false;	}
		if(isset($_POST['position']))	{$position		=	$_POST['position']; 	}
		if(isset($_POST['type']))		{ $type 		= 	$_POST['type'];			}

		$repository = 'media/files/';

		
		
			if (empty($_POST['page_element_id'])){ // NEW ENTRY
			
				$page_element->page_element_page_id	    = 	$page_id;

				$page_element->page_element_title 		= 	$title;
				$page_element->page_element_desc 		=	$desc;
				$page_element->page_element_icon		=	$icon;
				$page_element->page_element_position    = 	$position;
				$page_element->page_element_date  	    = 	$date;
				$page_element->page_element_active 		= 	$active;

				if(!empty($_FILES['file'])){
					$file = basename($_FILES['file']['name']) ;
					move_uploaded_file($_FILES['file']['tmp_name'], $repository . $file);
					$page_element->page_element_file 		= 	$file;
				}

				$page_element->save();

				if(!empty($_POST['other_page_id'][0])){
					foreach($_POST['other_page_id'] as $other_page_id){
						$page_element_other = $page_element;
						$page_element_other->page_element_id = null;
						$page_element_other->page_element_page_id = $other_page_id;
						$page_element_other->save();
					}
				}
				
			} // END IF NEW ENTRY  			

			else{ // UPDATE ENTRY
		     	$found_page_element = Page_element::find_by_id($page_element_id);
				$page_element->page_element_id = $found_page_element->page_element_id;
				$page_element->page_element_page_id     = $page_id;
				$page_element->page_element_title 		= 	isset($title) ? $title : $found_page_element->page_element_id;
				$page_element->page_element_desc 		= 	isset($desc) ? $desc : $found_page_element->page_element_desc;
				$page_element->page_element_icon 		= 	isset($icon) ? $icon : $found_page_element->page_element_icon;
				$page_element->page_element_position 	= 	isset($position) ? $position : $found_page_element->page_element_position;
				$page_element->page_element_title 		= 	isset($title) ? $title : $found_page_element->page_element_id;
				$page_element->page_element_date  	    = 	$date;
				$page_element->page_element_active 		= 	$active;

				if(!empty($_FILES['file']['name'])){
					$file = basename($_FILES['file']['name']) ;
					move_uploaded_file($_FILES['file']['tmp_name'], $repository . $file);
					$page_element->page_element_file 		= 	$file;
				}else{
					$page_element->page_element_file = $found_page_element->page_element_file;
				}

				$page_element->save();
						
				}  // END IF UPDATE
			 	
			 	if(!empty($type)) { 
			 		               redirect_to('app.php?item=listpageelements&id='.$page_element->page_element_page_id.'&type='.$type.'&msg=success');
			 		              }
			 		             else{
			 		             	redirect_to('app.php?item=listpageelements&id='.$page_element->page_element_page_id.'&msg=success');
			 		             } 

	} // END IF SUBMIT

	if ($_GET['option']=='delete'){  // DELETE
		$id  = $_GET['id'];
		$pid  = $_GET['pid'];
		$page_element = Page_element::find_by_id($id);
		$del  = $page_element->delete();
		redirect_to('app.php?item=listpageelements&id='.$pid.'&msg=del');
	}  // END IF DELETE 
	
	
?>
