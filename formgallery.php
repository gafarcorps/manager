<style>
    .img{
        position: relative;
    }
    .img .img-options{
        position: absolute;
        z-index: 99 !important;
        margin-left: auto;
        margin-right: auto;
        left: 0;
        right: 0;
        top: 45%;
        /*bottom: 0;*/
        text-align: center;
    }
    .img span{
        width: 10px;
        border: 2px solid #fff;
        border-radius: 50px;
        padding: 10px;
    }
    .img i{
        color: #fff;
        font-size: 20px
    }
    .img{
        opacity: 0.7;
    }
    .img:hover {
        opacity: 1;
        zoom: 10%;
    }
</style>



<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans PressManager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="dashboard.php">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="app.php?item=listpages">Pages</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Editer une page</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Galerie</header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row p-b-20">
                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group">
                                    <a href="#" id="add" class="btn btn-info">
                                        Ajouter une Photo <i class="fa fa-plus"></i>
                                    </a> &nbsp; &nbsp;
                                    <form action="savegallery.php" enctype="multipart/form-data" method="post">
                                        <input type="file" name="image[]" id="img" style="visibility: hidden" >
                                        <div class="btn-group pull-left" id="save" style="display: none">
                                            <button type="submit" name="submit" class="btn btn-block">Enregistrer</button>
                                        </div>
                                    </form>
                                    <label for="" class="pull-left" id="label-img-names"></label>
                                </div>
                            </div>
                        </div>

                        <?php
                        if(isset($_GET['msg']) && $_GET['msg']=='success') {
                            echo '<span class="clsAvailable"> Effectué avec succès. </span>';
                        }
                        ?>

                        <div class="col-md-12" style="text-align: center">
                            <?php
                                $galleries = Gallery::find_all();
                                foreach ($galleries as $gallery){
                            ?>
                                <div class="img" style="width: 30%; display: inline-block; margin: 0">
                                    <div class="img-options">
<!--                                        <span><a href=""><i class="fa fa-pencil"></i></a></span>-->
                                        <span><a href="#" data-href="savegallery.php?id=<?php echo $gallery->gallery_id ?>" class="delete-img"><i class="fa fa-trash"></i></a></span>
                                    </div>
                                    <img src="media/files/<?php echo $gallery->gallery_image_path ?>" alt="" style="height: 300px; width: 100%; object-fit: cover">
                                </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!-- end page container -->

        <script src="assets/plugins/jquery/jquery.min.js"></script>

        <script>
            $('#add').click(function (e) {
                e.preventDefault()
                $('#img').trigger('click')
                $('#save').show()
            })

            $('#img').change(function(){
                if($(this)[0].files.length == 1)
                    $('#label-img-names').text($(this)[0].files[0].name)
                else{
                    $('#label-img-names').text($(this)[0].files.length + ' fichiers sélectionnés')
                }
            })

            $('.delete-img').click(function(){
                var valid = confirm('Voulez-vous vraiment supprimer ?')
                if(valid == true)
                    window.location.href = $(this).attr('data-href')
            })
        </script>

