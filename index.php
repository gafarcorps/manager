<?php

require_once("includes/initialize.php");

if ($session->is_logged_in()) {
    redirect_to("app.php?item=home");
}
redirect_to("login.php");