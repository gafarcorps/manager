
<!-- start page content -->
<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Bienvenue dans Manager</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
										href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li><a class="parent-item" href="#">Email</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Inbox</li>
							</ol>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="card card-topline-gray">
								<div class="card-body no-padding height-9">
									<div class="inbox">
										<div class="row">
											<div class="col-md-3">
												<div class="inbox-sidebar">
													<a href="#" data-title="Compose"
														class="btn red compose-btn btn-block">
														<i class="fa fa-envelope"></i> Messagerie </a>
													<ul class="inbox-nav inbox-divider">
														<li class="active"><a href="app.php?item=ticket"><i
																	class="fa fa-inbox"></i> Boîte de réception <span
																	class="label mail-counter-style label-danger pull-right"><?php echo $ticket->count_all() ?></span></a>
														</li>
                                                        <li><a href="#"><i class="fa fa-paper-plane"></i> Messages envoyés</a>
                                                        </li>
                                                        <li><a href="#"><i class="fa fa-trash"></i> Corbeille</a>
                                                        </li>
                                                        <li><a href="#"><i class="fa fa-exclamation-circle"></i> Spam</a>
                                                        </li>
														<li><a href="#"><i class="fa fa-star"></i> Prioritaires</a>
														</li>
														<li><a href="#"><i class="fa fa-briefcase"></i> Résolus </a>
														</li>
														<li><a href="#"><i class=" fa fa-external-link"></i> En attente
																<span
																	class="label mail-counter-style label-info pull-right">30</span></a>
														</li>
													
													</ul>
													<!--
                                                    <ul class="nav nav-pills nav-stacked labels-info inbox-divider">
														<li>
															<h4>Labels</h4>
														</li>
														<li><a href="#"><i class="fa fa-tags"></i> Work</a>
														</li>
														<li>
															<a href="#">
																<i class=" fa fa-tags"></i> Design
															</a>
														</li>
														<li>
															<a href="#">
																<i class=" fa fa-tags "></i> Family
															</a>
														</li>
														<li>
															<a href="#">
																<i class=" fa fa-tags "></i> Friends
															</a>
														</li>
														<li>
															<a href="#">
																<i class=" fa fa-tags "></i> Office
															</a>
														</li>
													</ul>
													<ul class="nav nav-pills nav-stacked labels-info inbox-divider ">
														<li>
															<h4>Buddy online</h4>
														</li>
														<li>
															<a href="#">
																<i class=" fa fa-comments text-success"></i> Jhone Doe
																<span class="online-status">I do not think</span>
															</a>
														</li>
														<li>
															<a href="#">
																<i class=" fa fa-comments text-danger"></i> Sumon
																<span class="online-status">Busy with coding</span>
															</a>
														</li>
														<li>
															<a href="#">
																<i class=" fa fa-comments text-muted "></i> Anjelina
																Joli
																<span class="online-status">I out of control</span>
															</a>
														</li>
														<li>
															<a href="#">
																<i class=" fa fa-comments text-muted "></i> Jonathan
																Smith
																<span class="online-status">I am not here</span>
															</a>
														</li>
														<li>
															<a href="#">
																<i class=" fa fa-comments text-muted "></i> Tawseef
																<span class="online-status">I do not think</span>
															</a>
														</li>
													</ul>
                                                    -->
												</div>
											</div>
                                            
											<div class="col-md-9">
												<div class="inbox-body">
													<div class="inbox-header">
														<div class="mail-option no-pad-left">
                                                            <?php if(isset($_GET['id'])){ ?>
                                                                <div class="btn-group group-padding">
                                                                    <a class="btn mini tooltips" href="#"
                                                                        data-original-title="Corbeille"> <i
                                                                            class=" fa fa-trash-o fa-lg"></i>
                                                                    </a>
                                                                    <a class="btn mini tooltips" href="#"
                                                                       data-original-title="Prioritaire"> <i
                                                                                class=" fa fa-star"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="btn-group group-padding">
                                                                    <a class="btn btn-primary" href="#"> Ouvert</a>
                                                                    <a class="btn btn-default" href="#"> En traitement</a>
                                                                    <a class="btn btn-danger" href="#"> Critique</a>
                                                                    <a class="btn btn-success" href="#"> Résolu</a>
                                                                </div>
                                                                <div class="btn-group hidden-phone">
                                                                    <a class="btn mini blue-bgcolor" href="#"
                                                                        data-toggle="dropdown" aria-expanded="false"> Déplacer en
                                                                        <i class="fa fa-angle-down downcolor"></i>
                                                                    </a>
                                                                    <ul class="dropdown-menu">
                                                                        <li><a href="#"> Informatique
                                                                                </a>
                                                                        </li>
                                                                        <li><a href="#">
                                                                               Secretariat
                                                                            </a>
                                                                        </li>
                                                                        <li class="divider"></li>
                                                                        <li><a href="#">
                                                                                Comptabilité</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="btn-group hidden-phone">
                                                                    <a class="btn mini blue-bgcolor" href="#"
                                                                       data-toggle="dropdown" aria-expanded="false"> Assigner à
                                                                        <i class="fa fa-angle-down downcolor"></i>
                                                                    </a>
                                                                    <ul class="dropdown-menu">
                                                                        <li><a href="#">
                                                                                Menz</a>
                                                                        </li>
                                                                        <li><a href="#"> D9
                                                                            </a>
                                                                        </li>
                                                                        <li><a href="#">
                                                                                Hopy</a>
                                                                        </li>
                                                                        <li class="divider"></li>
                                                                        <li><a href="#">
                                                                                Bob</a>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="btn-group pull-right btn-prev-next">
                                                                    <a href="app.php?item=ticket&id=<?php echo $_GET['id'] - 1 ?>">
                                                                        <button class="btn btn-sm btn-primary" type="button">
                                                                            <i class="fa fa-chevron-left"></i>
                                                                        </button>
                                                                    </a>

                                                                    <a href="app.php?item=ticket&id=<?php echo $_GET['id'] + 1 ?>">
                                                                        <button class="btn btn-sm btn-primary" type="button">
                                                                            <i class="fa fa-chevron-right"></i>
                                                                        </button>
                                                                    </a>
                                                                </div>
                                                            <?php }else{ ?>
                                                                <div class="btn-group group-padding">
                                                                    <a class="btn mini tooltips" href="#"
                                                                       data-original-title="Corbeille"> <i
                                                                                class=" fa fa-trash-o fa-lg"></i>
                                                                    </a>
                                                                </div>
                                                                <div class="btn-group pull-right btn-prev-next">
                                                                    <button class="btn btn-sm btn-primary" type="button">
                                                                        <i class="fa fa-chevron-left"></i>
                                                                    </button>
                                                                    <button class="btn btn-sm btn-primary" type="button">
                                                                        <i class="fa fa-chevron-right"></i>
                                                                    </button>
                                                                </div>
                                                                <div class="todo-check pull-left m-l-20">
                                                                    <input type="checkbox" value="None" id="todo-check30">
                                                                    <label for="todo-check30"></label>
                                                                </div>
                                                            <?php } ?>

														</div>
													</div>
													<?php require_once('includes/initvar.php') ?>
													<div class="inbox-body no-pad table-responsive">
                                                        <?php if(!isset($_GET['id'])){ ?>
														<table class="table table-inbox table-hover">
															<tbody>
                                                             <?php
                                                                $found_tickets = Ticket::find_all_active();
                                                                foreach($found_tickets as $found_ticket){
                                                             ?>
                                                                
                                                                <tr class="<?php echo $found_ticket->ticket_status == 0 ? 'unread' : '' ?>">
																
																	<td class="inbox-small-cells">
																		<div class="todo-check pull-left">
																			<input type="checkbox" value="None"
																				id="todo-check1">
																			<label for="todo-check1"></label>
																		</div>
																	</td>
                                                                    <!--<td>
																		<a href="#" class="avatar">
																			<span class="bg-success">R</span>
																		</a>
																	</td>-->
																	<td class="inbox-small-cells"><i
																			class="fa fa-star <?php echo $found_ticket->ticket_priority == 1 ? 'inbox-started' : '' ?>"></i>
																	</td>
														
																	<td class="view-message  dont-show">
																		<a href="app.php?item=ticket&id=<?php echo $found_ticket->ticket_id ?>">
																			<?php echo $found_ticket->ticket_email ?>
																		</a>
																	</td>
																	<td class="view-message "><a
																			href="app.php?item=ticket&id=<?php echo $found_ticket->ticket_id ?>"><?php echo neatest_trim($found_ticket->ticket_msg, 50) ?></a></td>
																	<td class="view-message  inbox-small-cells">
																		<?php if($found_ticket->ticket_attachments) 
																			echo '<i class="fa fa-paperclip"></i>'; 
																		?>
																	</td>
																	<td class="view-message  text-right"><?php echo changedateusfr($found_ticket->ticket_datestart) ?></td>
																	</a>
																</tr>
																
                                                            <?php } ?>    
															
															</tbody>
														</table>
                                                        <?php }else{
                                                            $found_ticket = Ticket::find_by_id($_GET['id'])
                                                        ?>

                                                        <section class="mail-list">
															<div class="mail-sender">
																<div class="mail-heading">
																	<h4 class="vew-mail-header"><b><?php echo neatest_trim($found_ticket->ticket_msg, 20) ?>
																		</b></h4>
																</div>
																<hr>
																<div class="media">
																	
																	<div class="media-body">
																		<span class="date pull-right"><?php echo changedateusfr($found_ticket->ticket_datestart) ?>
																			</span>
																	
																		<small class="text-muted">De:
                                                                        <?php echo $found_ticket->ticket_email ?></small>
																	</div>
																</div>
															</div>
															<div class="view-mail">
																<p>
                                                                    <?php echo $found_ticket->ticket_msg ?>
																</p>
															</div>
															<?php if($found_ticket->ticket_attachments){ ?>
																<div class="attachment-mail">
																	<p>
																		<span><i class="fa fa-paperclip"></i> <?php echo count(explode($found_ticket->ticket_attachments, ',')) ?> pièce(s) jointe(s)
																			</span>
																	</p>
																	<ul>
																		<?php foreach(explode(',', $found_ticket->ticket_attachments) as $file){ ?>
																			<li>
																				<?php echo $file ?>
																				
																				</a>
																				<div class="links">
																					<a href="mail_files/<?php echo $file ?>" target="_blank">Télécharger</a>
																				</div>
																			</li>
																		<?php } ?>
																	
																	</ul>
																</div>
															<?php } ?>
															<div class="compose-btn pull-left">

                                                                <a href="email_compose.html"
                                                                   class="btn btn-sm btn-primary"><i
                                                                            class="fa fa-reply"></i> Répondre</a>
<!--																<button class="btn  btn-sm btn-default tooltips"-->
<!--																	data-original-title="Print" type="button"-->
<!--																	data-toggle="tooltip" data-placement="top" title="">-->
<!--																	<i class="fa fa-print"></i>-->
<!--																</button>-->
															</div>
														</section>

                                                        <?php } ?>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- end page content -->
   