
		 	<!-- start page content -->
			<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Bienvenue dans Manager</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
										href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li><a class="parent-item" href="#">Sections</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Menus</li>
							</ol>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="card card-box">
							 
								<div class="card-body ">
									<div class="row p-b-20">
										<div class="col-md-6 col-sm-6 col-6"> 
											<div class="btn-group">
											<!--	<a href="app.php?item=formpage&pid=11&option=new" id="addRow" class="btn btn-info">
													Ajouter une nouvelle section <i class="fa fa-plus"></i>
												</a> -->
												
											</div>
										</div>
										
										<div class="col-md-6 col-sm-6 col-6">
											<div class="btn-group pull-right"> 
											</div>
										</div>
									</div>

									<?php
												 if(isset($_GET['msg']) && $_GET['msg']=='success') {
													  echo '<span class="clsAvailable"> Enregistrement réussi. </span>';
												 	} 
									    ?>

										  <div class="row">
                        <div class="col-md-12">
                            <div class="card card-topline-aqua">
                                <div class="card-head">
                                    <header>SECTION ET MENUS</header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                        <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">
                                    <div class="table-scrollable">
                                        <table id="example1" class="display full-width">
													<thead>
														     <tr>  
																	<th class="center"></th>
																	<th class="center">Titre </th>
															        <th class="center"> Sous-menus </th>  
																	<th class="center"> Eléments </th>
																	<th class="center"> Action </th>
																</tr>
															</thead>
															<tbody>
															
																<?php
																	  $found_pages = Page :: find_all_page_sections();

																		foreach($found_pages as $found_page){

																		 
																					echo '<tr class="odd gradeX">';
																								echo '<td class="center">'.$found_page->page_position.'</td>';
																								 
																								echo '<td class="center"><a href="app.php?item=listpageelements&id='.$found_page->page_id.'&type=section">'.$found_page->page_title.'</td>';
																								 
																								$nb = Page :: count_page_children($found_page->page_id);
																								if(!empty($nb)){
																								echo '<td class="center"><a href="app.php?item=listpages&pid='.$found_page->page_id.'">'.$nb.'</a></td>';
																								}
																								else {
																								echo '<td class="center">0</td>';
																								}
																								   
                                                                                                echo "<td class='center'><a href='app.php?item=listpageelements&id=".$found_page->page_id."&type=section'>".$found_page->count_page_elements()."</a></td>";
																								echo '<td class="center">';
																										
 																										
 																										if($found_page->page_active==1) {
																											echo ' <a href="app.php?item=formpage&pid='.$found_page->page_id.'&option=edit" class="label label-info">Publié</a>';
																										 } 
																										else { echo ' <a href="app.php?item=formpage&pid='.$found_page->page_id.'&option=edit" class="label label-danger">Non Publié</a>';
																										} 
																								echo '</td>'; 
																								
																					echo '</tr>'; 
																		} // end foreach
																?>
																
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> 
			<!-- end page content -->