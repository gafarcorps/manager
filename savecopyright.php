<?php

require_once "includes/initialize.php";

if (isset($_POST['submit'])) {

    if (isset($_POST['nom'])) {
        $nom = $_POST['nom'];
    }

    if (isset($_POST['tel1'])) {
        $tel1 = $_POST['tel1'];
    }
    if (isset($_POST['tel2'])) {
        $tel2 = $_POST['tel2'];
    }

    if (isset($_POST['email'])) {
        $email = $_POST['email'];
    }

    if (isset($_POST['address'])) {
        $address = $_POST['address'];
    }

    if (isset($_POST['copyright_website_url'])) {
        $copyright_website_url = $_POST['copyright_website_url'];
    }
    if (isset($_POST['copyright_manager_url'])) {
        $copyright_manager_url = $_POST['copyright_manager_url'];
    }

    if (isset($_POST['texte'])) {
        $texte = $_POST['texte'];
    }

    if (isset($_POST['copyright_text'])) {
        $text = $_POST['copyright_text'];
    }

    $repository = 'media/files/';

    $copyright = new Copyright();

    $found_copyright = Copyright::find_by_id(1);

    $copyright->copyright_id = $found_copyright->copyright_id;
    $copyright->copyright_username = $nom;
    $copyright->copyright_text = $text;
    $copyright->copyright_tel1 = $tel1;
    $copyright->copyright_tel2 = $tel2;
    $copyright->copyright_email = $email;
    $copyright->copyright_address = $address;
    $copyright->copyright_website_url = $copyright_website_url;
    $copyright->copyright_manager_url = $copyright_manager_url;
    if (empty($found_copyright->copyright_date)) {
        $copyright->copyright_date = $found_copyright->copyright_date;
    } else {
        $copyright->copyright_date = date('Y-m-d');
    }

    if (!empty($_FILES['logo']['name'])) { // UPLOAD PAGE IMAGE
        $image = basename($_FILES['logo']['name']);
        move_uploaded_file($_FILES['logo']['tmp_name'], $repository . $image);
        $copyright->copyright_logo = $image;
    } else {
        $copyright->copyright_logo = $found_copyright->copyright_logo;
    }

    $copyright->copyright_active = 1;

    $copyright->update();

}

redirect_to('app.php?item=formcopyright');