<!-- start sidebar menu -->
<div class="sidebar-container">
    <div class="sidemenu-container navbar-collapse collapse fixed-menu">
        <div id="remove-scroll">
            <ul class="sidemenu page-header-fixed p-t-20" data-keep-expanded="false" data-auto-scroll="true"
                data-slide-speed="200">
                <li class="sidebar-toggler-wrapper hide">
                    <div class="sidebar-toggler">
                        <span></span>
                    </div>
                </li>
                <?php
                $found_userinfos = User_info::find_user_infos($session->user_account_id);
                $found_useraccount = User_account::find_by_id($session->user_account_id);
                $found_typeuser = Typeuser::find_by_id($found_useraccount->user_account_typeuser_id);

                echo '<li class="sidebar-user-panel">';
                echo '<div class="user-panel">';
                echo '<div class="row">';
                echo '<div class="sidebar-userpic">';
                echo '<img src="media/files/' . $found_userinfos->user_info_image . '" class="img-responsive" alt="' . $found_userinfos->full_name() . '">';
                echo '</div>';
                echo '</div>';
                echo '<div class="profile-usertitle">';
                echo '<div class="sidebar-userpic-name">' . $found_userinfos->full_name() . '</div>';
                echo '<div class="profile-usertitle-job">' . $found_typeuser->type_user_desc . '</div>';
                echo '</div>';

                echo '</div>';
                echo '</li>';
                ?>
                <li class="menu-heading">
                    <i class="material-icons" style="vertical-align: middle">subject</i>
                    <span> Menu Général</span>
                </li> 

                <?php
                $found_menus = Menu::list_menus_active();
                foreach ($found_menus as $found_menu) {
                    if (strpos($found_menu->menu_link, "app.php") !== false && $found_typeuser->type_user_id > 2) {
                        $menu_link = str_replace("app.php", "appex.php", $found_menu->menu_link);
                    }
                    elseif (strpos($found_menu->menu_link, "appex.php") !== false && $found_typeuser->type_user_id < 3) {
                        $menu_link = str_replace("appex.php", "app.php", $found_menu->menu_link);
                    }

                    echo '<li class="nav-item">';


                    if ($found_menu->menu_id == 21) {
//																					echo '<a class="tooltips" href="'.$found_menu->menu_link.'" data-placement="top" data-original-title="Logout">
//																						<i class="icon-logout"></i>'.$found_menu->menu_desc.' </a> ';
                    } // end if
                    else {
                        if ($privileges->is_active($found_typeuser->type_user_id, $found_menu->menu_id)) {
                            $menu_link = $found_menu->menu_link;
                            echo '<a href="' . $menu_link . '" class="nav-link nav-toggle">';
                            echo '<i class="material-icons">trending_flat</i>';
                            echo '<span class="title">' . $found_menu->menu_title . '</span>';
                            $found_menu_children = Menu::list_menu_children($found_menu->menu_id);
                            if (!empty($found_menu_children)) {
                                echo '<span class="arrow"></span>';
                            }
                            echo '</a>';
                        }
                    }

                    $found_menu_children = Menu::list_menu_children_active($found_menu->menu_id);
                    if (!empty($found_menu_children)) {
                        echo '<ul class="sub-menu">';
                        foreach ($found_menu_children as $found_menu_child) {
                            if (strpos($found_menu_child->menu_link, "app.php") !== false && $found_typeuser->type_user_id > 2) {
                                $menu_link = str_replace("app.php", "appex.php", $found_menu_child->menu_link);
                            } elseif (strpos($found_menu_child->menu_link, "appex.php") !== false && $found_typeuser->type_user_id < 3) {
                                $menu_link = str_replace("appex.php", "app.php", $found_menu_child->menu_link);
                            } else {
                                $menu_link = $found_menu_child->menu_link;
                            }
                            if ($found_menu_child->menu_is_extension == 0) {
                                echo '<li class="nav-item">';
                                if (strpos($found_menu_child->menu_link, "menu") !== false || strpos($found_menu_child->menu_link, "extension") !== false)
                                    echo '<a href="' . $menu_link . '" class="nav-link ">';
                                else
                                    echo '<a href="' . $menu_link . '&mid=' . $found_menu_child->menu_id . '" class="nav-link ">';
                                echo '<span class="title">' . $found_menu_child->menu_title . '</span>';
                                echo '</a>';

                                echo '</li>';
                            }
                        } // end foreach 
                        echo '</ul>';
                    } // end if


                    echo '</li>';
                } // end foreach 
                ?>
                <li class="nav-item">
                    <a class="tooltips" href="logout.php" data-placement="top" data-original-title="Logout">
                        <i class="icon-logout"></i> Logout </a> ';
                </li>
            </ul>
            </li>
            </ul>



        </div>  
    </div>
</div>

?>

