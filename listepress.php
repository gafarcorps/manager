<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Liste des fichiers numeriques</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="#"></a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active"> Liste des fichiers numeriques</li>
                </ol>
            </div>
        </div>
        <div class="tab-content tab-space">
            <div class="tab-pane active show" id="tab1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                        <div class="col-md-6 col-sm-6 col-6 mt-4">
                                <div class="btn-group">
                                    <a href="app.php?item=formepress&pid=11&option=new" id="addRow" class="btn btn-info">
                                        Nouveau fichier numérique<i class="fa fa-plus"></i>
                                    </a>

                                </div>
                                <!-- <div class="btn-group">
                                    <a href="app.php?item=disabledpages" id="addRow" class="btn btn-danger"><b>Corbeille </b></a> 
                                </div> -->
                            </div>
                            <div class="card-head">
                                <button id="panel-button"
                                        class="mdl-button mdl-js-button mdl-button--icon pull-right"
                                        data-upgraded=",MaterialButton">
                                    <i class="material-icons"></i>
                                </button>
                                <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                                    data-mdl-for="panel-button">
                                    <li class="mdl-menu__item"><i
                                            class="material-icons"></i>Action</li>
                                    <li class="mdl-menu__item"><i class="material-icons"></i>
                                        action</li>
                                    <li class="mdl-menu__item"><i
                                            class="material-icons"></i></li>
                                </ul>
                            </div>
                            <?php
                            if (isset($_GET['msg']) && $_GET['msg'] == 'success') {
                                echo '<span class="clsAvailable"> Enregistrement réussi. </span>';
                            }
                            ?>
                            <div class="card-body ">
                                <div class="table-scrollable">
                                    <table class="table table-hover table-checkable order-column full-width"
                                           id="example4">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th class="center"> Numéro de press</th>
                                                
                                                <th class="center"> Date de création</th>
                                                <th class="center"> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            
                                            $found_epresss = Epress::find_all();
                                            foreach ($found_epresss as $found_epress) {
                                           
                                                ?>
                                                <tr class="odd gradeX">
                                                    <td class="user-circle-img sorting_1">
                                                        <img src="images/<?php //echo $found_user->user_info_image ?>" style="height: 30px" alt="">
                                                    </td>
                                                    <td class="center"><?php echo $found_epress->epress_title; ?></td>
                                                    
                                                    <td class="center">
                                                            <?php echo changedateusfr($found_epress->epress_date_creation) ; ?> </td>
                                                   
                                                    <td class="center">
                                                        <a href="app.php?item=formepress&option=edit&eaid=<?php echo $found_epress->epress_id; ?>"
                                                           class="btn btn-tbl-edit btn-xs">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        <a href="media/files/<?php echo $found_epress->epress_file_path; ?>" download
                                                           class="btn btn-tbl-edit btn-xs">
                                                            <i class="fa fa-edit"></i>
                                                        </a>
                                                        <!--<a class="btn btn-tbl-delete btn-xs">
                                                                <i class="fa fa-trash-o "></i>
                                                        </a> -->
                                                    </td>
                                                </tr>

                                            <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab2">
                <div class="row">

                    <?php
                    $user_info = new User_info();
                    $user_account = new User_account();
                    $found_users = $user_info->find_all_active();
                    foreach ($found_users as $found_user) {
                        $found_user_account = $user_account->find_by_id($found_user->user_info_user_account_id);
                        ?>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="m-b-20">
                                    <div class="doctor-profile">
                                        <div class="profile-header bg-b-purple">
                                            <div class="user-name"><?php echo $found_user->user_info_lastname; ?></div>
                                            <div class="name-center"><?php echo $found_user->user_info_firstname; ?></div>
                                        </div>
                                        <img src="images/<?php echo $found_user->user_info_image ?>" class="user-img" alt="">
                                        <p>
                                            <?php echo $found_user->user_info_address; ?>
                                        </p>
                                        <div>
                                            <p>
                                                <i class="fa fa-phone"></i><a href="tel:<?php echo $found_user->user_info_tel; ?>">
                                                    <?php echo $found_user->user_info_tel; ?></a>
                                            </p>
                                        </div>
                                        <div class="profile-userbuttons">
                                            <a href="app.php?item=formuser&option=edit&uid=<?php echo $found_user->user_info_id; ?>"
                                               class="btn btn-circle deepPink-bgcolor btn-sm">Mofifier</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>


                </div>
            </div>
        </div>
    </div>
</div>