<?php

require_once("includes/initialize.php");

if ($session->is_logged_in()) {
    redirect_to("appex.php?item=home");
}
// Remember to give your form's submit tag a name="submit" attribute!
if (isset($_POST['submit'])) { // Form has been submitted.
    if (isset($_POST['username'])) {
        $email = trim($_POST['username']);
    }

    if (isset($_POST['password'])) {
        $password = trim($_POST['password']);
    }


    $found_user_account = User_account::find_by_email($email);
    if (empty($found_user_account)) {
        // email/password combo was not found in the database
        $message = "Username/password combination incorrect.";
        $session->logout();
        redirect_to("login.php?error_login=" . $message . "");
    }

    $hashed_password = password_hash($password, PASSWORD_BCRYPT);
    $is_match = password_verify($password, $found_user_account->user_account_password);

    if ($is_match) {
        $found_user = User_info::find_by_user_account($found_user_account->user_account_id);
        $found_app_request = App_request::find_active_by_customer_id($found_user->user_info_id);
        if (!empty($found_app_request))
            $found_extension_online = Menu::find_by_id(($found_app_request->app_request_menu_id));
        else
            $found_extension_online = Menu::find_by_id(1);
        $session->login($found_user_account, $found_extension_online);
        if ($found_user_account->user_account_typeuser_id == 1 || $found_user_account->user_account_typeuser_id == 2)
            redirect_to("app.php?item=home");
        elseif ($found_user_account->user_account_typeuser_id == 3)
            redirect_to("appex.php?item=home");
        else
            redirect_to("appex.php?item=view_form_builder_objects_db");
    } else {
        // email/password combo was not found in the database
        $message = "Username/password combination incorrect.";
        $session->logout();
        redirect_to("login.php?error_login=" . $message . "");
    }
}
?>