
		 	<!-- start page content -->
			<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Bienvenue dans Manager</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
										href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li><a class="parent-item" href="app.php?item=listcategories">Catégories</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Catégories désactivées</li>
							</ol>
						</div>
					</div>

					<div class="row">
						<div class="col-md-12">
							<div class="card card-box">
								<div class="card-head">
									<header>Liste des catégories supprimées</header>
									<div class="tools">
										<a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
										<a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
										<a class="t-close btn-color fa fa-times" href="javascript:;"></a>
									</div>
								</div>
								<div class="card-body ">
									<div class="row p-b-20">
										<div class="col-md-6 col-sm-6 col-6">
											<div class="btn-group">
												<a href="app.php?item=listcategories" id="addRow" class="btn btn-info">
													Catégories actives  
												</a> &nbsp;&nbsp;
												<a href="app.php?item=disabledcategories" id="addRow" class="btn btn-danger">
													Corbeille</i>
												</a>
												
											</div>
										</div>
										
										<div class="col-md-6 col-sm-6 col-6">
											<div class="btn-group pull-right"> 
											</div>
										</div>
									</div>

									<?php
												 if(isset($_GET['msg']) && $_GET['msg']=='success') {
													  echo '<span class="clsAvailable"> Enregistrement réussi. </span>';
												 	} 
									    ?>

										  <div class="row">
                        <div class="col-md-12">
                            <div class="card card-topline-aqua">
                                <div class="card-head">
                                    <header>CORBEILLE DES CATEGORIES</header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                        <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">
                                    <div class="table-scrollable">
                                        <table id="example1" class="display full-width">
													<thead>
														     <tr>  
																	<th>Titre </th> 
																	<th> Date </th> 
																	<th> Action </th>
																</tr>
															</thead>
															<tbody>
															
																<?php
																	 
																			$found_typetopics = Type_topic :: find_all_unabled();
																				foreach($found_typetopics as $found_typetopic){

																										echo '<tr class="odd gradeX">';
																										echo '<td><a href="app.php?item=formcategorie&cid='.$found_typetopic->type_topic_id.'&option=edit">'.$found_typetopic->type_topic_desc.'</td>';
																							         	echo '<td>'.$found_typetopic->type_topic_date.'</td>';  
																										echo '<td>'; 
																												echo ' <a href="app.php?item=formcategorie&cid='.$found_typetopic->type_topic_id.'&option=edit" class="btn btn-tbl-edit btn-xs">';
																												echo '<i class="fa fa-pencil"></i>';
																												echo '</a>';
																										echo '</td>'; 
																										
																							echo '</tr>'; 
																				} // end foreach 
																?>
																
											</tbody>
										</table>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div> 
			<!-- end page content -->