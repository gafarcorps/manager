
<?php

require_once("includes/initialize.php");

$customer_id = (!empty($_GET['customer_id'])) ? $_GET['customer_id'] : null;
if (!empty($customer_id))
    $found_customer = User_info::find_by_id($customer_id);

if (isset($_POST['contract_id'])) {
    $contract_id = $_POST['contract_id'];
} //$contract_id is the id of current contract

if (isset($_POST['contract_customer_id'])) {
    $contract_customer_id = $_POST['contract_customer_id'];
}
if (isset($_POST['contract_service_id'])) {
    $contract_service_id = $_POST['contract_service_id'];
}
if (isset($_POST['contract_title'])) {
    $contract_title = $_POST['contract_title'];
}
if (isset($_POST['contract_desc'])) {
    $contract_desc = $_POST['contract_desc'];
}
if (isset($_POST['contract_date'])) {
    $contract_date = $_POST['contract_date'];
}
if (isset($_POST['contract_datestart'])) {
    $contract_datestart = $_POST['contract_datestart'];
}
if (isset($_POST['contract_dateend'])) {
    $contract_dateend = $_POST['contract_dateend'];
}
if (isset($_POST['contract_date_breaking'])) {
    $contract_date_breaking = $_POST['contract_date_breaking'];
}
$contract_date_breaking = empty($contract_date_breaking) ? $contract_dateend : $contract_date_breaking;

//if (isset($_POST['contract_active'])) {
//    $contract_active = $_POST['contract_active'];
//} else
$contract_active = (date("Y-m-d") <= $contract_date_breaking) ? 1 : 0;

if (!empty($_FILES['image']['name'])) {
    $repository = 'media/files/';
    $file_upload = basename($_FILES['image']['name']);
}

//if (!empty($contract_active)) {
//    $contract_active = 1;
//} else {
//    $contract_active = 0;
//}

if (isset($_POST['new']) && !empty($session->user_account_id)) { // New contract menu 
    $contract->contract_customer_id = $contract_customer_id;
    $contract->contract_service_id = $contract_service_id;
    $contract->contract_title = $contract_title;
    $contract->contract_desc = $contract_desc;
    $contract->contract_date = $contract_date;
    $contract->contract_datestart = $contract_datestart;
    $contract->contract_dateend = $contract_dateend;
    $contract->contract_date_breaking = $contract_date_breaking;
    if (!empty($_FILES['image'])) {
        move_uploaded_file($_FILES['image']['tmp_name'], $repository . $file_upload);
        $contract->contract_file_path = $file_upload;
    }
    $contract->contract_date_creation = date('Y-m-d');

    $contract->contract_date_last_modification = date('Y-m-d');

    $contract->contract_user_creation = $session->user_account_id;
    $contract->contract_user_last_modification = $session->user_account_id;
    $contract->contract_deleted = 0;
    if ($contract_date_breaking)
        $contract->contract_active = $contract_active;

    $contract->save();

    redirect_to('app.php?item=listcontracts&customer_id=' . $customer_id . '&msg=success');
} // END IF NEW ENTRY  


if (isset($_POST['submit']) && !empty($contract_id) && !empty($session->user_account_id)) { // Updating a contract	
    if ($_POST['submit'] == 'break') {
        $found_contract = $contract->find_by_id($_GET['contract_id']);
        $found_contract->contract_date_breaking = $contract_date_breaking;
        $found_contract->contract_active = 0;
        $found_contract->save();
        
    } else {

        $found_contract = $contract->find_by_id($contract_id);

        $contract->contract_id = $found_contract->contract_id;
        $contract->contract_customer_id = $contract_customer_id;
        $contract->contract_service_id = $contract_service_id;
        $contract->contract_date = $contract_date;
        $contract->contract_datestart = $contract_datestart;
        $contract->contract_dateend = $contract_dateend;
        $contract->contract_date_breaking = $contract_date_breaking;
        if (!empty($contract_title)) {  // UPDATE IF THERE ANY POSTED TITLE VARIABLE
            $contract->contract_title = $contract_title;
        } else { // REPLACE THE ASSET VALUE
            $contract->contract_title = $found_contract->contract_title;
        }

        if (!empty($contract_desc)) { // UPDATE IF THERE ANY POSTED DESC VARIABLE
            $contract->contract_desc = $contract_desc;
        } else { // REPLACE THE ASSET VALUE
            $contract->contract_desc = $found_contract->contract_desc;
        }

        if (!empty($file_upload)) {
            move_uploaded_file($_FILES['image']['tmp_name'], $repository . $file_upload);
            $contract->contract_file_path = $file_upload;
        } else {
            $contract->contract_file_path = $found_contract->contract_file_path;
        }

        $contract->contract_date_creation = $found_contract->contract_date_creation;
        $contract->contract_date_last_modification = date('Y-m-d');

        $contract->contract_user_creation = $found_contract->contract_date_creation;
        $contract->contract_user_last_modification = $session->user_account_id;
        $contract->contract_deleted = 0;
        if ($contract_date_breaking)
            $contract->contract_active = $contract_active;

        $contract->save();
    }
    redirect_to('app.php?item=listcontracts&customer_id=' . $customer_id . '&msg=success');
}  // END IF UPDATE 

if (!empty($_GET['contract_id']) && !empty($session->user_account_id) && $_GET['option'] == 'delete') {  // UNABLE ENTRIE
    $contract_id = $_GET['contract_id'];
    $contract = $contract->find_by_id($contract_id);
    $del = $contract->unable();
    redirect_to('app.php?item=formcontract&contract_id=' . $contract_id . '&customer_id=' . $customer_id . '&option=edit&r=0');
}  // END IF UNABLE 

if (!empty($_GET['contract_id']) && !empty($session->user_account_id) && $_GET['option'] == 'recover') {  // DELETE ENTRIE
    $contract_id = $_GET['contract_id'];
    $contract = $contract->find_by_id($contract_id);
    $del = $contract->recover();
    redirect_to('app.php?item=formcontract&contract_id=' . $contract_id . '&customer_id=' . $customer_id . '&option=edit&r=1');
}  // END IF DELETE 
if (!empty($_GET['contract_id']) && !empty($session->user_account_id) && $_GET['option'] == 'break') {  // DELETE ACTIVE
    $contract_id = $_GET['contract_id'];
    $customer_id = $_GET['customer_id'];
    $contract = $contract->find_by_id($contract_id);
    $contract->contract_active = 0;
    $contract->save();
    redirect_to('app.php?item=listcontracts&customer_id=' . $customer_id . '&msg=success');
}  // END IF ACTIVE 

if (!empty($_GET['contract_id']) && !empty($session->user_account_id) && $_GET['option'] == 'delete2') {  // DELETE ENTRIE
    $contract_id = $_GET['contract_id'];
    $contract = $contract->find_by_id($contract_id);
    $del = $contract->delete();
    redirect_to('app.php?item=formcontract&contract_id=' . $contract_id . '&customer_id=' . $customer_id . '&option=edit&r=3');
}  // END IF DELETE 
?>
 