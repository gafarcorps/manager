<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="#"> Paramètres </a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Liste des privilèges</li>
                </ol>
            </div>
        </div> 

        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Liste des privilèges</header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row p-b-20">
                            <div class="col-md-6 col-sm-6 col-6">
                                <!--	<div class="btn-group">
                                                <a href="app.php?formmenu&option=new" id="addRow" class="btn btn-info">
                                                        Nouveau profil <i class="fa fa-plus"></i>
                                                </a>
                                        </div>
                                -->
                            </div>
                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group pull-right">

                                </div>
                            </div>
                            <div id="result"></div>
                            <div class="table-scrollable">
                                <table class="table table-hover table-checkable order-column full-width">
                                    <thead>
                                        <tr>  
                                            <th class="center">Profil </th>

                                            <?php
                                            $found_menus = $menu->list_menus_active();
                                            foreach ($found_menus as $found_menu) {
                                                echo '<th class="center">' . $found_menu->menu_title . '</th> ';
                                            }
                                            ?>

<!-- <th class="center"> Action </th> -->
                                        </tr>


                                    <form action="save_privileges.php" method="POST">
                                        <?php
                                        $typeuser = new Typeuser();
                                        $foundtypeusers = Typeuser::find_all();
                                        foreach ($foundtypeusers as $foundtypeuser) {
                                            echo '<tr><td class="center">' . $foundtypeuser->type_user_desc . '</td>';

                                            $foundprivileges = Privileges::find_all($foundtypeuser->type_user_id);

                                            foreach ($foundprivileges as $foundprivilege) {

                                                $found_menu = $menu->find_by_id($foundprivilege->privilege_menu_id);
                                                echo '<td class="center">';

                                                echo '<div class="checkbox checkbox-icon-black">';

                                                if (!empty($foundprivilege->privilege_active)) {
                                                    //echo '<input type="hidden"  class="form-check-input" value="'.$foundprivilege->privilege_type_user_id.'" />';
                                                    echo '<input type="checkbox" name="privilege_menu_id"  value="' . $foundprivilege->privilege_type_user_id . '-' . $foundprivilege->privilege_menu_id . '" class="form-check-input" checked="checked" onchange="onClickHandler()" />';
                                                } else {
                                                    echo '<input type="checkbox" name="privilege_menu_id"  value="' . $foundprivilege->privilege_type_user_id . '-' . $foundprivilege->privilege_menu_id . '" class="form-check-input" onchange="onClickHandler()" />';
                                                }

                                                echo '</div>';

                                                //	echo '<div  id="children" class="checkbox checkbox-icon-black"> </div>';

                                                echo '</td>';
                                            } // end foreach

                                            echo '</tr>';
                                        } // end foreach
                                        //	echo '</tr>';
                                        ?>

                                    </form>
                                    </thead>
                                    <tbody>




                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    function onClickHandler() {
        var update = [];
        $(".form-check-input").each(function () {
            if ($(this).is(":checked")) {
                update.push($(this).val() + '-1');
            } else {
                update.push($(this).val() + '-0');
            }
        });

        $.ajax({
            url: "save_privileges.php",
            method: "POST",
            data: {
                update: update
            },
            success: function (data) {
                $('#result').html(data);
            }
        });
    }

</script>



