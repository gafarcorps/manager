
<!-- start header -->
<?php
include('header.php');
?>
<!-- end header -->


<!-- start page container -->
<div class="page-container">

    <!-- start sidebar menu -->
    <?php
    include('sidebar.php');
    ?> 
    <!-- end sidebar menu -->


    <!-- start page content -->
    <!-- start page content -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="page-bar">
                <div class="page-title-breadcrumb">
                    <div class=" pull-left">
                        <div class="page-title">Bienvenue dans Manager</div>
                    </div>
                    <ol class="breadcrumb page-breadcrumb pull-right">
                        <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                               href="<?php echo $app_or_appex ?>.php?item=home">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                        </li>
                        <li><a class="parent-item" href="#">Extra</a>&nbsp;<i class="fa fa-angle-right"></i>
                        </li>
                        <li class="active">User Profile</li>
                    </ol>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <!-- BEGIN PROFILE SIDEBAR -->
                    
                    <div class="profile-sidebar">
                        <?php
                        /*
                          <div class="card card-topline-aqua">
                          <div class="card-body no-padding height-9">
                          <div class="row">
                          <div class="profile-userpic">
                          <img src="assets/img/dp.jpg" class="img-responsive" alt=""> </div>
                          </div>
                          <div class="profile-usertitle">
                          <div class="profile-usertitle-name"> John Deo </div>
                          <div class="profile-usertitle-job"> Jr. Professor </div>
                          </div>
                          <ul class="list-group list-group-unbordered">
                          <li class="list-group-item">
                          <b>Followers</b> <a class="pull-right">1,200</a>
                          </li>
                          <li class="list-group-item">
                          <b>Following</b> <a class="pull-right">750</a>
                          </li>
                          <li class="list-group-item">
                          <b>Friends</b> <a class="pull-right">11,172</a>
                          </li>
                          </ul>
                          <!-- END SIDEBAR USER TITLE -->
                          <!-- SIDEBAR BUTTONS -->
                          <div class="profile-userbuttons">
                          <button type="button"
                          class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary">Follow</button>
                          <button type="button"
                          class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-pink">Message</button>
                          </div>
                          <!-- END SIDEBAR BUTTONS -->
                          </div>
                          </div>


                          <div class="card">
                          <div class="card-head card-topline-aqua">
                          <header>About Me</header>
                          </div>
                          <div class="card-body no-padding height-9">
                          <div class="profile-desc">
                          Hello I am John Deo a Professor in xyz College Surat. I love to work with
                          all my college staff and seniour professors.
                          </div>
                          <ul class="list-group list-group-unbordered">
                          <li class="list-group-item">
                          <b>Gender </b>
                          <div class="profile-desc-item pull-right">Female</div>
                          </li>
                          <li class="list-group-item">
                          <b>Operation Done </b>
                          <div class="profile-desc-item pull-right">30+</div>
                          </li>
                          <li class="list-group-item">
                          <b>Degree </b>
                          <div class="profile-desc-item pull-right">B.A., M.A., P.H.D.</div>
                          </li>
                          <li class="list-group-item">
                          <b>Designation</b>
                          <div class="profile-desc-item pull-right">Jr. Professor</div>
                          </li>
                          </ul>
                          <div class="row list-separated profile-stat">
                          <div class="col-md-4 col-sm-4 col-6">
                          <div class="uppercase profile-stat-title"> 37 </div>
                          <div class="uppercase profile-stat-text"> Projects </div>
                          </div>
                          <div class="col-md-4 col-sm-4 col-6">
                          <div class="uppercase profile-stat-title"> 51 </div>
                          <div class="uppercase profile-stat-text"> Tasks </div>
                          </div>
                          <div class="col-md-4 col-sm-4 col-6">
                          <div class="uppercase profile-stat-title"> 61 </div>
                          <div class="uppercase profile-stat-text"> Uploads </div>
                          </div>
                          </div>
                          </div>
                          </div>
                          <div class="card">
                          <div class="card-head card-topline-aqua">
                          <header>Address</header>
                          </div>
                          <div class="card-body no-padding height-9">
                          <div class="row text-center m-t-10">
                          <div class="col-md-12">
                          <p>456, Pragri flat, varacha road, Surat
                          <br> Gujarat, India.</p>
                          </div>
                          </div>
                          </div>
                          </div>
                          <div class="card">
                          <div class="card-head card-topline-aqua">
                          <header>Work Expertise</header>
                          </div>
                          <div class="card-body no-padding height-9">
                          <div class="work-monitor work-progress">
                          <div class="states">
                          <div class="info">
                          <div class="desc pull-left">Java</div>
                          <div class="percent pull-right">75%</div>
                          </div>
                          <div class="progress progress-xs">
                          <div class="progress-bar progress-bar-danger progress-bar-striped active width-75"
                          role="progressbar" aria-valuenow="40" aria-valuemin="0"
                          aria-valuemax="100">
                          <span class="sr-only">75% </span>
                          </div>
                          </div>
                          </div>
                          <div class="states">
                          <div class="info">
                          <div class="desc pull-left">Php</div>
                          <div class="percent pull-right">40%</div>
                          </div>
                          <div class="progress progress-xs">
                          <div class="progress-bar progress-bar-success progress-bar-striped active width-40"
                          role="progressbar" aria-valuenow="40" aria-valuemin="0"
                          aria-valuemax="100">
                          <span class="sr-only">40% </span>
                          </div>
                          </div>
                          </div>
                          <div class="states">
                          <div class="info">
                          <div class="desc pull-left">Android</div>
                          <div class="percent pull-right">60%</div>
                          </div>
                          <div class="progress progress-xs">
                          <div class="progress-bar progress-bar-info progress-bar-striped active width-60"
                          role="progressbar" aria-valuenow="40" aria-valuemin="0"
                          aria-valuemax="100">
                          <span class="sr-only">60% </span>
                          </div>
                          </div>
                          </div>
                          </div>

                         */
                        ?>

                    </div>
                </div>
            </div>



            <!-- END BEGIN PROFILE SIDEBAR -->
            <!-- BEGIN PROFILE CONTENT -->
            <?php
            if (isset($_GET['item'])) {
                if (!empty($_GET['item'])) {
                    switch ($_GET['item']) {

                        case 'listmenus': include('listmenus.php');
                            break;

                        case 'listusers': include('listusers.php');
                            break;


                        default: include('home.php');
                            break;
                    }// end if
                } // end foreach
            } // end if isset 
            ?>

            <!-- end chat sidebar -->
        </div>
        <!-- end page container -->
        <?php
        include('footer.php');
        ?>