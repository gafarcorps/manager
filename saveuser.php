<?php

require_once("includes/initialize.php");

if (isset($_POST['submit'])) { // IF ISSET SUBMIT
    $user = new User_info();
    $user_account = new User_account();

    if (isset($_POST['username'])) {
        $username = $_POST['username'];
    }
    if (isset($_POST['email'])) {
        $email = $_POST['email'];
    }
    if (isset($_POST['password'])) {
        $password = $_POST['password'];
    } else {
        
    }
    if (isset($_POST['confirm-password'])) {
        $confirmpassword = $_POST['confirm-password'];
    } else {
        
    }
    if (!empty($password) && !empty($confirmpassword) && $password != $confirmpassword) {
        redirect_to($_SERVER['HTTP_REFERER']."&msg=error");
    }
    if (isset($_POST['firstname'])) {
        $firstname = $_POST['firstname'];
    }
    if (isset($_POST['lastname'])) {
        $lastname = $_POST['lastname'];
    }
    if (isset($_POST['telephone'])) {
        $telephone = $_POST['telephone'];
    }
    if (isset($_POST['address'])) {
        $address = $_POST['address'];
    }
    if (isset($_POST['user_account_lang'])) {
        $user_account_lang = $_POST['user_account_lang'];
    }
//    echo '>>'.$user_account_lang;
    if (isset($_POST['type'])) {
        $type = $_POST['type'];
    }
    if (isset($_POST['uid'])) {
        $uid = $_POST['uid'];
    }
    if (isset($_POST['uida'])) {
        $uida = $_POST['uida'];
    }

    if (!empty($_FILES['file_upload']['name'])) {
        $file_upload = basename($_FILES['file_upload']['name']);
    }

//    echo '>>'.$file_upload;
    if (!isset($_POST['uid'])) {


        //Create user account 
        $user_account->user_account_email = $email;

        if (!empty($password) && !empty($confirmpassword))
            $user_account->user_account_password = password_hash($confirmpassword, PASSWORD_BCRYPT);
        $user_account->user_account_date_creation = date("Y-m-d");
        $user_account->user_account_typeuser_id = $type;
        $user_account->user_account_extension = 1;
        $user_account->user_account_extension_user_id = -1;
        $user_account->user_account_lang = $user_account_lang;
        $user_account->user_account_active = 1;
        $user_account->user_account_extension = 1;
        $user_account->user_account_extension_user_id = -1;
        $user_account->user_account_extension_form_builder_id = -1;

        $user_account->user_account_user_creation = $session->user_account_id;
        $user_account->user_account_date_creation = date("Y-m-d");
        $user_account->user_account_user_last_update = $session->user_account_id;
        $user_account->user_account_date_last_connexion = date("Y-m-d");
        $user_account->save();

        $last_user_account_added = $user_account->find_last($session->user_account_id);

        //user info
        $user->user_info_firstname = $firstname;
        $user->user_info_lastname = $lastname;
        $user->user_info_address = $address;
        $user->user_info_tel = $telephone;
        $user->user_info_user_account_id = $last_user_account_added->user_account_id;
        $user->user_info_user_creation = $session->user_account_id;
        $user->user_info_logged_in = $session->user_account_id;
        $user->user_info_date_creation = date("Y-m-d");
        $user->user_info_date_last_connexion = date("Y-m-d");

        $repository = 'media/files/';
        if (isset($file_upload)) {
            move_uploaded_file($_FILES['file_upload']['tmp_name'], $repository . $file_upload);
            $user->user_info_image = $file_upload;
        }

        $user->user_info_active = 1;
        $user->save();
    } else { // UPDATE
        $found_user_info = $user->find_by_id($uid);

        $found_user_account = $user_account->find_by_id($uida);

        //update user account
        $found_user_account->user_account_id = $uida;

        $found_user_account->user_account_email = $email;
        $found_user_account->user_account_password = (!empty($confirmpassword)) ? password_hash($confirmpassword, PASSWORD_BCRYPT) : ($found_user_account->user_account_password);
        $found_user_account->user_account_typeuser_id = ($type) ? $type : $found_user_account->user_account_typeuser_id;
        $found_user_account->user_account_lang = $user_account_lang;
        $user_account->user_account_extension = 1;
        $user_account->user_account_extension_user_id = -1;
        $found_user_account->user_account_active = 1;

//        $found_user_account->user_account_user_creation = $session->user_account_id;
//        $found_user_account->user_account_date_creation = date("Y-m-d");
        $found_user_account->user_account_user_last_update = $session->user_account_id;
        $found_user_account->user_account_date_last_connexion = date("Y-m-d");
        $found_user_account->save();

        //update user info
        $found_user_info->user_info_firstname = $firstname;
        $found_user_info->user_info_lastname = $lastname;
        $found_user_info->user_info_address = $address;
        $found_user_info->user_info_tel = $telephone;
        $found_user_info->user_info_user_account_id = $uida;
        $found_user_info->user_info_active = 1;

        $repository = 'media/files/';
        if (isset($file_upload)) {
            move_uploaded_file($_FILES['file_upload']['tmp_name'], $repository . $file_upload);
            $found_user_info->user_info_image = $file_upload;
        }

//        $found_user_info->user_info_user_creation = $session->user_account_id;
        $found_user_info->user_info_logged_in = $session->user_account_id;
//        $found_user_info->user_info_date_creation = date("Y-m-d");
        $found_user_info->user_info_date_last_connexion = date("Y-m-d");
        $found_user_info->save();
    }

    // Get User Logged Typeuser
    $found_user_account_logged = User_account::find_by_id($session->user_account_id);
    $found_typeuser = Typeuser::find_by_id($found_user_account_logged->user_account_typeuser_id);

    if ($found_typeuser->type_user_id != 1 || empty($_GET['opt']))
        redirect_to($_SERVER['HTTP_REFERER']."&msg=success");
    else
        redirect_to('app.php?item=listusers&msg=success');
}
?>