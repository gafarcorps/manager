<?php

require_once("includes/initialize.php");
$epress = new Epress();

 
if (isset($_POST['submit'])) {
 
    if (isset($_POST['epress_title'])) {
        $epress_title = $_POST['epress_title'];
    }
    if (isset($_POST['epress_file_path'])) {
        $epress_file_path = $_POST['epress_file_path'];
    }
    if (isset($_POST['epress_active'])) {
        $epress_active = $_POST['epress_active'];
    }
    if(!empty($epress_active)){
        $epress_active = 1;
    }else{
        $epress_active = 0;
    }



    if (!empty($_FILES['epress_file_path']['name'])) {
        $epress_file_path = basename($_FILES['epress_file_path']['name']);
    }


    $repository = 'media/files/';

   
    if (empty($_POST['eaid'])) 
    { // NEW ENTRY 


        $epress->epress_title = $epress_title;
        $epress->epress_active = $epress_active;
        $epress->epress_date_creation = date('Y-m-d');


        if (isset($_FILES['epress_file_path'])) {
            move_uploaded_file($_FILES['epress_file_path']['tmp_name'], $repository . $epress_file_path);
            //$topic->topic_image_path = $file_upload;
        }

        //START  SAVE GALERY
        $errors = array();
        $extension = array("jpeg","jpg","png","gif", "pdf");
        $bytes = 1024;
        $allowedKB = 1000000;
        $totalBytes = $allowedKB * $bytes;
         
        if(isset($_FILES["files"])==false)
        {
            // echo "<b>Please, Select the files to upload!!!</b>";
            $epress->epress_file_path = $epress_file_path;
            $epress->save();
            redirect_to('app.php?item=listepress&msg=success');
        }

            //END GALERY
    } // END IF NEW ENTRY  
   
    else { // UPDATE ENTRY


        $eaid = $_POST['eaid'];
        $found_epress = $epress->find_by_id($eaid);
        $epress->epress_id = $eaid;
        if (!empty($epress_title)) {  // UPDATE IF THERE ANY POSTED TITLE VARIABLE
            $epress->epress_title = $epress_title;
        } else { // REPLACE THE ASSET VALUE
            $epress->epress_title = $found_epress->epress_title;
        }


        if (isset($epress_file_path)) {
            move_uploaded_file($_FILES['epress_file_path']['tmp_name'], $repository . $epress_file_path);
        }else{
            $epress->epress_file_path = $found_epress->epress_file_path;
        }


        $epress->epress_date_creation = date("Y-m-d");
        
        $epress->epress_active = $found_epress->epress_active;

        //START SAVE GALERY
        $errors = array();
        $extension = array("jpeg","jpg","png","gif", "pdf");
        $bytes = 1024;
        $allowedKB = 1000000;
        $totalBytes = $allowedKB * $bytes;
         
        if(isset($_FILES["files"])==false)
        {
            if (!empty($_FILES['epress_file_path']['name'])) 
            {
              $epress_file_path = basename($_FILES['epress_file_path']['name']); 
              $epress->epress_file_path = $epress_file_path;
              }
            $epress->save();
            //redirect_to($_SERVER['HTTP_REFERER']);
        }

            $count = count($errors);
            if($count != 0){
                foreach($errors as $error){
                    echo $error."<br/>";
                }
            } 
            //END GALERY
       
           // redirect_to($_SERVER['HTTP_REFERER']);

    } // END IF UPDATE  


        redirect_to('app.php?item=listepress&msg=success');
     
} // END IF SUBMIT


if (!empty($_GET['eaid']) && $_GET['option'] == 'delete') {  // DELETE ENTRIE
    $epressid = $_GET['eaid'];
    $found_epress = $epress->find_by_id($epressid);
    $del = $found_epress->unable();
    redirect_to('app.php?item=listepress&option=del');
    }  // END IF DELETE
  
    if (!empty($_GET['eaid'])  && !empty($session->user_account_id) && $_GET['option'] == 'recover') {  // DELETE ENTRIE
  
      $epressid = $_GET['eaid'];
      $found_epress = $epress->find_by_id($epressid);
      $del        = $found_epress->recover();
      redirect_to('app.php?item=listepress&option=recover');
  }  // END IF DELETE 

  if (!empty($_GET['eaid']) && !empty($_GET['indice']) && $_GET['option'] == 'deleteimage') {  // DELETE ENTRIE
     $epressid = $_GET['eaid'];
     $found_epress = $epress->find_by_id($epressid);
     $galeries= explode(",", $found_epress->epress_image_path);
     $indice = $_GET['indice'];

    unset($galeries[$indice]);
    
    $string = implode(",",$galeries);  

     $del  = $found_epress->deleteImage($string);

     header('Location: ' . $_SERVER['HTTP_REFERER']);
    //redirect_to('app.php?item=listepress&option=delimage');
    }  // END IF DELETE
  
 
?>