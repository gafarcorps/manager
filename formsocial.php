



<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="app.php?item=listpages">Blog</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Editer un réseau</li>
                </ol>
            </div>
        </div>

        <form action="savesocial.php" class="form-horizontal" method="POST" enctype="multipart/form-data">

            <?php
            if(isset($_GET['id'])) { $id  = $_GET['id']; }


            if(isset($_GET['option']) && $_GET['option'] == 'new'){

                echo '<div class="row">';
                echo '<div class="col-md-12">';
                //<!-- BEGIN PROFILE SIDEBAR -->
                echo '<div class="profile-sidebar">';
                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Attributs de l\'élément </div>';
                echo '</div>';

                echo '<div class="card card-topline-aqua">';
                echo '<div class="card-body no-padding height-9">';
                echo '<div class="row">';
                echo '</div>';
                echo '<br/>';


                echo '<div id="displayed" class="card-body no-padding height-9">';
                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b>Etat </b>';
                echo '<div class="profile-desc-item pull-right">';
                echo '<i class="fa fa-eye"></i> publié';
                echo '</div>';
                echo '</li>';


                echo '</ul><br/>';

                echo '</div>';


                echo '</div>';


                echo '</div>';
                echo '</div>';

                echo '<div class="card card-topline-aqua">';
                echo '<div class="card-body no-padding height-9">';
                echo '<div class="row">';
                echo '</div>';
                echo '<br/>';


                echo '<ul class="list-group list-group-unbordered">';

                echo '<li class="list-group-item">';
                echo '<b>Ordre</b> <a class="pull-right"></a>';
                echo '<select name="position" class="form-control">';
                for($i = 1; $i <= 10; $i++){
                    echo '<option value="'.$i.'" name="position">position '.$i.'</option>';
                }
                echo '</select>';
                echo '</li>';

                echo '</ul>';
                //<!-- END SIDEBAR USER TITLE -->
                echo '<br/>';
                echo '<div class="control-group">';
                echo '<label class="control-label" for="active"> Publier?</label>';
                echo '<div class="controls">';
                echo '<div class="form-group">';
                echo '<div class="checkbox checkbox-icon-black">';
                echo '<input id="rememberChk1" name="active" type="checkbox" checked="checked">';
                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- SIDEBAR BUTTONS -->
                echo '</div>';
                echo '</div>';

                echo '</div>';


                //<!-- END BEGIN PROFILE SIDEBAR -->
                //<!-- BEGIN PROFILE CONTENT -->
                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';

                echo '<h4> Ajouter un réseau </h4>';

                echo '</div>';
                echo '</div>';
                echo '<div class="white-box">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Nom:</label>';
                echo '<input name="name" type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Icône:</label>';
                echo '<input name="icon" id="icon"  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Lien:</label>';
                echo '<input name="link" id="title"  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';


                echo '</div>';
                echo '</div>';

                echo '</div>';
                echo '<br>';
                echo '&nbsp;&nbsp;';

                echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';


                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->
                echo '</div>';


            } //end if
            ?>


            <?php
            if(isset($_GET['option']) && $_GET['option'] == 'edit' && isset($_GET['id'])) {

                $found_social = Social::find_by_id($_GET['id']);

                echo '<div class="row">';
                echo '<div class="col-md-12">';
                //<!-- BEGIN PROFILE SIDEBAR -->
                echo '<div class="profile-sidebar">';

                //START CARD
                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Attributs de l\'élément </div>';
                echo '</div>';

                echo '<div class="card card-topline-aqua">';
                echo '<div class="card-body no-padding height-9">';
                echo '<div class="row">';
                echo '</div>';
                echo '<br/>';


                echo '<div id="displayed" class="card-body no-padding height-9">';
                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b>Etat </b>';
                echo '<div class="profile-desc-item pull-right">';
                echo '<i class="fa fa-eye"></i> '.($found_social->social_active) ? 'publié' : 'non publié';
                echo '</div>';
                echo '</li>';
                echo '</ul><br/>';
                echo '</div>';

                echo '</div>';

                echo '</div>';
                echo '</div>';
                // END CARD


                echo '<div class="card card-topline-aqua">';
                echo '<div class="card-body no-padding height-9">';
                echo '<div class="row">';
                echo '</div>';
                echo '<br/>';


                echo '<ul class="list-group list-group-unbordered">';

                echo '<li class="list-group-item">';
                echo '<b>Ordre</b> <a class="pull-right"></a>';
                echo '<select name="position" class="form-control">';
                for($i = 1; $i <= 10; $i++){
                    $selected = $found_social->social_position == $i ? 'selected' : '';
                    echo '<option value="'.$i.'" '.$selected.' name="position">position '.$i.'</option>';
                }
                echo '</select>';
                echo '</li>';

                echo '</ul>';
                //<!-- END SIDEBAR USER TITLE -->
                echo '<br/>';
                echo '<div class="control-group">';
                echo '<label class="control-label" for="active"> Publier?</label>';
                echo '<div class="controls">';
                echo '<div class="form-group">';
                echo '<div class="checkbox checkbox-icon-black">';
                $checked = $found_social->social_active ? 'checked' : '';
                echo '<input name="active" type="checkbox" '.$checked.'/>';
                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- SIDEBAR BUTTONS -->
                echo '</div>';
                echo '</div>';

                echo '</div>';


                //<!-- END BEGIN PROFILE SIDEBAR -->
                //<!-- BEGIN PROFILE CONTENT -->
                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';

                echo '<h4> Modifier un réseau </h4>';

                echo '</div>';
                echo '</div>';
                echo '<div class="white-box">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';

                echo '<input name="social_id" value="'.$_GET['id'].'" type="hidden" tabindex="1" id="subject"	class="form-control">';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Nom:</label>';
                echo '<input name="name" value="'.$found_social->social_name.'" type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';


                echo '<div class="form-group">';
                echo '<label for="subject" class="">Icône:</label>';
                echo '<input name="icon" value="'.$found_social->social_icon.'"  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Lien:</label>';
                echo '<input name="link" value="'.$found_social->social_link.'" id="title"  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';


                echo '</div>';
                echo '</div>';

                echo '</div>';
                echo '<br>';
                echo '&nbsp;&nbsp;';


                echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';


                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->
                echo '</div>';

            } // end if
            ?>

        </form>


        <!-- end page container -->

