<?php
      if (isset($_GET['mid'])) {
          $mid = $_GET['mid'];
    }
?>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Liste des clients</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="<?php echo $app_or_appex ?>.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="#"></a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active"> <a href="<?php echo $app_or_appex ?>.php?mid=<?php echo $mid ?>&item=listcustomers ">  Liste des clients </a></li>
                </ol>
            </div>
        </div>
        <ul class="nav nav-pills nav-pills-rose">
            <li class="nav-item tab-all"><a class="nav-link active show" href="#tab1" data-toggle="tab">Vue Liste</a></li>
            <li class="nav-item tab-all"><a class="nav-link" href="#tab2" data-toggle="tab">Vue Grille</a>
            </li>
        </ul>
        <div class="tab-content tab-space">
            <div class="tab-pane active show" id="tab1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <div class="card-head">
                                <button id="panel-button"
                                        class="mdl-button mdl-js-button mdl-button--icon pull-right"
                                        data-upgraded=",MaterialButton">
                                    <i class="material-icons"></i>
                                </button>
                                <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                                    data-mdl-for="panel-button">
                                    <li class="mdl-menu__item"><i
                                            class="material-icons"></i>Action</li>
                                    <li class="mdl-menu__item"><i class="material-icons"></i>
                                        action</li>
                                    <li class="mdl-menu__item"><i
                                            class="material-icons"></i></li>
                                </ul>
                            </div>
                            <div class="row p-b-20">
                                <div class="col-md-6 col-sm-6 col-6">
                                    <!--<div class="btn-group">
                                        <a href="<?php echo $app_or_appex ?>.php?item=formcustomer&option=new" id="addRow" class="btn btn-info">
                                            Nouveau client <i class="fa fa-plus"></i>
                                        </a>

                                    </div>
                                    <div class="btn-group">
                                      
                                        <a href="<?php echo $app_or_appex ?>.php?mid=<?php echo $mid ?>&item=listcustomers&option=disabled" id="addRow" class="btn btn-danger">
                                            Ancients clients </i>
                                        </a>

                                    </div>-->
                                </div>

                                <div class="col-md-6 col-sm-6 col-6">
                                    <div class="btn-group pull-right">
                                        <?php
                                        if (isset($_GET['pid'])) {
                                            $pid = $_GET['pid'];
                                            echo '<a href="'. $app_or_appex .'.php?item=listcustomers" class="btn deepPink-bgcolor">Retourner à liste précédente';
                                            echo '</a>';
                                        } // enf if
                                        ?>

                                    </div>
                                </div>
                            </div>
                            <div class="card-body ">
                                <div class="table-scrollable">
                                    <table class="table table-hover table-checkable order-column full-width"
                                           id="example4">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th class="center"> Nom de la boite</th>
                                                <th class="center"> Représentant </th>
                                                <!--<th class="center"> Telephone </th>-->
                                                <th class="center"> Email </th>
                                                <!--<th class="center"> Adresse </th>-->
                                                <th class="center">Groupes</th>
                                                <th class="center">Contrats</th>
                                                <th class="center">Tickets</th>
                                                <!--<th class="center">BDs</th>-->
                                                <th class="center"> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $found_groups = Link_group :: find_by_id($_GET['id']);
                                            $user_info = new User_info();
                                            
                                            foreach ($found_groups as $found_group) {
                                                $found_user= User_info::find_by_id($found_group->link_group_user_id);
                                                $found_user_account = $user_account->find_by_id($found_user->user_info_user_account_id);
                                                if ($found_user_account->user_account_typeuser_id == 4) {
                                                    ?>
                                                    <tr class="odd gradeX">
                                                        <td class="user-circle-img sorting_1">
                                                            <img src="images/<?php echo $found_userinfos->user_info_image ?>" style="height: 30px" alt="">
                                                        </td>

                                                        <td class="center"><?php echo $found_user->user_info_firstname; ?></td>
                                                        <td class="center"><?php echo $found_user->user_info_lastname; ?></td>
        <!--                                                        <td class="center"><a href="tel:<?php // echo $found_user->user_info_tel;      ?>">-->
                                                        <?php // echo $found_user->user_info_tel; ?> </a></td>
                                                        <td class="center"><a href="<?php echo $found_user_account->user_account_email; ?>">
                                                                <?php echo $found_user_account->user_account_email; ?> </a></td>
                                                        <!--<td class="center"><?php // echo $found_user->user_info_address;      ?></td>-->
                                                        <td>
                                                        <?php 
                                                            $groupeclasses =  Groupe::find_by_id_cus($found_user->user_info_id);
                                                            foreach( $groupeclasses as $groupclass){
                                                                echo "<button type='button' class='btn btn-round btn-info'>".$groupclass->groupe_libelle."</button>";   
                                                            }
                                                        ?>
                                                        </td>
                                                        <!--<td class="center"><?php echo $found_user->user_info_date_creation; ?></td>-->
                                                        <?php
                                                        $count_active_contracts = Contract::count_active();
                                                        ?>
                                                        <td class="center"><a href="<?php echo $app_or_appex ?>.php?item=listcontracts&customer_id=<?php echo $found_user->user_info_id; ?>"><span class="label label-<?php echo ($count_active_contracts > 0) ? "info" : "warning" ?>"><?php echo ($count_active_contracts > 0) ? "oui" : "non" ?></span> <i class="fa fa-eye"></i></a></td>
                                                        <?php
                                                        $count_active_tickets = Ticket::count_active();
                                                        ?>
                                                        <td class="center"><a href="<?php echo $app_or_appex ?>.php?item=listtickets&customer_id=<?php echo $found_user->user_info_id; ?>"><span class="label label-<?php echo ($count_active_contracts > 0) ? "info" : "warning" ?>"><?php echo ($count_active_contracts) ?></span> <i class="fa fa-eye"></i></a></td>
                                                        <?php
//                                                        $count_active_form_builders = Form_builder::count_by_customer();
                                                        ?>
                                                        <!--<td class="center"><a href="<?php // echo $app_or_appex ?>.php?item=listform_builder_tables&customer_id=<?php echo $found_user->user_info_id; ?>"><span class="label label-<?php echo ($count_active_form_builders > 0) ? "info" : "warning" ?>"><?php echo ($count_active_form_builders) ?></span> <i class="fa fa-eye"></i></a></td>-->
                                                        <td class="center">
                                                            <a href="<?php echo $app_or_appex ?>.php?item=formcustomer&option=edit&uid=<?php echo $found_user->user_info_id; ?>"
                                                               class="btn btn-tbl-edit btn-xs">
                                                                <i class="fa fa-pencil"></i>
                                                            </a>
                                                            <!--<a class="btn btn-tbl-delete btn-xs">
                                                                    <i class="fa fa-trash-o "></i>
                                                            </a> -->
                                                        </td>
                                                    </tr>

                                                <?php } ?>
                                            <?php } ?>
                                   

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab2">
                <div class="row">

                    <?php
                    $user_info = new User_info();
                    $user_account = new User_account();
                    $found_user_account_online = User_account::find_by_id($session->user_account_id);
                    if ($found_user_account_online->user_account_typeuser_id == 4)
                        $found_users = $user_info->find_all_active_by_customer($found_user_account_online->user_account_id);
                    else
                        $found_users = $user_info->find_all_active();
                    foreach ($found_users as $found_user) {
                        $found_user_account = $user_account->find_by_id($found_user->user_info_user_account_id);
                        if ($found_user_account->user_account_typeuser_id == 4) {
                            ?>
                            <div class="col-md-4">
                                <div class="card">
                                    <div class="m-b-20">
                                        <div class="doctor-profile">
                                            <div class="profile-header bg-b-purple">
                                                <div class="user-name"><?php echo $found_user->user_info_lastname; ?></div>
                                                <div class="name-center"><?php echo $found_user->user_info_firstname; ?></div>
                                            </div>
                                            <img src="images/<?php echo $found_user->user_info_image ?>" class="user-img" alt="">
                                            <p>
                                                <?php echo $found_user->user_info_address; ?>
                                            </p>
                                            <div>
                                                <p>
                                                    <i class="fa fa-phone"></i><a href="tel:<?php echo $found_user->user_info_tel; ?>">
                                                        <?php echo $found_user->user_info_tel; ?></a>
                                                </p>
                                            </div>
                                            <div class="profile-userbuttons">
                                                <a href="<?php echo $app_or_appex ?>.php?item=formcustomer&option=edit&uid=<?php echo $found_user->user_info_id; ?>"
                                                   class="btn btn-circle deepPink-bgcolor btn-sm">Mofifier</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php } ?>
                    <?php } ?>


                </div>
            </div>

        </div>
    </div>
</div>