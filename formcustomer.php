<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Client</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="<?php echo $app_or_appex ?>.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="<?php echo $app_or_appex ?>.php?item=listcustomers">Client</a>&nbsp
                    </li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <div class="card-head">
                        <header>Informations</header>
                    </div>
                    <?php
                    if (isset($_GET['msg']) && $_GET['msg'] == 'error') {
                        echo '<span class="clsAvailable" style="color: red"> Mauvaise confirmation du mot de passe. </span>';
                    }
                    ?>
                    <?php
                    if (isset($_GET['uid'])) {
                        $uid = $_GET['uid'];
                    }

                    if (isset($_GET['option']) && $_GET['option'] == 'new') {
                        ?>         
                        <form action="savecustomer.php" method="POST" enctype="multipart/form-data">
                            <div class="card-body row">

                                <div class="col-lg-6 p-t-20">
                                    <div
                                        class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class="mdl-textfield__input" type="file" id="file_upload" name="file_upload" value="<?php echo $found_user_info->user_info_image; ?>">
                                        <label class="mdl-textfield__label">Ajouter une photo</label>
                                    </div>
                                </div>

                                <div class="col-lg-6 p-t-20">
                                    <div
                                        class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class="mdl-textfield__input" type="text" id="txtFirstName" name="firstname">
                                        <label class="mdl-textfield__label">Nom de la boite</label>
                                    </div>
                                </div>
                                <div class="col-lg-6 p-t-20">
                                    <div
                                        class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class="mdl-textfield__input" type="text" id="txtLasttName" name="lastname">
                                        <label class="mdl-textfield__label">Représentant</label>
                                    </div>
                                </div>
                                <div class="col-lg-6 p-t-20">
                                    <div
                                        class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class="mdl-textfield__input" type="email" id="txtemail" name="email">
                                        <label class="mdl-textfield__label">Email</label>
                                        <span class="mdl-textfield__error">Entrez un adresse mail valide!</span>
                                    </div>
                                </div>

                                <!--                                <div class="col-lg-6 p-t-20">
                                                                    <div
                                                                        class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                                        <input class="mdl-textfield__input" type="password" id="txtPwd" name="password">
                                                                        <label class="mdl-textfield__label">Mot de passe</label>
                                                                    </div>
                                                                </div>-->
                                <input type="hidden" name="password" value="pass">
                                <div class="col-lg-6 p-t-20">
                                    <div
                                        class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class="mdl-textfield__input" type="text"
                                               pattern="-?[0-9]*(\.[0-9]+)?" id="text5" name="telephone">
                                        <label class="mdl-textfield__label" for="text5">Téléphone</label>
                                        <span class="mdl-textfield__error"></span>
                                    </div>
                                </div>
                                <!--                                <div class="col-lg-6 p-t-20">
                                                                    <div
                                                                        class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                                                        <input class="mdl-textfield__input" type="password" id="txtConfirmPwd" name="confirm-password">
                                                                        <label class="mdl-textfield__label">Confirmation Mot de passe</label>
                                                                    </div>
                                                                </div>-->
                                <input type="hidden" name="confirm-password" value="pass">

                                <!--                                <div class="col-lg-6 p-t-20">-->
                                <!--<div-->
                                <!--class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">-->
                                <!--<label for="sample2" class="mdl-textfield__label">Type Utilisateur</label>-->
                                <input type="hidden" name="type" value="4">

                                <!--</div>-->
                                <!--</div>-->

                                <div class="col-lg-12 p-t-20">
                                    <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                        <textarea class="mdl-textfield__input" rows="4" name="desc"></textarea>
                                        <label class="mdl-textfield__label" for="text7">Description</label>
                                    </div>
                                </div>

                                <div class="col-lg-6 p-t-20">
                                    <div
                                        class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class="mdl-textfield__input" type="text" id="txtConfirmPwd" name="customer_url">
                                        <label class="mdl-textfield__label">Site web</label>
                                    </div>
                                </div>

                                <div class="col-lg-6 p-t-20">
                                    <div
                                        class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                        <input class="mdl-textfield__input" type="text" id="txtConfirmPwd" name="address">
                                        <label class="mdl-textfield__label">Adresse</label>
                                    </div>
                                </div>
                                <?php
                                $found_groups = Activity::find_all();
                                echo '<div class="">';
                                echo '<label for="subject" class="">Groupe:</label>';
                                echo '<select name="groupe[]" multiple class=" select2-multiple select2-search">';
                                foreach($found_groups as $found_group){
                                    //echo "<option value='$found_client->customer_id'>$found_client->customer_firstname</option>";
                                    echo "<option value='$found_group->activity_id'>$found_group->activity_libelle</option>";
                                }
                                echo '</select>';
                                echo '</div>';
                                ?>
                            </div>
                            
                            <!--<div class="col-lg-12 p-t-20">
                                    <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                            <textarea class="mdl-textfield__input" rows="4" id="education"></textarea>
                                            <label class="mdl-textfield__label" for="text7">Education</label>
                                    </div>
                            </div>-->
                            <div class="col-lg-12 p-t-20 text-center">
                                <input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink" value="valider">
                                <!--<button type="button"
                                        class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Envoyer</button>
                                <button type="button"
                                        class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Annuler</button>-->
                            </div>
                    </div>
                </div>
                </form>
            <?php } ?>

            <?php
            if (isset($_GET['option']) && $_GET['option'] == 'edit' && isset($_GET['uid'])) {
                $user_info = new User_info();
                $user_account = new User_account();
                $found_user_info = $user_info->find_by_id($uid);
                $found_user_account = $user_account->find_by_id($found_user_info->user_info_user_account_id);

                // Get User Logged Typeuser
                $found_user_account_logged = User_account::find_by_id($session->user_account_id);
                $found_typeuser = Typeuser::find_by_id($found_user_account_logged->user_account_typeuser_id);
                ?>
                <form action="savecustomer.php" method="POST" enctype="multipart/form-data">
                    <div class="card-body row">
                        <input type="hidden" name="uid" value="<?php echo $found_user_info->user_info_id; ?>" />
                        <input type="hidden" name="uida" value="<?php echo $found_user_account->user_account_id; ?>" />

                        <div class="col-lg-6 p-t-20">
                            <div
                                class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                <input class="mdl-textfield__input" type="text" id="txtCustomernumber" name="customernumber" value="<?php echo $found_user_account->user_account_customer_number; ?>">
                                <label class="mdl-textfield__label">Numéro client</label>
                            </div>
                        </div>

                        <div class="col-lg-6 p-t-20">
                            <div
                                class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                <input class="mdl-textfield__input" type="file" id="file_upload" name="file_upload" >
                                <label class="mdl-textfield__label">Photo</label>
                            </div>
                        </div>

                        <div class="col-lg-6 p-t-20">
                            <div
                                class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                <input class="mdl-textfield__input" type="text" id="txtFirstName" name="firstname" value="<?php echo $found_user_info->user_info_firstname; ?>">
                                <label class="mdl-textfield__label">Nom de la boite</label>
                            </div>
                        </div>

                        <div class="col-lg-6 p-t-20">
                            <div
                                class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                <input class="mdl-textfield__input" type="text" id="txtLasttName" name="lastname"  value="<?php echo $found_user_info->user_info_lastname; ?>">
                                <label class="mdl-textfield__label">Représentant</label>
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div
                                class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                <input class="mdl-textfield__input" type="email" id="txtemail" name="email"  value="<?php echo $found_user_account->user_account_email; ?>">
                                <label class="mdl-textfield__label">Email</label>
                                <span class="mdl-textfield__error">Entrez un adresse mail valide!</span>
                            </div>
                        </div>

                        <div class="col-lg-6 p-t-20">
                            <div
                                class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                <input class="mdl-textfield__input" type="password" id="txtPwd" name="password">
                                <label class="mdl-textfield__label">Mot de passe</label>
                            </div>
                        </div>
                        <div class="col-lg-6 p-t-20">
                            <div
                                class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                <input class="mdl-textfield__input" type="password" id="txtConfirmPwd" name="confirm-password">
                                <label class="mdl-textfield__label">Confirmation Mot de passe</label>
                            </div>
                        </div>


                        <div class="col-lg-6 p-t-20">
                            <div
                                class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                <input class="mdl-textfield__input" type="text"
                                       pattern="-?[0-9]*(\.[0-9]+)?" id="text5" name="telephone" value="<?php echo $found_user_info->user_info_tel; ?>">
                                <label class="mdl-textfield__label" for="text5">Téléphone</label>
                                <span class="mdl-textfield__error"></span>
                            </div>
                        </div>

                        

                        <?php
//                        if ($found_typeuser->type_user_id == 1) {
                        ?>
                        <!--<div class="col-lg-6 p-t-20">-->
                        <!--<div-->
                        <!--class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">-->
                        <!--<label for="sample2" class="mdl-textfield__label">Type Utilisateur</label>-->
                        <!--<select class="mdl-textfield__input" name="type">-->
                        <?php
//                                        $type = new Typeuser();
//                                        $found_typeusers = $type->find_all();
//                                        foreach ($found_typeusers as $found_typeuser) {
//                                            $selected = ($found_typeuser->type_user_id == $found_user_account->user_account_typeuser_id) ? 'selected' : '';
//                                            echo '<option class="mdl-menu__item" value="' . $found_typeuser->type_user_id . '" ' . $selected . '>' . $found_typeuser->type_user_desc . '</option>';
//                                        }
                        ?>
                        <!--</select>-->
                        <!--</div>-->
                        <!--</div>-->
                        <?php // } ?>
                        <input type="hidden" name="type" value="4">


                        <div class="col-lg-6 p-t-20">
                            <div
                                class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                <input class="mdl-textfield__input" value="<?php echo $found_user_info->user_info_address ?>" type="text" id="txtConfirmPwd" name="address">
                                <label class="mdl-textfield__label">Adresse</label>
                            </div>
                        </div>

                        <div class="col-lg-6 p-t-20">
                            <div
                                class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                <input class="mdl-textfield__input" type="text" id="txtConfirmPwd" name="customer_url" value="<?php echo $found_user_info->user_info_customer_url ?>">
                                <label class="mdl-textfield__label">Site web</label>
                            </div>
                        </div>

                        <div class="col-lg-12 p-t-20">
                            <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                <textarea class="mdl-textfield__input" rows="4" name="desc"><?php echo $found_user_info->user_info_desc; ?></textarea>
                                <label class="mdl-textfield__label" for="text7">Description</label>
                            </div>
                        </div>

                        
                        <div class="col-lg-6 p-t-20">
                            <div
                                class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label txt-full-width">
                                 <?php  if(!empty($found_user_info->user_info_active)) { 
                                            echo '<input class="mdl-textfield__input" type="checkbox" id="checkbox" name="user_bl"  checked="checked" />'; 
                                        }
                                        else{
                                             echo '<input class="mdl-textfield__input" type="checkbox" id="checkbox" name="user_bl" />'; 
                                        }

                                  ?> 
                                <label class="mdl-textfield__label"> Actif?</label>
                            </div>
                        </div>

                        <?php
                            echo '<div class="col-lg-6 p-t-20">';
                            echo '<label for="subject" class="">Groupe:</label>';
                            echo '<select name="groupe[]" multiple class=" select2-multiple">';
                                //explode ids
                                //on part sur les clients présent dans le linkads
                                $links =  Link_activity :: find_by_id_cus($found_user_info->user_info_id);
                                $string = "";
                                $tmp_int = 0;
                                if(sizeof($links) != 0){
                                foreach($links as $link){
                                    if($tmp_int != $link->activity_id){
                                        $group_ids = Activity::find_by_id($link->activity_id);
                                        echo "<option value='$group_ids->activity_id' selected >$group_ids->activity_libelle</option>";
                                        $string.=$group_ids->activity_id.",";
                                        $tmp_int=$link->activity_id;
                                    }
                                }
                                    if($tmp_int != 0){
                                        $string = substr($string, 0, -1);
                                        $listgroups =  Activity::list_multiple_others($string);
                                        foreach($listgroups as $listgroup){
                                            echo "<option value='$listgroup->activity_id' >$listgroup->activity_libelle</option>";
                                        }
                                    }
                                }else if($tmp_int == 0){
            
                                    $listgroups =  Activity::find_all();
                                    foreach($listgroups as $listgroup){
                                        echo "<option value='$listgroup->activity_id' >$listgroup->activity_libelle</option>";
                                    }
            
                                }
            
                                
                            echo '</select>';
                            echo '</div>';
                        ?>



                        <!--<div class="col-lg-12 p-t-20">
                                <div class="mdl-textfield mdl-js-textfield txt-full-width">
                                        <textarea class="mdl-textfield__input" rows="4" id="education"></textarea>
                                        <label class="mdl-textfield__label" for="text7">Education</label>
                                </div>
                        </div>-->
                        <div class="col-lg-12 p-t-20 text-center">
                            <input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink" value="valider">
                            <!--<button type="button"
                                    class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 m-r-20 btn-pink">Envoyer</button>
                            <button type="button"
                                    class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-default">Annuler</button>-->
                        </div>
                    </div>
            </div>
            </form>

        <?php } ?>
    </div>
</div>
</div>
</div>