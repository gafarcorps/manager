
<?php

/**
 * Description of page
 *
 * @author Menz ALFA
 */
// Define the core paths
require_once('core_paths_definition.php');

require_once(LIB_PATH.DS.'initialize.php');


class Mention {

    //put your code here
    protected static $table_name = "copyright";
    protected static $db_fields = array(
        'mention_legale_id',
        //'mention_legale_id', 
        'mention_legale_logo', 
        'mention_legale_nom',
        'mention_legale_tel', 
        'mention_legale_email', 
        'mention_legale_adresse', 
        'mention_legale_numr', 
//        'mention_legale_date', 
        'mention_legale_appli_name', 
        'mention_legale_epress_title', 
        'mention_legale_epress_text', 
        'mention_legale_epress_image',
        'mention_legale_epress_active',
        'mention_legale_login_background',
        'mention_legale_login_text',
        'mention_legale_website'
        );
    public $mention_legale_id;
    public $mention_legale_logo;
    public $mention_legale_nom;
    public $mention_legale_tel;
    public $mention_legale_email;
    public $mention_legale_adresse;
    public $mention_legale_numr;
    public $mention_legale_appli_name;
    public $mention_legale_epress_title;
    public $mention_legale_epress_text;
    public $mention_legale_epress_image;
    public $mention_legale_epress_active;
    public $mention_legale_login_background;
    public $mention_legale_login_text;
    public $mention_legale_website;
    private $temp_path;
    private $temp_path2;
    private $temp_path3;
    protected $upload_dir = "../files/";
    protected $upload_dir2 = "../files/";
    public $errors = array();
    protected $upload_errors = array(
        // http://www.php.net/manual/en/features.file-upload.errors.php
        UPLOAD_ERR_OK => "No errors.",
        UPLOAD_ERR_INI_SIZE => "Larger than upload_max_filesize.",
        UPLOAD_ERR_FORM_SIZE => "Larger than form MAX_FILE_SIZE.",
        UPLOAD_ERR_PARTIAL => "Partial upload.",
        UPLOAD_ERR_NO_FILE => "No file.",
        UPLOAD_ERR_NO_TMP_DIR => "No temporary directory.",
        UPLOAD_ERR_CANT_WRITE => "Can't write to disk.",
        UPLOAD_ERR_EXTENSION => "File upload stopped by extension."
    );

    // Instanciation des attributs de la classe


    private static function instantiate($record) {
        // Could check that $record exists and is an array
        $object = new self;
        // Simple, long-form approach:
        // $object->file_id 				= $record['file_id'];
        // $object->username 	= $record['username'];
        // $object->password 	= $record['password'];
        // $object->first_name = $record['first_name'];
        // $object->last_name 	= $record['last_name'];
        // More dynamic, short-form approach:
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    private function has_attribute($attribute) {
        // We don't care about the value, we just want to know if the key exists
        // Will return true or false
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes() {
        // return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes() {
        global $database;
        $clean_attributes = array();
        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    // replaced with a custom save()
    public function save() {
        // A new record won't have an file_id yet.
        return isset($this->mention_legale_id) ? $this->update() : $this->create();
    }

    public function create() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - INSERT INTO table (key, key) VALUES ('value', 'value')
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= join(", ", array_keys($attributes));
        $sql .= ") VALUES ('";
        $sql .= join("', '", array_values($attributes));
        $sql .= "')";
        echo $sql;
        if ($database->query($sql)) {
            $this->mention_legale_id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }

    public function update() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}='{$value}'";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= join(", ", $attribute_pairs);
        $sql .= " WHERE mention_legale_id=" . $database->escape_value($this->mention_legale_id);
        echo $sql;
        $database->query($sql);
        return ($database->affected_rows() == 1) ? true : false;
    }

    public function delete() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - DELETE FROM table WHERE condition LIMIT 1
        // - escape all values to prevent SQL injection
        // - use LIMIT 1
        $sql = "DELETE FROM " . self::$table_name;
        $sql .= " WHERE mention_legale_id=" . $database->escape_value($this->mention_legale_id);
        $sql .= " LIMIT 1";
        $database->query($sql);
        return ($database->affected_rows() == 1) ? true : false;

        // NB: After deleting, the instance of User still 
        // exists, even though the database entry does not.
        // This can be useful, as in:
        //   echo $user->first_name . " was deleted";
        // but, for example, we can't call $user->update() 
        // after calling $user->delete().
    }

    // Pass in $_FILE(['uploaded_file']) as an argument
    public function attach_file($file, $field) {
        // Perform error checking on the form parameters
        if (!$file || empty($file) || !is_array($file)) {
            // error: nothing uploaded or wrong argument usage
            echo "No file was uploaded.";
            $this->errors[] = "No file was uploaded.";
            return false;
        } elseif ($file['error'] != 0) {
            // error: report what PHP says went wrong
            echo $file['size'];
            $this->errors[] = $this->upload_errors[$file['error']];
            echo $this->upload_errors[$file['error']];
            return false;
        } else {
            // Set object attributes to the form parameters.
            $this->temp_path = $file['tmp_name'];
            $this->$field = basename(date("Ymd") . time() . '_' . $file['name']);
//		  $this->file_type       = $file['type'];
//		  $this->file_size       = $file['size'];
            // Don't worry about saving anything to the database yet.
            return true;
        }
    }

    public function save2() {
        // A new record won't have an file_id yet.
        if (isset($this->mention_legale_id)) {
            echo 'i enter good update...';
            // Really just to update the caption
            //$this->update();
            if (!empty($this->errors)) {
                return false;
            }

            // Make sure the caption is not too long for the DB
//            if (strlen($this->image_caption) > 255) {
//                $this->errors[] = "The caption can only be 255 characters long.";
//                return false;
//            }

            // Can't save without file_path and temp location
            if (empty($this->mention_legale_logo) || empty($this->temp_path)) {

                $this->errors[] = "The file location was not available.";
                return false;
            }

            // Determine the target_path
            $target_path = $this->upload_dir . 'DS' . $this->mention_legale_logo;

            // Make sure a file doesn't already exist in the target location
            if (file_exists($target_path)) {
                echo "The file {$this->mention_legale_logo} already exists.";
                $this->errors[] = "The file {$this->mention_legale_logo} already exists.";
                return false;
            }

            // Attempt to move the file 

            if (move_uploaded_file($this->temp_path, $target_path)) {

                // Success
                // Save a corresponding entry to the database
                $this->update();
                    // We are done with temp_path, the file isn't there anymore
                unset($this->temp_path);
                return true;
            } else {
                // File was not moved.
                $this->errors[] = "The file upload failed, possibly due to incorrect permissions on the upload folder.";

                return false;
            }
            /** end if	* */
        } else {
            // Make sure there are no errors
            // Can't save if there are pre-existing errors
            if (!empty($this->errors)) {
                return false;
            }

            // Make sure the caption is not too long for the DB
            if (strlen($this->image_caption) > 255) {
                $this->errors[] = "The caption can only be 255 characters long.";
                return false;
            }

            // Can't save without file_path and temp location
            if (empty($this->mention_legale_logo) || empty($this->temp_path)) {

                $this->errors[] = "The file location was not available.";
                return false;
            }

            // Determine the target_path
            $target_path = $this->upload_dir . DS . $this->mention_legale_logo;

            // Make sure a file doesn't already exist in the target location
            if (file_exists($target_path)) {
                echo "The file {$this->mention_legale_logo} already exists.";
                $this->errors[] = "The file {$this->mention_legale_logo} already exists.";
                return false;
            }

            // Attempt to move the file 

            if (move_uploaded_file($this->temp_path, $target_path)) {

                // Success
                // Save a corresponding entry to the database
                if ($this->create()) {
                    // We are done with temp_path, the file isn't there anymore
                    unset($this->temp_path);
                    return true;
                }
            } else {
                // File was not moved.
                $this->errors[] = "The file upload failed, possibly due to incorrect permissions on the upload folder.";

                return false;
            }
        }
    }

    // Pass in $_FILE(['uploaded_file']) as an argument
    public function attach_file3($file) {
        // Perform error checking on the form parameters
        if (!$file || empty($file) || !is_array($file)) {
            // error: nothing uploaded or wrong argument usage
            echo "No file was uploaded.";
            $this->errors[] = "No file was uploaded.";
            return false;
        } elseif ($file['error'] != 0) {
            // error: report what PHP says went wrong
            echo $file['size'];
            $this->errors[] = $this->upload_errors[$file['error']];
            echo $this->upload_errors[$file['error']];
            return false;
        } else {
            // Set object attributes to the form parameters.
            $this->temp_path2 = $file['tmp_name'];
            $this->mention_legale_epress_image = basename(date("Ymd") . time() . '_' . $file['name']);
//		  $this->file_type       = $file['type'];
//		  $this->file_size       = $file['size'];
            // Don't worry about saving anything to the database yet.
            return true;
        }
    }

    public function save3() {
        // A new record won't have an file_id yet.
        if (isset($this->mention_legale_id)) {
            echo 'i enter good update...';
            // Really just to update the caption
            //$this->update();
            if (!empty($this->errors)) {
                return false;
            }

            // Make sure the caption is not too long for the DB
            if (strlen($this->image_caption) > 255) {
                $this->errors[] = "The caption can only be 255 characters long.";
                return false;
            }

            // Can't save without file_path and temp location
            if (empty($this->mention_legale_epress_image) || empty($this->temp_path2)) {

                $this->errors[] = "The file location was not available.";
                return false;
            }

            // Determine the target_path
            $target_path = $this->upload_dir2 . 'DS' . $this->mention_legale_epress_image;

            // Make sure a file doesn't already exist in the target location
            if (file_exists($target_path)) {
                echo "The file {$this->mention_legale_epress_image} already exists.";
                $this->errors[] = "The file {$this->mention_legale_epress_image} already exists.";
                return false;
            }

            // Attempt to move the file 

            if (move_uploaded_file($this->temp_path2, $target_path)) {

                // Success
                // Save a corresponding entry to the database
                if ($this->update()) {
                    // We are done with temp_path2, the file isn't there anymore
                    unset($this->temp_path2);
                    return true;
                }
            } else {
                // File was not moved.
                $this->errors[] = "The file upload failed, possibly due to incorrect permissions on the upload folder.";

                return false;
            }
            /** end if	* */
        } else {
            // Make sure there are no errors
            // Can't save if there are pre-existing errors
            if (!empty($this->errors)) {
                return false;
            }

            // Make sure the caption is not too long for the DB
            if (strlen($this->image_caption) > 255) {
                $this->errors[] = "The caption can only be 255 characters long.";
                return false;
            }

            // Can't save without file_path and temp location
            if (empty($this->mention_legale_epress_image) || empty($this->temp_path2)) {

                $this->errors[] = "The file location was not available.";
                return false;
            }

            // Determine the target_path
            $target_path = $this->upload_dir2 . 'DS' . $this->mention_legale_epress_image;

            // Make sure a file doesn't already exist in the target location
            if (file_exists($target_path)) {
                echo "The file {$this->mention_legale_epress_image} already exists.";
                $this->errors[] = "The file {$this->mention_legale_epress_image} already exists.";
                return false;
            }

            // Attempt to move the file 

            if (move_uploaded_file($this->temp_path2, $target_path)) {

                // Success
                // Save a corresponding entry to the database
                if ($this->create()) {
                    // We are done with temp_path2, the file isn't there anymore
                    unset($this->temp_path2);
                    return true;
                }
            } else {
                // File was not moved.
                $this->errors[] = "The file upload failed, possibly due to incorrect permissions on the upload folder.";

                return false;
            }
        }
    }

    public static function find_by_sql($sql = "") {
        global $database;
        $result_set = $database->query($sql);
        $object_array = array();
        while ($row = $database->fetch_array($result_set)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }
    
    public static function count_by_sql($sql) {
        global $database;
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }

    // Common Database Methods
    public static function find_all() {
        // retourne tous les enregistrements actifs et supprimés
        return self::find_by_sql("SELECT * FROM " . self::$table_name . "  ");
    }

    public static function find_all_active() {
        // retourne tous les enregistrements actifs
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE mention_legale_active = 1  ");
    }

    public static function find_all_unabled() {
        // retourne tous les enregistrements supprimés
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE mention_legale_active = 0  ");
    }

    public static function find_by_id($id = 0) {
        // retourne  un enregistrements actif
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE mention_legale_id={$id}");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function list_others($id = 0) {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE mention_legale_id<>{$id} AND mention_legale_active = 1");
    }

    public static function count_all() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name;
        $result_set = $database->query($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }

}

?>