<?php

require_once("includes/initialize.php");
$found_useraccount = User_account::find_by_id($session->user_account_id);
$app_or_appex = ($found_useraccount->user_account_typeuser_id > 2) ? "appex" : "app";

if (isset($_POST['submit'])) { // IF ISSET SUBMIT
    $user = new User_info();
    $user_account = new User_account();

    if (isset($_POST['username'])) {
        $username = $_POST['username'];
    }
    if (isset($_POST['email'])) {
        $email = $_POST['email'];
    }
    if (isset($_POST['password'])) {
        $password = $_POST['password'];
    }
    if (isset($_POST['confirm-password'])) {
        $confirmpassword = $_POST['confirm-password'];
    }
    if ($password != $confirmpassword) {
        redirect_to('app.php?item=formcustomer&option=new&msg=error');
    }
    if (isset($_POST['firstname'])) {
        $firstname = $_POST['firstname'];
    }
    if (isset($_POST['lastname'])) {
        $lastname = $_POST['lastname'];
    }
    if (isset($_POST['telephone'])) {
        $telephone = $_POST['telephone'];
    }
    if (isset($_POST['customer_url'])) {
        $customer_url = $_POST['customer_url'];
    }
    if (isset($_POST['desc'])) {
        $desc = $_POST['desc'];
    }
    if (isset($_POST['address'])) {
        $address = $_POST['address'];
    }
    if (isset($_POST['type'])) {
        $type = $_POST['type'];
    }
    if (isset($_POST['uid'])) {
        $uid = $_POST['uid'];
    }
    if (isset($_POST['uida'])) {
        $uida = $_POST['uida'];
    }

    if (isset($_POST['groupe'])) {
        $group = $_POST['groupe'];
    }

    if (!empty($_FILES['photo']['name'])) {
        $file_upload = basename($_FILES['photo']['name']);
    }
    $user_bl 	=	($_POST['user_bl']) ? true : false;

    $linkgrp = new Link_activity();

    if (!isset($_POST['uid'])) {


        //Create user account 
        $user_account->user_account_email = $email;
        $user_account->user_account_customer_number = -1;
        $hashed_password = password_hash($confirmpassword, PASSWORD_BCRYPT);
        $user_account->user_account_password = $hashed_password;
        $user_account->user_account_date_creation = date("Y-m-d");
        $user_account->user_account_typeuser_id = $type;
        $user_account->user_account_active = 1;
        
        $user_account->user_account_extension = 1;
        $user_account->user_account_extension_user_id = -1;
        $user_account->user_account_extension_form_builder_id = -1;
        
        $user_account->user_account_user_creation = $session->user_account_id;
        $user_account->user_account_date_creation = date("Y-m-d");
        $user_account->user_account_user_last_update = $session->user_account_id;
        $user_account->user_account_date_last_connexion = date("Y-m-d");
        
        $user_account->save();

        $last_user_account_added = $user_account->find_last();

        //user info
        $user->user_info_firstname = $firstname;
        $user->user_info_lastname = $lastname;
        $user->user_info_address = $address;
        $user->user_info_tel = $telephone;
        $user->user_info_customer_url = $customer_url;
        $user->user_info_desc = $desc;
        $user->user_info_user_account_id = $last_user_account_added->user_account_id;
        
        $user->user_info_user_creation = $session->user_account_id;
        $user->user_info_logged_in = $session->user_account_id;
        $user->user_info_date_creation = date("Y-m-d");
        $user->user_info_date_last_connexion = date("Y-m-d");
        
        $user->user_info_active = $user_bl;
        
        $repository = 'images/';
        if (isset($_FILES['file_upload'])) {
            move_uploaded_file($_FILES['file_upload']['tmp_name'], $repository . $file_upload);
            $user->user_info_image = $file_upload;
        }

        $user->save();

        $found_user = User_info::find_last();
//        echo '>>'.$found_user->user_info_id;
        $last_user_account_added->user_account_customer_number = $found_user->user_info_id;
        $last_user_account_added->save();

        if(sizeof($group) != 0){
            echo "size is ".sizeof($group);
            foreach($group as $grp){
                echo $grp;
              
                        $linkgrp->link_activity_activity_id = $grp;
                        $linkgrp->link_activity_user_id = $found_user->user_info_id;
                        $linkgrp->create(); 
                  
            }
        }

    } else { // UPDATE
        echo "delete";
        $found_user_info = $user->find_by_id($uid);

        $found_user_account = $user_account->find_by_id($uida);

        $linkeds = Link_activity :: find_by_id_cus($uid);
        //suppression des clients selectionnés
        foreach($linkeds as $linked){
            echo "delete";
            $linked->delete_by_user();
        } 

        //update user account
        $found_user_account->user_account_id = $uida;
        $found_user_account->user_account_email = $email;
        $found_user_account->user_account_password = (!empty($confirmpassword)) ? password_hash($confirmpassword, PASSWORD_BCRYPT) : ($found_user_account->user_account_password);
        $found_user_account->user_account_typeuser_id = ($type) ? $type : $found_user_account->user_account_typeuser_id;
        $found_user_account->user_account_active = 1;
        
//        $found_user_account->user_account_user_creation = $session->user_account_id;
//        $found_user_account->user_account_date_creation = date("Y-m-d");
        $found_user_account->user_account_user_last_update = $session->user_account_id;
        $found_user_account->user_account_date_last_connexion = date("Y-m-d");
        
        $found_user_account->save();

        //update user info
        $found_user_info->user_info_firstname = $firstname;
        $found_user_info->user_info_lastname = $lastname;
        $found_user_info->user_info_address = $address;
        $found_user_info->user_info_tel = $telephone;
        $found_user_info->user_info_customer_url = $customer_url;
        $found_user_info->user_info_desc = $desc; 
        $found_user_info->user_info_user_account_id = $uida;
        $found_user_info->user_info_active = $user_bl;
        $repository = 'images/';
        if (isset($file_upload)) {
            move_uploaded_file($_FILES['file_upload']['tmp_name'], $repository . $file_upload);
            $found_user_info->user_info_image = $file_upload;
        }
        
//        $found_user_info->user_info_user_creation = $session->user_account_id;
        $found_user_info->user_info_logged_in = $session->user_account_id;
//        $found_user_info->user_info_date_creation = date("Y-m-d");
        $found_user_info->user_info_date_last_connexion = date("Y-m-d");
        
        $found_user_info->save();

        //insertion dans les groupes
        if(sizeof($group) != 0){
            echo "size is update".sizeof($group);
            foreach($group as $grp){
                echo $grp;
              
                $linkgrp->link_activity_activity_id = $grp;
                $linkgrp->link_activity_user_id = $uid;
                $linkgrp->create(); 
                  
            }
        }


    }

    // Get User Logged Typeuser
    $found_user_account_logged = User_account::find_by_id($session->user_account_id);
    $found_typeuser = Typeuser::find_by_id($found_user_account_logged->user_account_typeuser_id);

//    if ($found_typeuser->type_user_id != 1)
//        redirect_to($_SERVER['HTTP_REFERER']);

    redirect_to($app_or_appex.'.php?item=listcustomers&msg=success');
}
?>