<?Php
//$found_extensions = Menu::list_extensions_active();
//foreach ($found_extensions as $found_extension) {
$found_typeusers = Typeuser::find_all_active($session->extension_id);
foreach ($found_typeusers as $found_typeuser) {
    $found_db_menus = Db_menu::list_menus_active($session->extension_id);
    foreach ($found_db_menus as $found_db_menu) {
        $db_privileges = Db_privilege::find_menu_and_type_user_and_extension($found_db_menu->menu_id, $found_typeuser->type_user_id, $session->extension_id);
        if (empty($db_privileges)) {
            $db_privilege = new Db_privilege();
            $db_privilege->privilege_typeuser_id = $found_typeuser->type_user_id;
            $db_privilege->privilege_db_menu_id = $found_db_menu->menu_id;
            $db_privilege->privilege_extension_id = $session->extension_id;
            $db_privilege->privilege_active = ($found_typeuser->type_user_id == 4 || $found_typeuser->type_user_id == 5) ? 0 : 1;
            $db_privilege->save();
        } else {
            foreach ($db_privileges as $db_privilege) {
                $db_privilege->privilege_typeuser_id = $found_typeuser->type_user_id;
                $db_privilege->privilege_db_menu_id = $found_db_menu->menu_id;
                $db_privilege->privilege_extension_id = $session->extension_id;
                $db_privilege->privilege_active = ($found_typeuser->type_user_id == 4 || $found_typeuser->type_user_id == 5) ? 0 : 1;
                $db_privilege->save();
            }
        }
    }
}
//}
?>
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="<?php echo $app_or_appex ?>.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <?php
                    if (!empty($_GET['mid'])) {
                        $found_menu = Db_menu:: find_by_id($_GET['mid']);
                        ?>
                        <li><a class="parent-item" href="<?php echo $app_or_appex ?>.php?item=listdb_privileges&mid=<?php echo $found_menu->menu_parent_id ?>"> <?php echo $found_menu->menu_title ?> </a>&nbsp;<i class="fa fa-angle-right"></i>
                        </li>
                    <?php } ?>
                    <li class="active">Liste des privilèges des menus des BDs</li>
                </ol>
            </div>
        </div> 

        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <?php
                        if (empty($_GET['mid']) || (!empty($_GET['mid']) && $_GET['mid'] == -1)) {
                            $title = "Liste des privilèges des menus des BDs";
                        } else {
                            $title = "Liste des privilèges des sous-menus du menu: <b style='color: blue'>" . $found_menu->menu_title . "<b>";
                        }
                        ?>
                        <header><?php echo $title ?></header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row p-b-20">
                            <div class="col-md-6 col-sm-6 col-6">
                                <!--	<div class="btn-group">
                                                <a href="app.php?formmenu&option=new" id="addRow" class="btn btn-info">
                                                        Nouveau profil <i class="fa fa-plus"></i>
                                                </a>
                                        </div>
                                -->
                                <div class="btn-group">
                                    <button class="btn btn-success  btn-outline dropdown-toggle"
                                            data-toggle="dropdown">Paramétrage avancé
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="<?php echo $app_or_appex ?>.php?item=listusers_db&extension_id=<?php echo $_GET['extension_id'] ?>" id="addRow">
                                                Mes utilisateurs <i class="fa fa-cogs"></i>
                                            </a>

                                        </li>
                                        <li>
                                            <a href="<?php echo $app_or_appex ?>.php?item=listdb_privileges&extension_id=<?php echo $_GET['extension_id'] ?>" id="addRow">
                                                Privilèges <i class="fa fa-cogs"></i>
                                            </a>

                                        </li>
                                        <li>
                                            <a href="<?php echo $app_or_appex ?>.php?item=listform_builders_db&list=root" id="addRow">
                                                Paramétrages <i class="fa fa-cogs"></i>
                                            </a>

                                        </li>
                                        <li>
                                            <a href="<?php echo $app_or_appex ?>.php?item=listform_builders_db" id="addRow">
                                                Bases de données <i class="fa fa-files-o"></i>
                                            </a>

                                        </li>
                                        <?php if (!empty($_GET['list'])) { ?>

                                            <li>
                                                <a href="<?php echo $app_or_appex ?>.php?item=disabledform_builders_db&customer_id=<?php echo $found_customer->user_info_id; ?>" id="addRow"><b> Corbeille </b><i class="fa fa-trash"></i></a> 

                                            </li>

                                        <?php } ?>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group pull-right">

                                </div>
                            </div>
                            <div id="result"></div>
                            <div class="table-scrollable">
                                <table class="table table-hover table-checkable order-column full-width">
                                    <thead>
                                        <tr>  
                                            <th class="center">Profil </th>

                                            <?php
                                            if (empty($_GET['mid'])|| (!empty($_GET['mid']) && $_GET['mid'] == -1)) {
                                                $found_menus = $db_menu->list_menus_active($session->extension_id);
                                            } else {
                                                $found_menus = Db_menu:: list_menu_children_active($_GET['mid']);
                                            }

                                            foreach ($found_menus as $found_menu) {
                                                if (Db_menu::count_menu_children_active($found_menu->menu_id) != 0)
                                                    echo '<th class="center"><a href="' . $app_or_appex . '.php?item=listdb_privileges&mid=' . $found_menu->menu_id . '">' . $found_menu->menu_title . '</a></th> ';
                                                else
                                                    echo '<th class="center">' . $found_menu->menu_title . '</th> ';
                                            }
                                            ?>

<!-- <th class="center"> Action </th> -->
                                        </tr>


                                    <form action="save_db_privileges.php" method="POST">

                                        <?php
                                        $typeuser = new Typeuser();
                                        $foundtypeusers = Typeuser::find_all();
                                        foreach ($foundtypeusers as $foundtypeuser) {
                                            if ($foundtypeuser->type_user_id == 3 || $foundtypeuser->type_user_id == 5) {
                                                echo '<tr><td class="center">' . $foundtypeuser->type_user_desc . '</td>';
                                                if (empty($_GET['mid']) || (!empty($_GET['mid']) && $_GET['mid'] == -1)) {
                                                    $found_menus = $db_menu->list_menus_active($session->extension_id);
                                                } else {
                                                    $found_menus = Db_menu:: list_menu_children_active($_GET['mid']);
                                                }
                                                foreach ($found_menus as $found_menu) {
                                                    $foundprivileges = Db_privilege::find_menu_and_type_user_and_extension($found_menu->menu_id, $foundtypeuser->type_user_id, $session->extension_id);

                                                    foreach ($foundprivileges as $foundprivilege) {

                                                        $found_menu = $db_menu->find_by_id($foundprivilege->privilege_db_menu_id);

                                                        echo '<td class="center">';

                                                        echo '<div class="checkbox checkbox-icon-black">';

                                                        if (!empty($foundprivilege->privilege_active)) {
                                                            //echo '<input type="hidden"  class="form-check-input" value="'.$foundprivilege->privilege_type_user_id.'" />';
                                                            echo '<input type="checkbox" name="privilege_menu_id"  value="' . $foundprivilege->privilege_typeuser_id . '-' . $foundprivilege->privilege_db_menu_id . '" class="form-check-input-db" checked="checked" onchange="onClickHandlerdb()" />';
                                                        } else {
                                                            echo '<input type="checkbox" name="privilege_menu_id"  value="' . $foundprivilege->privilege_typeuser_id . '-' . $foundprivilege->privilege_db_menu_id . '" class="form-check-input-db" onchange="onClickHandlerdb()" />';
                                                        }

                                                        echo '</div>';

                                                        //	echo '<div  id="children" class="checkbox checkbox-icon-black"> </div>';

                                                        echo '</td>';
                                                    } // end foreach
                                                }



                                                echo '</tr>';
                                            }
                                        } // end foreach
                                        //	echo '</tr>';
                                        ?>
                                    </form>


                                    </thead>
                                    <tbody>




                                    </tbody>

                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    function onClickHandlerdb() {
        var update = [];
        $(".form-check-input-db").each(function () {
            if ($(this).is(":checked")) {
                update.push($(this).val() + '-1');
            } else {
                update.push($(this).val() + '-0');
            }
        });

        $.ajax({
            url: "save_db_privileges.php",
            method: "POST",
            data: {
                update: update
            },
            success: function (data) {
                $('#result').html(data);
            }
        });
    }

</script>



