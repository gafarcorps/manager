 <!-- start footer -->
 <div class="page-footer">
     <div class="page-footer-inner"> <?php echo date('Y') ?> &copy; Workspace
         <a href="mailto:ask@creativesweb.info" target="_top" class="makerCss">CreativesWeb</a>
     </div>
     <div class="scroll-to-top">
         <i class="icon-arrow-up"></i>
     </div>
 </div>
 <!-- end footer -->
 </div>

 <!-- Sweet Alert -->
 <!-- start js include path -->
 <script src="assets/plugins/jquery/jquery.min.js"></script>
 <script src="assets/plugins/popper/popper.min.js"></script>
 <script src="assets/plugins/jquery-blockui/jquery.blockui.min.js"></script>
 <script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script>
 <!-- bootstrap -->
 <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
 <!-- Sweet Alert -->
 <script src="assets/plugins/sweet-alert/sweetalert.min.js"></script>
 <script src="assets/js/pages/sweet-alert/sweet-alert-data.js"></script>
 <!-- Common js-->
 <script src="assets/js/app.js"></script>
 <script src="assets/js/layout.js"></script>
 <script src="assets/js/theme-color.js"></script>
 <!-- Material -->
 <script src="assets/plugins/material/material.min.js"></script>
 <!-- animation -->
 <script src="assets/js/pages/ui/animations.js"></script>
 <!-- end js include path -->


 <!-- <script src="assets/plugins/jquery/jquery.min.js"></script>
	<script src="assets/plugins/popper/popper.min.js"></script>
	<script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
	<script src="assets/plugins/jquery-blockui/jquery.blockui.min.js"></script>  
	<script src="assets/plugins/jquery-slimscroll/jquery.slimscroll.min.js"></script> -->
 <script src="assets/plugins/moment/moment.min.js"></script>
 <script src="assets/plugins/counterup/jquery.waypoints.min.js"></script>
 <script src="assets/plugins/counterup/jquery.counterup.min.js"></script>
 <!-- owl carousel -->
 <script src="assets/plugins/owl-carousel/owl.carousel.js"></script>
 <script src="assets/js/pages/owl-carousel/owl_data.js"></script>

 <!-- bootstrap -->
 <!-- <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script> -->
 <!-- data tables -->
 <script src="assets/plugins/datatables/jquery.dataTables.min.js"></script>
 <script src="assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.js"></script>
 <script src="assets/js/pages/table/table_data.js"></script>

 <!-- Common js
	<script src="assets/js/app.js"></script>
	<script src="assets/js/layout.js"></script>
	<script src="assets/js/theme-color.js"></script> -->
 <!-- Material -->
 <!-- <script src="assets/plugins/material/material.min.js"></script>  -->
 <!-- animation -->
 <!-- <script src="assets/js/pages/ui/animations.js"></script> -->
 <!-- sparkline -->
 <script src="assets/plugins/sparkline/jquery.sparkline.min.js"></script>
 <script src="assets/js/pages/sparkline/sparkline-data.js"></script>
 <!-- summernote -->
 <script src="assets/plugins/summernote/summernote.min.js"></script>
 <script src="assets/js/pages/summernote/summernote-data.js"></script>
 <!-- dropzone -->
 <script src="assets/plugins/dropzone/dropzone.js"></script>
 <!--tags input-->
 <script src="assets/plugins/jquery-tags-input/jquery-tags-input.js"></script>
 <script src="assets/plugins/jquery-tags-input/jquery-tags-input-init.js"></script>
 <!--select2-->
 <script src="assets/plugins/select2/js/select2.js"></script>
 <script src="assets/js/pages/select2/select2-init.js"></script>
 <!-- gallery -->
 <script src="assets/plugins/light-gallery/js/lightgallery-all.js"></script> <!-- Light Gallery Plugin Js -->
 <script src="assets/plugins/light-gallery/js/image-gallery.js"></script>
 <!-- end js include path -->
 <!-- end js include path -->
 
 
    <link rel="stylesheet" href="assets/plugins/sweet-alert/sweetalert.min.css">
    <!-- Sweet Alert -->
	<script src="assets/plugins/sweet-alert/sweetalert.min.js"></script>
	<script src="assets/js/pages/sweet-alert/sweet-alert-data.js"></script>


 <!-- DataTable Options -->
 <script>
     $.fn.dataTable.ext.errMode = 'none';
     $('#example1').DataTable({
         language: {
             "decimal": "",
             "emptyTable": "Pas de données disponible dans cette liste",
             "info": "Afficher _START_ de _END_ de _TOTAL_ enregistrements",
             "infoEmpty": "Afficher 0 to 0 of 0 enregistrements",
             "infoFiltered": "(filtrés de _MAX_ total enregistrements)",
             "infoPostFix": "",
             "thousands": ",",
             "lengthMenu": "Afficher _MENU_ enregistrements",
             "loadingRecords": "Chargement...",
             "processing": "Traitement...",
             "search": "Rechercher:",
             "zeroRecords": "Aucun enregistrements correspondants trouvés",
             "paginate": {
                 "first": "Premier",
                 "last": "Dernier",
                 "next": "Suivant",
                 "previous": "Précédent"
             },
             "aria": {
                 "sortAscending": ": activer pour trier les colonnes par ordre croissant",
                 "sortDescending": ": activer pour trier les colonnes par ordre décroissant"
             }
         }
     })
 </script>

 <!-- Our Scripts -->
 <script src="assets/js/scripts.js"></script>
 </body>


 </html>