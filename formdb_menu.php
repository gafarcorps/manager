<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="app.php?item=listdb_menus">Gestionnaire des menus des bases de données
                        </a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Editer un menu</li>
                </ol>
            </div>
        </div>


        <form action="savedb_menu.php" class="form-horizontal" method="POST" enctype="multipart/form-data"> 

            <?php
            if (isset($_GET['option']) && $_GET['option'] == 'new') {
                echo '<div class="row">';
                echo '<div class="col-md-12">';
                //<!-- BEGIN PROFILE SIDEBAR -->
                echo '<div class="profile-sidebar">';
                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Attributs du menu </div>';
                echo '<div class="profile-usertitle-job"> du nouveau menu </div>';
                echo '</div>';


                echo '<div id="displayed" class="card-body no-padding height-9">';
                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b>Etat </b>';
                echo '<div class="profile-desc-item pull-right">';
                echo '<i class="fa fa-eye"></i> publié';
                echo '</div>';
                echo '</li>';

                echo '<li class="list-group-item">';
                echo '<b>Dernière mise à jour le </b>';
                echo '<div class="profile-desc-item pull-right">';
                echo '<i class="fa fa-clock-o"> </i> 27 juillet 2020 18h 50';
                echo '</div>';
                echo '</li>';


                echo '</ul><br/>';

                echo '</div>';


                echo '</div>';


                echo '<div class="card card-topline-aqua">';
                echo '<div class="card-body no-padding height-9">';
                echo '<div class="row">';
                echo '</div>';
                echo '<br/>';


                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b>Parent </b> <a class="pull-right"></a>';
                echo '<div class="form-group">';

                $found_menus = $db_menu->list_menus();

               
//                    if (isset($_GET['mid'])) {
//                        $mid = $_GET['mid'];

                        echo '<select name="parent_id" id="parent" class="form-control" >';
//
//                        $found_menu = $db_menu->find_by_id($mid);
//
//                        $found_parent = $db_menu->find_by_id($found_menu->menu_parent_id);
//                        echo '<option value="' . $found_parent->menu_id . '">' . $found_parent->menu_title . '</option>';

                        $found_menus = $db_menu->list_menus_active($session->extension_id);
                        foreach ($found_menus as $found_menu) {
                            $found_children_menus = $db_menu->list_menu_children_active($found_menu->menu_id);
                            if (!empty($found_children_menus)) {
                                echo '<optgroup label="' . $found_menu->menu_title . '">';
                                echo '<option value="' . $found_menu->menu_id . '">' . $found_menu->menu_title . '</option>';
                                foreach ($found_children_menus as $found_children_menu) {
                                    if (!empty($found_children_menu)) {
                                        echo '<option value="' . $found_children_menu->menu_id . '">' . $found_children_menu->menu_title . '</option>';
                                    } // end if	   
                                } // end foreach
                                echo '</optgroup>';
                            } // end if found_children_menus
                            else {
                                echo '<option value="' . $found_menu->menu_id . '">' . $found_menu->menu_title . '</option>';
                            }
                        }


                        echo '</select>';
//                    }
                


                echo '</div>';
                echo '</li>';


                echo '<li class="list-group-item">';
                echo '<b>Ordre </b> <a class="pull-right"></a>';
                echo '<div class="form-group">';

                echo '<select name="position" id="parent" class="form-control" >';
                for ($i = 1; $i < 20; $i++) {
                    echo "<option>$i</option>";
                }
                echo '</select>';


                echo '</div>';
                echo '</li>';

                echo '<li class="list-group-item">';
                echo '<b>Image </b> <a class="pull-right"></a>';
                echo '<div class="form-group">';
                echo '<input type="file" class="form-control" name="file_upload" >';
                echo '</div>';
                echo '</li>';

                echo '</ul>';
                //<!-- END SIDEBAR USER TITLE -->
                echo '<br/>';
                echo '<div class="control-group">';
                echo '<label class="control-label" for="active"> Publier ce menu?</label>';
                echo '<div class="controls">';
                echo '<div class="form-group">';
                echo '<div class="checkbox checkbox-icon-black">';
                echo '<input id="rememberChk1" name="menu_active" type="checkbox" checked="checked">';
                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';


                //<!-- SIDEBAR BUTTONS -->
                echo '</div>';
                echo '</div>';

                echo '</div>';


                //<!-- END BEGIN PROFILE SIDEBAR -->
                //<!-- BEGIN PROFILE CONTENT -->
                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';
                if (isset($_GET['mid']) && $_GET['option'] == 'new') {
                    echo '<h4> Créer un nouveau sous-menu </h4>';
                } else {
                    echo '<h4> Créer un nouveau menu </h4>';
                }
                echo '</div>';
                echo '</div>';
                echo '<div class="white-box">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Titre du menu:</label>';
                echo '<input name="menu_title" type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Titre du menu (anglais):</label>';
                echo '<input name="menu_title_en" type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Titre du menu (espagnol):</label>';
                echo '<input name="menu_title_es" type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">url du menu:</label>';
                echo '<input name="menu_url" id="title" value=""  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Description</label>';
                echo '<textarea name="menu_desc" type="text" tabindex="1" class="form-control"></textarea>';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Description infos</label>';
                echo '<textarea name="$db_menu_desc_infos" type="text" tabindex="1" class="form-control"></textarea>';
                echo '</div>';

                echo '</div>';
                echo '</div>';

                echo '</div>';
                echo '<br>';
                echo '&nbsp;&nbsp;';

                if (isset($_GET['mid']) && $_GET['option'] == 'new') {
                    $pid = $_GET['mid'];
                    echo '<input name="pid" type="hidden" value="' . $mid . '" >';
                    echo '<input type="submit" name="new" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';
                } else {
                    echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';
                }


                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->
                echo '</div>';
            } //end if
            ?>


            <?php
            if (isset($_GET['option']) && $_GET['option'] == 'edit' && isset($_GET['mid'])) {

                if (isset($_GET['mid'])) {
                    $mid = $_GET['mid'];
                }

                echo '<input type="hidden" name="mid" value="' . $mid . '" />';

                $found_menu = $db_menu->find_by_id($mid);


                echo '<div class="row">';
                echo '<div class="col-md-12">';
                //<!-- BEGIN PROFILE SIDEBAR -->
                echo '<div class="profile-sidebar">';
                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Attributs du menu </div>';
                echo '<div class="profile-usertitle-job"> du menu ' . $found_menu->menu_desc . ' </div>';
                echo '</div>';
                echo '<div class="card-body no-padding height-9">';

                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b>Etat </b>';
                echo '<div class="profile-desc-item pull-right">';
                if ($found_menu->menu_active == 1) {
                    echo '<i class="fa fa-eye"></i> activé';
                } else {
                    echo '<i class="fa fa-eye-slash"></i> désactivé';
                }

                echo '</div>';
                echo '</li>';


                echo '<li class="list-group-item">';
                if ($found_menu->menu_active == 1) {

                    echo '<b>Déplacer dans la corbeille </b>';
                    echo '<div class="profile-desc-item pull-right"> <a href="savedb_menu.php?mid=' . $mid . '&option=delete"><i class="fa fa-trash-o "></i></a> </div>';
                } else {
                    echo '<b>Restaurer ce menu</b>';
                    echo '<div class="profile-desc-item pull-right"> <a href="savedb_menu.php?mid=' . $mid . '&option=recover"><i class="fa  fa-recycle "></i></a> </div>';
                }

                echo '</li>';

                echo '</ul><br/>';

                echo '</div>';
                echo '</div>';


                echo '<div class="card card-topline-aqua">';
                echo '<div class="card-body no-padding height-9">';
                echo '<div class="row">';
                echo '<div class="profile-userpic">';
                echo '</div>';
                echo '</div>';

                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b>Parent </b> <a class="pull-right"></a>';
                echo '<div class="form-group">';

                echo '<select name="parent_id" id="parent" class="form-control" >';
                echo '<option value="11"></option>';
                $found_menu = $db_menu->find_by_id($mid);

                $found_parent = $db_menu->find_by_id($found_menu->menu_parent_id);
                echo '<option value="' . $found_parent->menu_id . '" selected>' . $found_parent->menu_title . '</option>';

                $found_menus = $db_menu->list_menus_active($session->extension_id);
                foreach ($found_menus as $found_menu) {
                    $found_children_menus = $db_menu->list_menu_children_active($found_menu->menu_id);
                    if (!empty($found_children_menus)) {
                        echo '<optgroup label="' . $found_menu->menu_title . '">';
                        echo '<option value="' . $found_menu->menu_id . '">' . $found_menu->menu_title . '</option>';
                        foreach ($found_children_menus as $found_children_menu) {
                            if (!empty($found_children_menu)) {
                                echo '<option value="' . $found_children_menu->menu_id . '">' . $found_children_menu->menu_title . '</option>';
                            } // end if	   
                        } // end foreach
                        echo '</optgroup>';
                    } // end if found_children_menus
                    else {
                        echo '<option value="' . $found_menu->menu_id . '">' . $found_menu->menu_title . '</option>';
                        
                    }
                }
                $found_parent = $db_menu->find_by_id(11);
                echo '<option value="' . $found_parent->menu_id . '">' . $found_parent->menu_title . '</option>';

                echo '</select>';


                echo '</div>';
                echo '</li>';


                echo '<li class="list-group-item">';
                echo '<b>Ordre </b> <a class="pull-right"></a>';
                echo '<div class="form-group">';
                $found_menu = $db_menu->find_by_id($mid);
                echo '<select name="position" id="parent" class="form-control" >';
                for ($i = 1; $i < 20; $i++) {
                    $selected = ($found_menu->menu_position == $i) ? 'selected' : '';
                    echo "<option $selected >$i</option>";
                }
                echo '</select>';


                echo '</div>';
                echo '</li>';
                $found_menu = $db_menu->find_by_id($mid);
                //echo'<div id="children">';
                //echo '</div>';

                echo '<li class="list-group-item">';
                echo '<b>Image </b> <a class="pull-right"></a>';
                echo '<div class="form-group">';
                echo '<input type="file" class="form-control" name="file_upload" >';
                echo '</div>';
                echo '<img src="media/files/' . $found_menu->menu_picture . '" width="200"/>';
                echo '</li>';

                echo '</ul>';


                echo '<div class="controls">';
                echo '<div class="form-group">';
                echo '<div class="checkbox checkbox-icon-black">';

                if ($found_menu->menu_active == 1) {
                    echo '<input id="rememberChk1" name="menu_active" type="checkbox" checked > Activé';
                } else {
                    echo '<input id="rememberChk1" name="menu_active" type="checkbox"> Désactivé';
                }

                echo '</div>';
                echo '</div>';
                echo '</div>';




                //<!-- END SIDEBAR BUTTONS -->
                echo '</div>';
                echo '</div>';

                echo '</div>';


                //<!-- END BEGIN PROFILE SIDEBAR -->
                //<!-- BEGIN PROFILE CONTENT -->

                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';
                echo '<h4> Modifier le menu : ' . $found_menu->menu_desc . ' </h4>';

                if (isset($_GET['r']) && $_GET['r'] == 0) {
                    echo '<span class="clsNotAvailable"> Ce menu a été déplacé dans la corbeille. </span> <br/>';
                } else if (isset($_GET['r']) && $_GET['r'] == 1) {
                    echo '<span class="clsAvailable"> Ce menu a été restauré. </span> <br/>';
                }



                echo '<div class="btn-group">';
                echo '<a href="app.php?item=formmenu&mid=' . $mid . '&option=new" id="addRow" class="btn btn-info">Nouveau menu <i class="fa fa-plus"></i></a>';
                echo '</div>';


                echo '</div>';
                echo '</div>';


                echo '<div class="white-box">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';


                echo '<div class="form-group">';
                echo '<label for="subject" class="">Titre du menu:</label>';
                echo '<input name="menu_title" id="title" value="' . $found_menu->menu_title . '"  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Titre du menu (anglais):</label>';
                echo '<input name="menu_title_en" id="title_en" value="' . $found_menu->menu_title_en . '"  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Titre du menu (espagnol):</label>';
                echo '<input name="menu_title_es" id="title_es" value="' . $found_menu->menu_title_es . '"  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="url" class="">Url du menu:</label>';
                echo '<input name="menu_url" id="title" value="' . $found_menu->menu_link . '"  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '<br>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Description infos</label>';
                echo '<textarea name="$db_menu_desc_infos" type="text" tabindex="1" class="form-control">' . $found_menu->menu_desc_infos . '</textarea>';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Description</label>';
                echo '<textarea name="menu_desc" type="text" tabindex="1" class="form-control">
                    ' . $found_menu->menu_desc . '
                </textarea>';
                echo '</div>';


                echo '&nbsp;&nbsp;';
                echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';

                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->

                echo '</div>';
            } // end if
            ?>


        </form>


        <!-- end page container -->

