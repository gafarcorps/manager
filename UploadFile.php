<?php

class UploadFile
{
    public $filename;
    public $file;
    public $size;
    public $error;

    public function __construct()
    {
    }

    public function uploadedFile(array $file){
        $this->file = $file['file']['tmp_name'];
        $this->filename = $file['file']['name'];
        $this->size = $file['file']['size'];
        $this->error = $file['file']['error'];
        $maxSize = 20000;
        $tabExtension = explode('.', $this->filename);
        $extension = strtolower(end($tabExtension));
         //Tableau des extensions que l'on accepte
        $extensions = ['jpg', 'png', 'jpeg', 'gif'];
        if(in_array($extension, $extensions) && $this->size <= $maxSize){
            move_uploaded_file($this->file, './upload/'.$this->filename);
        }
        else{
            echo "Mauvaise extension ou taille trop grande";
        }
    }
}