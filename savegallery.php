<?php

    require_once("includes/initialize.php");

    if(isset($_POST['submit']))  { // IF ISSET SUBMIT

        $repository = 'media/files/';

        if(!isset($_POST['album_id'])) { // CREATE
            for ($i = 0; $i < count($_FILES['image']['name']); $i++) {
                // move file
                $image = basename($_FILES['image']['name'][$i]);
                move_uploaded_file($_FILES['image']['tmp_name'][$i], $repository . $image);

                // save album
                if ($i == 0) {
                    $gallery_parent = new Gallery();
                    $gallery->gallery_parent_id = null;
                    $gallery_parent->gallery_name = $_POST['album_name'];
                    $gallery_parent->gallery_active = ($_POST['album_active']) ? true : false;
                    $gallery_parent->gallery_image_path = $image;
                    $gallery_parent->save();
                }

                // save gallery
                $gallery = new Gallery();
                $gallery->gallery_parent_id = $gallery_parent->gallery_id;
                $gallery->gallery_name = $_POST['name'][$i];
                $gallery->gallery_desc = $_POST['desc'][$i];
                $gallery->gallery_position = $_POST['position'][$i];
                $gallery->gallery_image_path = $image;
                $gallery->gallery_active = ($_POST['active'][$i]) ? true : false;
                $gallery->save();
                $gallery_parent_id = $gallery_parent->gallery_id;
            }
        }// END CREATE
        else{ // UPDATE
            // DELETE OLD PHOTOS
            $found_photos = Gallery::find_all_photos($_POST['album_id']);
            foreach($found_photos as $found_photo){
                $found_photo->delete();
            }

            // CREATE PHOTOS
            for ($i = 0; $i < count($_POST['name']); $i++) {
                // move file
                if(isset($_FILES['image']['name'][$i]) AND basename($_FILES['image']['name'][$i]) != ''){
                    $image = basename($_FILES['image']['name'][$i]) ;
                    move_uploaded_file($_FILES['image']['tmp_name'][$i], $repository . $image);
                }else{
                    $image = $_POST['old_image'][$i];
                }

                // save album
                if ($i == 0) {
                    $gallery_parent = Gallery::find_by_id($_POST['album_id']);
                    $gallery_parent->gallery_name = $_POST['album_name'];
                    $gallery_parent->gallery_image_path = $image;
                    $gallery_parent->gallery_active = ($_POST['album_active']) ? true : false;
                    $gallery_parent->save();
                    $gallery_parent_id = $gallery_parent->gallery_id;
                }

                // save gallery
                $gallery = new Gallery();
                $gallery->gallery_parent_id = $gallery_parent->gallery_id;
                $gallery->gallery_name = $_POST['name'][$i];
                $gallery->gallery_desc = $_POST['desc'][$i];
                $gallery->gallery_position = $_POST['position'][$i];
                $gallery->gallery_image_path = $image;
                $gallery->gallery_active = ($_POST['active'][$i]) ? true : false;
                $gallery->save();
                $gallery_parent_id = $gallery_parent->gallery_id;
            }

        } // END UPDATE

    } // END IF SUBMIT

    if(isset($_GET['id']) AND $_GET['option'] == 'delete')  { // DELETE
        $gallery = Gallery::find_by_id($_GET['id']);
        $gallery_parent_id = $gallery->gallery_parent_id;
        $gallery->delete();
    } // END DELETE

    redirect_to('app.php?item=photos&id='.$gallery_parent_id.'&msg=success');
?>