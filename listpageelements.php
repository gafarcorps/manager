
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="#">Pages</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Liste des pages</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <?php
                        if(isset($_GET['id']))
                            $found_page = Page::find_by_id($_GET['id']);
                        ?>
                        <header>Liste des Elements de la page <?php echo $found_page->page_title ?></header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row p-b-20">
                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group">
                                    <?php  
                                         echo '<a href="app.php?item=formpageelement&id='.$found_page->page_id.'&option=new" id="addRow" class="btn btn-info"> Nouvel élément <i class="fa fa-plus"></i>';
                                         
                                    ?>
                                    </a>

                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group pull-right">
                                    <?php
                                    if(isset($_GET['id']) && empty($_GET['type'])) {
                                        $pid = $_GET['id'];
                                        echo '<a href="app.php?item=listpages" class="btn deepPink-bgcolor">Retourner à liste précédente';
                                        echo '</a>';
                                    } // enf if
                                    else{
                                         echo '<a href="app.php?item=listsections" class="btn deepPink-bgcolor">Retourner à liste précédente';
                                         echo '</a>';
                                    }
                                    ?>

                                </div>
                            </div>
                        </div> 

                        <?php
                        if(isset($_GET['msg']) && $_GET['msg']=='success') {
                            echo '<span class="clsAvailable"> Enregistrement réussi. </span>';
                        }
                        ?>

                        <div class="table-scrollable">
                            <table class="table table-hover table-checkable order-column full-width"
                                   id="example4">
                                <thead>
                                <tr>


                                    <th class="center"></th>
                                    <th class="center">Titre </th>
                                    <th class="center">Position </th>
                                    <th class="center"> Action </th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php
                                if(isset($_GET['id']) && isset($_GET['type'])) {

                                    //$found_page_elements = Page_element :: find_page_children($pid);
                                    foreach($found_page->page_elements() as $page_element){
                                        echo '<tr class="odd gradeX">';
                                        echo '<td class="center">'.$page_element->page_element_id.'</td>';
                                        echo '<td class="center"><a href="app.php?item=formpageelement&id='.$page_element->page_element_id.'&option=edit&type='.$_GET['type'].'">'.$page_element->page_element_title.'</td>';
                                        echo '<td class="center">'.$page_element->page_element_position.'</td>';
                                        echo '<td class="center">';
                                        echo ' <a href="app.php?item=formpageelement&id='.$page_element->page_element_id.'&option=edit&type='.$_GET['type'].'" class="btn btn-tbl-edit btn-xs">';
                                        echo '<i class="fa fa-pencil"></i>';
                                        echo '</a>';
                                        echo '</td>';
                                        echo '</tr>';
                                    } // end foreach
                                }
                                else {

                                    //$found_page_elements = Page_element :: find_page_children($pid);
                                    foreach($found_page->page_elements() as $page_element){
                                        echo '<tr class="odd gradeX">';
                                        echo '<td class="center">'.$page_element->page_element_id.'</td>';
                                        echo '<td class="center"><a href="app.php?item=formpageelement&id='.$page_element->page_element_id.'&option=edit">'.$page_element->page_element_title.'</td>';
                                        echo '<td class="center">'.$page_element->page_element_position.'</td>';
                                    
                                        echo '<td class="center">';
                                        echo ' <a href="app.php?item=formpageelement&id='.$page_element->page_element_id.'&option=edit" class="btn btn-tbl-edit btn-xs">';
                                        echo '<i class="fa fa-pencil"></i>';
                                        echo '</a>';
                                        echo '</td>';
                                        echo '</tr>';
                                    } // end foreach
                                }
                                ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end page content -->
