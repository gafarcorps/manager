<?php 
	 require_once("includes/initialize.php");	
	 $page_elements 	 = new Page_element();
	 
	if(isset($_POST['submit']))   		{
	
		if(isset($_POST['pelid']))		{ $pelid  	    =	$_POST['pelid'];	    	}
		if(isset($_POST['pid']))     	{ $pid			= 	$_POST['pid'];				}
		if(isset($_POST['title']))		{ $title 		= 	$_POST['title'];			}
		if(isset($_POST['desc1']))		{ $desc1 		= 	$_POST['desc1'];   	        }
		if(isset($_POST['desc2']))		{ $desc2 		= 	$_POST['desc2'];   	        }			
		$date 	= 	date('Y-m-d');				 
		if(isset($_POST['active']))		{ $active		= 	$_POST['active'];			}			
		if(!empty($active))        		{ $page_elements_active = 1; } else {$page_elements_active = 0;}
		if(isset($_POST['position']))	{$position		    =	$_POST['position']; 	}
		
		
			if (empty($_POST['pelid'])){ // NEW ENTRY 
			
				$page_elements->page_elements_pid			= 	$pid;
				$page_elements->page_elements_title_fr 		= 	$title;
				$page_elements->page_elements_desc1_fr 		=	$desc1;
				$page_elements->page_elements_desc2_fr 		=	$desc2;
				$page_elements->page_elements_order  	    = 	$position;
				$page_elements->page_elements_date  	    = 	$date;
				$page_elements->page_elements_active 		= 	$page_elements_active;		
				
				$page_elements->save();
			    $pelpic_id = $page_elements->page_elements_id;
				
			} // END IF NEW ENTRY  			

			else{ // UPDATE ENTRY
		     
			   
				/*
				$found_page_position =  $page_elements->find_page_position($position); 
		    	
				$page_elements->page_elements_id			=   $found_page_position->page_elements_id;
				$page_elements->page_elements_pid			=   $found_page_position->page_elements_pid;
				$page_elements->page_elements_title_fr 		= 	$found_page_position->page_elements_title_fr;
				$page_elements->page_elements_desc1_fr 		=	$found_page_position->page_elements_desc1_fr;
				$page_elements->page_elements_desc2_fr 		=	$found_page_position->page_elements_desc2_fr;
				$page_elements->page_elements_link  	    = 	$found_page_elements->page_elements_link;
				$page_elements->page_elements_order  	    = 	$found_page_elements->page_elements_order;
				$page_elements->page_elements_date  	    = 	$found_page_position->page_elements_date;
				$page_elements->page_elements_active 		= 	$found_page_position->page_elements_active;	
				
			 	 $page_elements->save();	 	
				*/	  	
						
					    $found_page_elements = $page_elements->find_by_id($pelid);	
						
						$page_elements->page_elements_id  = $pelid;
						$pelpic_id = $page_elements->page_elements_id;
						
						if(!empty($page)){ // UPDATE IF THERE ANY POSTED DESC VARIABLE
							$page_elements->page_elements_pid 	 =  $page; 
						}  
						else{ // REPLACE THE ASSET VALUE
							$page_elements->page_elements_pid 	 = $found_page_elements->page_elements_pid;  
						}
						
						 
						
						if(!empty($title)){  // UPDATE IF THERE ANY POSTED TITLE VARIABLE
							$page_elements->page_elements_title_fr  = $title;
							$link   	                            = replace_accents($title); 
							$page_elements->page_elements_link   	= sanitize_url($link); 
						} 
						else{ // REPLACE THE ASSET VALUE
							$link      = replace_accents($found_page_elements->page_elements_title_fr); 
							$page_elements->page_elements_link  	= sanitize_url($link); 
						 
						}
						
						if(!empty($desc1)){ // UPDATE IF THERE ANY POSTED DESC VARIABLE
							$page_elements->page_elements_desc1_fr   =  $desc1; 
						} 
						else{ // REPLACE THE ASSET VALUE
							$page_elements->page_elements_desc1_fr   = $found_page_elements->page_elements_desc1_fr;  
						}
						
						if(!empty($desc2)){ // UPDATE IF THERE ANY POSTED DESC VARIABLE
							$page_elements->page_elements_desc2_fr   =  $desc2; 
						} 
						else{ // REPLACE THE ASSET VALUE
							$page_elements->page_elements_desc2_fr   = $found_page_elements->page_elements_desc2_fr;  
						}
							 
						$page_elements->page_elements_order = $position;
						
						if(!empty($datestart)){ // UPDATE IF THERE ANY POSTED DATE START VARIABLE
							$page_elements->page_elements_datestart = $datestart;
						} 
						else{ // REPLACE THE ASSET VALUE
							$page_elements->page_elements_date = $found_page_elements->page_elements_date;  
						}
 							$page_elements->page_elements_active 	= $page_elements_active; 
						
						
							$page_elements->save();	
						
				}  // END IF UPDATE 	
		
					
				if(!empty($_FILES['image'])) {// UPLOAD PAGE IMAGE				
											$page_element_image 				= new Page_element_image();											
											
											$page_element_image->image_pel_id   = $pelpic_id;
											$page_element_image->image_home  	= 0;
											$max_file_size = 10485760; 
											$page_element_image->attach_file($_FILES['image']);
											$page_element_image->save();									
										 									
				} // END IF  UPLOAD PAGE IMAGE
				 				
				$page = new Page();
				$found_page    	   		= Page :: find_by_id($pid);			
				$page->page_id        	= $pid;
				$page->page_title_fr 	= $found_page->page_title_fr;
				$page->page_title_en 	= $found_page->page_title_en;
				$page->page_desc_fr 	= $found_page->page_desc_fr;  
				$page->page_desc_en 	= $found_page->page_desc_en;
				$link                   = replace_accents($found_page->page_title_fr);
				$page->page_link  		= sanitize_url($link);
				$page->page_active		= $found_page->page_active;
				$page->page_date   	= date('Y-m-d');
				
				$page->save();
			
			 	redirect_to('listpageelements.php?pid='.$page_elements->page_elements_pid.'&msg=success');	
	} // END IF SUBMIT

	if (!empty($_GET['pelid']) && $_GET['option']=='delete'){  // DELETE ENTRIE
											
															$pelid  = $_GET['pelid'];												
															$page_elements = $page_elements->find_by_id($pelid);																
															$del  = $page_elements->delete();
															redirect_to('listpageelements.php?pid='.$page_elements->page_elements_pid.'&msg=del');
	}  // END IF DELETE 
	
	
?>
