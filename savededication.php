<?php

require_once("includes/initialize.php");

$text = $_GET['text'];

$dedication = new Topics();

$dedication->topic_typetopic_id = 66;
$dedication->topic_title = $text;
$dedication->topic_text = 'Dédicace';
$dedication->topic_date_creation = date('Y-m-d');
$dedication->topic_active = 0;
$dedication->save();

redirect_to($_SERVER['HTTP_REFERER']);


?>