<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Liste des utilisateurs</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="#"></a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active"> Liste des utilisateurs</li>
                </ol>
            </div>
        </div>
        <ul class="nav nav-pills nav-pills-rose">
            <li class="nav-item tab-all"><a class="nav-link active show" href="#tab1" data-toggle="tab">Vue Liste</a></li>
            <li class="nav-item tab-all"><a class="nav-link" href="#tab2" data-toggle="tab">Vue Grille</a>
            </li>
        </ul>
        <div class="tab-content tab-space">
            <div class="tab-pane active show" id="tab1">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-box">
                            <div class="card-head">
                                <button id="panel-button"
                                        class="mdl-button mdl-js-button mdl-button--icon pull-right"
                                        data-upgraded=",MaterialButton">
                                    <i class="material-icons"></i>
                                </button>
                                <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                                    data-mdl-for="panel-button">
                                    <li class="mdl-menu__item"><i
                                            class="material-icons"></i>Action</li>
                                    <li class="mdl-menu__item"><i class="material-icons"></i>
                                        action</li>
                                    <li class="mdl-menu__item"><i
                                            class="material-icons"></i></li>
                                </ul>
                            </div>
                            <?php
                            if (isset($_GET['msg']) && $_GET['msg'] == 'success') {
                                echo '<span class="clsAvailable"> Enregistrement réussi. </span>';
                            }
                            ?>
                            <div class="card-body ">
                                <div class="table-scrollable">
                                    <table class="table table-hover table-checkable order-column full-width"
                                           id="example4">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th class="center"> Nom </th>
                                                <th class="center"> Prénom </th>
                                                <th class="center"> Telephone </th>
                                                <th class="center"> Email </th>
                                                <th class="center"> Adresse </th>
                                                <th class="center"> Date de création</th>
                                                <th class="center"> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $user_info = new User_info();
                                            $user_account = new User_account();
                                            $found_users = $user_info->find_all_active();
                                            foreach ($found_users as $found_user) {
                                                echo '>>'.$found_user->user_info_id;
                                                $found_user_account = $user_account->find_by_id($found_user->user_info_user_account_id);
                                                 echo '--'.$found_user_account->user_account_id;
                                                ?>
                                                <tr class="odd gradeX">
                                                    <td class="user-circle-img sorting_1">
                                                        <img src="images/<?php echo $found_user->user_info_image ?>" style="height: 30px" alt="">
                                                    </td>
                                                    <td class="center"><?php echo $found_user->user_info_lastname; ?></td>
                                                    <td class="center"><?php echo $found_user->user_info_firstname; ?></td>
                                                    <td class="center"><a href="tel:<?php echo $found_user->user_info_tel; ?>">
                                                            <?php echo $found_user->user_info_tel; ?> </a></td>
                                                    <td class="center"><a href="<?php echo $found_user_account->user_account_email; ?>">
                                                            <?php echo $found_user_account->user_account_email; ?> </a></td>
                                                    <td class="center"><?php echo $found_user->user_info_address; ?></td>
                                                    <td class="center"><?php echo $found_user->user_info_date_creation; ?></td>
                                                    <td class="center">
                                                        <a href="app.php?item=formusers&option=edit&uid=<?php echo $found_user->user_info_id; ?>"
                                                           class="btn btn-tbl-edit btn-xs">
                                                            <i class="fa fa-pencil"></i>
                                                        </a>
                                                        <!--<a class="btn btn-tbl-delete btn-xs">
                                                                <i class="fa fa-trash-o "></i>
                                                        </a> -->
                                                    </td>
                                                </tr>

                                            <?php } ?>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tab-pane" id="tab2">
                <div class="row">

                    <?php
                    $user_info = new User_info();
                    $user_account = new User_account();
                    $found_users = $user_info->find_all_active();
                    foreach ($found_users as $found_user) {
                        $found_user_account = $user_account->find_by_id($found_user->user_info_user_account_id);
                        ?>
                        <div class="col-md-4">
                            <div class="card">
                                <div class="m-b-20">
                                    <div class="doctor-profile">
                                        <div class="profile-header bg-b-purple">
                                            <div class="user-name"><?php echo $found_user->user_info_lastname; ?></div>
                                            <div class="name-center"><?php echo $found_user->user_info_firstname; ?></div>
                                        </div>
                                        <img src="images/<?php echo $found_user->user_info_image ?>" class="user-img" alt="">
                                        <p>
                                            <?php echo $found_user->user_info_address; ?>
                                        </p>
                                        <div>
                                            <p>
                                                <i class="fa fa-phone"></i><a href="tel:<?php echo $found_user->user_info_tel; ?>">
                                                    <?php echo $found_user->user_info_tel; ?></a>
                                            </p>
                                        </div>
                                        <div class="profile-userbuttons">
                                            <a href="app.php?item=formuser&option=edit&uid=<?php echo $found_user->user_info_id; ?>"
                                               class="btn btn-circle deepPink-bgcolor btn-sm">Mofifier</a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>


                </div>
            </div>
        </div>
    </div>
</div>