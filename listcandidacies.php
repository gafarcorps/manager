<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="#"> Menus </a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Liste des candidatures</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Liste des candidatures</header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row p-b-20">
                            <div class="col-md-6 col-sm-6 col-6">
                                <a href="#" id="delete-all" data-target="savecandidate" class="btn btn-default" style="display: none">
                                    <i class="fa fa-trash"></i> Supprimer les éléments sélectionnés
                                </a>

                                <?php
                                if(isset($_GET['msg']) && $_GET['msg']=='success') {
                                    echo '<span class="clsAvailable"> Effectué avec succès. </span>';
                                }
                                ?>
                            </div>

                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group pull-right">
                                    <?php
                                    if(isset($_GET['mid'])) {
                                        $mid = $_GET['mid'];
                                        echo '<a href="listmenus.php" class="btn deepPink-bgcolor">Retourner à liste précédente';
                                        echo '</a>';
                                    } // enf if
                                    ?>

                                </div>
                            </div>
                        </div>

                        <div class="table-scrollable">
                            <table class="table table-hover table-checkable order-column full-width"
                                   id="example4">
                                <thead>
                                <tr>
                                    <th></th>
                                    <th class="center"> Date </th>
                                    <th class="center" style="width: 50% !important;">Noms & Prénoms</th>
                                    <th class="center"> Email </th>
                                    <th class="center"> Demande </th>
                                    <th class="center"> Lettre de Motivation </th>
                                    <th class="center"> CV </th>
                                    <th class="center"> Actions </th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php
                                    $found_candidacies = Candidate :: find_all();
                                    foreach($found_candidacies as $found_candidacy){
                                        echo '<tr class="odd gradeX">';
                                        echo '<td>
                                                    <label class="rt-chkbox rt-chkbox-single rt-chkbox-outline">
                                                        <input type="checkbox" class="checkboxes" value="'.$found_candidacy->candidate_id.'" />
                                                        <span></span>
                                                    </label>
                                                </td>';
                                        echo '<td class="center">'.$found_candidacy->cv()->file_date.'</td>';
                                        echo '<td class="center">'.$found_candidacy->candidate_fullname.'</td>';
                                        echo '<td class="center">'.$found_candidacy->candidate_email.'</td>';
                                        echo '<td class="center">'.$found_candidacy->candidate_type.'</td>';
                                        echo '<td class="center"><a href="docs/'.$found_candidacy->motivation()->file_path.'" target="_blank">'.$found_candidacy->motivation()->file_path.'</a></td>';
                                        echo '<td class="center"><a href="docs/'.$found_candidacy->cv()->file_path.'" target="_blank">'.$found_candidacy->cv()->file_path.'</a></td>';
                                        echo '<td>';
                                        echo ' <a href=# data-href="savecandidate.php?id='.$found_candidacy->candidate_id.'&option=delete" class="btn btn-tbl-edit btn-xs delete">';
                                        echo '<i class="fa fa-trash"></i>';
                                        echo '</a>';
                                        echo '</td>';
                                        echo '</tr>';
                                    } // end foreach
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

