<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="app.php?item=listservices">Gestionnaire des services</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Editer un service</li>
                </ol>
            </div>
        </div>


        <form action="saveservice.php" class="form-horizontal" method="POST" enctype="multipart/form-data">

            <?php
            if (isset($_GET['option']) && $_GET['option'] == 'new') {
                echo '<div class="row">';
                echo '<div class="col-md-12">';

                //<!-- BEGIN PROFILE CONTENT -->
                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';
                echo '<h4> Créer un service </h4>';
                echo '</div>';
                echo '</div>';
                echo '<div class="white-box col-lg-12" style="padding: 50px">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Nom du service</label>';
                echo '<input name="service_title" type="text" tabindex="1" id="service_title" class="form-control" required>';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="">Description du service</label>';
                echo '<textarea class="form-control" name="service_desc" id="service_desc"></textarea>';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="">Uploader le visuel</label>';
                echo '<input type="file" class="form-control" name="image">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Service actif?</label>';
                echo '&nbsp; <input name="service_active" type="checkbox" tabindex="1" id="service_active">';
                echo '</div>';

                echo '</div>';
                echo '</div>';

                echo '</div>';
                echo '<br>';
                echo '&nbsp;&nbsp;';
                echo '<input type="submit" name="new" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';


                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->
                echo '</div>';
            } //end if
            ?>


            <?php
            if (isset($_GET['option']) && $_GET['option'] == 'edit' && isset($_GET['service_id'])) {

                $service_id = $_GET['service_id'];
                $found_service = Service::find_by_id($_GET['service_id']);
                echo '<input type="hidden" name="service_id" value="' . $found_service->service_id . '">';

                echo '<div class="row">';
                echo '<div class="col-md-12">';
                //<!-- BEGIN PROFILE SIDEBAR -->
                echo '<div class="profile-sidebar">';
                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Attributs </div>';
                echo '<div class="profile-usertitle-job"> du service </div>';
                echo '</div>';
                echo '<div class="card-body no-padding height-9">';

                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b>Etat </b>';
                echo '<div class="profile-desc-item pull-right">';

                if ($found_service->service_deleted == 0) {
                    echo '<i class="fa fa-eye"></i> Activé';
                } else {
                    echo '<span class="label label-danger"><i class="fa fa-eye-slash"></i> Supprimé</span>';
                }


                echo '</div>';
                echo '</li>';


                echo '<li class="list-group-item">';
                if ($found_service->service_deleted == 0) {

                    echo '<b>Déplacer dans la corbeille </b>';
                    echo '<div class="profile-desc-item pull-right"> <a href="saveservice.php?service_id=' . $service_id . '&option=delete"><i class="fa fa-trash-o "></i></a> </div>';
                } else {
                    echo '<b>Restaurer ce service </b>';
                    echo '<div class="profile-desc-item pull-right"> <a href="saveservice.php?service_id=' . $service_id . '&option=recover"><i class="fa  fa-recycle "></i></a> </div>';
                }

                echo '</li>';

                echo '</ul><br/>';



                echo '<div class="controls">';
                echo '<div class="form-group">';
                echo '<div class="checkbox checkbox-icon-black">';
                echo '<br/><b> <a href="app.php?item=disabledservices&list=disabled"><b>Corbeille </b></a></b>';
                echo '</div>';

                //<!-- END SIDEBAR BUTTONS -->
                echo '</div>';
                echo '</div>';
                echo '</div>';

                echo '</div>';
                echo '</div>';


                //<!-- END BEGIN PROFILE SIDEBAR -->
                
                echo '<div class="row">';
                echo '<div class="col-md-12">';

                //<!-- BEGIN PROFILE CONTENT -->
                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';
                echo '<h4> Editer le service </h4>';
                echo '</div>';
                echo '</div>';
                echo '<div class="white-box col-lg-12" style="padding: 50px">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Nom du service</label>';
                echo '<input name="service_title" value="' . $found_service->service_title . '" type="text" tabindex="1" id="subject" class="form-control" required>';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="">Description du service</label>';
                echo '<textarea class="form-control" name="service_desc" id="service_desc">' . $found_service->service_desc . '</textarea>';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="">Uploader le visuel</label>';
                echo '<input type="file" class="form-control" name="image">';
                if (!empty($found_service->service_image_path))
                    echo '<a href="media/files/' . $found_service->service_image_path . '">"<img src="media/files/' . $found_service->service_image_path . '" width="500"/> </a>';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Service actif?</label>';
                $checked = ($found_service->service_active) ? 'checked' : '';
                echo '&nbsp; <input name="service_active" type="checkbox" tabindex="1" id="service_active" ' . $checked . '>';
                echo '</div>';

                echo '</div>';
                echo '</div>';
                ?>


                <?php
                echo '</div>';
                echo '<br>';
                echo '&nbsp;&nbsp;';
                echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';


                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->
                echo '</div>';
            } //end if
            ?>


        </form>


        <!-- end page container -->

        <script src="assets/plugins/jquery/jquery.min.js"></script>

        <script>
            $('#add').click(function () {
                $('#form-bloc').append($('#another-form').html())
            })
        </script>
