<?php

require_once("includes/initialize.php");
$topic = new Topics();
 
if (isset($_POST['submit'])) {
 
    if (isset($_POST['topicid'])) {
        $topicid = $_POST['topicid'];
    }
    if (isset($_POST['typetopic'])) {
        $typetopic = $_POST['typetopic'];
    }
    if (isset($_POST['title'])) {
        $topic_title = $_POST['title'];
    }

    if (isset($_POST['topic_text'])) {
        $topic_text = $_POST['topic_text'];
    }

    if (isset($_POST['type_topic'])) {
        $type_topic = $_POST['type_topic'];
    }

    if (isset($_POST['icon'])) {
        $topic_icon = $_POST['icon'];
    }

    if (isset($_POST['date_creation'])) {
        $topic_date_creation = $_POST['date_creation'];
    }else{
        $topic_date_creation = date('Y-m-d H:i:s');
    }


    if (!empty($_FILES['file_upload']['name'])) {
        $file_upload = basename($_FILES['file_upload']['name']);
    }

    if (isset($_POST['urlr'])) {
        $urlr = $_POST['urlr'];
    }

    $url = cleanInput($topic_title);
    $url = sanitize_url($url);
    $url = replace_accents($url);

    $repository = 'media/files/';

    $topic_active = ($_POST['active']) ? true : false;

    if (!empty($_POST['breakingnew'])) {
        $breakingnew = $_POST['breakingnew'];
        $breakingnew = 1;
    } else {
        $breakingnew = 0;
    } 

    if (!empty($_POST['breakingnew1'])) {
        $breakingnew1 = $_POST['breakingnew1'];
        $breakingnew1 = 1;
    } else {
        $breakingnew1 = 0;
    } 

    
    if (!empty($_POST['breakingnew2'])) {
        $breakingnew2 = $_POST['breakingnew2'];
        $breakingnew2 = 1;
    } else {
        $breakingnew2 = 0;
    } 

   
    if (empty($_POST['topicid'])) 
    { // NEW ENTRY 
        $topic->topic_typetopic_id = $typetopic;
        $topic->topic_author = $session->user_account_id;
        // $topic->topic_image_path = $file_upload.',';
        $topic->topic_title = $topic_title;
        $topic->topic_text = $topic_text;
        $topic->topic_icon = $topic_icon;


        if (isset($_FILES['file_upload'])) {
            move_uploaded_file($_FILES['file_upload']['tmp_name'], $repository . $file_upload);
            //$topic->topic_image_path = $file_upload;
        }

        $topic->topic_url = $url;
        $topic->topic_date_creation = date('Y-m-d H:i:s');
        $topic->topic_date_last_update = date('Y-m-d H:i:s');
        $topic->topic_top_slider = $breakingnew;
        $topic->topic_top_small = $breakingnew1;
        $topic->topic_views=0;
        $topic->topic_statut = $breakingnew2;
        $topic->topic_active = $topic_active;

        //START  SAVE GALERY
        $errors = array();
        $extension = array("jpeg","jpg","png","gif");
        $bytes = 1024;
        $allowedKB = 1000000;
        $totalBytes = $allowedKB * $bytes;
         
        if(isset($_FILES["files"])==false)
        {
            // echo "<b>Please, Select the files to upload!!!</b>";
            $topic->topic_image_path = $file_upload;
            $topic->save();
            redirect_to('app.php?item=listtopics&msg=success');
        }

        foreach($_FILES["files"]["tmp_name"] as $key=>$tmp_name)
        {
              $uploadThisFile = true;
                 
                $file_name=$_FILES["files"]["name"][$key];
                $file_tmp=$_FILES["files"]["tmp_name"][$key];
                 
                $ext=pathinfo($file_name,PATHINFO_EXTENSION);
     
                if(!in_array(strtolower($ext),$extension))
                {
                    array_push($errors, "File type is invalid. Name:- ".$file_name);
                    $uploadThisFile = false;
                    $topic->topic_image_path = $file_upload;
                    $topic->save();
                    redirect_to('app.php?item=listtopics&msg=success');
                }               
                 
                if($_FILES["files"]["size"][$key] > $totalBytes){
                    array_push($errors, "File size must be less than 100KB. Name:- ".$file_name);
                    $uploadThisFile = false;
                    $topic->topic_image_path = $file_upload;
                    $topic->save();
                    redirect_to('app.php?item=listtopics&msg=success');
                }
               
                if($uploadThisFile){
                    $filename=basename($file_name,$ext);
                    $newFileName=$filename.$ext;                
                    move_uploaded_file($_FILES["files"]["tmp_name"][$key],"media/files/".$newFileName);
                    if (!empty($_FILES['file_upload']['name'])) 
                    {
                    $galerie = $file_name;
                    $topic->topic_image_path = $file_upload.','.$galerie;
                    }
                     $topic->save();
                }
                
            }
            redirect_to('app.php?item=listtopics&msg=success');
            $count = count($errors);
            if($count != 0){
                foreach($errors as $error){
                    echo $error."<br/>";
                }
            }
            //END GALERY

        if(!empty($_POST['section_id'][0])){
                    foreach($_POST['section_id'] as $section_id){ 
                         
                        $topic_section->topic_section_pid = $topic->topic_id;
                        $topic_section->topic_section_sid = $section_id;
                        $topic_section->save();
                    }
                }
    } // END IF NEW ENTRY  
   
    else { // UPDATE ENTRY

        if(!empty($_POST['section_id'][0])){

                    foreach($_POST['section_id'] as $section_id){

                        $found_topic_section = $topic_section->find_by_ids($topicid, $section_id);
                        //echo $found_page_section->page_section_id.'<br/>'; 
                        if(empty($found_topic_section)){ 
                            $topic_section->topic_section_id  = null;
                            $topic_section->topic_section_pid = $topicid;
                            $topic_section->topic_section_sid = $section_id;
                            $topic_section->save();
                            }// end if
                            else{
                                $topic_section->topic_section_id  = $found_topic_section->topic_section_id; 
                                $topic_section->topic_section_pid = $topicid;
                                $topic_section->topic_section_sid = $section_id;
                                $topic_section->save();
                                }// end else                            
                            
                        
                    } // end foreach
                }// end if
        
        $found_topic = $topic->find_by_id($topicid);
        $topic->topic_id = $topicid;
        $topic->topic_typetopic_id = $typetopic;
        $topic->topic_author = $found_topic->topic_author;

        if (!empty($topic_title)) {  // UPDATE IF THERE ANY POSTED TITLE VARIABLE
            $topic->topic_title = $topic_title;
        } else { // REPLACE THE ASSET VALUE
            $topic->topic_title = $found_topic->topic_title;
        }

        if (!empty($topic_text)) { // UPDATE IF THERE ANY POSTED TEXT VARIABLE
            $topic->topic_text = $topic_text;
        } else { // REPLACE THE ASSET VALUE
            $topic->topic_text = $found_topic->topic_text;
        }

        if (isset($file_upload)) {
            move_uploaded_file($_FILES['file_upload']['tmp_name'], $repository . $file_upload);
        }

        if (!empty($topic_title)) { // UPDATE IF THERE ANY POSTED TEXT VARIABLE
            $topic->topic_url = $url;
        } else { // REPLACE THE ASSET VALUE
            $topic->topic_url = $found_topic->topic_url;
        }

        if (!empty($topic_icon)) { // UPDATE IF THERE ANY POSTED TEXT VARIABLE
            $topic->topic_icon = $topic_icon;
        } else { // REPLACE THE ASSET VALUE
            $topic->topic_icon = $found_topic->topic_icon;
        }

        if (!empty($topic_date_creation)) { // UPDATE IF THERE ANY POSTED TEXT VARIABLE
            $topic->topic_date_creation = $topic_date_creation;
        } else { // REPLACE THE ASSET VALUE
            $topic->topic_date_creation = $found_topic->topic_date_creation;
        }
        if (!empty($found_topic->topic_image_path)) { // UPDATE IF THERE ANY POSTED TEXT VARIABLE
            $topic->topic_image_path = $found_topic->topic_image_path;
        } 

        $topic->topic_date_creation = $topic->topic_date_creation;
        $topic->topic_date_last_update = date('Y-m-d H:i:s');
        $topic->topic_top_slider = $breakingnew;
        $topic->topic_top_small = $breakingnew1;
        $topic->topic_statut = $breakingnew2;
        $topic->topic_active = $topic_active;

        //START SAVE GALERY
        $errors = array();
        $extension = array("jpeg","jpg","png","gif");
        $bytes = 1024;
        $allowedKB = 1000000;
        $totalBytes = $allowedKB * $bytes;
         
        if(isset($_FILES["files"])==false)
        {
            if (!empty($_FILES['file_upload']['name'])) 
            {
              $file_upload = basename($_FILES['file_upload']['name']);
              $galeries= explode(",", $found_topic->topic_image_path);
              unset($galeries[0]);
              $string = implode(",",$galeries);  
              $topic->topic_image_path = $file_upload.','.$string;
              }
            $topic->save();
            redirect_to($_SERVER['HTTP_REFERER']);
        }

        foreach($_FILES["files"]["tmp_name"] as $key=>$tmp_name)
        {
            $uploadThisFile = true;
                 
                $file_name=$_FILES["files"]["name"][$key];
                $file_tmp=$_FILES["files"]["tmp_name"][$key];
                 
                $ext=pathinfo($file_name,PATHINFO_EXTENSION);
     
                if(!in_array(strtolower($ext),$extension))
                {
                    array_push($errors, "File type is invalid. Name:- ".$file_name);
                    $uploadThisFile = false;

                    if (!empty($_FILES['file_upload']['name'])) 
                    {
                        $file_upload = basename($_FILES['file_upload']['name']);
                        $galeries= explode(",", $found_topic->topic_image_path);
                        unset($galeries[0]);
                        $string = implode(",",$galeries);  
                        $topic->topic_image_path = $file_upload.','.$string;
                      }
                    $topic->save();
                    redirect_to($_SERVER['HTTP_REFERER']);
                }               
                 
                if($_FILES["files"]["size"][$key] > $totalBytes){
                    array_push($errors, "File size must be less than 100KB. Name:- ".$file_name);
                    $uploadThisFile = false;

                    if (!empty($_FILES['file_upload']['name'])) 
                    {
                        $file_upload = basename($_FILES['file_upload']['name']);
                        $galeries= explode(",", $found_topic->topic_image_path);
                        unset($galeries[0]);
                        $string = implode(",",$galeries);  
                        $topic->topic_image_path = $file_upload.','.$string;
                      }
                    $topic->save();
                    redirect_to($_SERVER['HTTP_REFERER']);
                }
                
                if($uploadThisFile){
                    $filename=basename($file_name,$ext);
                    $newFileName=$filename.$ext;                
                    move_uploaded_file($_FILES["files"]["tmp_name"][$key],"media/files/".$newFileName);
                  
                    $found_topic = $topic->find_by_id($topicid);

                    $galerie = $file_name;
                    $topic->topic_image_path = $found_topic->topic_image_path.','.$galerie;
            
                     $topic->save();
                   
                }
               
            }
            $count = count($errors);
            if($count != 0){
                foreach($errors as $error){
                    echo $error."<br/>";
                }
            } 
            //END GALERY
       
            redirect_to($_SERVER['HTTP_REFERER']);

    } // END IF UPDATE  

        // SAVE TAG 
        if (isset($_POST['etiquette'])) {
            foreach ($_POST['etiquette'] as $tag) {

                $press_tag->press_tag_topic_id = $topic->topic_id;
                $press_tag->press_tag_etiquette_id = $tag;
               $press_tag->create(); 
            } // END FOREACH 
        } // END if 

        redirect_to('app.php?item=listtopics&msg=success');
     
} // END IF SUBMIT


if (!empty($_GET['pid']) && $_GET['option'] == 'delete') {  // DELETE ENTRIE
    $topicid = $_GET['pid'];
    $found_topic = $topic->find_by_id($topicid);
    $del = $found_topic->unable();
    redirect_to('app.php?item=listtopics&option=del');
    }  // END IF DELETE
  
    if (!empty($_GET['pid'])  && !empty($session->user_account_id) && $_GET['option'] == 'recover') {  // DELETE ENTRIE
  
      $topicid = $_GET['pid'];
      $found_topic = $topic->find_by_id($topicid);
      $del        = $found_topic->recover();
      redirect_to('app.php?item=listtopics&option=recover');
  }  // END IF DELETE 

  if (!empty($_GET['pid']) && !empty($_GET['indice']) && $_GET['option'] == 'deleteimage') {  // DELETE ENTRIE
     $topicid = $_GET['pid'];
     $found_topic = $topic->find_by_id($topicid);
     $galeries= explode(",", $found_topic->topic_image_path);
     $indice = $_GET['indice'];

    unset($galeries[$indice]);
    
    $string = implode(",",$galeries);  

     $del  = $found_topic->deleteImage($string);

     header('Location: ' . $_SERVER['HTTP_REFERER']);
    //redirect_to('app.php?item=listtopics&option=delimage');
    }  // END IF DELETE
  
 
?>