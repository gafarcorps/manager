
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="appex.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="appex.php?item=listapp_requests">Gestionnaire des demandes d'applications</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Editer une demande d'application</li>
                </ol>
            </div>
        </div>


        <form action="saveapp_request.php" class="form-horizontal" method="POST" enctype="multipart/form-data">

            <?php
            if (isset($_GET['option']) && $_GET['option'] == 'new') {
                echo '<div class="row">';
                echo '<div class="col-md-12">';

                //<!-- BEGIN PROFILE CONTENT -->
                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';
                echo '<h4> Créer une demande d\'application : <strong>' . $found_customer->user_info_firstname . ' ' . $found_customer->user_info_lastname . '</strong></h4>';
                echo '</div>';
                echo '</div>';
                echo '<div class="white-box col-lg-12" style="padding: 50px">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';

                echo '<div class="col-lg-6 p-t-20">';
                echo '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">';
                echo '<label for="sample2" class="mdl-textfield__label">Client</label>';
                echo '<select class="mdl-textfield__input" name="app_request_customer_id">';

                $user_info = new User_info();
                $user_account = new User_account();
                $found_user_account_online = User_account::find_by_id($session->user_account_id);
                $found_users = $user_info->find_all_active();
                foreach ($found_users as $found_user) {
                    $found_user_account = $user_account->find_by_id($found_user->user_info_user_account_id);
                    if ($found_user_account->user_account_typeuser_id == 3) {

                        echo '<option class="mdl-menu__item" value="' . $found_user->user_info_id . '">' . $found_user->full_name() . '</option>';
                    }
                }

                echo '</select>';
                echo '</div>';
                echo '</div>';

                echo '<div class="col-lg-6 p-t-20">';
                echo '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">';
                echo '<label for="sample2" class="mdl-textfield__label">Menu</label>';
                echo '<select class="mdl-textfield__input" name="app_request_menu_id">';

                $menu = new Menu();
                $found_menus = $menu->list_extensions_active();
                foreach ($found_menus as $found_menu) {

                    echo '<option class="mdl-menu__item" value="' . $found_menu->menu_id . '">' . $found_menu->menu_title . '</option>';
                }

                echo '</select>';
                echo '</div>';
                echo '</div>';

                echo '<div class="col-lg-6 p-t-20">';
                echo '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">';
                echo '<label for="sample2" class="mdl-textfield__label">Image</label>';
                echo '<div class="form-group">';
                echo '<input type="file" class="form-control" name="app_request_image" >';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                echo '</div>';
                echo '</div>';

                echo '</div>';
                echo '<br>';
                echo '&nbsp;&nbsp;';
                echo '<input type="submit" name="new" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';


                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->
                echo '</div>';
            } //end if
            ?>


            <?php
            if (isset($_GET['option']) && $_GET['option'] == 'edit' && isset($_GET['app_request_id'])) {

                $app_request_id = $_GET['app_request_id'];
                $found_app_request = App_request::find_by_id($_GET['app_request_id']);
                echo '<input type="hidden" name="app_request_id" value="' . $found_app_request->app_request_id . '">';

                echo '<div class="row">';
                echo '<div class="col-md-12">';

                //<!-- BEGIN PROFILE CONTENT -->
                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';
                echo '<h4> Créer une demande d\'application : <strong>' . $found_customer->user_info_firstname . ' ' . $found_customer->user_info_lastname . '</strong></h4>';
                echo '</div>';
                echo '</div>';
                echo '<div class="white-box col-lg-12" style="padding: 50px">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';

                echo '<div class="col-lg-6 p-t-20">';
                echo '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">';
                echo '<label for="sample2" class="mdl-textfield__label">Client</label>';
                echo '<select class="mdl-textfield__input" name="app_request_customer_id">';
                $found_user = User_info::find_by_id($found_app_request->app_request_customer_id);
                echo '<option class="mdl-menu__item" value="' . $found_app_request->app_request_customer_id . '">' . $found_user->full_name() . '</option>';
                $user_info = new User_info();
                $user_account = new User_account();
                $found_user_account_online = User_account::find_by_id($session->user_account_id);
                $found_users = $user_info->find_all_active();
                foreach ($found_users as $found_user) {
                    $found_user_account = $user_account->find_by_id($found_user->user_info_user_account_id);
                    if ($found_user_account->user_account_typeuser_id == 3) {

                        echo '<option class="mdl-menu__item" value="' . $found_user->user_info_id . '">' . $found_user->full_name() . '</option>';
                    }
                }

                echo '</select>';
                echo '</div>';
                echo '</div>';

                echo '<div class="col-lg-6 p-t-20">';
                echo '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">';
                echo '<label for="sample2" class="mdl-textfield__label">Menu</label>';
                echo '<select class="mdl-textfield__input" name="app_request_menu_id">';
                $found_menu = Menu::find_by_id($found_app_request->app_request_menu_id);
                echo '<option class="mdl-menu__item" value="' . $found_app_request->app_request_menu_id . '">' . $found_menu->menu_title . '</option>';

                $menu = new Menu();
                $found_menus = $menu->list_extensions_active();
                foreach ($found_menus as $found_menu) {

                    echo '<option class="mdl-menu__item" value="' . $found_menu->menu_id . '">' . $found_menu->menu_title . '</option>';
                }

                echo '</select>';
                echo '</div>';
                echo '</div>';

                echo '<div class="col-lg-6 p-t-20">';
                echo '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">';
                echo '<label for="sample2" class="mdl-textfield__label">Actif ou non?</label>';
                echo '<select class="mdl-textfield__input" name="app_request_active">';
                if ($found_app_request->app_request_active == 1) {
                    echo '<option class="mdl-menu__item" value="1">Oui</option>';
                    echo '<option class="mdl-menu__item" value="0">Non</option>';
                } else {
                    echo '<option class="mdl-menu__item" value="0">Non</option>';
                    echo '<option class="mdl-menu__item" value="1">Oui</option>';
                }

                echo '</select>';
                echo '</div>';
                echo '</div>';

                echo '<div class="col-lg-6 p-t-20">';
                echo '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">';
                echo '<label for="sample2" class="mdl-textfield__label">Image</label>';
                echo '<div class="form-group">';
                echo '<input type="file" class="form-control" name="app_request_image" >';
                echo '<img src="../../images/app_requests/' . $found_app_request->app_request_image . '" width="200"/>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                echo '</div>';
                echo '</div>';

                echo '</div>';
                echo '<br>';
                echo '&nbsp;&nbsp;';
                echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';


                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->
                echo '</div>';
            } //end if
            ?>


        </form>


        <!-- end page container -->

        <script src="assets/plugins/jquery/jquery.min.js"></script>

        <script>
            $('#add').click(function () {
                $('#form-bloc').append($('#another-form').html())
            })
        </script>
