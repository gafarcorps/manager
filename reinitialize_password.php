<!DOCTYPE html>
<html>


    <!-- Mirrored from radixtouch.in/templates/admin/hotel/source/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Jul 2020 17:46:12 GMT -->
    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta name="description" content="Responsive Admin Template" />
        <meta name="author" content="SmartUniversity" />
        <title>Manager | Réinitialiser mon mot de passe</title>
        <!-- icons -->
        <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="assets/plugins/iconic/css/material-design-iconic-font.min.css">
        <!-- bootstrap -->
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- style -->
        <link rel="stylesheet" href="assets/css/pages/extra_pages.css">
        <!-- favicon -->
        <link rel="shortcut icon" href="assets/img/favicon.ico" />
    </head>

    <body>
        <div class="limiter">
            <div class="container-login100 page-background">
                <div class="wrap-login100">
                    <form  action="savereinitialize_password.php?user_account_id=<?php echo $_GET['user_account_id'] ?>" method="post"  class="login100-form validate-form">
                        <span class="login100-form-logo">
                            <i class="zmdi zmdi-flower"></i>
                        </span>
                        <span class="login100-form-title p-b-34 p-t-27">
                            R&eacute;initialiser mon mot de passe
                        </span>
                         <?php
                        if (!empty($_GET['msg']) && $_GET['msg'] == 'error') {
                            echo '<h4 style="color: red">D&eacutesol&eacute, mauvaise confirmation de mot de passe. Veuillez reprendre svp!</h4>';
                        } 
                        ?>
                        <div class="wrap-input100 validate-input" data-validate="Nouveau mot de passe">
                            <input class="input100" type="password" name="password" placeholder="Nouveau mot de passe">
                            <span class="focus-input100" data-placeholder="&#xf191;"></span>
                        </div>
                        <div class="wrap-input100 validate-input" data-validate="Confirmation mot de passe">
                            <input class="input100" type="password" name="confirm_password" placeholder="Confirmation mot de passe">
                            <span class="focus-input100" data-placeholder="&#xf191;"></span>
                        </div>
                       
                        <div class="container-login100-form-btn">
                            <button class="login100-form-btn">
                                Validez
                            </button>
                        </div>
                        
                    </form>
                </div>
            </div>
        </div>
        <!-- start js include path -->
        <script src="assets/plugins/jquery/jquery.min.js"></script>
        <!-- bootstrap -->
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/pages/extra_pages/login.js"></script>
        <!-- end js include path -->
    </body>


    <!-- Mirrored from radixtouch.in/templates/admin/hotel/source/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 05 Jul 2020 17:46:12 GMT -->
</html>