<?php 
	
    require_once("includes/initialize.php");	
	$gallery = new gallery();
	$gallery1 = new gallery();
    if(isset($_POST['submit']))	{	
					
		if(isset($_POST['galleryid_old']))		{$gallery_id_old      	=	$_POST['galleryid_old']; 	} 
		if(isset($_POST['galleryid_new']))		{$gallery_id_new      	=	$_POST['galleryid_new']; 	} //$galleryid is the id of current gallery
		if(isset($_POST['position']))			{$position		   		=	$_POST['position']; 	}
		
		$found_position_old = $gallery->find_by_id($gallery_id_old);
		$found_position_new = $gallery1->find_by_id($gallery_id_new);
		$gallery->gallery_id = $gallery_id_old;
		$gallery->gallery_photoalbum_id		= $found_position_old->gallery_photoalbum_id;
		$gallery->gallery_img_path 			= $found_position_old->gallery_img_path;
		$gallery->gallery_img_type 			= $found_position_old->gallery_img_type;
		$gallery->gallery_img_size 			= $found_position_old->gallery_img_size;
		$gallery->gallery_img_caption 		= $found_position_old->gallery_img_caption;
		$gallery->gallery_img_comment 		= $found_position_old->gallery_img_comment;
		$gallery->gallery_img_position 		= $found_position_new->gallery_img_position;
		$gallery->gallery_img_date 			= $found_position_old->gallery_img_date;
		
		$gallery1->gallery_id = $gallery_id_new;
		$gallery1->gallery_photoalbum_id 	= $found_position_new->gallery_photoalbum_id;
		$gallery1->gallery_img_path 		= $found_position_new->gallery_img_path;
		$gallery1->gallery_img_type 		= $found_position_new->gallery_img_type;
		$gallery1->gallery_img_size 		= $found_position_new->gallery_img_size;
		$gallery1->gallery_img_caption 		= $found_position_new->gallery_img_caption;
		$gallery1->gallery_img_comment 		= $found_position_new->gallery_img_comment;
		$gallery1->gallery_img_position 	= $position;
		$gallery1->gallery_img_date 		= $found_position_new->gallery_img_date;	
		
		
		$gallery->update();
		$gallery1->update();
		
		redirect_to('listgallery.php?albumid='.$found_position_old->gallery_photoalbum_id);
			 
	}
?>