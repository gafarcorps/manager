<?php

require_once("includes/initialize.php");


if (isset($_POST['service_id'])) {
    $service_id = $_POST['service_id'];
} //$service_id is the id of current service

if (isset($_POST['service_title'])) {
    $service_title = $_POST['service_title'];
}
if (isset($_POST['service_desc'])) {
    $service_desc = $_POST['service_desc'];
}

if (isset($_POST['service_active'])) {
    $service_active = $_POST['service_active'];
}

if (!empty($_FILES['image']['name'])) {
    $repository = 'media/files/';
    $image = basename($_FILES['image']['name']);
}

if (!empty($service_active)) {
    $service_active = 1;
} else {
    $service_active = 0;
}

if (isset($_POST['new']) && !empty($session->user_account_id)) { // New service menu 
    $service->service_title = $service_title;
    $service->service_desc = $service_desc;
    if (!empty($_FILES['image'])) {
        move_uploaded_file($_FILES['image']['tmp_name'], $repository . $image);
        $service->service_image_path = $image;
    }
    $service->service_date_creation = date('Y-m-d');
    ;
    $service->service_date_last_modification = date('Y-m-d');
    ;
    $service->service_user_creation = $session->user_account_id;
    $service->service_user_last_modification = $session->user_account_id;
    $service->service_deleted = 0;
    $service->service_active = $service_active;



    $service->save();

    redirect_to('app.php?item=listservices&msg=success');
} // END IF NEW ENTRY  


if (isset($_POST['submit']) && !empty($service_id) && !empty($session->user_account_id)) { // Updating a service			   	
    $found_service = $service->find_by_id($service_id);

    $service->service_id = $found_service->service_id;

    $service->service_title = $service_title;

    $service->service_desc = $service_desc;

    if (!empty($image)) {
        move_uploaded_file($_FILES['image']['tmp_name'], $repository . $image);
        $service->service_image_path = $image;
    } else {
        $service->service_image_path = $found_service->service_image_path;
    }

    $service->service_date_creation = $found_service->service_date_creation;
    $service->service_date_last_modification = date('Y-m-d');
    ;
    $service->service_user_creation = $found_service->service_date_creation;
    $service->service_user_last_modification = $session->user_account_id;
    $service->service_deleted = 0;
    $service->service_active = $service_active;

    $service->save();

    redirect_to('app.php?item=listservices&msg=success');
}  // END IF UPDATE 

if (!empty($_GET['service_id']) && !empty($session->user_account_id) && $_GET['option'] == 'delete') {  // UNABLE ENTRIE
    $service_id = $_GET['service_id'];
    $service = $service->find_by_id($service_id);
    $del = $service->unable();
    redirect_to('app.php?item=formservice&service_id=' . $service_id . '&option=edit&r=0');
}  // END IF UNABLE 

if (!empty($_GET['service_id']) && !empty($session->user_account_id) && $_GET['option'] == 'recover') {  // DELETE ENTRIE
    $service_id = $_GET['service_id'];
    $service = $service->find_by_id($service_id);
    $del = $service->recover();
    redirect_to('app.php?item=formservice&service_id=' . $service_id . '&option=edit&r=1');
}  // END IF DELETE 

if (!empty($_GET['service_id']) && !empty($session->user_account_id) && $_GET['option'] == 'delete2') {  // DELETE ENTRIE
    $service_id = $_GET['service_id'];
    $service = $service->find_by_id($service_id);
    $del = $service->delete();
    redirect_to('app.php?item=formservice&service_id=' . $service_id . '&option=edit&r=3');
}  // END IF DELETE 
?>
 