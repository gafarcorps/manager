
<?php

require_once("includes/initialize.php");

$customer_id = (!empty($_GET['customer_id'])) ? $_GET['customer_id'] : null;
if (!empty($customer_id))
    $found_customer = User_info::find_by_id($customer_id);

if (isset($_POST['ticket_id'])) {
    $ticket_id = $_POST['ticket_id'];
} //$ticket_id is the id of current ticket

if (isset($_POST['ticket_customer_id'])) {
    $ticket_customer_id = $_POST['ticket_customer_id'];
}
if (isset($_POST['ticket_service_id'])) {
    $ticket_service_id = $_POST['ticket_service_id'];
}
if (isset($_POST['ticket_title'])) {
    $ticket_title = $_POST['ticket_title'];
}
if (isset($_POST['ticket_desc'])) {
    $ticket_desc = $_POST['ticket_desc'];
}
if (isset($_POST['ticket_date'])) {
    $ticket_date = $_POST['ticket_date'];
}
if (isset($_POST['ticket_datestart'])) {
    $ticket_datestart = $_POST['ticket_datestart'];
}
if (isset($_POST['ticket_dateend'])) {
    $ticket_dateend = $_POST['ticket_dateend'];
}

if (isset($_POST['ticket_active'])) {
    $ticket_active = $_POST['ticket_active'];
} else
    $ticket_active = (date("Y-m-d") <= $ticket_dateend) ? 1 : 0;

if (!empty($_FILES['image']['name'])) {
    $repository = 'media/files/';
    $file_upload = basename($_FILES['image']['name']);
}

if (!empty($ticket_active)) {
    $ticket_active = 1;
} else {
    $ticket_active = 0;
}

if (isset($_POST['new']) && !empty($session->user_account_id)) { // New ticket menu 
    $ticket->ticket_customer_id = $ticket_customer_id;
    $ticket->ticket_service_id = $ticket_service_id;
    $ticket->ticket_title = $ticket_title;
    $ticket->ticket_desc = $ticket_desc;
    $ticket->ticket_date = $ticket_date;
    $ticket->ticket_datestart = $ticket_datestart;
    $ticket->ticket_dateend = $ticket_dateend;
    if (!empty($_FILES['image'])) {
        move_uploaded_file($_FILES['image']['tmp_name'], $repository . $file_upload);
        $ticket->ticket_file_path = $file_upload;
    }
    $ticket->ticket_date_creation = date('Y-m-d');

    $ticket->ticket_date_last_modification = date('Y-m-d');

    $ticket->ticket_user_creation = $session->user_account_id;
    $ticket->ticket_user_last_modification = $session->user_account_id;
    $ticket->ticket_deleted = 0;
    $ticket->ticket_active = $ticket_active;

    $ticket->save();

    redirect_to('app.php?item=listtickets&customer_id=' . $customer_id . '&msg=success');
} // END IF NEW ENTRY  


if (isset($_POST['submit']) && !empty($ticket_id) && !empty($session->user_account_id)) { // Updating a ticket			   	
    $found_ticket = $ticket->find_by_id($ticket_id);

    $ticket->ticket_id = $found_ticket->ticket_id;
    $ticket->ticket_customer_id = $ticket_customer_id;
    $ticket->ticket_service_id = $ticket_service_id;
    $ticket->ticket_date = $ticket_date;
    $ticket->ticket_datestart = $ticket_datestart;
    $ticket->ticket_dateend = $ticket_dateend;
    if (!empty($ticket_title)) {  // UPDATE IF THERE ANY POSTED TITLE VARIABLE
        $ticket->ticket_title = $ticket_title;
    } else { // REPLACE THE ASSET VALUE
        $ticket->ticket_title = $found_ticket->ticket_title;
    }

    if (!empty($ticket_desc)) { // UPDATE IF THERE ANY POSTED DESC VARIABLE
        $ticket->ticket_desc = $ticket_desc;
    } else { // REPLACE THE ASSET VALUE
        $ticket->ticket_desc = $found_ticket->ticket_desc;
    }

    if (!empty($file_upload)) {
        move_uploaded_file($_FILES['image']['tmp_name'], $repository . $file_upload);
        $ticket->ticket_file_path = $file_upload;
    } else {
        $ticket->ticket_file_path = $found_ticket->ticket_file_path;
    }

    $ticket->ticket_date_creation = $found_ticket->ticket_date_creation;
    $ticket->ticket_date_last_modification = date('Y-m-d');

    $ticket->ticket_user_creation = $found_ticket->ticket_date_creation;
    $ticket->ticket_user_last_modification = $session->user_account_id;
    $ticket->ticket_deleted = 0;
    $ticket->ticket_active = $ticket_active;

    $ticket->save();

    redirect_to('app.php?item=listtickets&customer_id=' . $customer_id . '&msg=success');
}  // END IF UPDATE 

if (!empty($_GET['ticket_id']) && !empty($session->user_account_id) && $_GET['option'] == 'delete') {  // UNABLE ENTRIE
    $ticket_id = $_GET['ticket_id'];
    $ticket = $ticket->find_by_id($ticket_id);
    $del = $ticket->unable();
    redirect_to('app.php?item=formticket&ticket_id=' . $ticket_id . '&customer_id=' . $customer_id . '&option=edit&r=0');
}  // END IF UNABLE 

if (!empty($_GET['ticket_id']) && !empty($session->user_account_id) && $_GET['option'] == 'recover') {  // DELETE ENTRIE
    $ticket_id = $_GET['ticket_id'];
    $ticket = $ticket->find_by_id($ticket_id);
    $del = $ticket->recover();
    redirect_to('app.php?item=formticket&ticket_id=' . $ticket_id . '&customer_id=' . $customer_id . '&option=edit&r=1');
}  // END IF DELETE 

if (!empty($_GET['ticket_id']) && !empty($session->user_account_id) && $_GET['option'] == 'delete2') {  // DELETE ENTRIE
    $ticket_id = $_GET['ticket_id'];
    $ticket = $ticket->find_by_id($ticket_id);
    $del = $ticket->delete();
    redirect_to('app.php?item=formticket&ticket_id=' . $ticket_id . '&customer_id=' . $customer_id . '&option=edit&r=3');
}  // END IF DELETE 
?>
 