<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <?php
                if (isset($_GET['mid'])) {
                    $mid = $_GET['mid'];
                    $found_menu = Menu:: find_by_id($mid);
                }
                ?>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="app.php?item=home">Accueil</a>&nbsp;<i
                                                           class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="app.php?item=listmenus<?php echo ($found_menu->menu_parent_id != 11) ? "&mid=" . $found_menu->menu_parent_id : "" ?>"> Menus </a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Liste des menus</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">

                        <header>Liste des menus <?Php echo (isset($_GET['mid'])) ? "<strong>(Catégorie: " . $found_menu->menu_title . ") </strong>" : "" ?></header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row p-b-20">
                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group">
                                    <a href="app.php?item=formmenu&mid=11&option=new" id="addRow" class="btn btn-info">
                                        Nouveau menu <i class="fa fa-plus"></i>
                                    </a>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group pull-right">
                                    <?php
                                    if (isset($_GET['mid'])) {
                                        $mid = $_GET['mid'];
                                        echo '<a href="listmenus.php" class="btn deepPink-bgcolor">Retourner à liste précédente';
                                        echo '</a>';
                                    } // enf if
                                    ?>

                                </div>
                            </div>
                        </div>
                        <?php
                        if (isset($_GET['msg']) && $_GET['msg'] == 'success') {
                            echo '<span class="clsAvailable"> Enregistrement réussi. </span>';
                        }
                        ?>

                        <div class="table-scrollable">
                            <table class="table table-hover table-checkable order-column full-width"
                                   id="example4">
                                <thead>
                                    <tr>
                                        <th class="center"> Ordre</th>
                                        <th class="center">Titre</th>
                                        <th class="center"> Type</th>
                                        <th class="center"> Sous-menus</th>
                                        <th class="center"> Action</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    if (isset($_GET['mid'])) {
                                        $mid = $_GET['mid'];
                                        $found_menus = Menu:: list_menu_children_active($mid);
                                        foreach ($found_menus as $found_menu) {
                                            echo '<tr class="odd gradeX">';
                                            echo '<td class="center">' . $found_menu->menu_position . '</td>';
                                            echo '<td class="center"><a href="app.php?item=formmenu&mid=' . $found_menu->menu_id . '&option=edit">' . $found_menu->menu_title . '</td>';
                                            echo '<td class="center">';
                                            $nb = Menu:: count_menu_children_active($found_menu->menu_id);
                                            if (empty($nb)) {
                                                echo 'Singleton';
                                            } else {
                                                echo 'Parent';
                                            }
                                            echo '</td>';

                                            if (!empty($nb)) {
                                                echo '<td class="center"><a href="app.php?item=listmenus&mid=' . $found_menu->menu_id . '">' . $nb . '</a></td>';
                                            } else {
                                                echo '<td class="center">0</td>';
                                            }
                                            echo '<td class="center">';
                                            echo ' <a href="app.php?item=formmenu&mid=' . $found_menu->menu_id . '&option=edit" class="btn btn-tbl-edit btn-xs">';
                                            echo '<i class="fa fa-pencil"></i>';
                                            echo '</a>';
                                            echo '</td>';

                                            echo '</tr>';
                                        } // end foreach
                                    } else {

                                        $found_menus = Menu:: list_menus_active();
                                        foreach ($found_menus as $found_menu) {
                                            echo '<tr class="odd gradeX">';
                                            echo '<td class="center">' . $found_menu->menu_position . '</td>';
                                            echo '<td class="center"><a href="app.php?item=formmenu&mid=' . $found_menu->menu_id . '&option=edit">' . $found_menu->menu_title . '</td>';
                                            echo '<td class="center">';
                                            $nb = Menu:: count_menu_children_active($found_menu->menu_id);
                                            if (empty($nb)) {
                                                echo 'Singleton';
                                            } else {
                                                echo 'Parent';
                                            }
                                            echo '</td>';

                                            if (!empty($nb)) {
                                                echo '<td class="center"><a href="app.php?item=listmenus&mid=' . $found_menu->menu_id . '">' . $nb . '</a></td>';
                                            } else {
                                                echo '<td class="center">0</td>';
                                            }


                                            echo '<td class="center">';
                                            echo ' <a href="app.php?item=formmenu&mid=' . $found_menu->menu_id . '&option=edit" class="btn btn-tbl-edit btn-xs">';
                                            echo '<i class="fa fa-pencil"></i>';
                                            echo '</a>';
                                            echo '</td>';

                                            echo '</tr>';
                                        } // end foreach
                                    } // else
                                    ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

