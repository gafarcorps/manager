
		 	<!-- start page content -->
             <div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Bienvenue dans Manager</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
										href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li><a class="parent-item" href="#">Blog</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Liste des derniers Commentaires</li>
							</ol>
						</div>
					</div>

                    <div class="row">
                        <?php
                            if(isset($_GET['msg']) && $_GET['msg']=='success') {
                                echo '<span class="clsAvailable"> Enregistrement réussi. </span>';
                            } 
                        ?>
                        <div class="col-md-12">
                            <div class="card card-topline-aqua">
                                <div class="card-head">
                                    <header>Liste des derniers Commentaires</header>
                                    <div class="tools">
                                        <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                        <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                        <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                    </div>
                                </div>
                                <div class="card-body ">
                                    <div class="table-scrollable">
                                        <table id="example1" class="display full-width">
                                            <thead>
                                                <tr>  
                                                    <th style="width:18px"><input type="checkbox" /></th>
                                                    <th>Article</th> 
                                                    <th>Nombre de commentaires</th> 
                                                </tr>   
                                            </thead>
                                            <tbody>
                                            <?php
                                                $found_comments = Comment_topic::find_all();
                                            
                                                foreach($found_comments as $found_comment){
                                                    $found_topic = $topic->find_by_id($found_comment->comment_topic_topicid);
                                                    echo '<tr class="table-flag-blue">';
                                                    echo '<td><input type="checkbox" /></td>';
                                                    echo '<td> <a href="savetopic.php?topicid='.$found_comment->comment_topic_topicid.'&option=edit">' . $found_topic->topic_title . '</a></td>';
                                                    echo '<td>' . $found_comment->count_by_topic($found_comment->comment_topic_topicid) . '</td>';
                                                    echo '</div>';
                                                    echo '</td>';
                                                    echo '</tr>';
                                                }
                                            ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
			                
		                
			<!-- end page content -->