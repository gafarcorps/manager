-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Hôte : mysql_hosting2
-- Généré le : lun. 06 déc. 2021 à 15:47
-- Version du serveur :  5.5.62-0ubuntu0.14.04.1
-- Version de PHP : 7.4.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `creativesweb_rt`
--

-- --------------------------------------------------------

--
-- Structure de la table `app_requests`
--

CREATE TABLE `app_requests` (
  `app_request_id` int(4) NOT NULL COMMENT 'Id',
  `app_request_menu_id` int(4) NOT NULL COMMENT 'Menu',
  `app_request_customer_id` int(4) NOT NULL COMMENT 'Client',
  `app_request_title` text COLLATE utf8_unicode_ci NOT NULL,
  `app_request_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `app_request_date` date NOT NULL COMMENT 'Date',
  `app_request_installation_date` date NOT NULL COMMENT 'Date installation',
  `app_request_user_creation` int(4) NOT NULL COMMENT 'Utilisateur ayant fait l''enregistrement	',
  `app_request_date_creation` date NOT NULL COMMENT 'Date de l''enregistrement	',
  `app_request_user_last_modification` int(4) NOT NULL COMMENT 'Utilisateur ayant fait la dernière modification	',
  `app_request_date_last_modification` date NOT NULL COMMENT 'Date de la dernière modification	',
  `app_request_deleted` tinyint(1) NOT NULL,
  `app_request_active` tinyint(1) NOT NULL COMMENT 'app_request actif (1) ou non (0) ?	'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `app_requests`
--

INSERT INTO `app_requests` (`app_request_id`, `app_request_menu_id`, `app_request_customer_id`, `app_request_title`, `app_request_desc`, `app_request_date`, `app_request_installation_date`, `app_request_user_creation`, `app_request_date_creation`, `app_request_user_last_modification`, `app_request_date_last_modification`, `app_request_deleted`, `app_request_active`) VALUES
(2, 61, 7, 'Demande du client LNBTP: Menu (Candidatures)', '', '2021-12-02', '0000-00-00', 11, '2021-12-02', 11, '2021-12-02', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `candidates`
--

CREATE TABLE `candidates` (
  `candidate_id` int(11) NOT NULL,
  `candidate_fullname` varchar(255) NOT NULL,
  `candidate_email` varchar(255) NOT NULL,
  `candidate_type` varchar(255) NOT NULL,
  `candidate_date` date NOT NULL,
  `candidate_active` tinyint(4) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `candidates`
--

INSERT INTO `candidates` (`candidate_id`, `candidate_fullname`, `candidate_email`, `candidate_type`, `candidate_date`, `candidate_active`) VALUES
(1, 'Menz Alfa', 'menz12x@gmail.com', 'Demande Emploi', '0000-00-00', 0),
(2, 'ABASSAH YAH-LOM DOSSEH', 'dossehyahlomabassah@gmail.com', 'Demande Stage', '0000-00-00', 0),
(3, 'ABASSAH YAH-LOM DOSSEH', 'dossehyahlomabassah@gmail.com', 'Demande Stage', '0000-00-00', 0),
(4, 'ABASSAH YAH-LOM DOSSEH', 'dossehyahlomabassah@gmail.com', 'Demande Stage', '0000-00-00', 0),
(5, 'ABASSAH YAH-LOM DOSSEH', 'dossehyahlomabassah@gmail.com', 'Demande Stage', '0000-00-00', 0);

-- --------------------------------------------------------

--
-- Structure de la table `contracts`
--

CREATE TABLE `contracts` (
  `contract_id` int(4) NOT NULL COMMENT 'Id',
  `contract_customer_id` int(4) NOT NULL COMMENT 'Client',
  `contract_service_id` int(4) NOT NULL COMMENT 'Service',
  `contract_title` text NOT NULL COMMENT 'Titre',
  `contract_desc` text NOT NULL COMMENT 'Description',
  `contract_date` date NOT NULL COMMENT 'Date de signature',
  `contract_datestart` date NOT NULL COMMENT 'Date début',
  `contract_dateend` date NOT NULL COMMENT 'Date fin',
  `contract_date_breaking` date NOT NULL COMMENT 'Date de rupture du contrat',
  `contract_file_path` varchar(255) NOT NULL COMMENT 'Fichier',
  `contract_user_creation` int(4) NOT NULL COMMENT 'Utilisateur ayant fait l''enregistrement	',
  `contract_date_creation` date NOT NULL COMMENT 'Date de l''enregistrement	',
  `contract_user_last_modification` int(4) NOT NULL COMMENT 'Utilisateur ayant fait la dernière modification	',
  `contract_date_last_modification` date NOT NULL COMMENT 'Date de la dernière modification	',
  `contract_deleted` tinyint(1) NOT NULL COMMENT 'Contrat supprimé (1) ou non (0)?',
  `contract_active` tinyint(1) NOT NULL COMMENT 'Contrat actif (1) ou non (0) ?'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `contracts`
--

INSERT INTO `contracts` (`contract_id`, `contract_customer_id`, `contract_service_id`, `contract_title`, `contract_desc`, `contract_date`, `contract_datestart`, `contract_dateend`, `contract_date_breaking`, `contract_file_path`, `contract_user_creation`, `contract_date_creation`, `contract_user_last_modification`, `contract_date_last_modification`, `contract_deleted`, `contract_active`) VALUES
(14, 7, 2, 'SITE WEB', '', '2011-01-01', '2011-01-01', '0000-00-00', '0000-00-00', '', 1, '2021-09-16', 1, '2021-09-16', 0, 0),
(15, 8, 2, 'Site web / maintenance', '', '2010-01-01', '0000-00-00', '0000-00-00', '0000-00-00', '', 1, '2021-09-16', 1, '2021-09-16', 0, 0),
(16, 9, 2, 'Site web ', '', '2012-01-01', '0000-00-00', '0000-00-00', '0000-00-00', '', 1, '2021-09-16', 1, '2021-09-16', 0, 0),
(17, 10, 2, 'Site web ', '', '2014-01-01', '0000-00-00', '0000-00-00', '0000-00-00', '', 1, '2021-09-16', 1, '2021-09-16', 0, 0),
(18, 11, 2, 'Site web ', '', '2016-01-01', '0000-00-00', '0000-00-00', '0000-00-00', '', 1, '2021-09-16', 1, '2021-09-16', 0, 0),
(19, 12, 2, 'Site web / Design Flyers / Maintenance', '', '2013-01-01', '2013-01-01', '2021-01-01', '2021-01-01', '', 1, '2021-09-16', 1, '2021-09-16', 0, 0),
(20, 13, 2, 'Site web / Design Flyers / Maintenance', '', '2013-01-01', '2013-01-01', '2021-01-01', '2021-01-01', '', 1, '2021-09-16', 1, '2021-09-16', 0, 0),
(21, 14, 2, 'Site web ', '', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '', 1, '2021-09-16', 1, '2021-09-16', 0, 0),
(22, 15, 2, 'Site web ', '', '2016-01-01', '2016-01-01', '0000-00-00', '0000-00-00', '', 1, '2021-09-16', 1, '2021-09-16', 0, 0),
(23, 15, 2, 'Site web ', '', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '', 1, '2021-09-16', 1, '2021-09-16', 0, 0),
(24, 17, 2, 'Site web ', '', '2011-01-01', '0000-00-00', '0000-00-00', '0000-00-00', '', 1, '2021-09-16', 1, '2021-09-16', 0, 0),
(25, 18, 2, 'Site web ', '', '2015-01-01', '0000-00-00', '2016-01-01', '2016-01-01', '', 1, '2021-09-16', 1, '2021-09-16', 0, 0),
(26, 19, 2, 'Site web ', '', '2015-01-01', '0000-00-00', '0000-00-00', '0000-00-00', '', 1, '2021-09-16', 1, '2021-09-16', 0, 0),
(27, 20, 2, 'Site web ', '', '2014-01-01', '0000-00-00', '0000-00-00', '0000-00-00', '', 1, '2021-09-16', 1, '2021-09-16', 0, 0),
(28, 21, 2, 'Site web ', '', '2016-01-01', '0000-00-00', '0000-00-00', '0000-00-00', '', 1, '2021-09-16', 1, '2021-09-16', 0, 0),
(29, 24, 2, 'Site web ', '', '2017-01-01', '0000-00-00', '0000-00-00', '0000-00-00', '', 2021, '2021-09-27', 1, '2021-09-27', 0, 0),
(30, 23, 2, 'Site web ', '', '2017-01-01', '0000-00-00', '0000-00-00', '0000-00-00', '', 1, '2021-09-27', 1, '2021-09-27', 0, 0),
(31, 24, 2, 'Site web ', '', '2017-01-01', '0000-00-00', '0000-00-00', '0000-00-00', '', 1, '2021-09-27', 1, '2021-09-27', 0, 0),
(32, 24, 2, 'Site web ', '', '2017-01-01', '0000-00-00', '0000-00-00', '0000-00-00', '', 1, '2021-09-27', 1, '2021-09-27', 0, 0),
(33, 25, 2, 'Site web ', '', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '', 1, '2021-09-27', 1, '2021-09-27', 0, 0),
(34, 26, 2, 'Site web ', '', '2017-01-01', '2017-01-01', '2018-01-01', '2018-01-01', '', 1, '2021-09-27', 1, '2021-09-27', 0, 0),
(35, 35, 13, 'azrtyu', '', '0000-00-00', '0000-00-00', '0000-00-00', '0000-00-00', '', 1, '2021-11-23', 1, '2021-11-23', 0, 0);

-- --------------------------------------------------------

--
-- Structure de la table `copyright`
--

CREATE TABLE `copyright` (
  `copyright_id` int(4) NOT NULL,
  `copyright_username` varchar(255) NOT NULL,
  `copyright_userdesc` text NOT NULL,
  `copyright_tel` varchar(255) NOT NULL,
  `copyright_email` varchar(255) NOT NULL,
  `copyright_address` varchar(255) NOT NULL,
  `copyright_numr` varchar(255) NOT NULL,
  `copyright_logo` varchar(255) NOT NULL,
  `copyright_date` date NOT NULL,
  `copyright_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `copyright`
--

INSERT INTO `copyright` (`copyright_id`, `copyright_username`, `copyright_userdesc`, `copyright_tel`, `copyright_email`, `copyright_address`, `copyright_numr`, `copyright_logo`, `copyright_date`, `copyright_active`) VALUES
(1, 'LNBTP', '', '+22822256283', 'lnbtp@lnbtp.com', 'P.K.9-ROUTE Lomé-Atakpamé B.P.20100 TOGO', '', 'lnbtp-logo.png', '2021-11-29', 0);

-- --------------------------------------------------------

--
-- Structure de la table `files`
--

CREATE TABLE `files` (
  `file_id` int(11) NOT NULL,
  `file_page_id` int(11) NOT NULL,
  `file_cand_id` int(11) NOT NULL,
  `file_path` text NOT NULL,
  `file_type` varchar(255) NOT NULL,
  `file_size` varchar(255) NOT NULL,
  `file_caption` varchar(255) NOT NULL,
  `file_date` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `files`
--

INSERT INTO `files` (`file_id`, `file_page_id`, `file_cand_id`, `file_path`, `file_type`, `file_size`, `file_caption`, `file_date`) VALUES
(15, 0, 19, 'footer_facebook.png', 'image/png', '2909', 'Lettre de motivation', '2021-03-11'),
(16, 0, 19, 'footer_rss.png', 'image/png', '3084', 'Curriculum vitae', '2021-03-11');

-- --------------------------------------------------------

--
-- Structure de la table `filter`
--

CREATE TABLE `filter` (
  `filter_id` int(4) NOT NULL,
  `filter_desc` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `filter`
--

INSERT INTO `filter` (`filter_id`, `filter_desc`) VALUES
(1, 1),
(2, 2),
(3, 3),
(4, 4),
(5, 5),
(6, 6),
(7, 7),
(8, 8),
(9, 9),
(10, 10),
(11, 11),
(12, 12),
(13, 13),
(14, 14),
(15, 15),
(16, 16),
(17, 17),
(18, 18),
(19, 19),
(20, 20),
(21, 21),
(22, 22),
(23, 23),
(24, 24),
(25, 25);

-- --------------------------------------------------------

--
-- Structure de la table `gallery`
--

CREATE TABLE `gallery` (
  `gallery_id` int(11) NOT NULL,
  `gallery_parent_id` int(11) DEFAULT NULL,
  `gallery_name` varchar(255) DEFAULT NULL,
  `gallery_desc` text,
  `gallery_position` int(11) DEFAULT NULL,
  `gallery_image_path` text,
  `gallery_active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `menu`
--

CREATE TABLE `menu` (
  `menu_id` int(4) NOT NULL,
  `menu_parent_id` int(4) NOT NULL,
  `menu_title` varchar(255) NOT NULL,
  `menu_desc` text NOT NULL,
  `menu_desc_infos` text NOT NULL,
  `menu_link` text NOT NULL,
  `menu_position` int(11) NOT NULL,
  `menu_is_extension` tinyint(1) NOT NULL,
  `menu_picture` varchar(255) NOT NULL,
  `menu_created_at` date NOT NULL,
  `menu_extension_vedette` tinyint(1) NOT NULL,
  `menu_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `menu`
--

INSERT INTO `menu` (`menu_id`, `menu_parent_id`, `menu_title`, `menu_desc`, `menu_desc_infos`, `menu_link`, `menu_position`, `menu_is_extension`, `menu_picture`, `menu_created_at`, `menu_extension_vedette`, `menu_active`) VALUES
(1, 11, 'Accueil', '', '', 'app.php?item=home', 1, 0, '', '0000-00-00', 0, 1),
(2, 11, 'Page', '', '', '', 2, 0, '', '0000-00-00', 0, 1),
(3, 11, 'Blog', '', '', '', 3, 0, '', '0000-00-00', 0, 1),
(4, 5, 'Clients', '                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        \r\n                Le gestionnaire des clients est un module qui permets de gÃ©rer tous les clients de votre entreprise\r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                ', '', 'appex.php?item=listcustomers', 6, 1, 'bba.jpg', '0000-00-00', 0, 1),
(5, 47, 'Marketing', '                                                            Cette catÃ©gorie gÃ¨re les extensions marketing, publicitÃ©s, etc..\r\n                \r\n                \r\n                ', '', 'app.php?item=listextensions', 7, 1, 'marketing.jpg', '0000-00-00', 0, 1),
(6, 11, 'Utilisateurs', '', '', '', 8, 0, '', '0000-00-00', 0, 1),
(7, 11, 'Parametres', '', '', '', 9, 0, '', '0000-00-00', 0, 1),
(8, 2, 'Pages du site', '', '', 'app.php?item=listpages', 0, 0, '', '0000-00-00', 0, 1),
(9, 3, 'Articles', '', '', 'app.php?item=listtopics', 1, 0, '', '0000-00-00', 0, 1),
(11, 11, 'no_parent', '', '', '', 0, 0, '', '0000-00-00', 0, 1),
(12, 3, 'CatÃ©gories', '', '', 'app.php?item=listcategories', 2, 0, '', '0000-00-00', 0, 1),
(13, 3, 'Etiquettes', '', '', 'app.php?item=listetiquettes', 1, 0, '', '0000-00-00', 0, 1),
(14, 4, 'Nouveau client', '', '', 'app.php?item=formcustomer&option=new', 1, 0, '', '0000-00-00', 0, 1),
(15, 6, 'Creer un nouvel utilisateur', '', '', 'app.php?item=formusers&option=new', 0, 0, '', '0000-00-00', 0, 1),
(16, 6, 'Tous les utilisateurs', '', '', 'app.php?item=listusers', 0, 0, '', '0000-00-00', 0, 1),
(17, 7, 'Profils et privileges', '', '', 'app.php?item=privileges', 0, 0, '', '0000-00-00', 0, 1),
(21, 11, 'Deconnexion', '', '', 'logout.php', 0, 0, '', '0000-00-00', 0, 1),
(22, 7, 'Gestionnaire des menus', '', '', 'app.php?item=listmenus', 0, 0, '', '0000-00-00', 0, 1),
(27, 5, 'Nouvelle pub', '', '', 'app.php?item=formmarketing&option=new', 0, 0, '', '0000-00-00', 0, 0),
(28, 2, 'Creer une nouvelle page', '', '', 'app.php?item=formpage&pid=11&option=new', 0, 0, '', '0000-00-00', 0, 1),
(29, 5, 'Pubs en ligne', '', '', 'app.php?item=listads', 0, 0, '', '0000-00-00', 0, 0),
(31, 4, 'Liste des clients', '', '', 'app.php?item=listcustomers', 2, 0, '', '0000-00-00', 0, 1),
(32, 5, 'Factures', '', '', 'app.php?item=forminvoice', 0, 0, '', '0000-00-00', 0, 0),
(33, 3, 'Commentaires', '', '', 'app.php?item=comments', 0, 0, '', '0000-00-00', 0, 0),
(34, 7, 'Copyright', '', '', 'app.php?item=formcopyright', 0, 0, '', '0000-00-00', 0, 1),
(35, 11, 'Galerie', '', '', 'app.php?item=gallery', 4, 0, '', '0000-00-00', 0, 1),
(36, 47, 'Candidatures', '                                        \r\n                \r\n                ', '', 'app.php?item=candidacies', 5, 1, '', '0000-00-00', 0, 0),
(37, 3, 'RÃ©seaux Sociaux', '', '', 'app.php?item=listsocials', 3, 0, '', '0000-00-00', 0, 1),
(38, 2, 'Sections', '', '', 'app.php?item=listsections', 5, 0, '', '0000-00-00', 0, 1),
(39, 38, 'Sections et menus', '', '', 'app.php?item=listsections', 1, 0, '', '0000-00-00', 0, 1),
(42, 41, 'Nouveau service', '', '', 'app.php?item=formservice&option=new', 1, 0, '', '0000-00-00', 0, 1),
(43, 41, 'Tous les services', '', '', 'app.php?item=listservices', 2, 0, '', '0000-00-00', 0, 1),
(44, 47, 'Services', '                    \r\n                ', '', 'app.php?item=listservices', 3, 1, '', '0000-00-00', 0, 0),
(45, 7, 'Test', '', '', '', 1, 1, 'Kevin-Denkey.jpg', '0000-00-00', 0, 0),
(46, 7, 'Test', '', '', '', 1, 1, 'Kevin-Denkey.jpg', '0000-00-00', 0, 0),
(47, 11, 'Extensions', '                                        La liste des extensions\r\n                \r\n                ', '', 'app.php?item=listextensions', 8, 1, '', '0000-00-00', 0, 1),
(48, 47, 'Jobs', '                    Cette catÃ©gorie gÃ¨re les extensions emplois, recrutements, etc....\r\n                ', '', 'app.php?item=listjobs', 1, 1, '', '0000-00-00', 0, 0),
(49, 47, 'Entreprises', '                                                   Cette catÃ©gorie gÃ¨re les extensions candidatures, etc..                                                                     \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                ', '', 'appex.php?item=candidacies', 2, 1, 'e96669f09c136d4598a04830d9874ad5.jpg', '0000-00-00', 0, 1),
(50, 11, 'Commentaires', '', '', '', 3, 0, '', '0000-00-00', 0, 1),
(51, 50, 'Liste des commentaires', '', '', 'app.php?item=listtopics_comments', 1, 0, '', '0000-00-00', 0, 1),
(52, 50, 'Nouveau commentaire', '', '', 'app.php?item=formtopic_comment?option=new', 2, 0, '', '0000-00-00', 0, 0),
(53, 11, 'Je teste10', '                    Golazo10\r\n                ', '', 'app.php?item=testage!!', 1, 0, '', '0000-00-00', 0, 0),
(56, 47, 'LogithÃ¨que', '                                                                                                                        \r\n                \r\n                \r\n                \r\n                \r\n                \r\n                ', '', 'app.php?item=listextensions', 1, 0, '', '0000-00-00', 0, 1),
(57, 11, 'Marketing', '                                                            Cette catÃ©gorie gÃ¨re les extensions marketing, publicitÃ©s, etc..\r\n                \r\n                \r\n                ', '', 'app.php?item=listextensions', 10, 0, '', '2021-11-18', 0, 0),
(58, 11, 'Marketing', '                                                            Cette catÃ©gorie gÃ¨re les extensions marketing, publicitÃ©s, etc..\r\n                \r\n                \r\n                ', '', 'app.php?item=listextensions', 10, 0, '', '2021-11-19', 0, 0),
(59, 11, 'Marketing', '                                                                                Cette catÃ©gorie gÃ¨re les extensions marketing, publicitÃ©s, etc..\r\n                \r\n                \r\n                \r\n                ', '', 'app.php?item=listextensions', 10, 0, '', '0000-00-00', 0, 0),
(60, 47, 'Liste des demandes', '                    \r\n                ', '', 'app.php?item=listapps_requests', 1, 0, '', '0000-00-00', 0, 1),
(61, 49, 'Candidatures', '                                        \r\n                \r\n                ', '', 'app.php?item=candidacies', 1, 1, 'conseil-gestion-candidature-e1495032739419.png', '0000-00-00', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `mysql_notif`
--

CREATE TABLE `mysql_notif` (
  `mysql_notif_id` int(11) NOT NULL,
  `mysql_notif_error` text NOT NULL,
  `mysql_notif_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `page_elements`
--

CREATE TABLE `page_elements` (
  `page_element_id` int(11) NOT NULL,
  `page_element_page_id` int(11) NOT NULL,
  `page_element_title` varchar(255) DEFAULT NULL,
  `page_element_desc` text,
  `page_element_file` text,
  `page_element_icon` varchar(255) DEFAULT NULL,
  `page_element_position` int(11) DEFAULT NULL,
  `page_element_url` text,
  `page_element_date` date DEFAULT NULL,
  `page_element_active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Structure de la table `page_section`
--

CREATE TABLE `page_section` (
  `page_section_id` int(4) NOT NULL,
  `page_section_pid` int(4) NOT NULL,
  `page_section_sid` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `press_comments_topic`
--

CREATE TABLE `press_comments_topic` (
  `comment_topic_id` int(4) NOT NULL,
  `comment_topic_topicid` int(4) NOT NULL,
  `comment_topic_text` text COLLATE utf8_unicode_ci NOT NULL,
  `comment_topic_author` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `comment_topic_author_email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `comment_topic_date` date NOT NULL,
  `comment_topic_published` tinyint(1) NOT NULL DEFAULT '0',
  `comment_topic_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `press_comments_topic`
--

INSERT INTO `press_comments_topic` (`comment_topic_id`, `comment_topic_topicid`, `comment_topic_text`, `comment_topic_author`, `comment_topic_author_email`, `comment_topic_date`, `comment_topic_published`, `comment_topic_active`) VALUES
(1, 1, 'Je commente...', 'Cenciz UNDER', 'tcho@om.nete', '2021-11-16', 1, 1),
(2, 1, 'Je commente aussi mec! C\'est la violada! Tristada', 'Momo HENNI 10', 'tcho10@om.net', '2021-11-16', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `press_etiquette`
--

CREATE TABLE `press_etiquette` (
  `etiquette_id` int(4) NOT NULL,
  `etiquette_desc` text NOT NULL,
  `etiquette_active` tinyint(1) NOT NULL,
  `etiquette_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `press_etiquette`
--

INSERT INTO `press_etiquette` (`etiquette_id`, `etiquette_desc`, `etiquette_active`, `etiquette_date`) VALUES
(1, 'news', 0, '2021-06-24 01:50:09');

-- --------------------------------------------------------

--
-- Structure de la table `press_page`
--

CREATE TABLE `press_page` (
  `page_id` int(11) NOT NULL,
  `page_parent_id` int(4) NOT NULL,
  `page_uid` int(4) NOT NULL,
  `page_title` text COLLATE utf8_unicode_ci NOT NULL,
  `page_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `page_link` text COLLATE utf8_unicode_ci NOT NULL,
  `page_position` int(4) NOT NULL,
  `page_date` datetime NOT NULL,
  `page_image_path` text COLLATE utf8_unicode_ci,
  `page_only_desc` tinyint(4) NOT NULL,
  `page_section` tinyint(4) NOT NULL,
  `page_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `press_page`
--

INSERT INTO `press_page` (`page_id`, `page_parent_id`, `page_uid`, `page_title`, `page_desc`, `page_link`, `page_position`, `page_date`, `page_image_path`, `page_only_desc`, `page_section`, `page_active`) VALUES
(49, 0, 3, 'Barre latÃ©rale droite', '', '', 12, '2021-06-03 10:06:14', '', 0, 1, 1),
(57, 0, 3, 'Pied de page du site', '', '', 21, '2021-06-09 22:10:15', '', 0, 1, 1),
(58, 0, 3, 'Haut de page', '', '', 13, '2021-06-10 23:01:18', '', 0, 1, 1),
(59, 0, 3, 'Barre latÃ©rale gauche', '', '', 14, '2021-06-10 23:03:19', '', 0, 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `press_tag`
--

CREATE TABLE `press_tag` (
  `press_tag_topic_id` int(4) NOT NULL,
  `press_tag_etiquette_id` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `press_tag`
--

INSERT INTO `press_tag` (`press_tag_topic_id`, `press_tag_etiquette_id`) VALUES
(220, 1),
(220, 1);

-- --------------------------------------------------------

--
-- Structure de la table `press_topics`
--

CREATE TABLE `press_topics` (
  `topic_id` int(4) NOT NULL,
  `topic_typetopic_id` int(4) NOT NULL,
  `topic_author` int(4) NOT NULL,
  `topic_title` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `topic_text` text CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `topic_url` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `topic_image_path` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `topic_date_creation` date NOT NULL,
  `topic_top_slider` tinyint(1) NOT NULL,
  `topic_top_small` tinyint(4) NOT NULL,
  `topic_views` int(4) NOT NULL,
  `topic_icon` varchar(50) NOT NULL,
  `topic_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `press_topics`
--

INSERT INTO `press_topics` (`topic_id`, `topic_typetopic_id`, `topic_author`, `topic_title`, `topic_text`, `topic_url`, `topic_image_path`, `topic_date_creation`, `topic_top_slider`, `topic_top_small`, `topic_views`, `topic_icon`, `topic_active`) VALUES
(1, 14, 3, 'Test 1.0', 'Golazo', '', '', '2021-11-01', 0, 0, 0, '', 1);

-- --------------------------------------------------------

--
-- Structure de la table `press_type_topic`
--

CREATE TABLE `press_type_topic` (
  `type_topic_id` int(4) NOT NULL,
  `type_topic_desc` varchar(255) NOT NULL,
  `type_topic_date` date NOT NULL,
  `type_topic_active` tinyint(1) NOT NULL,
  `type_topic_position` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `press_type_topic`
--

INSERT INTO `press_type_topic` (`type_topic_id`, `type_topic_desc`, `type_topic_date`, `type_topic_active`, `type_topic_position`) VALUES
(14, 'ActualitÃ©s', '2021-06-24', 1, 0);

-- --------------------------------------------------------

--
-- Structure de la table `privileges`
--

CREATE TABLE `privileges` (
  `privilege_type_user_id` int(4) NOT NULL,
  `privilege_menu_id` int(4) NOT NULL,
  `privilege_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `privileges`
--

INSERT INTO `privileges` (`privilege_type_user_id`, `privilege_menu_id`, `privilege_active`) VALUES
(1, 1, 1),
(1, 2, 1),
(1, 3, 1),
(1, 4, 1),
(1, 5, 1),
(1, 6, 1),
(1, 7, 1),
(1, 21, 1),
(1, 35, 1),
(1, 36, 1),
(1, 47, 1),
(1, 48, 1),
(1, 49, 1),
(1, 50, 1),
(1, 51, 1),
(1, 52, 1),
(1, 53, 1),
(1, 56, 1),
(1, 57, 1),
(1, 58, 1),
(1, 59, 1),
(1, 60, 1),
(1, 61, 1),
(2, 1, 1),
(2, 2, 1),
(2, 3, 1),
(2, 4, 0),
(2, 6, 0),
(2, 7, 0),
(2, 13, 0),
(2, 21, 1),
(2, 35, 1),
(2, 38, 1),
(2, 47, 0),
(2, 48, 0),
(2, 49, 0),
(2, 50, 0),
(2, 51, 0),
(2, 52, 0),
(2, 53, 0),
(2, 56, 0),
(2, 57, 0),
(2, 58, 0),
(2, 59, 0),
(2, 60, 0),
(2, 61, 0),
(3, 1, 1),
(3, 2, 1),
(3, 3, 1),
(3, 4, 0),
(3, 6, 0),
(3, 7, 0),
(3, 21, 0),
(3, 35, 1),
(3, 36, 1),
(3, 38, 0),
(3, 47, 0),
(3, 48, 0),
(3, 49, 0),
(3, 50, 0),
(3, 51, 0),
(3, 52, 0),
(3, 53, 0),
(3, 56, 0),
(3, 57, 0),
(3, 58, 0),
(3, 59, 0),
(3, 60, 0),
(3, 61, 0),
(4, 1, 1),
(4, 2, 1),
(4, 3, 1),
(4, 4, 0),
(4, 6, 0),
(4, 7, 0),
(4, 13, 0),
(4, 21, 1),
(4, 35, 1),
(4, 38, 0),
(4, 47, 0),
(4, 48, 0),
(4, 49, 0),
(4, 50, 0),
(4, 51, 0),
(4, 52, 0),
(4, 53, 0),
(4, 56, 0),
(4, 57, 0),
(4, 58, 0),
(4, 59, 0),
(4, 60, 0),
(4, 61, 0);

-- --------------------------------------------------------

--
-- Structure de la table `sections`
--

CREATE TABLE `sections` (
  `section_id` int(4) NOT NULL,
  `section_desc` varchar(255) NOT NULL,
  `section_type` tinyint(4) NOT NULL,
  `section_active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `sections`
--

INSERT INTO `sections` (`section_id`, `section_desc`, `section_type`, `section_active`) VALUES
(1, 'CÃ´tÃ© gauche de page', 1, 1),
(2, 'Bas de page', 1, 1),
(3, 'CÃ´tÃ© droit de page', 1, 1),
(4, 'Haut de page', 1, 1),
(10, 'Aucune', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `services`
--

CREATE TABLE `services` (
  `service_id` int(11) NOT NULL COMMENT 'Id',
  `service_title` varchar(100) NOT NULL COMMENT 'Titre',
  `service_desc` text NOT NULL COMMENT 'Description',
  `service_image_path` varchar(255) NOT NULL COMMENT 'Visuel',
  `service_user_creation` int(4) NOT NULL COMMENT 'Utilisateur ayant fait l''enregistrement',
  `service_date_creation` date NOT NULL COMMENT 'Date de l''enregistrement',
  `service_user_last_modification` int(4) NOT NULL COMMENT 'Utilisateur ayant fait la dernière modification',
  `service_date_last_modification` date NOT NULL COMMENT 'Date de la dernière modification',
  `service_deleted` tinyint(1) NOT NULL COMMENT 'Supprimé (1) ou non (0)',
  `service_active` tinyint(1) NOT NULL COMMENT 'Actif (1) ou non (0)'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `services`
--

INSERT INTO `services` (`service_id`, `service_title`, `service_desc`, `service_image_path`, `service_user_creation`, `service_date_creation`, `service_user_last_modification`, `service_date_last_modification`, `service_deleted`, `service_active`) VALUES
(1, 'Community Management', '', '', 0, '0000-00-00', 1, '2021-07-27', 0, 1),
(2, 'Services web', '', '', 1, '0000-00-00', 0, '0000-00-00', 0, 1),
(3, 'Design', '', '', 1, '0000-00-00', 0, '0000-00-00', 0, 1),
(4, 'E-marketing', '', '', 1, '0000-00-00', 0, '0000-00-00', 0, 1),
(5, 'Formations', '', '', 1, '0000-00-00', 0, '0000-00-00', 0, 1),
(6, 'Infog&eacute;rance', '', '', 1, '0000-00-00', 0, '0000-00-00', 0, 1),
(7, 'Personnel', '', '', 1, '0000-00-00', 0, '0000-00-00', 0, 1),
(8, 'Maintenance', '', '', 1, '0000-00-00', 0, '0000-00-00', 0, 1),
(9, 'Ticket non assign&eacute;', '', '', 1, '0000-00-00', 0, '0000-00-00', 0, 1),
(10, 'T&acirc;ches internes', '', '', 1, '0000-00-00', 0, '0000-00-00', 0, 1),
(11, 'Comptabilit&eacute;', '', '', 1, '0000-00-00', 0, '0000-00-00', 0, 1),
(12, 'Gestion client&egrave;le', '', '', 1, '0000-00-00', 0, '0000-00-00', 0, 1),
(13, 'Renseignements', '', '', 1, '0000-00-00', 0, '0000-00-00', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `socials`
--

CREATE TABLE `socials` (
  `social_id` int(11) NOT NULL,
  `social_name` varchar(100) NOT NULL,
  `social_link` text NOT NULL,
  `social_icon` varchar(200) DEFAULT NULL,
  `social_position` int(11) NOT NULL,
  `social_active` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `socials`
--

INSERT INTO `socials` (`social_id`, `social_name`, `social_link`, `social_icon`, `social_position`, `social_active`) VALUES
(1, 'Facebook', 'facebook.com', '<i class=\'bx bxl-facebook\'></i>', 1, 1),
(2, 'Twitter', 'twitter', '<i class=\'bx bxl-twitter\'></i>', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `tickets`
--

CREATE TABLE `tickets` (
  `ticket_id` int(4) NOT NULL COMMENT 'Id',
  `ticket_service_id` int(4) NOT NULL COMMENT 'Service',
  `ticket_customer_id` int(4) NOT NULL COMMENT 'Client',
  `ticket_email` varchar(100) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Adresse email du Client',
  `ticket_title` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Sujet du message (Titre du ticket)',
  `ticket_desc` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'Contenu du message (Description du ticket)',
  `ticket_date` date NOT NULL COMMENT 'Date à laquelle le ticket est initialisé',
  `ticket_datestart` date NOT NULL COMMENT 'Date début exécution',
  `ticket_dateend` date NOT NULL COMMENT 'Date fin exécution',
  `ticket_priority` int(4) NOT NULL COMMENT 'Priorité du ticket',
  `ticket_file_path` varchar(255) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Fichier attaché au ticket',
  `ticket_user_creation` int(4) NOT NULL COMMENT 'Utilisateur ayant fait l''enregistrement	',
  `ticket_date_creation` date NOT NULL COMMENT 'Date de l''enregistrement	',
  `ticket_user_last_modification` int(4) NOT NULL COMMENT 'Utilisateur ayant fait la dernière modification	',
  `ticket_date_last_modification` date NOT NULL COMMENT 'Date de la dernière modification	',
  `ticket_deleted` tinyint(1) NOT NULL COMMENT 'Ticket supprimé (1) ou non (0) ?	',
  `ticket_active` tinyint(1) NOT NULL COMMENT 'Ticket actif (1) ou non (0) ?	'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `topic_section`
--

CREATE TABLE `topic_section` (
  `topic_section_id` int(4) NOT NULL,
  `topic_section_pid` int(4) NOT NULL,
  `topic_section_sid` int(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `type_user`
--

CREATE TABLE `type_user` (
  `type_user_id` int(4) NOT NULL,
  `type_user_user_creation` int(4) NOT NULL,
  `type_user_user_last_modification` int(4) NOT NULL,
  `type_user_date_creation` date NOT NULL,
  `type_user_date_last_modification` date NOT NULL,
  `type_user_desc` varchar(20) NOT NULL,
  `type_user_deleted` tinyint(1) NOT NULL,
  `type_user_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `type_user`
--

INSERT INTO `type_user` (`type_user_id`, `type_user_user_creation`, `type_user_user_last_modification`, `type_user_date_creation`, `type_user_date_last_modification`, `type_user_desc`, `type_user_deleted`, `type_user_active`) VALUES
(1, 0, 0, '0000-00-00', '0000-00-00', 'root', 0, 1),
(2, 0, 0, '0000-00-00', '0000-00-00', 'admin', 0, 1),
(3, 0, 0, '0000-00-00', '0000-00-00', 'blogueur', 0, 1),
(4, 0, 0, '0000-00-00', '0000-00-00', 'client', 0, 1);

-- --------------------------------------------------------

--
-- Structure de la table `user_account`
--

CREATE TABLE `user_account` (
  `user_account_id` int(4) NOT NULL,
  `user_account_customer_number` int(4) NOT NULL DEFAULT '-1',
  `user_account_typeuser_id` int(4) NOT NULL,
  `user_account_email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_account_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_account_user_creation` int(4) NOT NULL,
  `user_account_date_creation` date NOT NULL,
  `user_account_user_last_update` int(4) NOT NULL,
  `user_account_date_last_connexion` date NOT NULL,
  `user_account_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `user_account`
--

INSERT INTO `user_account` (`user_account_id`, `user_account_customer_number`, `user_account_typeuser_id`, `user_account_email`, `user_account_password`, `user_account_user_creation`, `user_account_date_creation`, `user_account_user_last_update`, `user_account_date_last_connexion`, `user_account_active`) VALUES
(-1, -1, 1, 'default', '', 0, '0000-00-00', 0, '0000-00-00', 0),
(1, 1, 1, 'root@gmail.com', '$2y$10$HgrpyxnBBimaxSHc143DvuAy.f3xbwOjgE20Uwa9mSVcBEeCr9eri', 0, '0000-00-00', 1, '2020-07-03', 1),
(3, 3, 2, 'admin@gmail.com', '$2y$10$Moiz/14DN8mZv1wOZ5tCz.uJFY1QA7jGrZDrLl80Dsb1ZfTQb7PZa', 0, '0000-00-00', 3, '0000-00-00', 1),
(8, 4, 4, 'ask@creativesweb.info', 'pass', 0, '2021-07-16', 0, '2021-12-01', 1),
(10, -1, 2, 'menz12x@gmail.com', '$2y$10$5vzr4dU/0Gv7vbirAYzlf..qFNhb6JOo7M0UbLA4M2xwiEbK4P7G2', 0, '2021-07-29', 0, '0000-00-00', 1),
(11, 7, 4, 'lnbtp@lnbtp.com', 'pass', 0, '2021-09-16', 0, '2021-11-26', 1),
(12, 8, 4, 'contact@hotelladouceur.com', 'pass', 0, '2021-09-16', 0, '0000-00-00', 0),
(13, 9, 4, 'info@mission-togo.ch', 'pass', 0, '2021-09-16', 0, '0000-00-00', 0),
(14, 10, 4, 'info@southsudanmission.ch', 'pass', 0, '2021-09-16', 0, '0000-00-00', 0),
(15, 11, 4, 'info@missionmali.ch', 'pass', 0, '2021-09-16', 0, '0000-00-00', 0),
(16, 12, 4, 'contact@larondedespetits.ch', 'pass', 0, '2021-09-16', 0, '0000-00-00', 0),
(17, 13, 4, 'contact@arcencielpeinture.ch', 'pass', 0, '2021-09-16', 0, '0000-00-00', 0),
(18, 14, 4, 'info@ahdis.org', 'pass', 0, '2021-09-16', 0, '0000-00-00', 0),
(19, 15, 4, 'info@nigerian-mission.ch', 'pass', 0, '2021-09-16', 0, '0000-00-00', 0),
(20, 16, 4, 'info@zambianmission.ch', 'pass', 0, '2021-09-16', 0, '0000-00-00', 0),
(21, 17, 4, 'info@limpharm.net', 'pass', 0, '2021-09-16', 0, '0000-00-00', 0),
(22, 18, 4, 'info@gnosissuisse.ch', 'pass', 0, '2021-09-16', 0, '0000-00-00', 0),
(23, 19, 4, 'contact@afemet.org', 'pass', 0, '2021-09-16', 0, '0000-00-00', 0),
(24, 20, 4, 'infos@htranslucens.ch', 'pass', 0, '2021-09-16', 0, '0000-00-00', 0),
(25, 21, 4, 'info@togoqueens.com', 'pass', 0, '2021-09-16', 0, '0000-00-00', 0),
(26, 22, 4, 'contact@leotahotels.com', 'pass', 0, '2021-09-27', 0, '0000-00-00', 0),
(27, 23, 4, 'contact@voyagersimple.com', 'pass', 0, '2021-09-27', 0, '0000-00-00', 0),
(28, 24, 4, 'contact@228hotels.com', 'pass', 0, '2021-09-27', 0, '0000-00-00', 0),
(29, 25, 4, 'contact@symphoniemag.com', 'pass', 0, '2021-09-27', 0, '0000-00-00', 0),
(30, 26, 4, 'contact@togoplay.info', 'pass', 0, '2021-09-27', 0, '0000-00-00', 0),
(31, 27, 4, '', 'pass', 0, '2021-11-10', 0, '0000-00-00', 0),
(32, 28, 4, 'info@bdlconsulting.com', 'pass', 0, '2021-11-17', 0, '0000-00-00', 0),
(48, 32, 4, 'om@om.net', 'pass', 0, '2021-11-22', 0, '0000-00-00', 1),
(50, 33, 4, 'p@om.net', '$2y$10$GPso1J8v2.SIx/EJK/2/.ucr5xgFwonKQgj6xxP0v9Q8auUWPVff6', 0, '2021-11-23', 0, '0000-00-00', 1),
(52, 34, 4, 'VG@OM.NET', '$2y$10$DITOV7E9CFtsiSW69sxQF.wDLr6WssWziuw7NtalCojrh9U26ga3i', 0, '2021-11-23', 0, '0000-00-00', 1),
(53, 35, 4, 'oyoyo@om.net', '$2y$10$M5x.zycqWPqOLAofWPuns.0vUtEZWlxfKJ5gF65vL3ejGm3F4W1NW', 0, '2021-11-23', 0, '2021-11-23', 1),
(54, 36, 4, 'cr7@mu.ok', '$2y$10$woQVAatEDnAC1pQNozSeaepMKMmejmdrkZZREcPQUFb1BfgicdJNC', 11, '2021-11-23', 0, '2021-11-23', 1);

-- --------------------------------------------------------

--
-- Structure de la table `user_info`
--

CREATE TABLE `user_info` (
  `user_info_id` int(4) NOT NULL,
  `user_info_user_account_id` int(4) NOT NULL,
  `user_info_firstname` varchar(40) COLLATE utf8_unicode_ci NOT NULL,
  `user_info_lastname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `user_info_email` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `user_info_address` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `user_info_tel` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `user_info_desc` text COLLATE utf8_unicode_ci NOT NULL,
  `user_info_customer_url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_info_image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_info_user_creation` int(4) NOT NULL,
  `user_info_logged_in` int(4) NOT NULL,
  `user_info_date_creation` date NOT NULL,
  `user_info_date_last_connexion` date NOT NULL,
  `user_info_active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Déchargement des données de la table `user_info`
--

INSERT INTO `user_info` (`user_info_id`, `user_info_user_account_id`, `user_info_firstname`, `user_info_lastname`, `user_info_email`, `user_info_address`, `user_info_tel`, `user_info_desc`, `user_info_customer_url`, `user_info_image`, `user_info_user_creation`, `user_info_logged_in`, `user_info_date_creation`, `user_info_date_last_connexion`, `user_info_active`) VALUES
(-1, -1, '', '', '', '', '', '', '', '', 1, 1, '0000-00-00', '0000-00-00', 1),
(1, 1, 'John', 'Doe', '', 'Camionette', '+22899445999', '', '', 'avatardefault_92824.png', 1, 1, '0000-00-00', '2020-07-03', 1),
(3, 3, '', 'Admin', '', '', '', '', '', 'avatardefault_92824.png', 1, 1, '0000-00-00', '0000-00-00', 1),
(4, 8, 'CREATIVESWEB', 'Menz ALFA', '', 'Adidoadin, LomÃ©-TOGO', '0022890474738', 'Mot de passe FTP: ask@Q6j3CKKv*U2Ybnxtz\r\nServeur: mysql_hosting2\r\nUser: creativesweb18\r\nPass: J2eDMRJd\r\nBase de donnÃ©es: creativesweb_rt\r\n\r\nZone client: clients.netoxygen.ch\r\nIdentifiant: malfa\r\nMdp: telema\r\n\r\n', 'http://creativesweb.info/rt', '', 1, 1, '2021-07-16', '2021-12-01', 1),
(7, 11, 'LNBTP', 'Mr TCHAMDJA ', '', 'P.K.9-ROUTE LomÃ©-AtakpamÃ© B.P.20100 TOGO', '0022822256283', 'Nom du serveur: ftp.lnbtp.com \r\nNom d\'utilisateur: lnbtp.com\r\nMot de passe: Mh5XDGmg@45Mh5XDGmg@49\r\nType de connexion: passive\r\nMYSQL\r\nPhpmyadmin :https://phpmyadmin.netoxygen.ch\r\nUser: lnbtp3\r\nPwd: hX82CX9w\r\nserveur: mysql_hosting3', 'http://lnbtp.com', 'lnbtp-logo.png', 1, 1, '2021-09-16', '2021-11-26', 1),
(8, 12, 'LA DOUCEUR', 'Mme ALFA', '', 'Kara quartier Chaminade', '0022826601164', '', '', '', 1, 1, '2021-09-16', '0000-00-00', 1),
(9, 13, 'Mission Permanente du Togo Ã  GenÃ¨ve', 'Amb Yackoley Johnson', '', 'GENEVE SUISSE', '00415668300', '', '', '', 1, 1, '2021-09-16', '0000-00-00', 1),
(10, 14, 'Mission Permanente du Soudan du Sud Ã  G', '', '', 'GENEVE SUISSE', '00415668300', '', '', '', 1, 1, '2021-09-16', '0000-00-00', 1),
(11, 15, 'Mission Permanente du Mali  Ã  GenÃ¨ve', 'Mr Mamadou Henri KonatÃ©', '', 'GENEVE SUISSE', '00415668300', '', '', '', 1, 1, '2021-09-16', '0000-00-00', 1),
(12, 16, 'La Ronde Des Petits', '', '', 'GENEVE SUISSE', '0041766790235', '', '', '', 1, 1, '2021-09-16', '0000-00-00', 1),
(13, 17, 'Arc En Ciel Peinture', '', '', 'GENEVE SUISSE', '0041766790233', '', '', '', 1, 1, '2021-09-16', '0000-00-00', 1),
(14, 18, 'Ong  Ahdis', '', '', 'GENEVE SUISSE', '00221338245283', '', '', '', 1, 1, '2021-09-16', '0000-00-00', 1),
(15, 19, 'Mission Permanente du Nigeria Ã  GenÃ¨ve', '', '', 'GENEVE SUISSE', '0041227301414', 'ftp.nigerian-mission.ch\r\nuserÂ : bdoq_creativesweb\r\nmdpÂ : Cre@tiveWeb2021\r\n\r\nhttps://h2-phpmyadmin.infomaniak.com/index.php\r\nServer: bdoq.myd.infomaniak.com\r\nUser: bdoq_creative\r\nPsw: Cre@tive2021\r\n', '', '', 1, 1, '2021-09-16', '0000-00-00', 1),
(16, 20, 'Mission Permanente de la Zambie Ã  GenÃ¨', '', '', 'GENEVE SUISSE', '0041227614400', '', '', '', 1, 1, '2021-09-16', '0000-00-00', 1),
(17, 21, 'Limpharm', '', '', 'LOME-TOGO', '0022890063003', '', '', '', 1, 1, '2021-09-16', '0000-00-00', 1),
(18, 22, 'Gnosis Suisse', '', '', 'GENEVE SUISSE', '0041762837226', '', '', '', 1, 1, '2021-09-16', '0000-00-00', 1),
(19, 23, 'AFEMET', '', '', 'LOME-TOGO', '0022890831777', '', '', '', 1, 1, '2021-09-16', '0000-00-00', 1),
(20, 24, 'Htranslucens', '', '', 'GENEVE SUISSE', '0041791719074', '', '', '', 1, 1, '2021-09-16', '0000-00-00', 1),
(21, 25, 'Togoqueens', '', '', 'LOME-TOGO', '0022890474738', '', '', '', 1, 1, '2021-09-16', '0000-00-00', 1),
(22, 26, 'Leota hotels', '', '', 'LOME TOGO', '0022822218368', 'Nom du serveur: ftp.leotahotels.com \r\nNom d\'utilisateur: leotahotels.com\r\nMot de passe: 21MMECUX\r\nType de connexion: passive\r\n\r\n\r\nMYSQL\r\n\r\nlogin: leotahotels1\r\npwd: 9StUXXKi\r\nphpmyadmin : https://phpmyadmin.netoxygen.ch\r\nserveur: mysqldb2\r\n', '', '', 1, 1, '2021-09-27', '0000-00-00', 1),
(23, 27, 'Voyagersimple', '', '', 'LomÃ© TOGO', '0022898111008', 'Nom du serveur: ftp.voyagezsimple.com  \r\nNom d\'utilisateur: voyagezsimple.com\r\nMot de passe: BdNBEBt9\r\nType de connexion: passive\r\n\r\nMYSQL\r\n        \r\nlogin: voyagezsimple1        \r\npwd: nHQ8bSnT\r\nphpmyadmin : https://phpmyadmin.netoxygen.ch\r\nserveur: mysql_hosting2', '', '', 1, 1, '2021-09-27', '0000-00-00', 1),
(24, 28, '228hotels', '', '', 'LomÃ© TOGO', '0022890474738', 'Nom du serveur: ftp.228hotels.com \r\nNom d\'utilisateur: 228hotels.com\r\nMot de passe: 8jWJb9Pn\r\nType de connexion: passive\r\n\r\nMYSQL\r\n        \r\nlogin: 228hotels1\r\npwd: hyjnmQFX\r\nphpmyadmin : https://phpmyadmin.netoxygen.ch\r\nserveur: mysql_hosting2', '', '', 1, 1, '2021-09-27', '0000-00-00', 1),
(25, 29, 'Symphoniemag', '', '', 'LOME TOGO', '0022899669491', '\r\nNom du serveur: ftp.symphoniemag.com \r\nNom d\'utilisateur: symphoniemag.com\r\nMot de passe: dW2BdptK\r\nType de connexion: passive\r\n\r\nMYSQL\r\n        \r\nlogin: symphoniemag1\r\npwd: dc7X1XBt\r\nphpmyadmin : https://phpmyadmin.netoxygen.ch\r\nserveur: mysql_hosting2', '', '', 1, 1, '2021-09-27', '0000-00-00', 1),
(26, 30, 'Togoplay', '', '', 'LOME TOGO', '0022890474738', 'Nom du serveur: ftp.togoplay.info \r\nNom d\'utilisateur: togoplay.info\r\nMot de passe: XWHqrfmD\r\nType de connexion: passive\r\n\r\nMYSQL\r\n        \r\nlogin: togoplay1 \r\npwd: 7zmmwmic\r\nphpmyadmin : https://phpmyadmin.netoxygen.ch\r\nserveur: mysql_hosting2\r\n ', '', '', 1, 1, '2021-09-27', '0000-00-00', 1),
(27, 31, 'Permanent Mission of Sierra Leone', 'Mr Gberie', '', '', '', 'Nom du serveur: ftp.missionsierraleone.ch (seulement lorsque votre domaine aura Ã©tÃ© activÃ© chez le registrar)\r\nNom d\'utilisateur: missionsierraleone.ch\r\nMot de passe: nG5X3kZY\r\n\r\ndefined(\'DB_SERVER\') ? null : define(\"DB_SERVER\", \"mysql_hosting2\");\r\ndefined(\'DB_USER\')   ? null : define(\"DB_USER\", \"missionsierrale1\");\r\ndefined(\'DB_PASS\')   ? null : define(\"DB_PASS\", \"n68zXkeS\");', '', '', 1, 1, '2021-11-10', '0000-00-00', 1),
(28, 32, 'BDL CONSULTING', 'Boudy Diaw', '', '', '', 'Nom du serveur: ftp.cabinet-bdl.com \r\nNom d\'utilisateur: cabinet-bdl.com\r\nMot de passe: n2bzPVfd\r\nType de connexion: passive\r\n', '', '', 1, 1, '2021-11-17', '0000-00-00', 1),
(32, 48, 'Olympique de Marseille', 'Pablo LONGORIA', '', 'Marseille-FRANCE', '00332158054448', 'Plus grand club de FRance', '', '', 1, 1, '2021-11-22', '0000-00-00', 1),
(33, 50, 'Test micro', 'Pol LIROLA', '', '', '', '', '', '', 1, 1, '2021-11-23', '0000-00-00', 1),
(34, 52, 'Test', 'ValÃ¨re GERMAIN', '', '', '', '', '', '', 1, 1, '2021-11-23', '0000-00-00', 1),
(35, 53, 'Je teste frÃ¨re!!!', 'Oups', '', '', '', '', '', '', 1, 1, '2021-11-23', '2021-11-23', 1),
(36, 54, 'Testons vivant...', 'CR7', '', '', '', '', '', '', 11, 11, '2021-11-23', '2021-11-23', 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `app_requests`
--
ALTER TABLE `app_requests`
  ADD PRIMARY KEY (`app_request_id`),
  ADD KEY `app_request_service_id` (`app_request_menu_id`),
  ADD KEY `app_request_user_id` (`app_request_customer_id`);

--
-- Index pour la table `candidates`
--
ALTER TABLE `candidates`
  ADD PRIMARY KEY (`candidate_id`);

--
-- Index pour la table `contracts`
--
ALTER TABLE `contracts`
  ADD PRIMARY KEY (`contract_id`);

--
-- Index pour la table `copyright`
--
ALTER TABLE `copyright`
  ADD PRIMARY KEY (`copyright_id`);

--
-- Index pour la table `files`
--
ALTER TABLE `files`
  ADD PRIMARY KEY (`file_id`);

--
-- Index pour la table `filter`
--
ALTER TABLE `filter`
  ADD PRIMARY KEY (`filter_id`);

--
-- Index pour la table `gallery`
--
ALTER TABLE `gallery`
  ADD PRIMARY KEY (`gallery_id`);

--
-- Index pour la table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`menu_id`);

--
-- Index pour la table `page_elements`
--
ALTER TABLE `page_elements`
  ADD PRIMARY KEY (`page_element_id`);

--
-- Index pour la table `page_section`
--
ALTER TABLE `page_section`
  ADD PRIMARY KEY (`page_section_id`),
  ADD KEY `page_section_pid` (`page_section_pid`,`page_section_sid`),
  ADD KEY `page_section_ibfk_2` (`page_section_sid`);

--
-- Index pour la table `press_comments_topic`
--
ALTER TABLE `press_comments_topic`
  ADD PRIMARY KEY (`comment_topic_id`),
  ADD KEY `comment_topic_topicid` (`comment_topic_topicid`);

--
-- Index pour la table `press_etiquette`
--
ALTER TABLE `press_etiquette`
  ADD PRIMARY KEY (`etiquette_id`);

--
-- Index pour la table `press_page`
--
ALTER TABLE `press_page`
  ADD PRIMARY KEY (`page_id`);

--
-- Index pour la table `press_tag`
--
ALTER TABLE `press_tag`
  ADD KEY `press_tag_topic_id` (`press_tag_topic_id`),
  ADD KEY `press_tag_etiquette_id` (`press_tag_etiquette_id`);

--
-- Index pour la table `press_topics`
--
ALTER TABLE `press_topics`
  ADD PRIMARY KEY (`topic_id`),
  ADD KEY `topic_typetopic_id` (`topic_typetopic_id`),
  ADD KEY `topic_author` (`topic_author`);

--
-- Index pour la table `press_type_topic`
--
ALTER TABLE `press_type_topic`
  ADD PRIMARY KEY (`type_topic_id`);

--
-- Index pour la table `privileges`
--
ALTER TABLE `privileges`
  ADD PRIMARY KEY (`privilege_type_user_id`,`privilege_menu_id`),
  ADD KEY `privilege_type_user_id` (`privilege_type_user_id`),
  ADD KEY `privilege_menu_id` (`privilege_menu_id`);

--
-- Index pour la table `sections`
--
ALTER TABLE `sections`
  ADD PRIMARY KEY (`section_id`);

--
-- Index pour la table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`service_id`);

--
-- Index pour la table `socials`
--
ALTER TABLE `socials`
  ADD PRIMARY KEY (`social_id`);

--
-- Index pour la table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`ticket_id`),
  ADD KEY `ticket_service_id` (`ticket_service_id`),
  ADD KEY `ticket_user_id` (`ticket_customer_id`);

--
-- Index pour la table `topic_section`
--
ALTER TABLE `topic_section`
  ADD PRIMARY KEY (`topic_section_id`),
  ADD KEY `topic_section_pid` (`topic_section_pid`),
  ADD KEY `topic_section_sid` (`topic_section_sid`);

--
-- Index pour la table `type_user`
--
ALTER TABLE `type_user`
  ADD PRIMARY KEY (`type_user_id`);

--
-- Index pour la table `user_account`
--
ALTER TABLE `user_account`
  ADD PRIMARY KEY (`user_account_id`),
  ADD UNIQUE KEY `user_account_email` (`user_account_email`),
  ADD KEY `user_account_typeuser_id` (`user_account_typeuser_id`),
  ADD KEY `user_account_customer_number` (`user_account_customer_number`);

--
-- Index pour la table `user_info`
--
ALTER TABLE `user_info`
  ADD PRIMARY KEY (`user_info_id`),
  ADD KEY `user_info_user_account_id` (`user_info_user_account_id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `app_requests`
--
ALTER TABLE `app_requests`
  MODIFY `app_request_id` int(4) NOT NULL AUTO_INCREMENT COMMENT 'Id', AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `candidates`
--
ALTER TABLE `candidates`
  MODIFY `candidate_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT pour la table `contracts`
--
ALTER TABLE `contracts`
  MODIFY `contract_id` int(4) NOT NULL AUTO_INCREMENT COMMENT 'Id', AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT pour la table `copyright`
--
ALTER TABLE `copyright`
  MODIFY `copyright_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `files`
--
ALTER TABLE `files`
  MODIFY `file_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT pour la table `filter`
--
ALTER TABLE `filter`
  MODIFY `filter_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT pour la table `gallery`
--
ALTER TABLE `gallery`
  MODIFY `gallery_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `menu`
--
ALTER TABLE `menu`
  MODIFY `menu_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=62;

--
-- AUTO_INCREMENT pour la table `page_elements`
--
ALTER TABLE `page_elements`
  MODIFY `page_element_id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `page_section`
--
ALTER TABLE `page_section`
  MODIFY `page_section_id` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `press_comments_topic`
--
ALTER TABLE `press_comments_topic`
  MODIFY `comment_topic_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `press_etiquette`
--
ALTER TABLE `press_etiquette`
  MODIFY `etiquette_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `press_page`
--
ALTER TABLE `press_page`
  MODIFY `page_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=60;

--
-- AUTO_INCREMENT pour la table `press_topics`
--
ALTER TABLE `press_topics`
  MODIFY `topic_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `press_type_topic`
--
ALTER TABLE `press_type_topic`
  MODIFY `type_topic_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT pour la table `sections`
--
ALTER TABLE `sections`
  MODIFY `section_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT pour la table `services`
--
ALTER TABLE `services`
  MODIFY `service_id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Id', AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT pour la table `socials`
--
ALTER TABLE `socials`
  MODIFY `social_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT pour la table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `ticket_id` int(4) NOT NULL AUTO_INCREMENT COMMENT 'Id';

--
-- AUTO_INCREMENT pour la table `topic_section`
--
ALTER TABLE `topic_section`
  MODIFY `topic_section_id` int(4) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `user_account`
--
ALTER TABLE `user_account`
  MODIFY `user_account_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=55;

--
-- AUTO_INCREMENT pour la table `user_info`
--
ALTER TABLE `user_info`
  MODIFY `user_info_id` int(4) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `app_requests`
--
ALTER TABLE `app_requests`
  ADD CONSTRAINT `app_requests_ibfk_1` FOREIGN KEY (`app_request_menu_id`) REFERENCES `menu` (`menu_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `app_requests_ibfk_2` FOREIGN KEY (`app_request_customer_id`) REFERENCES `user_info` (`user_info_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `page_section`
--
ALTER TABLE `page_section`
  ADD CONSTRAINT `page_section_ibfk_1` FOREIGN KEY (`page_section_pid`) REFERENCES `press_page` (`page_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `page_section_ibfk_2` FOREIGN KEY (`page_section_sid`) REFERENCES `sections` (`section_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `press_comments_topic`
--
ALTER TABLE `press_comments_topic`
  ADD CONSTRAINT `press_comments_topic_ibfk_1` FOREIGN KEY (`comment_topic_topicid`) REFERENCES `press_topics` (`topic_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `press_topics`
--
ALTER TABLE `press_topics`
  ADD CONSTRAINT `press_topics_ibfk_1` FOREIGN KEY (`topic_typetopic_id`) REFERENCES `press_type_topic` (`type_topic_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `press_topics_ibfk_2` FOREIGN KEY (`topic_author`) REFERENCES `user_account` (`user_account_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `privileges`
--
ALTER TABLE `privileges`
  ADD CONSTRAINT `privileges_ibfk_1` FOREIGN KEY (`privilege_type_user_id`) REFERENCES `type_user` (`type_user_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `privileges_ibfk_2` FOREIGN KEY (`privilege_menu_id`) REFERENCES `menu` (`menu_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `user_account`
--
ALTER TABLE `user_account`
  ADD CONSTRAINT `user_account_ibfk_2` FOREIGN KEY (`user_account_customer_number`) REFERENCES `user_info` (`user_info_id`),
  ADD CONSTRAINT `user_account_ibfk_1` FOREIGN KEY (`user_account_typeuser_id`) REFERENCES `type_user` (`type_user_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Contraintes pour la table `user_info`
--
ALTER TABLE `user_info`
  ADD CONSTRAINT `user_info_ibfk_1` FOREIGN KEY (`user_info_user_account_id`) REFERENCES `user_account` (`user_account_id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
