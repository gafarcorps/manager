-- phpMyAdmin SQL Dump
-- version 4.9.5
-- https://www.phpmyadmin.net/
--
-- Hôte : mysql_hosting2
-- Généré le : ven. 07 jan. 2022 à 20:59
-- Version du serveur :  5.5.62-0ubuntu0.14.04.1
-- Version de PHP : 7.4.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `creativesweb_rt`
--

-- --------------------------------------------------------

--
-- Structure de la table `type_user`
--

CREATE TABLE `type_user` (
  `type_user_id` int(4) NOT NULL,
  `type_user_user_creation` int(4) NOT NULL,
  `type_user_user_last_modification` int(4) NOT NULL,
  `type_user_date_creation` date NOT NULL,
  `type_user_date_last_modification` date NOT NULL,
  `type_user_desc` varchar(20) NOT NULL,
  `type_user_deleted` tinyint(1) NOT NULL,
  `type_user_active` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `type_user`
--

INSERT INTO `type_user` (`type_user_id`, `type_user_user_creation`, `type_user_user_last_modification`, `type_user_date_creation`, `type_user_date_last_modification`, `type_user_desc`, `type_user_deleted`, `type_user_active`) VALUES
(1, 0, 0, '0000-00-00', '0000-00-00', 'root', 0, 1),
(2, 0, 0, '0000-00-00', '0000-00-00', 'admin', 0, 1),
(3, 0, 0, '0000-00-00', '0000-00-00', 'guest', 0, 1),
(4, 0, 0, '0000-00-00', '0000-00-00', 'client', 0, 1);

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `type_user`
--
ALTER TABLE `type_user`
  ADD PRIMARY KEY (`type_user_id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
