
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="#">Blog</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Réseaux sociaux</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Liste des Réseaux sociaux</header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row p-b-20">
                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group">
                                    <a href="app.php?item=formsocial&option=new" id="addRow" class="btn btn-info">
                                        Nouveau réseau social <i class="fa fa-plus"></i>
                                    </a>

                                </div>
                            </div>


                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group pull-right">
                                    <?php
                                    if(isset($_GET['id']) && empty($_GET['type'])) {
                                        $pid = $_GET['id'];
                                        echo '<a href="app.php?item=listpages" class="btn deepPink-bgcolor">Retourner à liste précédente';
                                        echo '</a>';
                                    } // enf if
                                    else{
                                         echo '<a href="app.php?item=listsections" class="btn deepPink-bgcolor">Retourner à liste précédente';
                                         echo '</a>';
                                    }
                                    ?>

                                </div>
                            </div>
                        </div> 

                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group pull-right">
                                </div>
                            </div>
                        </div>

                        <?php
                        if(isset($_GET['msg']) && $_GET['msg']=='success') {
                            echo '<span class="clsAvailable"> Enregistrement réussi. </span>';
                        }
                        ?>



                        <div class="table-scrollable">
                            <table id="example1" class="display full-width">
                                <thead>
                                <tr>
                                    <th>Position</th>
                                    <th>Nom</th>
                                    <th style="width:100px">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php

                                $found_socials = Social :: find_all();

                                 foreach ($found_socials as $found_social) {
 
                                    echo '<tr class="table-flag-blue">';
                                    echo '<td>'.($found_social->social_position).'</td>';
                                    echo '<td style="width:300px">'.($found_social->social_name).'</td>';
                                    echo '<td>';
                                    echo '<div class="btn-group">'; // Update and delete buttons
                                    echo '<a class="btn btn-small show-tooltip" title="Edit" href="app.php?item=formsocial&id=' . $found_social->social_id . '&option=edit"><i class="fa fa-edit"></i></a>';
//                                echo '<a class="btn btn-small btn-danger show-tooltip" title="Delete" href="savead.php?aid=' . $found_ad->advertise_id . '&option=delete"><i class="icon-trash"></i></a>';
                                    echo '</div>';
                                    echo '</td>';
                                    echo '</tr>';
                                }// end foreach
                                ?>
                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>
            <!-- end page content -->