    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
	<!-- start page content -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<div class="page-bar">
				<div class="page-title-breadcrumb">
					<div class=" pull-left">
						<div class="page-title">Bienvenue dans Manager</div>
					</div>
					<ol class="breadcrumb page-breadcrumb pull-right">
						<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
						</li>
						<li><a class="parent-item" href="app.php?item=listcategories">Gestionnaire des catégories</a>&nbsp;<i class="fa fa-angle-right"></i>
						</li>
						<li class="active">Editer une catégorie</li>
					</ol>
				</div>
			</div>


			<form action="savecategorie.php" class="form-horizontal" method="POST" enctype="multipart/form-data">

				<?php
				if (isset($_GET['option']) && $_GET['option'] == 'new') {
					echo '<div class="row">';
					echo '<div class="col-md-12">';
					//<!-- BEGIN PROFILE SIDEBAR -->
					echo '<div class="profile-sidebar">';
					echo '<div class="card">';
					echo '<div class="profile-usertitle">';
					echo '<div class="profile-usertitle-name"> Attributs </div>';
					echo '<div class="profile-usertitle-job"> de la nouvelle catégorie </div>';
					echo '</div>';


					echo '<div id="displayed" class="card-body no-padding height-9">';
					echo '<ul class="list-group list-group-unbordered">';
					echo '<li class="list-group-item">';
					echo '<b>Etat </b>';
					echo '<div class="profile-desc-item pull-right">';
					echo '<i class="fa fa-eye"></i> ';
					echo '</div>';
					echo '</li>';

					echo '<li class="list-group-item">';
					echo '<b>Date de création</b>';
					echo '<div class="profile-desc-item pull-right">';
					echo '<i class="fa fa-clock-o"> </i> ' . date('Y-m-d h:i:s') . ' ';
					echo '</div>';
					echo '</li>';

					echo 'Activer cette catégorie?';
					echo '<div class="controls">';
					echo '<div class="form-group">';
					echo '<div class="checkbox checkbox-icon-black">';
					echo '<input id="rememberChk1" name="categorie_active" type="checkbox" checked="checked">';
					echo '</div>';
					echo '<div class="card-body no-padding height-9">';


					echo '<ul class="list-group list-group-unbordered">';
					echo '<li class="list-group-item">';
					//	echo '<b>Position</b> <a class="pull-right"></a>';
					echo '<select name="position" class="form-control">';
					echo '<option value="1">position 1</option>';
					echo '<option value="2">position 2</option>';
					echo '<option value="3">position 3</option>';
					echo '<option value="4">position 4</option>';
					echo '<option value="5">position 5</option>';
					echo '<option value="6">position 6</option>';
					echo '<option value="7">position 7</option>';
					echo '<option value="8">position 8</option>';
					echo '<option value="9">position 9</option>';
					echo '<option value="10">position 10</option>';
					echo '</select>';
					echo '</li>';

					echo '</ul>';
					echo '</div>';

					echo '</ul><br/>';

					echo '</div>';


					echo '</div>';


					echo '</div>';


					//<!-- END BEGIN PROFILE SIDEBAR -->

					//<!-- BEGIN PROFILE CONTENT -->
					echo '<div class="profile-content">';
					echo '<div class="row">';
					echo '<div class="profile-tab-box">';
					echo '<div class="p-l-20">';
					echo '<h4> Créer une nouvelle catégorie </h4>';
					echo '</div>';
					echo '</div>';
					echo '<div class="white-box">';
					//<!-- Tab panes -->
					echo '<div class="tab-content">';
					echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

					echo '<div class="inbox-body no-pad">';
					echo '<div class="mail-list">';
					echo '<div class="compose-mail">';
					echo '<div class="form-group">';
					echo '<label for="subject" class="">Titre de la catégorie:</label>';
					echo '<input name="categorie_desc" type="text" tabindex="1" id="subject" class="form-control">';
					echo '</div>';
					echo '</div>';
					echo '</div>';

					echo '</div>';
					echo '<br>';
					echo '&nbsp;&nbsp;';
					echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';


					echo '</div>';
					echo '</div>';
					echo '</div>';
					echo '</div>';

					//<!-- END PROFILE CONTENT -->
					echo '</div>';
					echo '</div>';
					echo '</div>';
					//<!-- end page content -->
					echo '</div>';
				} //end if
				?>


				<?php
				if (isset($_GET['option']) && $_GET['option'] == 'edit' && isset($_GET['cid'])) {

					if (isset($_GET['cid'])) {
						$cid  = $_GET['cid'];
					}

					echo '<input type="hidden" name="cid" value="' . $cid . '" />';

					$found_cat = $type_topic->find_by_id($cid);


					echo '<div class="row">';
					echo '<div class="col-md-12">';
					//<!-- BEGIN PROFILE SIDEBAR -->
					echo '<div class="profile-sidebar">';
					echo '<div class="card">';
					echo '<div class="profile-usertitle">';
					echo '<div class="profile-usertitle-name"> Attributs de la page </div>';
					echo '<div class="profile-usertitle-job"> de la page ' . $found_cat->type_topic_active . ' </div>';
					echo '</div>';
					echo '<div class="card-body no-padding height-9">';

					echo '<ul class="list-group list-group-unbordered">';
					echo '<li class="list-group-item">';
					echo '<b>Etat </b>';
					echo '<div class="profile-desc-item pull-right">';
					if ($found_cat->type_topic_active == 1) {
						echo '<i class="fa fa-eye"></i> activé';
					} else {
						echo '<i class="fa fa-eye-slash"></i> désactivé';
					}

					echo '</div>';
					echo '</li>';


					echo '<li class="list-group-item btn-sweetalert">';
					if ($found_cat->type_topic_active == 1) {

						echo '<b>Déplacer dans la corbeille </b>';
						echo '<button class="btn btn-small btn-danger show-tooltip btn-sweetalert" type="button" data-type="ajax-loader-categorie-delete" data-id="'.$cid.'"><i class="fa fa-trash-o "></i></button>';
					} else {
						echo '<b>Restaurer cette page </b></b>';
						echo '<button class="btn btn-small btn-warning show-tooltip btn-sweetalert" type="button" data-type="ajax-loader-categorie-restor" data-id="'.$cid.'"><i class="fa  fa-recycle "></i></button>';
					}

					echo '</li>';

					echo '</ul><br/>';



					echo '<div class="controls">';
					echo '<div class="form-group">';
					echo '<div class="checkbox checkbox-icon-black">';
					$found_cat  = $type_topic->find_by_id($cid);
					if ($found_cat->type_topic_active == 1) {
						echo '<input id="rememberChk1" name="categorie_active" type="checkbox" checked="checked"> activé';
					} else {
						echo '<input id="rememberChk1" name="categorie_active" type="checkbox" checked="checked"> désactivé';
					}
					echo '<li class="list-group-item">';
					echo '<b>Ordre</b> <a class="pull-right"></a>';
					echo '<select name="position" class="form-control">';
					$found_cat     = $type_topic->find_by_id($cid);
					echo '<option value="' . $found_cat->type_topic_position . '">Position ' . $found_cat->type_topic_position . '</option>';
					$found_items = Filter::list_others($found_cat->type_topic_position);
					foreach ($found_items as $found_item) {
						echo '<option value="' . $found_item->filter_id . '">Position ' . $found_item->filter_desc . '</option>';
					}

					echo '</select>';
					echo '</li>';

					echo '</div>';

					//<!-- END SIDEBAR BUTTONS -->
					echo '</div>';
					echo '</div>';
					echo '</div>';

					echo '</div>';
					echo '</div>';


					//<!-- END BEGIN PROFILE SIDEBAR -->
					//<!-- BEGIN PROFILE CONTENT -->

					echo '<div class="profile-content">';
					echo '<div class="row">';
					echo '<div class="profile-tab-box">';
					echo '<div class="p-l-20">';
					echo '<h4> Modifier le contenu : ' . $found_cat->type_topic_desc . ' </h4>';

					if (isset($_GET['r']) && $_GET['r'] == 0) {
						echo '<span class="clsNotAvailable"> Cette catégorie a été déplacé dans la corbeille. </span> <br/>';
					} else if (isset($_GET['r']) && $_GET['r'] == 1) {
						echo '<span class="clsAvailable"> Cette catégorie a été restauré. </span> <br/>';
					}



					echo '<div class="btn-group">';
					echo '<a href="app.php?item=formcategorie&mid=' . $cid . '&option=new" id="addRow" class="btn btn-info">Nouvelle catégorie <i class="fa fa-plus"></i></a>';
					echo '</div>';


					echo '</div>';
					echo '</div>';


					echo '<div class="white-box">';
					//<!-- Tab panes -->
					echo '<div class="tab-content">';
					echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

					echo '<div class="inbox-body no-pad">';
					echo '<div class="mail-list">';
					echo '<div class="compose-mail">';


					echo '<div class="form-group">';
					echo '<label for="subject" class="">Titre du catégorie:</label>';
					echo '<input name="categorie_desc" id="title" value="' . $found_cat->type_topic_desc . '"  type="text" tabindex="1" id="subject"	class="form-control">';
					echo '</div>';

					echo '</div>';
					echo '</div>';
					echo '</div>';
					echo '<br>';
					echo '&nbsp;&nbsp;';
					echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';

					echo '</div>';
					echo '</div>';
					echo '</div>';
					echo '</div>';

					//<!-- END PROFILE CONTENT -->
					echo '</div>';
					echo '</div>';
					echo '</div>';
					//<!-- end page content -->

					echo '</div>';
				} // end if


				?>

			</form>

			<!-- end page container -->
