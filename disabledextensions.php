<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                            href="<?php echo $app_or_appex ?>.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="<?php echo $app_or_appex ?>.php?item=listpages">Pages</a>&nbsp;<i
                            class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Liste des menus extensions</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Liste des extensions désactivées</header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row p-b-20">
                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group">
                                    <a href="<?php echo $app_or_appex ?>.php?item=formmenu&mid=11&option=new" id="addRow" class="btn btn-info">
                                        Nouvelle extension <i class="fa fa-plus"></i>
                                    </a>

                                </div>
                                <div class="btn-group">
                                    <a href="<?php echo $app_or_appex ?>.php?item=disabledextensions" id="addRow" class="btn btn-danger"><b>Corbeille
                                        </b></a>
                                </div>
                                <div class="btn-group">
                                    <a href="<?php echo $app_or_appex ?>.php?item=listextensions" id="addRow" class="btn btn-success"><b>Liste des extensions
                                        </b></a>
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group pull-right">
                                    <?php
                                    if (isset($_GET['pid'])) {
                                        $pid = $_GET['pid'];
                                        echo '<a href="<?php echo $app_or_appex ?>.php?item=listextensions" class="btn deepPink-bgcolor">Retourner à liste précédente';
                                        echo '</a>';
                                    } // enf if
                                    ?>

                                </div>
                            </div>
                        </div>

                        <?php
                        if (isset($_GET['msg']) && $_GET['msg'] == 'success') {
                            echo '<span class="clsAvailable"> Enregistrement réussi. </span>';
                        }
                        ?>


                        <div class="row">
                            <?php
                            $menu = Menu::list_menu_extensions_inactive();
                            foreach ($menu as $mn){
                                echo '<div class="col-lg-3 col-md-6 col-12 col-sm-6">';
                                echo  '<div class="blogThumb">';
                                     if(!empty($mn->menu_picture)){
                                        echo '<div class="thumb-center">
                                            <img class="img-responsive" alt="user" src="media/files/'. $mn->menu_picture .'"/>
                                        </div>';
                                    } else {
                                        echo '<div class="thumb-center">
                                            <img class="img-responsive" alt="user" src="media/files/1.jpg"/>
                                        </div>';
                                    }
                                    
                                    echo '<div class="text-muted"><h4>NOM : '. $mn->menu_title . '</h4></div>' ;
                                    echo '<div class="text-muted">DESCRIPTION : <h4>'. $mn->menu_desc . '</h4>';
                                    echo '<h3>'. $mn->menu_extension_vedette == 1 ? '<span class="badge bagde-info"> Vedette </span>' : '<span class="badge bagde-default"> Non </span>'. '</h3></br>';
                                    echo '<a href="'. $app_or_appex .'.php?item=formmenu&mid=' . $mn->menu_id . '&option=active"" class="btn btn-success btn-rounded waves-effect waves-light m-t-20">Active extension</a>'; echo '&nbsp;&nbsp;'; 
                                    echo '</div>';
                                echo '</div>';
                            echo '</div>';
                            
                        }
                            ?>

                            <?php echo '<br/><b> </b>'; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>