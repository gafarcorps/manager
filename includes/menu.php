<?php

/**
 * Description of page
 *
 * @author Menz ALFA
 */
// Define the core paths
require_once('core_paths_definition.php');

require_once(LIB_PATH . DS . 'initialize.php');

class Menu {

    //put your code here
    protected static $table_name = "menu";
    protected static $db_fields = array(
        'menu_id',
        'menu_parent_id',
        'menu_title',
        'menu_title_en',
        'menu_title_es',
        'menu_desc',
        'menu_desc_infos',
        'menu_link',
        'menu_picture',
        'menu_position',
        'menu_created_at',
        'menu_is_extension',
        'menu_active',
        'menu_extension_vedette'
    );
    public $menu_id;
    public $menu_parent_id;
    public $menu_title;
    public $menu_title_en;
    public $menu_title_es;
    public $menu_desc;
    public $menu_desc_infos;
    public $menu_link;
    public $menu_picture;
    public $menu_position;
    public $menu_created_at;
    public $menu_is_extension;
    public $menu_extension_vedette;
    public $menu_active;

    private static function instantiate($record) {
        // Could check that $record exists and is an array
        $object = new self;
        // Simple, long-form approach:
        // $object->id 				= $record['id'];
        // $object->username 	= $record['username'];
        // $object->password 	= $record['password'];
        // $object->first_name = $record['first_name'];
        // $object->last_name 	= $record['last_name'];
        // More dynamic, short-form approach:
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    private function has_attribute($attribute) {
        // We don't care about the value, we just want to know if the key exists
        // Will return true or false
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes() {
        // return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes() {
        global $database;
        $clean_attributes = array();
        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    public static function find_by_sql($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_array($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function find_by_sql_assoc($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_assoc($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function count_by_sql($sql) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    public static function count_menus() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name . " WHERE menu_id <> 11 AND menu_id <> 21 AND menu_parent_id = 11  ORDER BY menu_id ASC";
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    public function save() {
        // A new record won't have an id yet.
        return isset($this->menu_id) ? $this->update() : $this->create();
    }

    public function create() {
        global $database;
        $attributes = $this->sanitized_attributes();
        // Construction de la requête préparée
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= implode(", ", array_keys($attributes));
        $sql .= ") VALUES (";
        $sql .= implode(", ", array_fill(0, count($attributes), "?")); // Remplacement des valeurs par des marqueurs de paramètres
        $sql .= ")";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        // Exécution de la requête avec les valeurs des attributs comme paramètres
        if ($stmt->execute(array_values($attributes))) {
            $this->menu_id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }

    public function update() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= implode(", ", $attribute_pairs);
        $sql .= " WHERE menu_id=?";
        
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
        $value = array_values($attributes);
        $value[] = $this->menu_id;
        if ($stmt->execute($value)) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }
    }

    public function delete() {
        global $database;
        $sql = "DELETE FROM " . self::$table_name;
        $sql .= " WHERE menu_id= ?";
        $sql .= " LIMIT 1";
        // Préparation de la requête permet de prevenir les injections SQL
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec l'ID comme paramètre
        if ($stmt->execute([$this->menu_id])) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }

    }

    public function unable() {
        global $database;
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql  = "UPDATE " . self::$table_name . " SET menu_active = 0 ";
        $sql .= "WHERE menu_id= ?";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
        $value = array_values($attributes);
        $value[] = $this->menu_id;
        if ($stmt->execute($value)) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }
    }

    public function recover() {
        global $database;
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql  = "UPDATE " . self::$table_name . " SET menu_active = 1 ";
        $sql .= "WHERE menu_id= ?";
       // Préparation de la requête
       $stmt = $database->prepare($sql);
        
       // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
       $value = array_values($attributes);
       $value[] = $this->menu_id;
       if ($stmt->execute($value)) {
           return ($database->affected_rows($stmt) == 1) ? true : false;
       } else {
           return false;
       }
    }

    public static function find_all() {
        // retourne tous les enregistrements actifs et supprimés
        $sql = "SELECT * FROM " . self::$table_name . " ORDER BY menu_id ASC ";
        return self::find_by_sql($sql);
    }

    public static function find_by_id($id = 0) {
        // retourne  un enregistrements actif
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE menu_id={$id}");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function find_menu_children($parent_id = 0) {
        return self::find_by_sql_assoc("SELECT * FROM " . self::$table_name . "  WHERE  menu_parent_id = {$parent_id} AND menu_parent_id <> 11   ORDER BY menu_id ASC");
    }

    public static function find_menu_parent($parent_id = 0) {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . "  WHERE menu_parent_id = {$parent_id}  ORDER BY menu_id ASC");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function find_by_user($id = 0) {
        $sql = "SELECT * FROM " . self::$table_name . " INNER JOIN privileges ON privilege_menu_id = menu_id INNER JOIN  typer_user privileges ON privilege_type_user_id = type_user_id WHERE type_user_id= {$id} AND menu_id <> 11 AND menu_parent_id = 11  ORDER BY menu_id ASC ";
        return self::find_by_sql($sql);
    }

    public static function list_menu_extensions($mid = 0) {
        $sql1 = "SELECT * FROM " . self::$table_name . " WHERE menu_id <> 11  AND menu_parent_id = 11  ORDER BY menu_id ASC ";
        $sql2 = "SELECT * FROM " . self::$table_name . " WHERE menu_id <> 11  AND menu_parent_id ={$mid}   ORDER BY menu_id ASC ";

        return !empty($mid) ? self::find_by_sql($sql2) : self::find_by_sql($sql1);
    }

    public static function list_menus() {
        $sql = "SELECT * FROM " . self::$table_name . " WHERE menu_id <> 11  AND menu_parent_id = 11  ORDER BY menu_id ASC ";
        return self::find_by_sql($sql);
    }

    public static function list_menus_active() {
        $sql = "SELECT * FROM " . self::$table_name . " WHERE menu_id <> 11  AND menu_parent_id = 11 AND menu_active = 1  ORDER BY menu_position ASC ";
        return self::find_by_sql($sql);
    }

    public static function list_extensions($mid = 0) {
        $sql = "SELECT * FROM " . self::$table_name . "  ";
        $sql .= " WHERE menu_id <> 11 AND menu_is_extension = 1 ";
        if (!empty($mid))
            $sql .= "  AND menu_parent_id = $mid ";
        else
            $sql .= "  AND menu_parent_id = 11 ";
        $sql .= "  ORDER BY menu_position ASC ";
        return self::find_by_sql($sql);
    }

    public static function list_extensions_active($mid = 0) {
        $sql = "SELECT * FROM " . self::$table_name . "  ";
        $sql .= " WHERE menu_id <> 47  AND menu_is_extension = 1 AND menu_active = 1   ";
        if (!empty($mid))
            $sql .= "  AND menu_parent_id = $mid ";
//        else
//            $sql .= "  AND menu_parent_id = 11 ";
        $sql .= "  ORDER BY menu_position ASC ";
//        echo $sql;
        return self::find_by_sql($sql);
    }

    public static function list_menu_children($id = 0) {
        $sql = "SELECT * FROM " . self::$table_name . " WHERE menu_parent_id = {$id} AND menu_parent_id <> 11  ORDER BY menu_id ASC ";
        return self::find_by_sql($sql);
    }

    public static function list_menu_children_active($id = 0) {
        $sql = "SELECT * FROM " . self::$table_name . " WHERE menu_parent_id = {$id} AND menu_parent_id <> 11 AND menu_active = 1  ORDER BY menu_position ASC ";
        return self::find_by_sql($sql);
    }

    public static function list_others($id = 0) {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE menu_id <> {$id}  ORDER BY menu_id ASC ");
    }

    public static function count_menu_children($mid = 0) {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name . " WHERE menu_parent_id = {$mid} ";
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    public static function count_menu_children_active($mid = 0) {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name . " WHERE menu_parent_id = {$mid} AND menu_active = 1 ";
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }
    public static function count_extensions_children_active($mid = 0) {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name . " WHERE menu_parent_id = {$mid} AND menu_active = 1 AND menu_is_extension = 1 ";
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    // La liste des menus qui sont une extensions
    public static function liste_menu_extensions() {
        $sql = "select * from " . self::$table_name . " where menu_is_extension = 1 ";
        return self::find_by_sql($sql);
    }

    // La liste des menus qui sont une extension mais inactive
    public static function liste_menu_extensions_inactive() {
        $sql = "select * from " . self::$table_name . " where menu_is_extension = 1 and menu_active = 0 ";
        return self::find_by_sql($sql);
    }

    public static function find_last() {
        // retourne  un enregistrements actif
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " ORDER BY menu_id DESC LIMIT 1");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

}

$menu = new Menu();
?>
