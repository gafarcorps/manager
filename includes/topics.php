
<?php

/**
 * Description of page
 *
 * @author Menz ALFA
 */
// Define the core paths
require_once('core_paths_definition.php');

require_once(LIB_PATH.DS.'initialize.php');


class Topics {

    protected static $table_name = "press_topics";
    protected static $db_fields = array('topic_id','topic_typetopic_id','topic_author', 'topic_title','topic_text','topic_image_path','topic_url','topic_date_creation','topic_top_slider','topic_top_small','topic_views','topic_icon','topic_statut','topic_active');
   
    public $topic_id;
    public $topic_typetopic_id;
    public $topic_author;
    public $topic_title;
    public $topic_text;
    public $topic_image_path;
    public $topic_url;
    public $topic_date_creation;
    public $topic_top_slider;
    public $topic_top_small;
    public $topic_views;
    public $topic_icon;
    public $topic_statut;
    public $topic_active;
    
 // Common Topics requests

  // Instanciation des attributs de la classe

    private static function instantiate($record) {
        // Could check that $record exists and is an array
                $object = new self;
            foreach ($record as $attribute => $value) {
                if ($object->has_attribute($attribute)) {
                    $object->$attribute = $value;
                }
            }
            return $object;
    }

    private function has_attribute($attribute) {

        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes() {
// return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

//    protected function sanitized_attributes() {
//        global $database;
//        $clean_attributes = array();
//         foreach ($this->attributes() as $key => $value) {
//            $clean_attributes[$key] = $database->value;
//        }
//        return $clean_attributes;
//    }
    
     protected function sanitized_attributes() {
        global $database;
        $clean_attributes = array();
        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    public static function find_by_sql($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_array($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }
    
    
    public static function count_by_sql($sql) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }
    

    public function save() { 
        return isset($this->topic_id) ? $this->update() : $this->create();
    }

    public function create() {
        global $database;
        $attributes = $this->sanitized_attributes();
        // Construction de la requête préparée
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= implode(", ", array_keys($attributes));
        $sql .= ") VALUES (";
        $sql .= implode(", ", array_fill(0, count($attributes), "?")); // Remplacement des valeurs par des marqueurs de paramètres
        $sql .= ")";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        // Exécution de la requête avec les valeurs des attributs comme paramètres
        if ($stmt->execute(array_values($attributes))) {
            $this->topic_id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }
    

    public function update() {
        global $database;
        
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= implode(", ", $attribute_pairs);
        $sql .= " WHERE topic_id=?";
        
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
        $value = array_values($attributes);
        $value[] = $this->topic_id;
        if ($stmt->execute($value)) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }
    }
    
    public function delete() {
        global $database;
        
        $sql = "DELETE FROM " . self::$table_name . " WHERE topic_id=?";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec l'ID comme paramètre
        if ($stmt->execute([$this->topic_id])) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }
    }
    
 //  Specific Topics requests
   
    public static function find_all($limit = 0) {
        global $database;
        $sql = "SELECT * FROM " . self::$table_name . " ORDER BY topic_id DESC";
        if ($limit > 0) {
            $sql .= " LIMIT {$limit}";
        }
        return self::find_by_sql($sql);
    }

    public static function find_all_active($limit = 0) {
        global $database;
        $year = date('Y');
        $sql = "SELECT * FROM " . self::$table_name . " WHERE topic_active = 1 AND topic_date_creation LIKE '%{$year}%' ORDER BY topic_date_creation DESC";
        if ($limit > 0) {
            $sql .= " LIMIT {$limit}";
        }
        return self::find_by_sql($sql);
    }

    public static function find_all_unabled($limit = 0) {
        global $database;
        $sql = "SELECT * FROM " . self::$table_name . " WHERE topic_active = 0 ORDER BY topic_date ";
        if ($limit > 0) {
            $sql .= "LIMIT {$limit}";
        }
        return self::find_by_sql($sql);
    }

    public static function find_by_typetopic($id = 0, $limit = 0) {
        global $database;
        $year = date('Y');
        $sql = "SELECT * FROM " . self::$table_name . " WHERE topic_typetopic_id = ? AND topic_active = 1 ORDER BY topic_id DESC";
        if ($limit > 0) {
            $sql .= " LIMIT ?";
            return self::find_by_sql($sql, [$id, $limit]);
        } else {
            return self::find_by_sql($sql, [$id]);
        }
    }
    
    public static function find_typetopic_top_small($id = 0, $limit = 0) {
        global $database;
        $sql = "SELECT * FROM " . self::$table_name . " WHERE topic_typetopic_id = ? AND topic_top_small = 1 AND topic_active = 1 ORDER BY topic_id DESC";
        if ($limit > 0) {
            $sql .= " LIMIT ?";
            return self::find_by_sql($sql, [$id, $limit]);
        } else {
            return self::find_by_sql($sql, [$id]);
        }
    }
    
    public static function find_last_by_typetopic($limit = 0) {
        global $database;
        $sql = "SELECT * FROM " . self::$table_name . " WHERE topic_typetopic_id NOT IN (6, 5, 4, 1, 13) AND topic_active = 1 GROUP BY topic_typetopic_id ORDER BY topic_typetopic_id DESC";
        if ($limit > 0) {
            $sql .= " LIMIT ?";
            return self::find_by_sql($sql, [$limit]);
        } else {
            return self::find_by_sql($sql);
        }
    }    

    //BY FOFANA   // LISTE DES ARCHIVES COTE MANAGER
    public static function find_all_archive() {
        global $database;
        $year = date('Y');
        $sql = "SELECT * FROM " . self::$table_name . " WHERE topic_active = 1 AND NOT LIKE topic_date_creation '%" . $year . "%' ORDER BY topic_date_creation DESC";
        return self::find_by_sql($sql);
    }

    public static function find_by_topsmalltopic($limit = 0) {
        global $database;
        $sql = "SELECT * FROM " . self::$table_name . " WHERE topic_top_small = 1 AND topic_active = 1 ORDER BY topic_id DESC";
        if ($limit > 0) {
            $sql .= " LIMIT ?";
            return self::find_by_sql($sql, [$limit]);
        } else {
            return self::find_by_sql($sql);
        }
    }

    public static function find_by_id($id = 0) {
        global $database;
        $sql = "SELECT * FROM " . self::$table_name . " WHERE topic_id = ? LIMIT 1";
        $result_array = self::find_by_sql($sql, [$id]);
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function list_others($id = 0) {
        global $database;
        $sql = "SELECT * FROM " . self::$table_name . " WHERE topic_top_slider = 1 AND topic_id <> ? AND topic_active = 1";
        return self::find_by_sql($sql, [$id]);
    }

    public static function count_all_active() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name . " WHERE topic_active = 1";
        $result_set = $database->prepare($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }
    
    public static function count_all_unabled() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name . " WHERE topic_active = 0";
        $result_set = $database->prepare($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }
    
    public static function count_by_typetopic($id = 0) {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name . " WHERE topic_typetopic_id = ? AND topic_published = 1 AND topic_active = 1";
        $result_set = $database->prepare($sql, [$id]);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }    

    public static function count_all() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name;
        $result_set = $database->prepare($sql);
        $row = $database->fetch_array($result_set);
        return array_shift($row);
    }
    
    public static function find_topic_children($parent_id = 0) {
        global $database;
        $sql = "SELECT * FROM " . self::$table_name . " WHERE topic_parent_id = ?";
        $result_set = $database->prepare($sql, [$parent_id]);
        $object_array = [];
        while ($row = $database->fetch_assoc($result_set)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }
    
    public static function find_by_keywords($search = "") {
        global $database;
        $sql = "SELECT topic_id FROM " . self::$table_name . " WHERE topic_published = 1 AND (topic_title_fr LIKE ? OR topic_text_fr LIKE ?)";
        $result_set = $database->prepare($sql, ["%$search%", "%$search%"]);
        return $result_set;
    }
    
    public static function find_headlines($id = 0, $limit = 0) {
        global $database;
        $year = date('Y');
        $sql = "SELECT * FROM " . self::$table_name . " WHERE topic_active = 1 AND topic_typetopic_id = ? AND topic_date_creation LIKE ?";
        if ($limit > 0) {
            $sql .= " LIMIT ?";
            $result_set = $database->prepare($sql, [$id, "%$year%", $limit]);
        } else {
            $result_set = $database->prepare($sql, [$id, "%$year%"]);
        }
        $object_array = [];
        while ($row = $database->fetch_assoc($result_set)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }
    
    public static function find_last_topic() {
        global $database;
        $sql = "SELECT * FROM " . self::$table_name . " WHERE topic_active = ? ORDER BY topic_date DESC LIMIT 1";
        $result_set = $database->prepare($sql, [1]);
        $row = $database->fetch_assoc($result_set);
        return !empty($row) ? self::instantiate($row) : false;
    }
    
    public static function find_last_top_topic($limit = 0) {
        global $database;
        $sql = "SELECT * FROM " . self::$table_name . " WHERE topic_active = ? AND topic_top_slider = ? ORDER BY topic_date_creation DESC LIMIT ?";
        $result_set = $database->prepare($sql, [1, 1, $limit]);
        $object_array = [];
        while ($row = $database->fetch_assoc($result_set)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }
    
    public static function list_other_top_topics() {
        global $database;
        $sql = "SELECT * FROM " . self::$table_name . " WHERE topic_active = ? AND topic_top_slider = ? ORDER BY topic_id DESC LIMIT 1";
        $result_set = $database->prepare($sql, [1, 1]);
        $row = $database->fetch_assoc($result_set);
        return !empty($row) ? self::instantiate($row) : false;
    }
    
    public static function list_other_topics($id, $limit, $type_topic_id = null) {
        global $database;
        if ($type_topic_id) {
            $sql = "SELECT * FROM " . self::$table_name . " WHERE topic_id <> ? AND topic_active = ? AND topic_typetopic_id = ? ORDER BY topic_id DESC LIMIT ?";
            $result_set = $database->prepare($sql, [$id, 1, $type_topic_id, $limit]);
        } else {
            $sql = "SELECT * FROM " . self::$table_name . " WHERE topic_id <> ? AND topic_active = ? ORDER BY topic_id DESC LIMIT ?";
            $result_set = $database->prepare($sql, [$id, 1, $limit]);
        }
        $object_array = [];
        while ($row = $database->fetch_assoc($result_set)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }
    
    public function has_comments() {
        global $database;
        $sql = "SELECT * FROM press_comments_topic WHERE comment_topic_topicid = ?";
        $result_set = $database->prepare($sql, [$this->topic_id]);
        return $database->num_rows($result_set) > 0;
    }    

    public static function find_last_comment_topics($limit = 4) {
        $headlines = self::find_headlines($limit);
        $result_array = [];
        foreach ($headlines as $headline) {
            if ($headline->has_comments()) {
                $result_array[] = $headline;
            }
        }
        return $result_array;
    }
    
    // BY FOFANA / VERIFIE LA QUALITE DE L'ARTICLE
    public static function verify_topic($image, $title, $text) {
        if (empty($image)) {
            return '<span class="label label-sm label-warning"> Image manquante </span>';
        } elseif (empty($title)) {
            return '<span class="label label-sm label-warning"> Titre manquant </span>';
        } elseif (empty($text)) {
            return '<span class="label label-sm label-danger"> Texte manquant </span>';
        } elseif (!empty($image) && !empty($image) && !empty($image)) {
            return '<span class="label label-sm label-success">Impeccable</span>';
        }
    }
    
    public function author_name($id) {
        $author = User_info::find_by_id($id);
        return $author ? $author->full_name() : '';
    }    

    public static function find_by_type_topic_title($desc) {
        $type_topic = Type_topic::find_by_sql("SELECT * FROM press_type_topic WHERE type_topic_desc = :desc LIMIT 1", [':desc' => $desc]);
        $type_topic = array_shift($type_topic);
        if ($type_topic) {
            return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE topic_typetopic_id = :id", [':id' => $type_topic->type_topic_id]);
        } else {
            return [];
        }
    }
    
    public static function find_by_type_topic_id($id) {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE topic_typetopic_id = :id LIMIT 8", [':id' => $id]);
    }
    

}
   $topic = new Topics();

  /*  $methods = get_class_methods('Topics'); 
    foreach($methods as $method){
        echo $method .'<br/> <br/>';
        }
  */
?>