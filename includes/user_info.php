
<?php

// Define the core paths
require_once('core_paths_definition.php');

require_once(LIB_PATH . DS . 'initialize.php');

class User_info {

    protected static $table_name = "user_info";
    protected static $db_fields = array(
        'user_info_id',
        'user_info_user_account_id',
        'user_info_firstname',
        'user_info_lastname',
        'user_info_address',
        'user_info_tel',
        'user_info_desc',
        'user_info_customer_url',
        'user_info_image',
        'user_info_user_creation',
        'user_info_logged_in',
        'user_info_date_creation',
        'user_info_date_last_connexion',
        'user_info_active'
    );
    public $user_info_id;
    public $user_info_user_account_id;
    public $user_info_firstname;
    public $user_info_lastname;
    public $user_info_address;
    public $user_info_tel;
    public $user_info_desc;
    public $user_info_customer_url;
    public $user_info_image;
    public $user_info_user_creation;
    public $user_info_logged_in;
    public $user_info_date_creation;
    public $user_info_date_last_connexion;
    public $user_info_active;

    private static function instantiate($record) {
        // Could check that $record exists and is an array
        $object = new self;
        // Simple, long-form approach:
        // $object->id 				= $record['id'];
        // $object->username 	= $record['username'];
        // $object->password 	= $record['password'];
        // $object->first_name = $record['first_name'];
        // $object->last_name 	= $record['last_name'];
        // More dynamic, short-form approach:
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    private function has_attribute($attribute) {
        // We don't care about the value, we just want to know if the key exists
        // Will return true or false
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes() {
        // return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes() {
        global $database;
        $clean_attributes = array();
        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    public function save() {
        // A new record won't have an id yet.
        return isset($this->user_info_id) ? $this->update() : $this->create_user();
    }
    
    public function create_user() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - INSERT INTO table (key, key) VALUES ('value', 'value')
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        // Construction de la requête préparée
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= implode(", ", array_keys($attributes));
        $sql .= ") VALUES (";
        $sql .= implode(", ", array_fill(0, count($attributes), "?")); // Remplacement des valeurs par des marqueurs de paramètres
        $sql .= ")";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        // Exécution de la requête avec les valeurs des attributs comme paramètres
        if ($stmt->execute(array_values($attributes))) {
            $this->user_info_id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }

    public function update() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= implode(", ", $attribute_pairs);
        $sql .= " WHERE user_info_id=?";
        
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
        $value = array_values($attributes);
        $value[] = $this->user_info_id;
        if ($stmt->execute($value)) {
            return ($stmt->rowCount() == 1) ? true : false;
        } else {
            return false;
        }
    }

    public function delete() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - DELETE FROM table WHERE condition LIMIT 1
        // - escape all values to prevent SQL injection
        // - use LIMIT 1
        $sql = "DELETE FROM " . self::$table_name . " WHERE user_info_id=?";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec l'ID comme paramètre
        if ($stmt->execute([$this->user_info_id])) {
            return ($stmt->rowCount() == 1) ? true : false;
        } else {
            return false;
        }

        // NB: After deleting, the instance of User_account still 
        // exists, even though the database entry does not.
        // This can be useful, as in:
        //   echo $user->first_name . " was deleted";
        // but, for example, we can't call $user->update() 
        // after calling $user->delete().
    }

    public static function find_by_sql($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_array($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function count_by_sql($sql) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    public function full_name() {
        if (isset($this->user_info_firstname) && isset($this->user_info_lastname)) {
            return trim($this->user_info_firstname) . " " . trim($this->user_info_lastname);
        } else {
            return "";
        }
    }

    public static function count_all() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    // Common Database Methods
    public static function find_all() {
        // retourne tous les enregistrements actifs et supprimés
        //echo '>>'."SELECT * FROM " . self::$table_name . "  ";
        return self::find_by_sql("SELECT * FROM " . self::$table_name . "  ");
    }

    public static function find_all_active() {
        // retourne tous les enregistrements actifs
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE user_info_active = 1  ");
    }
    public static function find_all_active_by_customer($customer_id = 0) {
        // retourne tous les enregistrements actifs
//        echo "SELECT * FROM " . self::$table_name . " WHERE user_info_user_creation = $customer_id AND user_info_active = 1  ";
        $sql = "SELECT * FROM " . self::$table_name . " WHERE user_info_user_creation = ? AND user_info_active = 1  ";
        return self::find_by_sql($sql, [$customer_id]);
    }

    public static function find_all_unabled() {
        // retourne tous les enregistrements supprimés
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE user_info_active = 0  ");
    }

    public static function find_by_id($id = 0) {
        // retourne  un enregistrements actif
        global $database;
        $sql = "SELECT * FROM " . self::$table_name . " WHERE user_info_id = ?";
        $result_array = self::find_by_sql($sql, [$id]);
        return !empty($result_array) ? array_shift($result_array) : false;
    }
    
     public static function find_last($user_info_user_creation) {
        // retourne  un enregistrements actif
//        echo "SELECT * FROM " . self::$table_name . " ORDER BY user_info_id DESC LIMIT 1"; 
         if(!empty($user_account_user_creation))
             $url_add = " WHERE user_info_user_creation = $user_info_user_creation ";
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " $url_add ORDER BY user_info_id DESC LIMIT 1");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function find_user_infos($id = 0) {
        // retourne  un enregistrements actif
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE user_info_user_account_id={$id}");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function list_others($id = 0) {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE user_info_id<>{$id} AND user_info_active = 1");
    }
    
    public static function find_by_user_account($user_account_id = 0) {
        // retourne  un enregistrements actif
//        echo "SELECT * FROM " . self::$table_name . " WHERE user_info_active = 1 AND user_info_user_account_id={$user_account_id}";
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE user_info_active = 1 AND user_info_user_account_id={$user_account_id}");
        return !empty($result_array) ? array_shift($result_array) : false;
    }
    public static function find_by_user_account_even_inactive($user_account_id = 0) {
        // retourne  un enregistrements actif
//        echo "SELECT * FROM " . self::$table_name . " WHERE user_info_active = 1 AND user_info_user_account_id={$user_account_id}";
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE user_info_user_account_id={$user_account_id}");
        return !empty($result_array) ? array_shift($result_array) : false;
    }
    
}

//  $methods = get_class_methods('User_info'); 
//    foreach($methods as $method){
//        echo $method .'<br/> <br/>';
//        }


?>