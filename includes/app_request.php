<?php

/**
 * Description of app_request
 *
 * @author D9
 */
// Define the core paths
require_once('core_paths_definition.php');

require_once(LIB_PATH.DS.'initialize.php');

class App_request {
    

    // app_request attributes
    protected static $table_name = "app_requests";
    protected static $db_fields = array(
      'app_request_id',
      'app_request_customer_id',
      'app_request_menu_id',
      'app_request_title',
      'app_request_desc',
      'app_request_date',
      'app_request_type',
      'app_request_image',
      'app_request_user_creation',      
      'app_request_date_creation',      
      'app_request_user_last_modification',      
      'app_request_date_last_modification',      
      'app_request_deleted',      
      'app_request_active'
        );
    public $app_request_id;
    public $app_request_customer_id;
    public $app_request_menu_id;
    public $app_request_title;
    public $app_request_desc;
    public $app_request_date;
    public $app_request_type;
    public $app_request_image;
    public $app_request_user_creation;
    public $app_request_date_creation;
    public $app_request_user_last_modification;
    public $app_request_date_last_modification;
    public $app_request_deleted;
    public $app_request_active;

    // Instanciation des attributs de la classe

    private static function instantiate($record) {
        // permet de retourner un enregistrement d'une table dans une instance de la classe
        $object = new self;
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    // Assigne tous les champs de table aux attributs de la classe
    private function has_attribute($attribute) {
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes() {
        // return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes() {
        // La fonction sanitize permet d'échapper les caratères spéciaux des
        global $database;
        $clean_attributes = array();
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] =  $database->escape_value($value);
        }
        return $clean_attributes;
    }

    // Common database methods

    public static function find_by_sql($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_array($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function find_by_sql_assoc($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_assoc($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function count_by_sql($sql) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    public function save() {
        return isset($this->app_request_id) ? $this->update() : $this->create();
    }

    public function create() {
        global $database;
        $attributes = $this->sanitized_attributes();
        // Construction de la requête préparée
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= implode(", ", array_keys($attributes));
        $sql .= ") VALUES (";
        $sql .= implode(", ", array_fill(0, count($attributes), "?")); // Remplacement des valeurs par des marqueurs de paramètres
        $sql .= ")";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        // Exécution de la requête avec les valeurs des attributs comme paramètres
        if ($stmt->execute(array_values($attributes))) {
            $this->app_request_id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }

    public function update() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= implode(", ", $attribute_pairs);
        $sql .= " WHERE app_request_id=?";
        
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
        $value = array_values($attributes);
        $value[] = $this->app_request_id;
        if ($stmt->execute($value)) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }
    }

    public function unable() {
        global $database;
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql  = "UPDATE " . self::$table_name . " SET app_request_deleted = 0 ";
        $sql .= "WHERE app_request_id= ?";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
        $value = array_values($attributes);
        $value[] = $this->app_request_id;
        if ($stmt->execute($value)) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }
    }

    public function recover() {
        global $database;
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql  = "UPDATE " . self::$table_name . " SET app_request_deleted = 1 ";
        $sql .= "WHERE app_request_id= ?";
       // Préparation de la requête
       $stmt = $database->prepare($sql);
        
       // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
       $value = array_values($attributes);
       $value[] = $this->app_request_id;
       if ($stmt->execute($value)) {
           return ($database->affected_rows($stmt) == 1) ? true : false;
       } else {
           return false;
       }
    }

    public function delete() {
        global $database;
        $sql = "DELETE FROM " . self::$table_name;
        $sql .= " WHERE app_request_id= ?";
        $sql .= " LIMIT 1";
        // Préparation de la requête permet de prevenir les injections SQL
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec l'ID comme paramètre
        if ($stmt->execute([$this->app_request_id])) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }

    }

    public static function find_all() {
        // retourne tous les enregistrements actifs et supprimés
        return self::find_by_sql("SELECT * FROM " . self::$table_name . "  ORDER BY app_request_date DESC ");
    }

    public static function find_all_active() {
        // retourne tous les enregistrements actifs
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE app_request_active = 1 AND app_request_deleted = 0  ORDER BY app_request_date DESC ");
    }
    public static function find_all_by_menu($menu_id = 0) {
        // retourne tous les enregistrements actifs
//        echo '>>'."SELECT * FROM " . self::$table_name . " WHERE app_request_menu_id = $menu_id AND app_request_active = 1 AND app_request_deleted = 0  ORDER BY app_request_date DESC ";
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE app_request_menu_id = $menu_id AND app_request_active = 1 AND app_request_deleted = 0  ORDER BY app_request_date DESC ");
    }
    public static function find_all_active_by_customer_and_type($customer_id = 0, $type = 0) {
        // retourne tous les enregistrements actifs
//        echo "SELECT * FROM " . self::$table_name . " WHERE app_request_customer_id = $customer_id AND app_request_type = $type AND app_request_active = 1 AND app_request_deleted = 0  ORDER BY app_request_date DESC ";
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE app_request_customer_id = $customer_id AND app_request_type = $type AND app_request_active = 1 AND app_request_deleted = 0  ORDER BY app_request_date DESC ");
    }

    public static function find_all_unabled() {
        // retourne tous les enregistrements supprimés
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE app_request_active = 0 ORDER BY app_request_date DESC ");
    }
    public static function find_all_not_deleted_by_type($type = 0) {
        // retourne tous les enregistrements actifs
//        echo "SELECT * FROM " . self::$table_name . " WHERE app_request_deleted = 0  ORDER BY app_request_date DESC ";
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE app_request_type = $type AND app_request_deleted = 0  ORDER BY app_request_date DESC ");
    }
    public static function find_all_not_deleted() {
        // retourne tous les enregistrements actifs
//        echo "SELECT * FROM " . self::$table_name . " WHERE app_request_deleted = 0  ORDER BY app_request_date DESC ";
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE app_request_deleted = 0  ORDER BY app_request_date DESC ");
    }

    public static function find_all_deleted() {
        // retourne tous les enregistrements supprimés
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE app_request_deleted = 1 ORDER BY app_request_date DESC ");
    }

    public static function find_by_id($id = 0) {
        // retourne  un enregistrements actif
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE app_request_id={$id}");
        return !empty($result_array) ? array_shift($result_array) : false;
    }
    public static function find_active_by_customer_id($customer_id = 0, $type = 0) {
        // retourne  un enregistrements actif
//        echo "SELECT * FROM " . self::$table_name . " WHERE app_request_customer_id = $customer_id AND app_request_active = 1 AND app_request_deleted = 0  ORDER BY app_request_date DESC LIMIT 1 ";
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE app_request_customer_id = $customer_id AND app_request_active = 1 AND app_request_deleted = 0  ORDER BY app_request_date DESC LIMIT 1 ");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function list_others($id = 0) {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE app_request_id <> {$id} AND app_request_active = 1");
    }
 
    
    public static function count_all() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }
    public static function count_active() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name." WHERE app_request_active = 1 AND app_request_deleted = 0";
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }
    public static function count_by_menu_and_customer($menu_id = 0, $customer_id = 0) {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name." WHERE app_request_customer_id = $customer_id AND app_request_menu_id = $menu_id AND app_request_deleted = 0";
//        echo $sql;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }
     public static function find_by_menu_and_customer($menu_id = 0, $customer_id = 0) {
        // retourne  un enregistrements actif
//         echo "SELECT * FROM " . self::$table_name . " WHERE app_request_customer_id = $customer_id AND  app_request_menu_id = $menu_id AND app_request_deleted = 0 AND app_request_active = 1 ";
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE app_request_customer_id = $customer_id AND  app_request_menu_id = $menu_id AND app_request_deleted = 0 AND app_request_active = 1 ");
        return !empty($result_array) ? array_shift($result_array) : false;
    }
}

 $app_request = new App_request();

?>
