<?php

require_once('initialize.php');

class Gallery {

    protected static $table_name = "gallery";
    protected static $db_fields = array(
        'gallery_id',
        'gallery_image_path',
        'gallery_active',
        'gallery_parent_id',
        'gallery_name',
        'gallery_desc',
        'gallery_position',
        'gallery_image_link',
        'gallery_video_link'
    );
    public $gallery_id;
    public $gallery_image_path;
    public $gallery_parent_id;
    public $gallery_name;
    public $gallery_desc;
    public $gallery_position;
    public $gallery_active;
    public $gallery_image_link;
    public $gallery_video_link;

    // Common Gallery requests
    // Instanciation des attributs de la classe

    private static function instantiate($record) {
        // Could check that $record exists and is an array
        $object = new self;
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    private function has_attribute($attribute) {

        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes() {
// return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

//    protected function sanitized_attributes() {
//        global $database;
//        $clean_attributes = array();
//         foreach ($this->attributes() as $key => $value) {
//            $clean_attributes[$key] = $database->value;
//        }
//        return $clean_attributes;
//    }

    protected function sanitized_attributes() {
        global $database;
        $clean_attributes = array();
        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    public static function find_by_sql($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_array($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function find_by_sql_assoc($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_assoc($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function count_by_sql($sql) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    public function save() {
        return isset($this->gallery_id) ? $this->update() : $this->create();
    }

    public function create() {
        global $database;
        $attributes = $this->sanitized_attributes();
        // Construction de la requête préparée
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= implode(", ", array_keys($attributes));
        $sql .= ") VALUES (";
        $sql .= implode(", ", array_fill(0, count($attributes), "?")); // Remplacement des valeurs par des marqueurs de paramètres
        $sql .= ")";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        // Exécution de la requête avec les valeurs des attributs comme paramètres
        if ($stmt->execute(array_values($attributes))) {
            $this->gallery_id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }

    public function update() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= implode(", ", $attribute_pairs);
        $sql .= " WHERE gallery_id=?";
        
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
        $value = array_values($attributes);
        $value[] = $this->gallery_id;
        if ($stmt->execute($value)) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }
    }

    public function delete() {
        global $database;
        $sql = "DELETE FROM " . self::$table_name;
        $sql .= " WHERE gallery_id= ?";
        $sql .= " LIMIT 1";
        // Préparation de la requête permet de prevenir les injections SQL
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec l'ID comme paramètre
        if ($stmt->execute([$this->gallery_id])) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }

    }
    //  Specific Gallery requests

    public static function find_all($limit = 0) {
        // retourne tous les enregistrements actifs et supprimés
        return ($limit == 0) ? self::find_by_sql("SELECT * FROM " . self::$table_name . " ORDER BY gallery_id DESC") : self::find_by_sql("SELECT * FROM " . self::$table_name . " ORDER BY gallery_id DESC LIMIT {$limit} ");
    }

    public static function find_all_albums($limit = 0) {
        // retourne tous les albums actifs et supprimés
        return ($limit == 0) ? self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE gallery_parent_id=0 ORDER BY gallery_id DESC") : self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE gallery_parent_id=0 ORDER BY gallery_id DESC LIMIT {$limit} ");
    }

    public static function find_all_albums_active($limit = 0) {
        // retourne tous les albums actifs et supprimés
        return ($limit == 0) ? self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE gallery_parent_id=0 AND gallery_active=1 ORDER BY gallery_id DESC") : self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE gallery_parent_id=0 AND gallery_active=1 ORDER BY gallery_id DESC LIMIT {$limit} ");
    }

    public static function find_all_photos($parent_id, $limit = 0) {
        // retourne toutes les photos actifs et supprimés d'un album
        return ($limit == 0) ? self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE gallery_parent_id={$parent_id} ORDER BY gallery_id") : self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE gallery_parent_id={$parent_id} ORDER BY gallery_id DESC LIMIT {$limit} ");
    }

    public static function find_all_photos_active($parent_id, $limit = 0) {
        // retourne toutes les photos actifs et supprimés d'un album
        return ($limit == 0) ? self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE gallery_parent_id={$parent_id} AND gallery_active=1 ORDER BY gallery_position") : self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE gallery_parent_id={$parent_id} AND gallery_active=1 ORDER BY gallery_id DESC LIMIT {$limit} ");
    }

    public static function find_all_active($limit = 0) {
        // retourne tous les enregistrements actifs
        $sql1 = "SELECT * FROM " . self::$table_name . " WHERE gallery_active = 1 ";
        $sql2 = "SELECT * FROM " . self::$table_name . " WHERE gallery_active = 1 LIMIT {$limit} ";

        return ($limit == 0) ? self::find_by_sql($sql1) : self::find_by_sql($sql2);
    }

    public static function find_all_unabled($limit = 0) {
        // retourne tous les enregistrements supprimés
        $sql1 = "SELECT * FROM " . self::$table_name . " WHERE gallery_active = 0  ASC ";
        $sql2 = "SELECT * FROM " . self::$table_name . " WHERE gallery_active = 0 DESC LIMIT {$limit}  ";

        return ($limit == 0) ? self::find_by_sql($sql1) : self::find_by_sql($sql2);
    }

    public static function find_by_id($id = 0) {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE gallery_id={$id} LIMIT 1");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function list_others($id = 0) {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " where gallery_published = 1  AND gallery_id <> {$id} AND gallery_active = 1");
    }

    public static function count_all_active() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name . " ";
        $sql .= "WHERE gallery_active = 1 ORDER BY gallery_date DESC";
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    public static function count_all() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    public static function find_last_gallery() {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . "  ORDER BY gallery_id DESC LIMIT 1");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function find_by_album_id($aid = 0, $limit = 0) {
        // retourne tous les enregistrements actifs et supprimés
        return ($limit == 0) ? self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE gallery_parent_id={$aid} ORDER BY gallery_position ASC") : self::find_by_sql("SELECT * FROM " . self::$table_name . "  WHERE gallery_parent_id={$aid} ORDER BY gallery_position ASC LIMIT {$limit} ");
    }

    public static function find_by_album_id_and_position($id = 0, $position = 0) {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE gallery_parent_id={$id} AND gallery_position = $position LIMIT 1");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

}

$gallery = new Gallery();
?>