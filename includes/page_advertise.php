<?php

// Define the core paths
require_once('core_paths_definition.php');

require_once(LIB_PATH.DS.'initialize.php');

class Advertise {
	
	protected static $table_name="advertise";
    protected static $db_fields=array(
        'advertise_id',
        'advertise_typebanner_id',
        'advertise_customer',
        'image_path',
        'image_type',
        'image_size',
        'image_caption',
        'advertise_title',
        'advertise_link',
        'advertise_datestart',
        'advertise_dateend',
        'advertise_position',
        'advertise_active',
        'advertise_date_creation',
        'advertise_payed',
        'advertise_pack_id'
    );

    public $advertise_id;
	public $advertise_typebanner_id;
	public $advertise_customer;
	public $image_path;
	public $image_type;
	public $image_size;
	public $image_caption;	
	public $advertise_title;	 
	public $advertise_link;	
	public $advertise_datestart;
	public $advertise_dateend;
	public $advertise_position;
	public $advertise_active;
	public $advertise_payed;
	public $advertise_date_creation;
	public $advertise_pack_id;
	private $temp_path;
    protected $upload_dir="../pub/";
    public $errors=array();
  
  protected $upload_errors = array(
		// http://www.php.net/manual/en/features.file-upload.errors.php
	  UPLOAD_ERR_OK 				=> "No errors.",
	  UPLOAD_ERR_INI_SIZE  	=> "Larger than upload_max_filesize.",
	  UPLOAD_ERR_FORM_SIZE 	=> "Larger than form MAX_FILE_SIZE.",
	  UPLOAD_ERR_PARTIAL 		=> "Partial upload.",
	  UPLOAD_ERR_NO_FILE 		=> "No file.",
	  UPLOAD_ERR_NO_TMP_DIR => "No temporary directory.",
	  UPLOAD_ERR_CANT_WRITE => "Can't write to disk.",
	  UPLOAD_ERR_EXTENSION 	=> "File upload stopped by extension."
	);

	// Pass in $_FILE(['uploaded_file']) as an argument
  public function attach_file($file) {
		// Perform error checking on the form parameters
		if(!$file || empty($file) || !is_array($file)) {
		  // error: nothing uploaded or wrong argument usage
		  echo "No file was uploaded."; 
		  $this->errors[] = "No file was uploaded.";
		  return false;
		} elseif($file['error'] != 0) {
		  // error: report what PHP says went wrong
		  $this->errors[] = $this->upload_errors[$file['error']];
		  echo $this->upload_errors[$file['error']];
		  return false;
		} else {
			// Set object attributes to the form parameters.
		  $this->temp_path        = $file['tmp_name'];
		  $this->image_path       = basename($file['name']);
		  $this->image_type       = $file['type'];
		  $this->image_size       = $file['size'];
		 
			// Don't worry about saving anything to the database yet.
			return true;

		}
	}
  
	public function save() {
		// A new record won't have an advertise_id yet.
		if(isset($this->advertise_id)) {
			
			// Really just to update the caption
			//$this->update();
			if(!empty($this->errors)) { return false; }
		  
			// Make sure the caption is not too long for the DB
		  if(strlen($this->image_caption) > 255) {
				$this->errors[] = "The caption can only be 255 characters long.";
				return false;
			}
		  
		  // Can't save without image_path and temp location
		  if(empty($this->image_path) || empty($this->temp_path)) {
		    
			$this->errors[] = "The file location was not available.";
		    return false;
			}
			
			// Determine the target_path 
		  $target_path = $this->upload_dir .'/'. $this->image_path;
		  
		  // Make sure a file doesn't already exist in the target location
		  if(file_exists($target_path)) {
			echo "The file {$this->image_path} already exists.";
		    $this->errors[] = "The file {$this->image_path} already exists.";
		    return false;
		  }
		
			// Attempt to move the file 
			
			if(move_uploaded_file($this->temp_path, $target_path)) {
		  	  
			// Success
				// Save a corresponding entry to the database
				if($this->update()) {
					// We are done with temp_path, the file isn't there anymore
					unset($this->temp_path);
					return true;
				}
			} else {  
				// File was not moved.
		    $this->errors[] = "The file upload failed, possibly due to incorrect permissions on the upload folder.";
		    
			return false;
			}
		    /** end if	**/
		} else {
			// Make sure there are no errors
			
			// Can't save if there are pre-existing errors
		  if(!empty($this->errors)) { return false; }
		  
			// Make sure the caption is not too long for the DB
		  if(strlen($this->image_caption) > 255) {
				$this->errors[] = "The caption can only be 255 characters long.";
				return false;
			}
		  
		  // Can't save without image_path and temp location
		  if(empty($this->image_path) || empty($this->temp_path)) {
		    
			$this->errors[] = "The file location was not available.";
		    return false;
			}
			
			// Determine the target_path
		  $target_path = $this->upload_dir .'/'. $this->image_path;
		  
		  // Make sure a file doesn't already exist in the target location
		  if(file_exists($target_path)) {
			echo "The file {$this->image_path} already exists.";
		    $this->errors[] = "The file {$this->image_path} already exists.";
		    return false;
		  }
		
			// Attempt to move the file 
			
			if(move_uploaded_file($this->temp_path, $target_path)) {
		  	  
			// Success
				// Save a corresponding entry to the database
				if($this->create()) {
					// We are done with temp_path, the file isn't there anymore
					unset($this->temp_path);
					return true;
				}
			} else {  
				// File was not moved.
		    $this->errors[] = "The file upload failed, possibly due to incorrect permissions on the upload folder.";
		    
			return false;
			}
		}
	 }
	
	public function destroy() {
		// First remove the database entry
		if($this->delete()) {
			// then remove the file
		  // Note that even though the database entry is gone, this object 
			// is still around (which lets us use $this->image_path()).
			$target_path = '../pub/'.$this->image_path();
			return unlink($target_path) ? true : false;
		} else {
			// database delete failed
			return false;
		}
	}

	public function image_path() {
	  return $this->upload_dir.DS.$this->image_path;
	}
	
	/*public function size_as_text() {
		if($this->size < 1024) {
			return "{$this->size} bytes";
		} elseif($this->size < 1048576) {
			$size_kb = round($this->size/1024);
			return "{$size_kb} KB";
		} else {
			$size_mb = round($this->size/1048576, 1);
			return "{$size_mb} MB";
		}
	}
	
	public function comments() {
		return Comment::find_comments_on($this->advertise_id);
	}*/
	
		
	// Common Database Methods
	public static function find_all() {
		return self::find_by_sql("SELECT * FROM ".self::$table_name);
    }
  
    public static function find_latest() {
		return self::find_by_sql("SELECT * FROM ".self::$table_name." ORDER BY advertise_id DESC LIMIT 2");
    }
 
    public static function find_current_ad ($aid=0, $bid=0) {
	  global $database;
	  
        $result_array = self::find_by_sql("SELECT * FROM ".self::$table_name." WHERE TO_DAYS(NOW()) - TO_DAYS(advertise_dateend)<=30 AND advertise_typetopic_id={$aid} AND advertise_typebanner_id={$bid}  LIMIT 1");
		return !empty($result_array) ? array_shift($result_array) : false;
    }
  
    public static function find_by_id($advertise_id=0) {
      global $database;
      $result_array = self::find_by_sql("SELECT * FROM ".self::$table_name." WHERE advertise_id=".$database->escape_value($advertise_id)." LIMIT 1");
      return !empty($result_array) ? array_shift($result_array) : false;
   }
  
  
    public static function find_by_page($id=0) {
        return self::find_by_sql("SELECT * FROM ".self::$table_name." WHERE advertise_page_id={$id}");
    }
 
    public static function count_all() {
        global $database;
        $sql = "SELECT COUNT(*) FROM ".self::$table_name." ORDER BY advertise_id DESC ";
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }
  
    public static function find_by_sql($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_array($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function find_by_sql_assoc($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_assoc($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function count_by_sql($sql) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    private static function instantiate($record) {
        // Could check that $record exists and is an array
        $object = new self;
        // Simple, long-form approach:
        // $object->advertise_id 				= $record['advertise_id'];
        // $object->username 	= $record['username'];
        // $object->password 	= $record['password'];
        // $object->first_name = $record['first_name'];
        // $object->last_name 	= $record['last_name'];

        // More dynamic, short-form approach:
        foreach($record as $attribute=>$value){
            if($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }
	
	private function has_attribute($attribute) {
	  // We don't care about the value, we just want to know if the key exists
	  // Will return true or false
	  return array_key_exists($attribute, $this->attributes());
	}

	protected function attributes() { 
		// return an array of attribute names and their values
	  $attributes = array();
	  foreach(self::$db_fields as $field) {
	    if(property_exists($this, $field)) {
	      $attributes[$field] = $this->$field;
	    }
	  }
	  return $attributes;
	}
	
	protected function sanitized_attributes() {
	  global $database;
	  $clean_attributes = array();
	  // sanitize the values before submitting
	  // Note: does not alter the actual value of each attribute
	  foreach($this->attributes() as $key => $value){
	    $clean_attributes[$key] = $database->escape_value($value);
	  }
	  return $clean_attributes;
	}
	
	// replaced with a custom save()
	// public function save() {
	//   // A new record won't have an advertise_id yet.
	//   return isset($this->advertise_id) ? $this->update() : $this->create();
	// }
	
	  function record() {
	//   A new record won't have an gallery_id yet.
	    return isset($this->advertise_id) ? $this->update() : $this->create();
	  }

	public function create() {
        global $database;
        $attributes = $this->sanitized_attributes();
        // Construction de la requête préparée
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= implode(", ", array_keys($attributes));
        $sql .= ") VALUES (";
        $sql .= implode(", ", array_fill(0, count($attributes), "?")); // Remplacement des valeurs par des marqueurs de paramètres
        $sql .= ")";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        // Exécution de la requête avec les valeurs des attributs comme paramètres
        if ($stmt->execute(array_values($attributes))) {
            $this->advertise_id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }

	public function update() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= implode(", ", $attribute_pairs);
        $sql .= " WHERE advertise_id=?";
        
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
        $value = array_values($attributes);
        $value[] = $this->advertise_id;
        if ($stmt->execute($value)) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }
    }

	public function delete() {
        global $database;
        $sql = "DELETE FROM " . self::$table_name;
        $sql .= " WHERE advertise_id= ?";
        $sql .= " LIMIT 1";
        // Préparation de la requête permet de prevenir les injections SQL
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec l'ID comme paramètre
        if ($stmt->execute([$this->advertise_id])) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }

    }

}

$advertise = new Advertise();

?>