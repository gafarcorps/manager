<?php

// Define the core paths
require_once('core_paths_definition.php');

require_once(LIB_PATH.DS.'initialize.php');

class User_account {

    protected static $table_name = "user_account";
    protected static $db_fields = array(
        'user_account_id',
        'user_account_typeuser_id',
        'user_account_email',
        'user_account_password',
        'user_account_user_creation',
        'user_account_date_creation',
        //'user_account_logged_in',
        'user_account_date_last_connexion',
        'user_account_lang',
        'user_account_extension',
        'user_account_extension_user_id',
        'user_account_extension_form_builder_id',
        'user_account_active'
    );
    public $user_account_id;
    public $user_account_typeuser_id;
    public $user_account_email;
    public $user_account_password;
    public $user_account_user_creation;
    public $user_account_date_creation;
    //public $user_account_logged_in;
    public $user_account_date_last_connexion;
    public $user_account_lang;
    public $user_account_extension;
    public $user_account_extension_user_id;
    public $user_account_extension_form_builder_id;
    public $user_account_active;

    private static function instantiate($record) {
        // Could check that $record exists and is an array
        $object = new self;
        // Simple, long-form approach:
        // $object->id 				= $record['id'];
        // $object->username 	= $record['username'];
        // $object->password 	= $record['password'];
        // $object->first_name = $record['first_name'];
        // $object->last_name 	= $record['last_name'];
        // More dynamic, short-form approach:
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    private function has_attribute($attribute) {
        // We don't care about the value, we just want to know if the key exists
        // Will return true or false
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes() {
        // return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes() {
        global $database;
        $clean_attributes = array();
        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    public function save() {
        // A new record won't have an id yet.
        return isset($this->user_account_id) ? $this->update() : $this->create_user();
    }

    public function create_user() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - INSERT INTO table (key, key) VALUES ('value', 'value')
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        // Construction de la requête préparée
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= implode(", ", array_keys($attributes));
        $sql .= ") VALUES (";
        $sql .= implode(", ", array_fill(0, count($attributes), "?")); // Remplacement des valeurs par des marqueurs de paramètres
        $sql .= ")";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        // Exécution de la requête avec les valeurs des attributs comme paramètres
        if ($stmt->execute(array_values($attributes))) {
            $this->user_account_id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }

    public function update() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= implode(", ", $attribute_pairs);
        $sql .= " WHERE user_account_id=?";
        
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
        $value = array_values($attributes);
        $value[] = $this->user_account_id;
        if ($stmt->execute($value)) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }
    }

    public function delete() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - DELETE FROM table WHERE condition LIMIT 1
        // - escape all values to prevent SQL injection
        // - use LIMIT 1
        $sql = "DELETE FROM " . self::$table_name . " WHERE user_account_id=?";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec l'ID comme paramètre
        if ($stmt->execute([$this->user_account_id])) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }

    }

    public static function find_by_sql($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_array($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function count_by_sql($sql) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    /*public function full_name() {
        // Vérifier si l'objet contient les propriétés nécessaires
        if (isset($this->user_account_firstname) && isset($this->user_account_lastname)) {
            // Concaténer le prénom et le nom avec un espace entre eux
            return trim($this->user_account_firstname) . " " . trim($this->user_account_lastname);
        } else {
            // Retourner une chaîne vide si les propriétés ne sont pas définies
            return "";
        }
    }*/

    public static function count_all() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    public static function authenticate($username = "", $password = "") {
        global $database;
        
        $sql = "SELECT * FROM " . self::$table_name;
        $sql .= " WHERE user_account_email = '{$username}' ";
        $sql .= "AND user_account_password = '{$password}' ";
        $sql .= "AND user_account_active = 1 ";
        $sql .= "LIMIT 1";
        $result_array = self::find_by_sql($sql);
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    // Common Database Methods
    public static function find_all() {
        // retourne tous les enregistrements actifs et supprimés
        return self::find_by_sql("SELECT * FROM " . self::$table_name . "  ");
    }

    public static function find_all_active() {
        // retourne tous les enregistrements actifs
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE user_account_active = 1  ");
    }
    public static function find_by_typeuser_and_extension($user_account_typeuser_id, $user_account_extension) {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE user_account_active = 1 AND user_account_typeuser_id >= $user_account_typeuser_id AND user_account_extension = $user_account_extension");
    }

    public static function find_all_unabled() {
        // retourne tous les enregistrements supprimés
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE user_account_active = 0  ");
    }

    public static function find_by_id($id = 0) {
        // retourne  un enregistrements actif
        global $database;
        $sql = "SELECT * FROM " . self::$table_name . " WHERE user_account_id = ?";
        $result_array = self::find_by_sql($sql, [$id]);
        return !empty($result_array) ? array_shift($result_array) : false;
    }
  
    public static function list_others($id = 0) {
        $sql = "SELECT * FROM " . self::$table_name . " WHERE user_account_id<> ? AND user_account_active = 1";
        return self::find_by_sql($sql, [$id]);
    }

    public static function find_pass($email) {
        $sql = "SELECT user_account_password FROM " . self::$table_name . " WHERE user_account_email='$email' AND user_account_active = 1 LIMIT 1";
//        echo $sql;
        $result_array = self::find_by_sql($sql);
        return !empty($result_array) ? array_shift($result_array) : false;
    }
    public static function find_by_email($email = "") {
        $sql = "SELECT * FROM " . self::$table_name . " WHERE user_account_email='$email' AND user_account_active = 1 LIMIT 1";
//        echo $sql;
        $result_array = self::find_by_sql($sql);
        return !empty($result_array) ? array_shift($result_array) : false;
    }
    public static function find_last($user_account_user_creation) {
        // retourne  le dernier enregistrements
        if(!empty($user_account_user_creation))
        $url_add = "WHERE user_account_user_creation = $user_account_user_creation";
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " $url_add ORDER BY user_account_id DESC LIMIT 1");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

}

$user_account = new User_account();

//  $methods = get_class_methods('User_account'); 
//    foreach($methods as $method){
//        echo $method .'<br/> <br/>';
//        }

?>