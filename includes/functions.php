<?php

// Core validation functions
// These need to be called from another validation function which 
// handles error reporting.
//
// For example:
//
// $errors = array(;
// function validate_presence_on($required_fields) {
//   global $errors;
//   foreach($required_fields as $field) {
//     if (!has_presence($_POST[$field])) {
//       $errors[$field] = "'" . $field . "' can't be blank";
//     }
//   }
// }
// * validate value has presence
// use trim() so empty spaces don't count
// use === to avoid false positives
// empty() would consider "0" to be empty
function has_presence($value) {
    $trimmed_value = trim($value);
    return isset($trimmed_value) && $trimmed_value !== "";
}

// * validate value has string length
// leading and trailing spaces will count
// options: exact, max, min
// has_length($first_name, ['exact' => 20])
// has_length($first_name, ['min' => 5, 'max' => 100])
function form_builder_type_name($form_builder_type = 0) {
    if($form_builder_type == 1) return "Table classique";
    elseif($form_builder_type == 2) return "Table utilisateur";
    elseif($form_builder_type == 3) return "Table communiqué";
    elseif($form_builder_type == 4) return "Table groupe d'acteur";
    elseif($form_builder_type == 5) return "Table contributions";
    elseif($form_builder_type == 6) return "Table événements à venir";    
}
function form_builder_type_name_en($form_builder_type = 0) {
    if($form_builder_type == 1) return "Classic table";
    elseif($form_builder_type == 2) return "User table";
    elseif($form_builder_type == 3) return "Statement table";
    elseif($form_builder_type == 4) return "Actor group table";
    elseif($form_builder_type == 5) return "Contributions table";
    elseif($form_builder_type == 6) return "Upcoming events table";    
}
function has_length($value, $options = array()) {
    if (isset($options['max']) && (strlen($value) > (int) $options['max'])) {
        return false;
    }
    if (isset($options['min']) && (strlen($value) < (int) $options['min'])) {
        return false;
    }
    if (isset($options['exact']) && (strlen($value) != (int) $options['exact'])) {
        return false;
    }
    return true;
}

// * validate value has a format matching a regular expression
// Be sure to use anchor expressions to match start and end of string.
// (Use \A and \Z, not ^ and $ which allow line returns.) 
// 
// Example:
// has_format_matching('1234', '/\d{4}/') is true
// has_format_matching('12345', '/\d{4}/') is also true
// has_format_matching('12345', '/\A\d{4}\Z/') is false
function has_format_matching($value, $regex = '//') {
    return preg_match($regex, $value);
}

// * validate value is a number
// submitted values are strings, so use is_numeric instead of is_int
// options: max, min
// has_number($items_to_order, ['min' => 1, 'max' => 5])
function has_number($value, $options = array()) {
    if (!is_numeric($value)) {
        return false;
    }
    if (isset($options['max']) && ($value > (int) $options['max'])) {
        return false;
    }
    if (isset($options['min']) && ($value < (int) $options['min'])) {
        return false;
    }
    return true;
}

// * validate value is included in a set
function has_inclusion_in($value, $set = array()) {
    return in_array($value, $set);
}

// * validate value is excluded from a set
function has_exclusion_from($value, $set = array()) {
    return !in_array($value, $set);
}

// * validate uniqueness
// A common validation, but not an easy one to write generically.
// Requires going to the database to check if value is already present.
// Implementation depends on your database set-up.
// Instead, here is a mock-up of the concept.
// Be sure to escape the user-provided value before sending it to the database.
// Table and column will be provided by us and escaping them is optional.
// Also consider whether you want to trim whitespace, or make the query 
// case-sensitive or not.
//
// function has_uniqueness($value, $table, $column) {
//   $escaped_value = mysql_escape($value);
//   sql = "SELECT COUNT(*) as count FROM {$table} WHERE {$column} = '{$escaped_value}';"
//   if count > 0 then value is already present and not unique
// }


function find_date($dateTime) {
    $tab = explode(" ", $dateTime);
    return $tab[0];
}

function find_time($dateTime) {
    $tab = explode(" ", $dateTime);
    return $tab[1];
}

function find_heure($heure) {
    $tab = explode(":", $heure);
    return $tab[0];
}

function find_minute($heure) {
    $tab = explode(":", $heure);
    return $tab[1];
}

function find_seconde($heure) {
    $tab = explode(":", $heure);
    return $tab[2];
}

function find_day($date) {
    $date_explosed = explode("-", $date);
    $day = $date_explosed[2];
    return $day;
}

function find_month($date) {
    $date_explosed = explode("-", $date);
    $month = $date_explosed[1];
    return $month;
}

function find_year($date) {
    $date_explosed = explode("-", $date);
    $year = $date_explosed[0];
    return $year;
}

function change_months($idM = 0) {
    $idMois = $idM;

    switch ($idMois) {
        case 1:
            $mois = "JAN";
            break;

        case 2:
            $mois = "FEV";
            break;

        case 3:
            $mois = "MAR";
            break;

        case 4:
            $mois = "AVR";
            break;

        case 5:
            $mois = "MAI";
            break;

        case 0:
            $mois = "JUN";
            break;

        case 7:
            $mois = "JUL";
            break;

        case '08':
            $mois = "AOU";
            break;

        case '09':
            $mois = "SEP";
            break;

        case 10:
            $mois = "OCT";
            break;

        case 11:
            $mois = "NOV";
            break;

        case 12:
            $mois = "DEC";
            break;
    }
    return $mois;
}

//function dateUStoFR($dateUS, $delimiter = '-', $separator = '/') {
//    $date = explode($delimiter, $dateUS);
//    $dateFR = sprintf("%02d%s%02d%s%04d", $date[2], $separator, $date[1], $separator, $date[0]);
//    return $dateFR;
//}
//
//function dateFRtoUS($dateFR, $delimiter = '-', $separator = '-') {
//    $date = explode($delimiter, $dateFR);
//    $dateUS = sprintf("%04d%s%02d%s%02d", $date[2], $separator, $date[1], $separator, $date[0]);
//    return $dateUS;
//}

/*function changedateusfr($dateus) {
    $datefr = $dateus{8} . $dateus{9} . "/" . $dateus{5} . $dateus{6} . "/" . $dateus{0} . $dateus{1} . $dateus{2} . $dateus{3};
    return $datefr;
}*/

//function changedatefrus($datefr) {
//    $dateus = $datefr{6} . $datefr{7} . $datefr{8} . $datefr{9} . "-" . $datefr{3} . $datefr{4} . "-" . $datefr{0} . $datefr{1};
//    return $dateus;
//}
//
//function changedatetimeusfr($dateus) {
//    $datefr = $dateus{8} . $dateus{9} . "/" . $dateus{5} . $dateus{6} . "/" . $dateus{0} . $dateus{1} . $dateus{2} . $dateus{3} . " " . $dateus{0} . ":" . $dateus{1} . ":" . $dateus{2};
//    return $datefr;
//}

function neatest_trim($content, $chars) {
    if (strlen($content) > $chars) {
        $content = str_replace('&nbsp;', ' ', $content);
        $content = str_replace("\n", '', $content);
        // use with wordpress    
        //$content = strip_tags(strip_shortcodes(trim($content)));
        $content = strip_tags(trim($content));
        $content = preg_replace('/\s+?(\S+)?$/', '', mb_substr($content, 0, $chars));

        $content = trim($content);
        return $content;
    } else {
        return $content;
    }
}

function strip_zeros_from_date($marked_string = "") {
    // first remove the marked zeros
    $no_zeros = str_replace('*0', '', $marked_string);
    // then remove any remaining marks
    $cleaned_string = str_replace('*', '', $no_zeros);
    return $cleaned_string;
}

/**
 * Function: sanitize
 * Returns a sanitized string, typically for URLs.
 *
 * Parameters:
 *     $string - The string to sanitize.
 *     $force_lowercase - Force the string to lowercase?
 *     $anal - If set to *true*, will remove all non-alphanumeric characters.
 */
function replace_accents($string) {
    return str_replace(array('à', 'á', 'â', 'ã', 'ä', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'À', '�?', 'Â', 'Ã', 'Ä', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', '�?', 'Î', '�?', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ù', 'Ú', 'Û', 'Ü', '�?', '’', "'", '«', '»'), array('a', 'a', 'a', 'a', 'a', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'A', 'A', 'A', 'A', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'N', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', '-', '-', '-', '-'), $string);
}

function cleanInput($input) {

    $search = array(
        '@<script[^>]*?>.*?</script>@si', // Strip out javascript
        '@<[\/\!]*?[^<>]*?>@si', // Strip out HTML tags
        '@<style[^>]*?>.*?</style>@siU', // Strip style tags properly
        '@<![\s\S]*?--[ \t\n\r]*>@'         // Strip multi-line comments
    );

    $output = preg_replace($search, '', $input);
    return $output;
}

function sanitize($input) {
//    if (is_array($input)) {
//        foreach ($input as $var => $val) {
//            $output[$var] = sanitize($val);
//        }
//    } else {
//        if (get_magic_quotes_gpc()) {
//            $input = stripslashes($input);
//        }
//        $input = cleanInput($input);
//        $output = mysql_real_escape_string($input);
//    }
    return $input;
}

function sanitize_url($string, $force_lowercase = true, $anal = false) {
    $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
        "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
        "â€�?", "â€“", ",", "<", ".", ">", "/", "?", "'", "’","« ", "» ");

    $clean = trim(str_replace($strip, "-", strip_tags($string)));
    $clean = preg_replace('/\s+/', "-", $clean);
    $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean;
    return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
            mb_strtolower($clean, 'UTF-8') :
            strtolower($clean) :
            $clean;
}
function sanitize_db_fields($string, $force_lowercase = true, $anal = false) {
    $strip = array("~", "`", "!", "@", "#", "$", "%", "^", "&", "*", "(", ")", "_", "=", "+", "[", "{", "]",
        "}", "\\", "|", ";", ":", "\"", "'", "&#8216;", "&#8217;", "&#8220;", "&#8221;", "&#8211;", "&#8212;",
        "â€�?", "â€“", ",", "<", ".", ">", "/", "?", "'", "’", "-","«", "»");

    $clean = trim(str_replace($strip, "_", strip_tags($string)));
    $clean = preg_replace('/\s+/', "_", $clean);
    $clean = ($anal) ? preg_replace("/[^a-zA-Z0-9]/", "", $clean) : $clean;
    return ($force_lowercase) ?
            (function_exists('mb_strtolower')) ?
            mb_strtolower($clean, 'UTF-8') :
            strtolower($clean) :
            $clean;
}


function find_month_in_french($month_number = 0) {

    if ($month_number == 1)
        return "Janvier";
    else if ($month_number == 2)
        return "Février";
    else if ($month_number == 3)
        return "Mars";
    else if ($month_number == 4)
        return "Avril";
    else if ($month_number == 5)
        return "Mai";
    else if ($month_number == 6)
        return "Juin";
    else if ($month_number == 7)
        return "Juillet";
    else if ($month_number == '08')
        return "Août";
    else if ($month_number == '09')
        return "Septembre";
    else if ($month_number == 10)
        return "Octobre";
    else if ($month_number == 11)
        return "Novembre";
    else if ($month_number == 12)
        return "Décembre";
}

function dateBetweenTwoDates($date1, $date2) {

    $date = array();
    $date1 = strtotime($date1);
    $date2 = strtotime($date2);

    $nbjour = ($date2 - $date1) / 60 / 60 / 24; //Nombre de jours entre les deux

    for ($i = 0; $i <= $nbjour; $i++) {
        $date[$i] = date('Y-m-d', $date1);
//echo strftime('%d-%m-%Y',$date1);

        $date1 += 60 * 60 * 24; //On additionne d'un jour (en seconde)
    }
    return $date;
}

function wd_remove_accents($str, $charset = 'utf-8') {
    $str = htmlentities($str, ENT_NOQUOTES, $charset);

    $str = preg_replace('#&([A-za-z])(?:acute|cedil|caron|circ|grave|orn|ring|slash|th|tilde|uml);#', '\1', $str);
    $str = preg_replace('#&([A-za-z]{2})(?:lig);#', '\1', $str); // pour les ligatures e.g. '&oelig;'
    $str = preg_replace('#&[^;]+;#', '', $str); // supprime les autres caractères
    $str = preg_replace(' ', '-', $str); // supprime les autres caractères

    return $str;
}

//function redirect_to($location = NULL) {
//    if ($location != NULL) {
//        header("Location: {$location}");
//        exit;
//    }
//}

function redirect_to($location = NULL) {
    if ($location != NULL) {
//        header("Location: {$location}");
        echo '<script>document.location.href="' . $location . '"</script>';
        exit;
    }
}
function output_message($message = "") {
    if (!empty($message)) {
        return "<p class=\"message\">{$message}</p>";
    } else {
        return "";
    }
}

function nl2br2($string) {
    $string = str_replace('.', "<br />", $string);
    return $string;
}


function h($string) {
    return htmlspecialchars($string);
}

// Sanitize for JavaScript output
function j($string) {
    return json_encode($string);
}

// Sanitize for use in a URL
function u($string) {
    return urlencode($string);
}

/*function day_later($date_start, $days){
    global $database;
    $sql = "SELECT DATE_ADD('$date_start', INTERVAL $days DAY)";
    $result_set = $database->query($sql);
    //return $database->fetch_array($result_set)[0];
}*/

/*
  function __autoload($class_name) {
  $class_name = strtolower($class_name);
  $path = LIB_PATH.DS."{$class_name}.php";
  if(file_exists($path)) {
  require_once($path);
  } else {
  die("The file {$class_name}.php could not be found.");
  }
  }

  function include_layout_template($template="") {
  include(SITE_ROOT.DS.'public'.DS.'layouts'.DS.$template);
  }

  function log_action($action, $message="") {
  $logfile = SITE_ROOT.DS.'logs'.DS.'log.txt';
  $new = file_exists($logfile) ? false : true;
  if($handle = fopen($logfile, 'a')) { // append
  $timestamp = strftime("%Y-%m-%d %H:%M:%S", time());
  $content = "{$timestamp} | {$action}: {$message}\n";
  fwrite($handle, $content);
  fclose($handle);
  if($new) { chmod($logfile, 0755); }
  } else {
  echo "Could not open log file for writing.";
  }
  }
 */
?>

