<?php

// A class to help work with Sessions
// In our case, primarily to manage logging users in and out
// Keep in mind when working with sessions that it is generally 
// inadvisable to store DB-related objects in sessions

class Session {

    private $logged_in = false;
    public $user_account_id;
    public $extension_id;
    public $message;
    public $reader_id;

    function __construct() {
        session_start();
        $this->check_message();
        $this->check_login();
        $this->check_reader_login();

        if ($this->logged_in) {
            // actions à effectuer immédiatement si l'utilisateur est connecté
        } else {
            // actions à effectuer immédiatement si l'utilisateur n'est pas connecté
        }
    }

    public function is_logged_in() {
        return $this->logged_in;
    }

    public function login($user_account_id, $extension_id) {
        // la base de données devrait trouver l'utilisateur en fonction du nom d'utilisateur/mot de passe

        if ($user_account_id) {
            $this->user_account_id = $_SESSION['user_account_id'] = $user_account_id->user_account_id;
            if (!empty($extension_id))
                $this->extension_id = $_SESSION['extension_id'] = $extension_id->menu_id;
            $this->logged_in = true;
        }
    }
    public function login_reader($reader_id) {
        // la base de données devrait trouver l'utilisateur en fonction du nom d'utilisateur/mot de passe

        if ($reader_id) {
            $this->reader_id = $_SESSION['reader_id'] = $reader_id->id;
            $this->logged_in = true;
        }
    }

    public function logout_reader() {
        unset($_SESSION['reader_id']);
        unset($this->reader_id);
        $this->logged_in = false;
    }

    public function logout() {
        unset($_SESSION['user_account_id']);
        unset($this->user_account_id);
        unset($_SESSION['extension_id']);
        unset($this->extension_id);
        $this->logged_in = false;
    }

    private function check_login() {
        if (isset($_SESSION['user_account_id'])) {
            $this->user_account_id = $_SESSION['user_account_id'];
            $this->extension_id = $_SESSION['extension_id'];
            $this->logged_in = true;
        } else {
            unset($this->user_account_id);
            $this->logged_in = false;
        }
    }
    private function check_reader_login() {
        if (isset($_SESSION['reader_id'])) {
            $this->reader_id = $_SESSION['reader_id'];
            $this->logged_in = true;
        } else {
            unset($this->reader_id);
            $this->logged_in = false;
        }
    }

    private function check_message() {
        // Y a-t-il un message stocké dans la session ?
        if (isset($_SESSION['message'])) {
            // Ajoutez-le en tant qu'attribut et effacez la version stockée
            $this->message = $_SESSION['message'];
            unset($_SESSION['message']);
        } else {
            $this->message = "";
        }
    }

}
$session = new Session();
//$message = $session->message();
?>
