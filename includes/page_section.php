
<?php

/**
 * Description of page
 *
 * @author Menz ALFA
 */
// Define the core paths
require_once('core_paths_definition.php');

require_once(LIB_PATH.DS.'initialize.php');

class Page_section {

    //put your code here
    protected static $table_name = "page_section";
    protected static $db_fields = array(
    
        'page_section_id',
        'page_section_pid', 
        'page_section_sid' 
        );
    public $page_section_id;
    public $page_section_pid;
    public $page_section_sid;
   
    

    private static function instantiate($record) {
        // Could check that $record exists and is an array
        $object = new self;
        // Simple, long-form approach:
        // $object->id 				= $record['id'];
        // $object->username 	= $record['username'];
        // $object->password 	= $record['password'];
        // $object->first_name = $record['first_name'];
        // $object->last_name 	= $record['last_name'];
        // More dynamic, short-form approach:
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    private function has_attribute($attribute) {
        // We don't care about the value, we just want to know if the key exists
        // Will return true or false
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes() {
        // return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes() {
        global $database;
        $clean_attributes = array();
        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    public function save() {
        // A new record won't have an id yet.
        return (isset($this->page_section_id))  ? $this->update() : $this->create();
    }

    public function create() {
        global $database;
        $attributes = $this->sanitized_attributes();
        // Construction de la requête préparée
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= implode(", ", array_keys($attributes));
        $sql .= ") VALUES (";
        $sql .= implode(", ", array_fill(0, count($attributes), "?")); // Remplacement des valeurs par des marqueurs de paramètres
        $sql .= ")";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        // Exécution de la requête avec les valeurs des attributs comme paramètres
        if ($stmt->execute(array_values($attributes))) {
            $this->page_section_id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }

    public function update() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= implode(", ", $attribute_pairs);
        $sql .= " WHERE page_section_id=?";
        
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
        $values = array_values($attributes);
        $values[] = $this->page_section_id;
        if ($stmt->execute($values)) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }
    }

    public function delete() {
        global $database;
        $sql = "DELETE FROM " . self::$table_name;
        $sql .= " WHERE page_section_id= ?";
        $sql .= " LIMIT 1";
        // Préparation de la requête permet de prevenir les injections SQL
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec l'ID comme paramètre
        if ($stmt->execute([$this->page_section_id])) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }

    }

    public static function find_by_sql($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_array($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

      public static function execute_sql($sql = "") {
        global $database;
        // Préparation de la requête permet de prevenir les injections SQL
        $stmt = $database->prepare($sql);
        // Exécution de la requête 
        return ($stmt->rowCount() == 1) ? true : false;
    }

    public static function count_by_sql($sql) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    public static function count_all() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    
     public static function find_all() {
        // retourne tous les enregistrements actifs
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " ORDER BY page_section_id DESC ");
    }  

     public static function find_by_ids($pid = 0, $sid = 0) {
        // retourne  un enregistrements actif
        $sql= "SELECT * FROM " . self::$table_name . " WHERE page_section_pid={$pid} AND page_section_sid={$sid} ";
        echo $sql;
        $result_array = self::find_by_sql($sql);
        return !empty($result_array) ? array_shift($result_array) : false;
    }


     public static function find_by_page($pid=0) {
        // retourne tous les enregistrements actifs
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE page_section_pid={$pid} ORDER BY page_section_id DESC ");
    } 
     
    public static function list_others($id = 0) {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE page_section_id<>{$id} ");
    }
 
    
    public static function find_all_page_sections() {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE page_section = 1 ");
    }


     public static function list_top_sections($sid = 4) {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE topic_section_sid={$sid} ");
    }

     public static function list_left_sections($sid = 1) {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE topic_section_sid={$sid} ");
    }
     public static function list_right_sections($sid = 3) {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE topic_section_sid={$sid} ");
    }

     public static function list_bottom_sections($sid = 2) {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE topic_section_sid={$sid} ");
    }
 

    


}

$page_section = new Page_section();

?>
