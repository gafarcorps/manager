<?php

class MySQLDatabase {
    
    private PDO $connection;
    public ?string $last_query = null;

    function __construct() {
        $this->open_connection();
    }

    public function open_connection($db_server = "localhost", $db_user = "root", $db_pass = "", $db_name = "missionmali_db") {
        $dsn = "mysql:host=$db_server;dbname=$db_name;charset=utf8mb4";
        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];
        try {
            $this->connection = new PDO($dsn, $db_user, $db_pass, $options);
        } catch (PDOException $e) {
            throw new Exception("Database connection failed: " . $e->getMessage());
        }
    }

    public function close_connection() {
        $this->connection = null;
    }

    public function prepare($sql) {
        $this->last_query = $sql;
        $result = $this->connection->prepare($sql);
        $this->confirm_query($result);
        return $result;
    }

    public function query($sql) {
        $this->last_query = $sql;
        $result = $this->connection->query($sql);
        $this->confirm_query($result);
        return $result;
    }

    public function bindParam($statement, $parameter, $variable, $data_type = PDO::PARAM_STR) {
        return $statement->bindParam($parameter, $variable, $data_type);
    }

    public function bindValue($statement, $parameter, $value, $data_type = PDO::PARAM_STR) {
        return $statement->bindValue($parameter, $value, $data_type);
    }

    public function execute($statement) {
        return $statement->execute();
    }

    public function escape_value($value) {
        try {
            // Préparation d'une requête PDO avec un paramètre non nommé
            $stmt = $this->connection->prepare("SELECT :value");
            // Liaison de la valeur à ce paramètre
            $stmt->bindValue(':value', $value);
            // Exécution de la requête
            $stmt->execute();
            // Récupération de la valeur échappée
            $escaped_value = $stmt->fetchColumn();
            // Retourner la valeur échappée
            return $escaped_value;
        } catch (PDOException $e) {
            throw new Exception("Database connection failed: " . $e->getMessage());
        }
    }
    
    public function fetch_array($result_set) {
        return $result_set->fetch(PDO::FETCH_BOTH);
    }

    public function fetch_assoc($result_set) {
        return $result_set->fetch(PDO::FETCH_ASSOC);
    }

    public function num_rows($result_set) {
        return $result_set->rowCount();
    }

    public function insert_id() {
        return $this->connection->lastInsertId();
    }

    public function affected_rows($statement) {
        return $statement->rowCount();
    }

    public function confirm_query($result) {
        if (!$result) {
            $error_info = $this->connection->errorInfo();
            $output = "Database query failed: " . $error_info[2] . "<br /><br />";
            $output .= "Last SQL query: " . $this->last_query;
            throw new Exception($output);
        }
    }
}

$database = new MySQLDatabase();
?>