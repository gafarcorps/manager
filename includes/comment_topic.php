<?php

// Define the core paths
require_once('core_paths_definition.php');

require_once(LIB_PATH . DS . 'initialize.php');

class Comment_topic {

    protected static $table_name = "press_comments_topic";
    protected static $db_fields = array(
        'comment_topic_id',
        'comment_topic_topicid',
        'comment_topic_text',
        'comment_topic_author',
        'comment_topic_author_email',
        'comment_topic_published',
        'comment_topic_date',
        'comment_topic_active',
    );
    public $comment_topic_id;
    //public $comment_topic_user_creation;
    //public $comment_topic_user_logged_in;
    public $comment_topic_topicid;
    public $comment_topic_text;
    public $comment_topic_author;
    public $comment_topic_author_email;
    public $comment_topic_published;
    public $comment_topic_date;
    public $comment_topic_active;

    //public $comment_topic_date_creation;
    //public $comment_topic_date_last_connexion;
    //public $comment_topic_active;

    private static function instantiate($record) {
        // Could check that $record exists and is an array
        $object = new self;
        // Simple, long-form approach:
        // $object->id 				= $record['id'];
        // $object->username 	= $record['username'];
        // $object->password 	= $record['password'];
        // $object->first_name = $record['first_name'];
        // $object->last_name 	= $record['last_name'];
        // More dynamic, short-form approach:
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    private function has_attribute($attribute) {
        // We don't care about the value, we just want to know if the key exists
        // Will return true or false
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes() {
        // return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes() {
        global $database;
        $clean_attributes = array();
        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    public function save() {
        // A new record won't have an id yet.
        return isset($this->comment_topic_id) ? $this->update() : $this->create();
    }

    public function create() {
        global $database;
        $attributes = $this->sanitized_attributes();
        // Construction de la requête préparée
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= implode(", ", array_keys($attributes));
        $sql .= ") VALUES (";
        $sql .= implode(", ", array_fill(0, count($attributes), "?")); // Remplacement des valeurs par des marqueurs de paramètres
        $sql .= ")";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        // Exécution de la requête avec les valeurs des attributs comme paramètres
        if ($stmt->execute(array_values($attributes))) {
            $this->comment_topic_id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }

    public function update() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= implode(", ", $attribute_pairs);
        $sql .= " WHERE comment_topic_id=?";
        
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
        $value = array_values($attributes);
        $value[] = $this->comment_topic_id;
        if ($stmt->execute($value)) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }
    }


    public function unable() {
        global $database;
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql  = "UPDATE " . self::$table_name . " SET comment_topic_active = 0 ";
        $sql .= "WHERE comment_topic_id= ?";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
        $value = array_values($attributes);
        $value[] = $this->comment_topic_id;
        if ($stmt->execute($value)) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }
    }

    public function recover() {
        global $database;
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql  = "UPDATE " . self::$table_name . " SET comment_topic_active = 1 ";
        $sql .= "WHERE comment_topic_id= ?";
       // Préparation de la requête
       $stmt = $database->prepare($sql);
        
       // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
       $value = array_values($attributes);
       $value[] = $this->comment_topic_id;
       if ($stmt->execute($value)) {
           return ($database->affected_rows($stmt) == 1) ? true : false;
       } else {
           return false;
       }
    }

    public function delete() {
        global $database;
        $sql = "DELETE FROM " . self::$table_name;
        $sql .= " WHERE comment_topic_id= ?";
        $sql .= " LIMIT 1";
        // Préparation de la requête permet de prevenir les injections SQL
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec l'ID comme paramètre
        if ($stmt->execute([$this->comment_topic_id])) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }

    }

    public static function find_by_sql($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_array($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function find_by_sql_assoc($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_assoc($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function count_by_sql($sql) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }
// Common Database Methods
    public static function find_all() {
        // retourne tous les enregistrements actifs et supprimés
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " ORDER BY comment_topic_date DESC, comment_topic_id DESC");
    }

    public static function find_all_active($limit) {
        // retourne tous les enregistrements actifs
        //$sql ="SELECT * FROM " . self::$table_name . " WHERE comment_topic_active = 1 ORDER BY comment_topic_date DESC, comment_topic_id DESC";
        //echo $sql;
        if (empty($limit)) {
//            echo "SELECT * FROM " . self::$table_name . " WHERE comment_topic_active = 1 ORDER BY comment_topic_date DESC, comment_topic_id DESC ";
            return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE comment_topic_active = 1 ORDER BY comment_topic_date DESC, comment_topic_id DESC ");
        } else
            return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE comment_topic_active = 1 ORDER BY comment_topic_date DESC, comment_topic_id DESC LIMIT $limit");
    }

    //BY FOFANA
    public static function find_all_not_published() {
        // retourne tous les enregistrements NON PUBLIER
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE comment_topic_active = 1 AND comment_topic_published = 0 ORDER BY comment_topic_date DESC");
    }

    public static function find_all_unabled() {
        // retourne tous les enregistrements supprimés
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE comment_topic_active = 0 ORDER BY comment_topic_date DESC, comment_topic_id DESC");
    }

    public static function find_by_id($id = 0) {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE comment_topic_id={$id} LIMIT 1");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function find_by_topic($id = 0) {
//      echo "SELECT * FROM ".self::$table_name." WHERE comment_topic_topicid={$id} AND comment_topic_published = 0 ORDER BY comment_topic_date ASC, comment_topic_id ASC";
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE comment_topic_topicid={$id} AND comment_topic_published = 1  ORDER BY comment_topic_date ASC, comment_topic_id ASC");
    }

    public static function find_unabled_by_topic($id = 0) {
//      echo "SELECT * FROM ".self::$table_name." WHERE comment_topic_topicid={$id} AND comment_topic_published = 0 ORDER BY comment_topic_date ASC, comment_topic_id ASC";
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE comment_topic_topicid={$id} AND comment_topic_published = 0  ORDER BY comment_topic_date ASC, comment_topic_id ASC");
    }

    public static function find_popular_limit_n($limit = 0) {
//            echo "SELECT comment_topic_topicid,(COUNT(*))as compte FROM ".self::$table_name." where comment_topic_published = 1 group by comment_topic_topicid order by compte desc LIMIT 8";
        return self::find_by_sql("SELECT comment_topic_topicid,(COUNT(*))as compte FROM " . self::$table_name . " where comment_topic_published = 1  group by comment_topic_topicid order by compte desc LIMIT $limit");
    }

    public static function count_by_topic($id) {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name . " WHERE comment_topic_topicid='$id' AND comment_topic_published='1'";
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    public static function count_annonce() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name . " WHERE comment_topic_published='1'  ORDER BY comment_topic_id ASC";
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    public static function count_valid_by_topic($id) {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name . " WHERE comment_topic_topicid='$id' AND comment_topic_published='1' ";
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    public static function count_novalid_by_topic($id) {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name . " WHERE comment_topic_topicid='$id' AND comment_topic_published='0' ";
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

}

$comment_topic = new Comment_topic();
?>