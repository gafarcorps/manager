
<?php
/**
 * Description of page
 *
 * @author Menz ALFA
 */
// Define the core paths
require_once('core_paths_definition.php');

require_once(LIB_PATH.DS.'initialize.php');


class Copyright {

    //put your code here
    protected static $table_name = "copyright";
    protected static $db_fields = array(
        'copyright_id', 
        'copyright_logo',
        'copyright_username',
        'copyright_userdesc',
        'copyright_tel1',
        'copyright_tel2',
        'copyright_email',
        'copyright_address',
        'copyright_user_image', 
        'copyright_date', 
        'copyright_manager_url', 
        'copyright_website_url',
        'copyright_user_active'
        );
    public $copyright_id;
    public $copyright_username;
    public $copyright_userdesc;
    public $copyright_tel1;
    public $copyright_tel2;
    public $copyright_email;
    public $copyright_address;
    public $copyright_logo;
    public $copyright_date;
    public $copyright_manager_url;
    public $copyright_website_url;
    public $copyright_active;

    private $temp_path;
    private $temp_path2;
    private $temp_path3;
    protected $upload_dir = "media/files/";
    protected $upload_dir2 = "media/files/";
    public $errors = array();
    protected $upload_errors = array(
        // http://www.php.net/manual/en/features.file-upload.errors.php
        UPLOAD_ERR_OK => "No errors.",
        UPLOAD_ERR_INI_SIZE => "Larger than upload_max_filesize.",
        UPLOAD_ERR_FORM_SIZE => "Larger than form MAX_FILE_SIZE.",
        UPLOAD_ERR_PARTIAL => "Partial upload.",
        UPLOAD_ERR_NO_FILE => "No file.",
        UPLOAD_ERR_NO_TMP_DIR => "No temporary directory.",
        UPLOAD_ERR_CANT_WRITE => "Can't write to disk.",
        UPLOAD_ERR_EXTENSION => "File upload stopped by extension."
    );

    // Instanciation des attributs de la classe


    private static function instantiate($record) {
        // Could check that $record exists and is an array
        $object = new self;
        // Simple, long-form approach:
        // $object->file_id 	= $record['file_id'];
        // $object->username 	= $record['username'];
        // $object->password 	= $record['password'];
        // $object->first_name  = $record['first_name'];
        // $object->last_name 	= $record['last_name'];
        // More dynamic, short-form approach:
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    private function has_attribute($attribute) {
        // We don't care about the value, we just want to know if the key exists
        // Will return true or false
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes() {
        // return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes() {
        global $database;
        $clean_attributes = array();
        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    public static function find_by_sql($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_array($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function find_by_sql_assoc($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_assoc($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function count_by_sql($sql) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    public static function count_all() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name;
         $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    // replaced with a custom save()
    public function save() {
        // A new record won't have an file_id yet.
        return isset($this->copyright_id) ? $this->update() : $this->create();
    }

    public function create() {
        global $database;
        $attributes = $this->sanitized_attributes();
        // Construction de la requête préparée
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= implode(", ", array_keys($attributes));
        $sql .= ") VALUES (";
        $sql .= implode(", ", array_fill(0, count($attributes), "?")); // Remplacement des valeurs par des marqueurs de paramètres
        $sql .= ")";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        // Exécution de la requête avec les valeurs des attributs comme paramètres
        if ($stmt->execute(array_values($attributes))) {
            $this->copyright_id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }

    public function update() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= implode(", ", $attribute_pairs);
        $sql .= " WHERE copyright_id=?";
        
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
        $value = array_values($attributes);
        $value[] = $this->copyright_id;
        if ($stmt->execute($value)) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }
    }

    public function delete() {
        global $database;
        $sql = "DELETE FROM " . self::$table_name;
        $sql .= " WHERE copyright_id= ?";
        $sql .= " LIMIT 1";
        // Préparation de la requête permet de prevenir les injections SQL
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec l'ID comme paramètre
        if ($stmt->execute([$this->copyright_id])) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }

    }

    // Pass in $_FILE(['uploaded_file']) as an argument
    public function attach_file($file, $field) {
        // Perform error checking on the form parameters
        if (!$file || empty($file) || !is_array($file)) {
            // error: nothing uploaded or wrong argument usage
            echo "No file was uploaded.";
            $this->errors[] = "No file was uploaded.";
            return false;
        } elseif ($file['error'] != 0) {
            // error: report what PHP says went wrong
            echo $file['size'];
            $this->errors[] = $this->upload_errors[$file['error']];
            echo $this->upload_errors[$file['error']];
            return false;
        } else {
            // Set object attributes to the form parameters.
            $this->temp_path = $file['tmp_name'];
            $this->$field = basename(date("Ymd") . time() . '_' . $file['name']);
//		  $this->file_type       = $file['type'];
//		  $this->file_size       = $file['size'];
            // Don't worry about saving anything to the database yet.
            return true;
        }
    }

    public function save2() {
        // A new record won't have an file_id yet.
        if (isset($this->copyright_id)) {
            echo 'i enter good update...';
            // Really just to update the caption
            //$this->update();
            if (!empty($this->errors)) {
                return false;
            }

            // Make sure the caption is not too long for the DB
//            if (strlen($this->image_caption) > 255) {
//                $this->errors[] = "The caption can only be 255 characters long.";
//                return false;
//            }

            // Can't save without file_path and temp location
            if (empty($this->copyright_logo) || empty($this->temp_path)) {

                $this->errors[] = "The file location was not available.";
                return false;
            }

            // Determine the target_path
            $target_path = $this->upload_dir . 'DS' . $this->copyright_logo;

            // Make sure a file doesn't already exist in the target location
            if (file_exists($target_path)) {
                echo "The file {$this->copyright_logo} already exists.";
                $this->errors[] = "The file {$this->copyright_logo} already exists.";
                return false;
            }

            // Attempt to move the file 

            if (move_uploaded_file($this->temp_path, $target_path)) {

                // Success
                // Save a corresponding entry to the database
                $this->update();
                    // We are done with temp_path, the file isn't there anymore
                unset($this->temp_path);
                return true;
            } else {
                // File was not moved.
                $this->errors[] = "The file upload failed, possibly due to incorrect permissions on the upload folder.";

                return false;
            }
            /** end if	* */
        } else {
            // Make sure there are no errors
            // Can't save if there are pre-existing errors
            if (!empty($this->errors)) {
                return false;
            }

            // Make sure the caption is not too long for the DB
            /*if (strlen($this->image_caption) > 255) {
                $this->errors[] = "The caption can only be 255 characters long.";
                return false;
            }*/

            // Can't save without file_path and temp location
            if (empty($this->copyright_logo) || empty($this->temp_path)) {

                $this->errors[] = "The file location was not available.";
                return false;
            }

            // Determine the target_path
            $target_path = $this->upload_dir . DS . $this->copyright_logo;

            // Make sure a file doesn't already exist in the target location
            if (file_exists($target_path)) {
                echo "The file {$this->copyright_logo} already exists.";
                $this->errors[] = "The file {$this->copyright_logo} already exists.";
                return false;
            }

            // Attempt to move the file 

            if (move_uploaded_file($this->temp_path, $target_path)) {

                // Success
                // Save a corresponding entry to the database
                if ($this->create()) {
                    // We are done with temp_path, the file isn't there anymore
                    unset($this->temp_path);
                    return true;
                }
            } else {
                // File was not moved.
                $this->errors[] = "The file upload failed, possibly due to incorrect permissions on the upload folder.";

                return false;
            }
        }
    }

    public function attach_file3($file) {
        // Perform error checking on the form parameters
        if (!$file || empty($file) || !is_array($file)) {
            // error: nothing uploaded or wrong argument usage
            echo "No file was uploaded.";
            $this->errors[] = "No file was uploaded.";
            return false;
        } elseif ($file['error'] != 0) {
            // error: report what PHP says went wrong
            echo $file['size'];
            $this->errors[] = $this->upload_errors[$file['error']];
            echo $this->upload_errors[$file['error']];
            return false;
        } else {
            // Set object attributes to the form parameters.
            $this->temp_path2 = $file['tmp_name'];
            //$this->copyright_epress_image = basename(date("Ymd") . time() . '_' . $file['name']);
            //		  $this->file_type       = $file['type'];
            //		  $this->file_size       = $file['size'];
            // Don't worry about saving anything to the database yet.
            return true;
        }
    }

    public function save3() {
        // A new record won't have an file_id yet.
        if (isset($this->copyright_id)) {
            echo 'i enter good update...';
            // Really just to update the caption
            //$this->update();
            if (!empty($this->errors)) {
                return false;
            }

            // Make sure the caption is not too long for the DB
            /*if (strlen($this->image_caption) > 255) {
                $this->errors[] = "The caption can only be 255 characters long.";
                return false;
            }*/

            // Can't save without file_path and temp location
           /*if (empty($this->copyright_epress_image) || empty($this->temp_path2)) {

                $this->errors[] = "The file location was not available.";
                return false;
            }*/

            // Determine the target_path
            //$target_path = $this->upload_dir2 . 'DS' . $this->copyright_epress_image;

            // Make sure a file doesn't already exist in the target location
            /*if (file_exists($target_path)) {
                echo "The file {$this->copyright_epress_image} already exists.";
                $this->errors[] = "The file {$this->copyright_epress_image} already exists.";
                return false;
            }*/

            // Attempt to move the file 

            /*if (move_uploaded_file($this->temp_path2, $target_path)) {

                // Success
                // Save a corresponding entry to the database
                if ($this->update()) {
                    // We are done with temp_path2, the file isn't there anymore
                    unset($this->temp_path2);
                    return true;
                }
            } else {
                // File was not moved.
                $this->errors[] = "The file upload failed, possibly due to incorrect permissions on the upload folder.";

                return false;
            }
            /** end if	* */
        } else {
            // Make sure there are no errors
            // Can't save if there are pre-existing errors
            if (!empty($this->errors)) {
                return false;
            }

            // Make sure the caption is not too long for the DB
            /*if (strlen($this->image_caption) > 255) {
                $this->errors[] = "The caption can only be 255 characters long.";
                return false;
            }

            // Can't save without file_path and temp location
            if (empty($this->copyright_epress_image) || empty($this->temp_path2)) {

                $this->errors[] = "The file location was not available.";
                return false;
            }*/

            // Determine the target_path
            //$target_path = $this->upload_dir2 . 'DS' . $this->copyright_epress_image;

            // Make sure a file doesn't already exist in the target location
            /*if (file_exists($target_path)) {
                echo "The file {$this->copyright_epress_image} already exists.";
                $this->errors[] = "The file {$this->copyright_epress_image} already exists.";
                return false;
            }*/

            // Attempt to move the file 

            /*if (move_uploaded_file($this->temp_path2, $target_path)) {

                // Success
                // Save a corresponding entry to the database
                if ($this->create()) {
                    // We are done with temp_path2, the file isn't there anymore
                    unset($this->temp_path2);
                    return true;
                }
            } else {
                // File was not moved.
                $this->errors[] = "The file upload failed, possibly due to incorrect permissions on the upload folder.";

                return false;
            }*/
        }
    }

    // Common Database Methods
    public static function find_all() {
        // retourne tous les enregistrements actifs et supprimés
        return self::find_by_sql("SELECT * FROM " . self::$table_name . "  ");
    }

    public static function find_all_active() {
        // retourne tous les enregistrements actifs
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE copyright_active = 1  ");
    }

    public static function find_all_unabled() {
        // retourne tous les enregistrements supprimés
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE copyright_active = 0  ");
    }

    public static function find_by_id($id = 0) {
        // echo "SELECT * FROM " . self::$table_name . " WHERE copyright_id={$id}";
        // retourne  un enregistrements actif
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE copyright_id={$id}");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function list_others($id = 0) {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE copyright_id<>{$id} AND copyright_active = 1");
    }

}

?>