<?php

/**
 * Description of page
 *
 * @author Menz ALFA
 */
// Define the core paths
require_once('core_paths_definition.php');

require_once(LIB_PATH . DS . 'initialize.php');

class Type_topic {

    //put your code here
    protected static $table_name = "press_type_topic";
    protected static $db_fields = array(
        'type_topic_id',
        'type_topic_desc',
        'type_topic_date',
        'type_topic_position',
        'type_topic_active'
    );
    public $type_topic_id;
    public $type_topic_desc;
    public $type_topic_date;
    public $type_topic_position;
    public $type_topic_active;

    private static function instantiate($record) {
        // Could check that $record exists and is an array
        $object = new self;
        // Simple, long-form approach:
        // $object->id 				= $record['id'];
        // $object->username 	= $record['username'];
        // $object->password 	= $record['password'];
        // $object->first_name = $record['first_name'];
        // $object->last_name 	= $record['last_name'];
        // More dynamic, short-form approach:
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    private function has_attribute($attribute) {
        // We don't care about the value, we just want to know if the key exists
        // Will return true or false
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes() {
        // return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes() {
        global $database;
        $clean_attributes = array();
        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    public static function find_by_sql($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_array($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function count_by_sql($sql) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    /*public static function count_by_sql($sql) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return array_shift($row);
    }*/

    public function save() {
        // A new record won't have an id yet.
        return isset($this->type_topic_id) ? $this->update() : $this->create();
    }

    public function create() {
        global $database;
    
        $attributes = $this->sanitized_attributes();
    
        // Construction de la requête préparée
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= implode(", ", array_keys($attributes));
        $sql .= ") VALUES (";
        $sql .= implode(", ", array_fill(0, count($attributes), "?")); // Remplacement des valeurs par des marqueurs de paramètres
        $sql .= ")";
    
        // Préparation de la requête
        $stmt = $database->prepare($sql);
    
        // Exécution de la requête avec les valeurs des attributs comme paramètres
        if ($stmt->execute(array_values($attributes))) {
            $this->type_topic_id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }    

    /*public function create() {
        global $database;
    
        // On récupère les attributs sanitizés
        $attributes = $this->sanitized_attributes();
    
        // On construit la requête SQL avec des paramètres nommés
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= implode(", ", array_keys($attributes));
        $sql .= ") VALUES (";
        $sql .= implode(", ", array_map(fn($key) => ":$key", array_keys($attributes)));
        $sql .= ")";
    
        // On prépare la requête
        $stmt = $database->prepare($sql);
    
        // On lie les valeurs aux paramètres nommés
        foreach ($attributes as $key => $value) {
            $stmt->bindValue(":$key", $value);
        }
    
        // On exécute la requête
        try {
            $database->execute($stmt);
            $this->type_topic_id = $database->insert_id();
            return true;
        } catch (Exception $e) {
            // Gérer l'erreur ici
            return false;
        }
    }*/
    
    public function update() {
        global $database;
        
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = [];
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= implode(", ", $attribute_pairs);
        $sql .= " WHERE type_topic_id=?";
        
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
        $value = array_values($attributes);
        $value[] = $this->type_topic_id;
        if ($stmt->execute($value)) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }
    }
    

    public function delete() {
        global $database;

        $sql = "DELETE FROM " . self::$table_name . " WHERE type_topic_id=?";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec l'ID comme paramètre
        if ($stmt->execute([$this->type_topic_id])) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }
    }

    public static function find_all($limit = 0) {
        $sql = "SELECT * FROM " . self::$table_name . " ORDER BY type_topic_id DESC";
        if ($limit > 0) {
            $sql .= " LIMIT {$limit}";
        }
        return self::find_by_sql($sql);
    }
    
    public static function find_all_active() {
        $sql = "SELECT * FROM " . self::$table_name . " WHERE type_topic_active = 1 ORDER BY type_topic_id DESC";
        return self::find_by_sql($sql);
    }
    
    public static function find_all_unabled() {
        $sql = "SELECT * FROM " . self::$table_name . " WHERE type_topic_active = 0 ORDER BY type_topic_id DESC";
        return self::find_by_sql($sql);
    }
    
    public static function find_by_id($id = 0) {
        $sql = "SELECT * FROM " . self::$table_name . " WHERE type_topic_id = ? LIMIT 1";
        $params = [ $id ];
        return self::find_by_sql($sql, $params)[0] ?? false;
    }
    
    public static function list_others($id = 0) {
        $sql = "SELECT * FROM " . self::$table_name . " WHERE type_topic_id <> ?";
        $params = [ $id ];
        return self::find_by_sql($sql, $params);
    }
    
    public static function array_more_than_n_topics_limit_m($n = 0, $l = 0) {
        $sql = "SELECT type_topic_id, (SELECT COUNT(*) FROM topics WHERE topic_typetopic_id = type_topic_id) AS topic_count ";
        $sql .= "FROM " . self::$table_name . " ";
        $sql .= "WHERE type_topic_active = 1 AND (SELECT COUNT(*) FROM topics WHERE topic_typetopic_id = type_topic_id) > :n ";
        $sql .= "ORDER BY type_topic_position ASC ";
        $sql .= "LIMIT :l";
        $params = [':n' => $n, ':l' => $l];
        $result = self::find_by_sql($sql, $params);
        return array_column($result, 'type_topic_id');
    }
    
    public static function array_less_than_n_topics_limit_m($n = 0, $l = 0) {
        $sql = "SELECT type_topic_id, (SELECT COUNT(*) FROM topics WHERE topic_typetopic_id = type_topic_id) AS topic_count ";
        $sql .= "FROM " . self::$table_name . " ";
        $sql .= "WHERE type_topic_active = 1 AND (SELECT COUNT(*) FROM topics WHERE topic_typetopic_id = type_topic_id) <= :n ";
        $sql .= "ORDER BY type_topic_position ASC ";
        $sql .= "LIMIT :l";
        $params = [':n' => $n, ':l' => $l];
        $result = self::find_by_sql($sql, $params);
        return array_column($result, 'type_topic_id');
    }
    
    public static function find_all_limit_n($n = 0) {
        $sql = "SELECT * FROM " . self::$table_name . " WHERE type_topic_active = 1 ORDER BY type_topic_position ASC LIMIT :n";
        $params = [':n' => $n];
        return self::find_by_sql($sql, $params);
    }
    
    public static function find_by_search($search = "") {
        $sql = "SELECT type_topic_id FROM press_type_topic WHERE type_topic_desc_fr LIKE :search";
        $params = [':search' => '%' . $search . '%'];
        return self::find_by_sql($sql, $params);
    }
       

    public static function find_by_position($id = 0) {
        $sql = "SELECT * FROM " . self::$table_name . " WHERE type_topic_position = :id LIMIT 1";
        $params = [':id' => $id];
        return self::find_by_sql($sql, $params)[0] ?? false;
    }
    
    public static function count_all() {
        $sql = "SELECT COUNT(*) FROM " . self::$table_name;
        return self::find_by_sql($sql)[0]['COUNT(*)'];
    }
    
    public static function count_active() {
        $sql = "SELECT COUNT(*) FROM " . self::$table_name . " WHERE type_topic_active = 1";
        return self::find_by_sql($sql)[0]['COUNT(*)'];
    }
    
    public static function count_for_search($search = "") {
        $sql = "SELECT COUNT(*) FROM " . self::$table_name . " WHERE type_topic_desc_fr LIKE :search";
        $params = [':search' => '%' . $search . '%'];
        return self::find_by_sql($sql, $params)[0]['COUNT(*)'];
    }
    
    public static function array_find_all() {
        $sql = "SELECT type_topic_id FROM " . self::$table_name . " WHERE type_topic_active = 1 ORDER BY type_topic_position ASC";
        $results = self::find_by_sql($sql);
        return array_column($results, 'type_topic_id');
    }
    

    public function unable() {
        $sql = "UPDATE " . self::$table_name . " SET type_topic_active = 0 WHERE type_topic_id = :id";
        $params = [':id' => $this->type_topic_id];
        return self::find_by_sql($sql, $params);
    }
    
    public function recover() {
        $sql = "UPDATE " . self::$table_name . " SET type_topic_active = 1 WHERE type_topic_id = :type_topic_id";
        $params = [':type_topic_id' => $this->type_topic_id];
        return self::find_by_sql($sql, $params);
    }
    
    public static function find_first_topic($limit = 0) {
        $sql = "SELECT * FROM " . self::$table_name . " WHERE type_topic_active = 1 AND type_topic_position <> 0 ORDER BY type_topic_position ASC LIMIT :limit";
        $params = [':limit' => $limit];
        return self::find_by_sql($sql, $params);
    }
}    
    $type_topic = new Type_topic();
?>
