<?php

/**
 * Description of page
 *
 * @author Menz ALFA
 */
// Define the core paths
require_once('core_paths_definition.php');

require_once(LIB_PATH.DS.'initialize.php');

class Page {
    

    // Page attributes
    protected static $table_name = "press_page";
    protected static $db_fields = array(
      'page_id',
      'page_parent_id',
      'page_uid',
      'page_title',
      'page_desc',
      'page_link',
      'page_position',
      'page_date',
      'page_image_path',
      'page_only_desc',
      'page_section',      
      'page_active'
        );
    public $page_id;
    public $page_parent_id;
    public $page_uid;
    public $page_title;
    public $page_desc;
    public $page_link;
    public $page_position;
    public $page_date;
    public $page_image_path;
    public $page_only_desc;
    public $page_section;
    public $page_active;

    // Instanciation des attributs de la classe

    private static function instantiate($record) {
        // permet de retourner un enregistrement d'une table dans une instance de la classe
        $object = new self;
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    // Assigne tous les champs de table aux attributs de la classe
    private function has_attribute($attribute) {
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes() {
        // return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes() {
        // La fonction sanitize permet d'échapper les caratères spéciaux des
        global $database;
        $clean_attributes = array();
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] =  $database->escape_value($value);
        }
        return $clean_attributes;
    }

    // Common database methods

    public static function find_by_sql($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_array($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }


    public static function find_by_sql_assoc($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_assoc($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public function save() {
        return isset($this->page_id) ? $this->update() : $this->create();
    }

    public function create() {
        global $database;
        $attributes = $this->sanitized_attributes();
        // Construction de la requête préparée
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= implode(", ", array_keys($attributes));
        $sql .= ") VALUES (";
        $sql .= implode(", ", array_fill(0, count($attributes), "?")); // Remplacement des valeurs par des marqueurs de paramètres
        $sql .= ")";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        // Exécution de la requête avec les valeurs des attributs comme paramètres
        if ($stmt->execute(array_values($attributes))) {
            $this->page_id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }


    public function update() {
        global $database;
        // Don't forget your SQL syntax and good habits:
        // - UPDATE table SET key='value', key='value' WHERE condition
        // - single-quotes around all values
        // - escape all values to prevent SQL injection
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= implode(", ", $attribute_pairs);
        $sql .= " WHERE page_id=?";
        
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
        $value = array_values($attributes);
        $value[] = $this->page_id;
        if ($stmt->execute($value)) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }
    }

    public function delete() {
        global $database;
        $sql = "DELETE FROM " . self::$table_name;
        $sql .= " WHERE page_id= ?";
        $sql .= " LIMIT 1";
        // Préparation de la requête permet de prevenir les injections SQL
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec l'ID comme paramètre
        if ($stmt->execute([$this->page_id])) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }

    }

    public function unable() {
        global $database;
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql  = "UPDATE " . self::$table_name . " SET page_active = 0 ";
        $sql .= "WHERE page_id= ?";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
        $value = array_values($attributes);
        $value[] = $this->page_id;
        if ($stmt->execute($value)) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }
    }

    public function recover() {
        global $database;
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql  = "UPDATE " . self::$table_name . " SET page_active = 1 ";
        $sql .= "WHERE page_id= ?";
       // Préparation de la requête
       $stmt = $database->prepare($sql);
        
       // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
       $value = array_values($attributes);
       $value[] = $this->page_id;
       if ($stmt->execute($value)) {
           return ($database->affected_rows($stmt) == 1) ? true : false;
       } else {
           return false;
       }
    }
    public static function find_all() {
        // retourne tous les enregistrements actifs et supprimés
        return self::find_by_sql("SELECT * FROM " . self::$table_name . "  ORDER BY page_position ASC ");
    }
    public static function find_all_active_prrincipal() {
        // retourne tous les enregistrements actifs et supprimés
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE page_parent_id = 0 AND page_section = 0 AND page_active = 1 ORDER BY page_position ASC ");
    }


    public static function find_page_children($parent_id=0) {
                  return self::find_by_sql_assoc("SELECT * FROM ".self::$table_name."  WHERE  page_parent_id = {$parent_id}  AND page_section=0 AND page_active=1 ORDER BY page_id ASC");
}

   public static function find_page_parent($pid=0) {
                  return self::find_by_sql("SELECT * FROM ".self::$table_name."  WHERE page_parent_id = {$pid}  AND page_section=0 ORDER BY page_id ASC");

}

   public static function find_the_parent($pid=0) {
                  $result_array = self::find_by_sql("SELECT * FROM ".self::$table_name."  WHERE page_id = {$pid}  ORDER BY page_id ASC");
                  return !empty($result_array) ? array_shift($result_array) : false;
}

    public static function find_all_active() {
        // retourne tous les enregistrements actifs
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE page_active = 1 AND page_section = 0  ORDER BY page_position ASC ");
    }

    public static function find_all_parent_active($id=0) {
        // retourne tous les enregistrements actifs
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE  page_parent_id = {$id} AND page_section = 0 AND page_active = 1 ORDER BY page_position ASC ");
    }

     public static function find_all_children_active($parent_id=0) {
        // retourne tous les enregistrements actifs
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE  page_parent_id = {$parent_id} AND page_active = 1 ORDER BY page_position ASC ");
    }

     public static function find_all_page_sections() {
        // retourne tous les enregistrements actifs
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE page_section = 1 ORDER BY page_position ASC ");
    }
    

    public static function find_all_unabled() {
        // retourne tous les enregistrements supprimés
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE page_active = 0 ORDER BY page_position ASC ");
    }

    public static function find_by_id($id = 0) {
        // retourne  un enregistrements actif
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name ." WHERE page_id={$id}");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function list_others($id = 0) {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE page_id <> {$id} AND page_parent_id <> {$id} AND page_active = 1");
    }
 
    public static function find_page_position($id = 0) {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE page_position={$id}");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function find_by_title($title) {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE page_title='$title'");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function count_all() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    public static function count_page_children($page_id = 0) {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name." WHERE page_parent_id = {$page_id} AND page_section=0  AND page_active = 1";
        // echo $sql;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    public static function count_page_children_active($page_id = 0) {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name." WHERE page_parent_id = {$page_id} AND page_section=0  AND page_active = 1 AND page_active = 1";
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    public function has_children(){
        return (self::count_page_children($this->page_id));
    }

    public function page_elements() {
        return Page_element::find_by_sql("SELECT * FROM page_elements  WHERE  page_element_page_id = ".$this->page_id."  ORDER BY page_element_id ASC");
    }

    public function count_page_elements(){
        global $database;
        $sql = "SELECT count(*) FROM page_elements  WHERE  page_element_page_id = ".$this->page_id." ORDER BY page_element_id ASC";
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return ($row) ? array_shift($row) : 0;
    }


 

}

 $page = new Page();

?>
