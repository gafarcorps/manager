<?php
// Define the core paths
require_once 'core_paths_definition.php';

// load config file first
require_once LIB_PATH . DS . 'config.php';

// load basic functions next so that everything after can use them
require_once LIB_PATH . DS . 'functions.php';

// load core objects
require_once LIB_PATH . DS . 'session.php';

require_once LIB_PATH . DS . 'database.php';
require_once LIB_PATH . DS . 'mysql_notif.php';
//require_once(LIB_PATH.DS.'database_object.php');
require_once LIB_PATH . DS . 'pagination.php';
require_once LIB_PATH . DS . 'lang.php';
/*
require_once(LIB_PATH.DS."phpMailer".DS."class.phpmailer.php");
require_once(LIB_PATH.DS."phpMailer".DS."class.smtp.php");
require_once(LIB_PATH.DS."phpMailer".DS."language".DS."phpmailer.lang-en.php");
 */

// load user-related classes
require_once LIB_PATH . DS . 'user_info.php';
require_once LIB_PATH . DS . 'user_account.php';
require_once LIB_PATH . DS . 'type_user.php';

// load page-related classes
require_once LIB_PATH . DS . 'page.php';
require_once LIB_PATH . DS . 'page_element.php';
require_once LIB_PATH . DS . 'page_section.php';
require_once LIB_PATH . DS . 'page_advertise.php';
require_once LIB_PATH . DS . 'sections.php';
require_once LIB_PATH . DS . 'social.php';
require_once LIB_PATH . DS . 'type_banner.php';

// load topic-related classes
require_once LIB_PATH . DS . 'topics.php';
require_once LIB_PATH . DS . 'topic_section.php';
require_once LIB_PATH . DS . 'typetopic.php';
require_once LIB_PATH . DS . 'comment_topic.php';
require_once LIB_PATH . DS . 'press_tag.php';
require_once LIB_PATH . DS . 'etiquette.php';
require_once LIB_PATH . DS . 'files.php';
require_once LIB_PATH . DS . 'filter.php';

// load menu_privileges-related classes
require_once LIB_PATH . DS . 'menu.php';
require_once LIB_PATH . DS . 'privileges.php';

// load gallery-related classes
require_once LIB_PATH . DS . 'gallery.php';

//require_once LIB_PATH . DS . 'copyr.php';
//require_once LIB_PATH . DS . 'ticket.php';

/* Form_builder*/
//require_once LIB_PATH . DS . 'db_menu.php';
//require_once LIB_PATH . DS . 'db_privileges.php';
require_once LIB_PATH . DS . 'app_request.php';
//require_once LIB_PATH . DS . 'form_builder.php';
//require_once LIB_PATH . DS . 'form_builder_table.php';
//require_once LIB_PATH . DS . 'form_builder_object.php';
/* Epress*/
//require_once LIB_PATH . DS . 'abonnes.php';
//require_once LIB_PATH . DS . 'readers_typetopic.php';
//require_once LIB_PATH . DS . 'abonnement.php';
//require_once LIB_PATH . DS . 'reader_menu.php';
//require_once LIB_PATH . DS . 'type_abonnement.php';
//require_once LIB_PATH . DS . 'epress.php';

/* Qualiactions*/ 
require_once LIB_PATH . DS . 'group.php';
//require_once LIB_PATH . DS . 'link_group.php';
//require_once LIB_PATH . DS . 'activity.php';
//require_once LIB_PATH . DS . 'link_activity.php';

//require_once LIB_PATH . DS . 'service.php';  
//require_once LIB_PATH . DS . 'contract.php';