<?php

/**
 * Description of type_user
 *
 * @author D9
 */
// Define the core paths
require_once('core_paths_definition.php');

require_once(LIB_PATH . DS . 'initialize.php');

class Typeuser {

    // type_user attributes
    protected static $table_name = "type_user";
    protected static $db_fields = array(
        'type_user_id',
        'type_user_title',
        'type_user_desc',
        'type_user_image_path',
        'type_user_date_creation',
        'type_user_user_creation',
        'type_user_user_last_modification',
        'type_user_date_last_modification',
        'type_user_deleted',
        'type_user_active'
    );
    public $type_user_id;
    public $type_user_title;
    public $type_user_desc;
    public $type_user_image_path;
    public $type_user_date_creation;
    public $type_user_user_creation;
    public $type_user_user_last_modification;
    public $type_user_date_last_modification;
    public $type_user_deleted;
    public $type_user_active;

    // Instanciation des attributs de la classe

    private static function instantiate($record) {
        // permet de retourner un enregistrement d'une table dans une instance de la classe
        $object = new self;
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    // Assigne tous les champs de table aux attributs de la classe
    private function has_attribute($attribute) {
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes() {
        // return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes() {
        // La fonction sanitize permet d'échapper les caratères spéciaux des
        global $database;
        $clean_attributes = array();
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    // Common database methods

    public static function find_by_sql($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_array($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function find_by_sql_assoc($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_assoc($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function count_by_sql($sql) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    public function save() {
        return isset($this->type_user_id) ? $this->update() : $this->create();
    }

    public function create() {
        global $database;
        $attributes = $this->sanitized_attributes();
        // Construction de la requête préparée
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= implode(", ", array_keys($attributes));
        $sql .= ") VALUES (";
        $sql .= implode(", ", array_fill(0, count($attributes), "?")); // Remplacement des valeurs par des marqueurs de paramètres
        $sql .= ")";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        // Exécution de la requête avec les valeurs des attributs comme paramètres
        if ($stmt->execute(array_values($attributes))) {
            $this->type_user_id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }

    public function update() {
        global $database;
    
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= implode(", ", $attribute_pairs);
        $sql .= " WHERE type_user_id=?";
        
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
        $value = array_values($attributes);
        $value[] = $this->type_user_id;
        if ($stmt->execute($value)) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }
    }

    public function unable() {
        global $database;
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql  = "UPDATE " . self::$table_name . " SET type_user_active = 0 ";
        $sql .= "WHERE type_user_id= ?";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
        $value = array_values($attributes);
        $value[] = $this->type_user_id;
        if ($stmt->execute($value)) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }
    }

    public function recover() {
        global $database;
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql  = "UPDATE " . self::$table_name . " SET type_user_active = 1 ";
        $sql .= "WHERE type_user_id= ?";
       // Préparation de la requête
       $stmt = $database->prepare($sql);
        
       // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
       $value = array_values($attributes);
       $value[] = $this->type_user_id;
       if ($stmt->execute($value)) {
           return ($database->affected_rows($stmt) == 1) ? true : false;
       } else {
           return false;
       }
    }

    public function delete() {
        global $database;
        $sql = "DELETE FROM " . self::$table_name;
        $sql .= " WHERE type_user_id= ?";
        $sql .= " LIMIT 1";
        // Préparation de la requête permet de prevenir les injections SQL
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec l'ID comme paramètre
        if ($stmt->execute([$this->type_user_id])) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }

    }

    public static function find_all() {
        // retourne tous les enregistrements actifs et supprimés
        return self::find_by_sql("SELECT * FROM " . self::$table_name . "  ORDER BY type_user_id DESC ");
    }

    public static function find_all_active() {
        // retourne tous les enregistrements actifs
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE type_user_active = 1 AND type_user_deleted = 0  ORDER BY type_user_id DESC ");
    }
    
    public static function find_all_unabled() {
        // retourne tous les enregistrements désactivés
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE type_user_active = 0 ORDER BY type_user_id DESC ");
    }
    
    public static function find_all_not_deleted() {
        // retourne tous les enregistrements non supprimés
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE type_user_deleted = 0 ORDER BY type_user_id DESC ");
    }
    public static function find_all_deleted() {
        // retourne tous les enregistrements supprimés
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE type_user_deleted = 1  ORDER BY type_user_id DESC ");
    }

    public static function find_by_id($id = 0) {
        // retourne  un enregistrements actif
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE type_user_id={$id}");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function list_others($id = 0) {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE type_user_id <> {$id} AND type_user_active = 1 AND type_user_deleted = 0");
    }

    public static function count_all() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

}

$type_user = new Typeuser();
?>
