<?php

/**
 * Description of page
 *
 * @author Menz ALFA
 */
// Define the core paths
require_once('core_paths_definition.php');

require_once(LIB_PATH . DS . 'initialize.php');

class Page_element {

    //put your code here
    protected static $table_name = "page_elements";
    protected static $db_fields = array(
        'page_element_id',
        'page_element_page_id',
        'page_element_title',
        'page_element_file',
        'page_element_desc',
        'page_element_url',
        'page_element_icon',
        'page_element_date',
        'page_element_position',
        'page_element_active'
    );
    public $page_element_id;
    public $page_element_page_id;
    public $page_element_title;
    public $page_element_file;
    public $page_element_desc;
    public $page_element_url;
    public $page_element_icon;
    public $page_element_date;
    public $page_element_position;
    public $page_element_active;

    private static function instantiate($record) {
        // Could check that $record exists and is an array
        $object = new self;
        // Simple, long-form approach:
        // $object->id 				= $record['id'];
        // $object->username 	= $record['username'];
        // $object->password 	= $record['password'];
        // $object->first_name = $record['first_name'];
        // $object->last_name 	= $record['last_name'];
        // More dynamic, short-form approach:
        foreach ($record as $attribute => $value) {
            if ($object->has_attribute($attribute)) {
                $object->$attribute = $value;
            }
        }
        return $object;
    }

    private function has_attribute($attribute) {
        // We don't care about the value, we just want to know if the key exists
        // Will return true or false
        return array_key_exists($attribute, $this->attributes());
    }

    protected function attributes() {
        // return an array of attribute names and their values
        $attributes = array();
        foreach (self::$db_fields as $field) {
            if (property_exists($this, $field)) {
                $attributes[$field] = $this->$field;
            }
        }
        return $attributes;
    }

    protected function sanitized_attributes() {
        global $database;
        $clean_attributes = array();
        // sanitize the values before submitting
        // Note: does not alter the actual value of each attribute
        foreach ($this->attributes() as $key => $value) {
            $clean_attributes[$key] = $database->escape_value($value);
        }
        return $clean_attributes;
    }

    public static function find_by_sql($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_array($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function find_by_sql_assoc($sql = "", $params = []) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute($params); // Passer les paramètres à la méthode execute()
        $object_array = [];
        while ($row = $database->fetch_assoc($stmt)) {
            $object_array[] = self::instantiate($row);
        }
        return $object_array;
    }

    public static function count_by_sql($sql) {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    public static function execute_sql($sql = "") {
        global $database;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        return ($database->affected_rows($stmt) == 1) ? true : false;
    }


    public function save() {
        // A new record won't have an id yet.
        return isset($this->page_element_id) ? $this->update() : $this->create();
    }

    public function create() {
        global $database;
        $attributes = $this->sanitized_attributes();
        // Construction de la requête préparée
        $sql = "INSERT INTO " . self::$table_name . " (";
        $sql .= implode(", ", array_keys($attributes));
        $sql .= ") VALUES (";
        $sql .= implode(", ", array_fill(0, count($attributes), "?")); // Remplacement des valeurs par des marqueurs de paramètres
        $sql .= ")";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        // Exécution de la requête avec les valeurs des attributs comme paramètres
        if ($stmt->execute(array_values($attributes))) {
            $this->page_element_id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }

    public function update() {
        global $database;
    
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql = "UPDATE " . self::$table_name . " SET ";
        $sql .= implode(", ", $attribute_pairs);
        $sql .= " WHERE page_element_id=?";
        
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
        $value = array_values($attributes);
        $value[] = $this->page_element_id;
        if ($stmt->execute($value)) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }
    }

    public function delete() {
        global $database;
        $sql = "DELETE FROM " . self::$table_name;
        $sql .= " WHERE page_element_id= ?";
        $sql .= " LIMIT 1";
        // Préparation de la requête permet de prevenir les injections SQL
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec l'ID comme paramètre
        if ($stmt->execute([$this->page_element_id])) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }

    }

    public static function find_all($limit = 0) {
        // retourne tous les enregistrements actifs et supprimés
        return ($limit == 0) ? self::find_by_sql("SELECT * FROM " . self::$table_name . " ORDER BY page_element_id DESC") : self::find_by_sql("SELECT * FROM " . self::$table_name . " ORDER BY page_element_id DESC LIMIT {$limit} ");
    }

    public static function find_all_active() {
        // retourne tous les enregistrements actifs
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE page_element_active = 1 ORDER BY page_element_id DESC");
    }

    public static function find_all_unabled() {
        // retourne tous les enregistrements supprimés
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE page_element_active = 0 ORDER BY page_element_id DESC");
    }

    public static function find_by_id($id = 0) {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE page_element_id={$id} LIMIT 1");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function find_by_page_id($id) {
//        echo '>'."SELECT * FROM " . self::$table_name . " WHERE page_element_page_id={$id} ORDER BY page_element_position ";
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE page_element_page_id={$id} ORDER BY page_element_position ");
    }

    public static function list_others($id = 0) {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE page_element_id<>{$id}");
    }

    ##Rubriques ayant plus de n commentaires

    public static function array_less_than_n_topics_limit_m($n = 0, $l = 0) {
        $sql = "SELECT page_element_id, (SELECT COUNT(*) FROM topics WHERE topic_typetopic_id = page_element_id)";
        $sql .= " FROM " . self::$table_name . "";
        $sql .= " WHERE page_element_active = 1 AND (SELECT COUNT(*) FROM topics WHERE topic_typetopic_id = page_element_id) <= :n";
        $sql .= " ORDER BY page_element_position ASC";
        $sql .= " LIMIT :l";
    
        $topics = self::find_by_sql($sql, [':n' => $n, ':l' => $l]);
    
        $result = [];
        foreach ($topics as $topic) {
            $result[] = $topic->page_element_id;
        }
    
        return $result;
    }
    

    public static function array_more_than_n_topics_limit_m($n = 0, $l = 0) {
        $sql = "SELECT page_element_id, (SELECT COUNT(*) FROM topics WHERE topic_typetopic_id = page_element_id)";
        $sql .= " FROM " . self::$table_name . "";
        $sql .= " WHERE page_element_active = 1 AND (SELECT COUNT(*) FROM topics WHERE topic_typetopic_id = page_element_id) > :n";
        $sql .= " ORDER BY page_element_position ASC";
        $sql .= " LIMIT :l";
    
        $topics = self::find_by_sql($sql, [':n' => $n, ':l' => $l]);
    
        $result = [];
        foreach ($topics as $topic) {
            $result[] = $topic->page_element_id;
        }
    
        return $result;
    }
    
    ##Rubriques ayant plus de n commentaires
    ## n derniers articles

    public static function find_all_limit_n($n = 0) {
        return self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE page_element_active = 1 ORDER BY page_element_position ASC limit $n");
    }

    public static function find_by_search($search = "") {
        return self::find_by_sql("SELECT page_element_id FROM press_page_element WHERE page_element_desc_fr LIKE '%" . $search . "%'");
    }

    public static function find_by_position($id = 0) {
        $result_array = self::find_by_sql("SELECT * FROM " . self::$table_name . " WHERE page_element_position={$id} LIMIT 1");
        return !empty($result_array) ? array_shift($result_array) : false;
    }

    public static function count_all() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name;
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    public static function count_active() {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name . " WHERE page_element_active = 1";
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    public static function count_for_search($search = "") {
        global $database;
        $sql = "SELECT COUNT(*) FROM " . self::$table_name . " WHERE page_element_desc_fr LIKE '%" . $search . "%'";
        $stmt = $database->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch(PDO::FETCH_NUM);
        return $row[0];
    }

    ## n Derniers articles 

    public static function array_find_all() {
        $sql = "SELECT page_element_id";
        $sql .= " FROM " . self::$table_name . "";
        $sql .= " WHERE page_element_active = 1";
        $sql .= " ORDER BY page_element_position ASC";
    
        $topics = self::find_by_sql($sql);
    
        $result = [];
        foreach ($topics as $topic) {
            $result[] = $topic->page_element_id;
        }
    
        return $result;
    }
    

    public function unable() {
        global $database;
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql  = "UPDATE " . self::$table_name . " SET page_element_active = 0 ";
        $sql .= "WHERE page_element_id= ?";
        // Préparation de la requête
        $stmt = $database->prepare($sql);
        
        // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
        $value = array_values($attributes);
        $value[] = $this->page_element_id;
        if ($stmt->execute($value)) {
            return ($database->affected_rows($stmt) == 1) ? true : false;
        } else {
            return false;
        }
    }

    public function recover() {
        global $database;
        $attributes = $this->sanitized_attributes();
        $attribute_pairs = array();
        foreach ($attributes as $key => $value) {
            $attribute_pairs[] = "{$key}=?";
        }
        $sql  = "UPDATE " . self::$table_name . " SET page_element_active = 1 ";
        $sql .= "WHERE page_element_id= ?";
       // Préparation de la requête
       $stmt = $database->prepare($sql);
        
       // Exécution de la requête avec les valeurs des attributs et de l'ID comme paramètres
       $value = array_values($attributes);
       $value[] = $this->page_element_id;
       if ($stmt->execute($value)) {
           return ($database->affected_rows($stmt) == 1) ? true : false;
       } else {
           return false;
       }
    }

}

$page_element = new Page_element();
?>
