<?php
require_once("includes/initialize.php");
require_once("includes/verifsession.php");
?>
<?php
$found_userinfos = User_info::find_user_infos($session->user_account_id);
$found_useraccount = User_account::find_by_id($session->user_account_id);
$found_typeuser = Typeuser::find_by_id($found_useraccount->user_account_typeuser_id);
$app_or_appex = ($found_useraccount->user_account_typeuser_id > 2) ? "appex" : "app";
?>
<!DOCTYPE html>
<html lang="en">
    <!-- BEGIN HEAD -->

    <head>

        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta name="description" content="Responsive Admin Template" />
        <meta name="author" content="Manager3.0" />
        <title> Manager | Version 3.0 </title>
        <!-- icons -->
        <link href="assets/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <!--bootstrap -->
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/summernote/summernote.css" rel="stylesheet">
        <!-- data tables -->
        <link href="assets/plugins/datatables/plugins/bootstrap/dataTables.bootstrap4.min.css" rel="stylesheet"
              type="text/css" />
        <!-- morris chart -->
        <link href="assets/plugins/morris/morris.css" rel="stylesheet" type="text/css" />
        <!-- Material Design Lite CSS -->
        <link rel="stylesheet" href="assets/plugins/material/material.min.css">
        <link rel="stylesheet" href="assets/css/material_style.css">
        <!-- animation -->
        <link href="assets/css/pages/animate_page.css" rel="stylesheet">
        <!-- inbox style -->
        <link href="assets/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/style.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/responsive.css" rel="stylesheet" type="text/css" />
        <link href="assets/css/theme-color.css" rel="stylesheet" type="text/css" />
        <!-- Owl Carousel Assets -->
        <link href="assets/plugins/owl-carousel/owl.carousel.css" rel="stylesheet">
        <link href="assets/plugins/owl-carousel/owl.theme.css" rel="stylesheet"> 
        <!-- dropzone -->
        <link href="assets/plugins/dropzone/dropzone.css" rel="stylesheet" media="screen">
        <!--tagsinput-->
        <link href="assets/plugins/jquery-tags-input/jquery-tags-input.css" rel="stylesheet">
        <!--select2-->
        <link href="assets/plugins/select2/css/select2.css" rel="stylesheet" type="text/css" />
        <link href="assets/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />


        <!-- animation -->
        <link href="assets/css/pages/animate_page.css" rel="stylesheet">
        <link rel="stylesheet" href="assets/plugins/sweet-alert/sweetalert.min.css">
        <!-- gallery -->
        <link href="assets/plugins/light-gallery/css/lightgallery.css" rel="stylesheet">


        <!-- favicon -->
        <link rel="shortcut icon" href="assets/img/favicon.ico" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
        <script type="text/javascript" src="tiny_mce/js/tinymce/tinymce.min.js"></script>
        <script type="text/javascript">

            tinymce.init({
            selector: "textarea",
                    automatic_uploads: true,
                    theme: "modern",
//                    width: 920,
                    height: 300,
                    subfolder: "",
                    plugins: [
                            "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                            "searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking",
                            "table contextmenu directionality emoticons paste textcolor filemanager"
                    ],
                    image_advtab: true,
                    toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect forecolor backcolor | link unlink anchor | image media | print preview code"
            });
//            function popup
//            {
//            window.open('popup.php,'Pop - up','toolbar = 0, location = 0, directories = 0, menuBar = 0, resizable = 0, scrollbars = yes, width = 470, height = 400, left = 75, top = 60');
//            }


        </script>


    </head>

    <!-- END HEAD -->

    <body
        class="page-header-fixed sidemenu-closed-hidelogo page-content-white page-md header-white dark-sidebar-color logo-dark">
        <div class="page-wrapper">
            <!-- start header -->
            <div class="page-header navbar navbar-fixed-top">
                <div class="page-header-inner ">
                    <!-- logo start -->
                    <div class="page-logo">
                        <a href="<?php echo $app_or_appex ?>.php?item=home">
<!--						<img alt="" src="assets/img/logo.png" width="40">-->
                            <span class="logo-default">Manager</span> </a>
                    </div>
                    <!-- logo end -->
                    <ul class="nav navbar-nav navbar-left in">
                        <li><a href="#" class="menu-toggler sidebar-toggler"><i class="icon-menu"></i></a></li>
                    </ul>
                    <form class="search-form-opened" action="#" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Rechercher..." name="query">
                            <span class="input-group-btn search-btn">
                                <a href="javascript:;" class="btn submit">
                                    <i class="icon-magnifier"></i>
                                </a>
                            </span>
                        </div>
                    </form>
                    <!-- start mobile menu -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse"
                       data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- end mobile menu -->
                    <!-- start header menu -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                             <!-- start notification dropdown -->
                            <?php  $found_topics_comments = Comment_topic :: find_all_not_published(); ?>
<!--                            <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                                            data-close-others="true">
                                            <i class="fa fa-bell-o"></i>
                                            <span class="badge headerBadgeColor1"><?php echo count($found_topics_comments) ?></span>
                                    </a>
                                    <ul class="dropdown-menu animated swing">
                                            <li class="external">
                                                    <h3><span class="bold">Notifications</span></h3>
                                                    <span class="notification-label purple-bgcolor">Commentaires</span>
                                            </li>
                                            <li>
                                                    <ul class="dropdown-menu-list small-slimscroll-style" data-handle-color="#637283">
                                                        <?php
                                                    foreach ($found_topics_comments as $found_topic_comment) {
                                                            echo'<li>
                                                                    <a href="app.php?item=listtopics_comments">
                                                                    <span class="photo">
                                                                            <img src="assets/img/user/avatar.jpg" class="img-circle" alt="" style="width:45px;height:45px">
                                                                    </span>
                                                                    <span class="subject">
                                                                            <span class="from"> ' . $found_topic_comment->comment_topic_author . '</span>
                                                                            <span class="time">' . $found_topic_comment->comment_topic_date . '</span>
                                                                    </span></br>
                                                                     <span class="message"> ' . neatest_trim($found_topic_comment->comment_topic_text,50) . '</span>
                                                            </a>
                                                    </li>';
                                                    }
                                                    ?>
                                                          
                                                    </ul>
                                                     <div class="dropdown-menu-footer">
                                                            <a href="javascript:void(0)"> All notifications </a>
                                                    </div> 
                                            </li>
                                    </ul>
                            </li>-->
                            <!-- end notification dropdown -->
                            <!-- start message dropdown -->
                            <!--<li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                                            data-close-others="true">
                                            <i class="fa fa-envelope-o"></i>
                                            <span class="badge headerBadgeColor2"> 2 </span>
                                    </a>
                                    <ul class="dropdown-menu animated slideInDown">
                                            <li class="external">
                                                    <h3><span class="bold">Messages</span></h3>
                                                    <span class="notification-label cyan-bgcolor">New 2</span>
                                            </li>
                                            <li>
                                                    <ul class="dropdown-menu-list small-slimscroll-style" data-handle-color="#637283">
                                                            <li>
                                                                    <a href="#">
                                                                            <span class="photo">
                                                                                    <img src="assets/img/user/user2.jpg" class="img-circle" alt="">
                                                                            </span>
                                                                            <span class="subject">
                                                                                    <span class="from"> Sarah Smith </span>
                                                                                    <span class="time">Just Now </span>
                                                                            </span>
                                                                            <span class="message"> Jatin I found you on LinkedIn... </span>
                                                                    </a>
                                                            </li>
                                                            <li>
                                                                    <a href="#">
                                                                            <span class="photo">
                                                                                    <img src="assets/img/user/user3.jpg" class="img-circle" alt="">
                                                                            </span>
                                                                            <span class="subject">
                                                                                    <span class="from"> John Deo </span>
                                                                                    <span class="time">16 mins </span>
                                                                            </span>
                                                                            <span class="message"> Fwd: Important Notice Regarding Your Domain
                                                                                    Name... </span>
                                                                    </a>
                                                            </li>
                                                            <li>
                                                                    <a href="#">
                                                                            <span class="photo">
                                                                                    <img src="assets/img/user/user1.jpg" class="img-circle" alt="">
                                                                            </span>
                                                                            <span class="subject">
                                                                                    <span class="from"> Rajesh </span>
                                                                                    <span class="time">2 hrs </span>
                                                                            </span>
                                                                            <span class="message"> pls take a print of attachments. </span>
                                                                    </a>
                                                            </li>
                                                            <li>
                                                                    <a href="#">
                                                                            <span class="photo">
                                                                                    <img src="assets/img/user/user8.jpg" class="img-circle" alt="">
                                                                            </span>
                                                                            <span class="subject">
                                                                                    <span class="from"> Lina Smith </span>
                                                                                    <span class="time">40 mins </span>
                                                                            </span>
                                                                            <span class="message"> Apply for Ortho Surgeon </span>
                                                                    </a>
                                                            </li>
                                                            <li>
                                                                    <a href="#">
                                                                            <span class="photo">
                                                                                    <img src="assets/img/user/user5.jpg" class="img-circle" alt="">
                                                                            </span>
                                                                            <span class="subject">
                                                                                    <span class="from"> Jacob Ryan </span>
                                                                                    <span class="time">46 mins </span>
                                                                            </span>
                                                                            <span class="message"> Request for leave application. </span>
                                                                    </a>
                                                            </li>
                                                    </ul>
                                                    <div class="dropdown-menu-footer">
                                                            <a href="#"> All Messages </a>
                                                    </div>
                                            </li>
                                    </ul>
                            </li>-->
                            <!-- end message dropdown -->
                            <!-- start manage user dropdown -->
                            <li class="dropdown dropdown-user">
                                <?php
                                $found_userinfos = User_info::find_user_infos($session->user_account_id);
                                $found_useraccount = User_account::find_by_id($session->user_account_id);
                                $found_typeuser = Typeuser::find_by_id($found_useraccount->user_account_typeuser_id);
                                $app_or_appex = ($found_useraccount->user_account_typeuser_id > 2) ? "appex" : "app";
                                ?>

                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown"
                                   data-close-others="true">
                                       <?php echo '<img src="media/files/' . $found_userinfos->user_info_image . '" class="img-circle" alt="' . $found_userinfos->full_name() . '">'; ?> 
                                    <span class="username username-hide-on-mobile"> <?php echo $found_userinfos->full_name(); ?> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default animated jello">
<!--                                    <li>
                                        <a href="<?php // echo $app_or_appex ?>.php?item=view_form_builder_objects_db">
                                            <i class="icon-user"></i> Voir profil
                                        </a>
                                    </li>-->
                                    <li>
                                        <a href="app.php?item=formusers&opt=me&option=edit&uid=<?php echo $found_userinfos->user_info_id ?>">
                                            <i class="icon-user"></i> Editer profil 
                                        </a>
                                    </li>

                                    <li>
                                        <a href="logout.php">
                                            <i class="icon-logout"></i> Déconnexion 
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- end manage user dropdown -->
                            <!--<li class="dropdown dropdown-quick-sidebar-toggler">
                                    <a id="headerSettingButton" class="mdl-button mdl-js-button mdl-button--icon pull-right"
                                            data-upgraded=",MaterialButton">
                                            <i class="material-icons">settings</i>
                                    </a>
                            </li>-->
                        </ul>
                    </div>
                </div>
            </div>
            <!-- end header -->



