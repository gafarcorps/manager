<?php
// Generation du flux rss lié au site 
require_once("includes/initialize.php");
header( "Content-type: text/xml");
?>

<rss version="2.0">
    <channel>
        <?php 
        $site = "https://creativesweb.info/";
        $found_ads = Advertise :: find_all();
        //$id_site = 6; // id du site
        $today = $today = date("Y-m-d");

        foreach ($found_ads as $found_ad) {
            //recherche des id lié au site et en cours 
            $found_links = Linkads :: find_all_customer($found_ad->advertise_id, $_GET['sid']);
            //echo $today ." ".$found_ad->advertise_datestart." ".isset($found_links->linkads_id);
                if(isset($found_links->linkads_id) && $found_ad->advertise_dateend >= $today && 
                $found_ad->advertise_datestart <= $today && $found_ad->advertise_typebanner_id == $_GET['bid'] ){
                    
        ?>
        <item>            
            <image>
            <title></title>   
            <url><?php echo $site.'pub/'.$found_ad->image_path; ?></url>
            </image>
        </item>
        <?php } 
        }
        ?>       
        
    </channel>
</rss>