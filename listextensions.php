<div class="page-content-wrapper">
    <div class="page-content">
        <?php
//        if (strpos("app.php", $_SERVER['HTTP_REFERER']) !== false) {
//            $url = "app.php";
//        } elseif (strpos("appex.php", $_SERVER['HTTP_REFERER']) !== false) {
//            $url = "appex.php";
//        }
        if (isset($_GET['mid'])) {
            $mid = $_GET['mid'];
        } else {
            $mid = 47;
        }
        $found_menu = Menu::find_by_id($mid);
        ?>
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="<?php echo $app_or_appex ?>.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="<?php echo $app_or_appex ?>.php?item=listextensions">Applications</a>&nbsp;<i
                            class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Logithèque</li>
                </ol>
            </div>
        </div>



        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <?php if (isset($_GET['mid'])) { ?>
                            <header>Liste des extensions: Catégorie <?php echo $found_menu->menu_title; ?></header>
                        <?php } else { ?>
                            <header>Logithèque</header>
                        <?php } ?>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row p-b-20">
                            <div class="col-md-6 col-sm-6 col-6">
                                <!--                                <div class="btn-group">
                                                                    <a href="app.php?item=formmenu&mid=11&option=new" id="addRow" class="btn btn-info">
                                                                        Nouvelle extension <i class="fa fa-plus"></i>
                                                                    </a>
                                
                                                                </div>
                                                                <div class="btn-group">
                                                                    <a href="app.php?item=disabledextensions" id="addRow" class="btn btn-danger"><b>Corbeille
                                                                        </b></a>
                                                                </div>
                                                                <div class="btn-group">
                                                                    <a href="app.php?item=listextensions" id="addRow" class="btn btn-success"><b>Liste des extensions
                                                                        </b></a>
                                                                </div>-->
                            </div>

                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group pull-right">
                                    <?php
                                    if (isset($_GET['mid'])) {
                                        $mid = $_GET['mid'];
                                        echo '<a href="' . $app_or_appex . '.php?item=listextensions" class="btn deepPink-bgcolor">Retourner à liste précédente';
                                        echo '</a>';
                                    } // enf if
                                    ?>

                                </div>
                            </div>
                        </div>

                        <?php
                        if (isset($_GET['msg']) && $_GET['msg'] == 'success') {
                            echo '<span class="clsAvailable"> Enregistrement réussi. </span>';
                        }
                        ?>


                        <div class="row table btn-sweetalert" >

                            <?php
                            $found_extensions = Menu::list_extensions_active($mid);
                            foreach ($found_extensions as $found_extension) {
                                echo '<div class="col-lg-3 col-md-6 col-12 col-sm-6">';
                                echo '<div class="blogThumb">';
                                if (!empty($found_extension->menu_picture)) {
                                    echo '<div class="thumb-center">
                                            <img class="img-responsive" alt="user" src="media/files/' . $found_extension->menu_picture . '"/>
                                        </div>';
                                }
//                                if($found_typeuser->type_user_id == 1)
//                                echo '<div class="text-muted"><h3><a href="app.php?item=formmenu&mid=' . $found_extension->menu_id . '&option=edit">' . $found_extension->menu_title . '</a></h3></div>';
//                                else
                                echo '<div class="text-muted"><h3>' . $found_extension->menu_title . '</h3></div>';
                                echo '<div class="text-muted"><h4>' . $found_extension->menu_desc . '</h4>';
                                if (empty($_GET['mid'])) {
                                    if (Menu::count_extensions_children_active($found_extension->menu_id) > 0)
                                        echo '<a href="' . $app_or_appex . '.php?item=listextensions&mid=' . $found_extension->menu_id . '"><span class="badge bagde-info"> Extensions liées: ' . Menu::count_extensions_children_active($found_extension->menu_id) . ' </span></a></br></br>';
                                }
                                if ($found_extension->menu_extension_vedette == 1) {
                                    echo '<span class="badge bagde-info"> Vedette </span></br>';
                                }
                                if (!empty($_GET['mid']) && Menu::count_extensions_children_active($found_extension->menu_id) == 0) {
                                    $found_userinfos = User_info::find_user_infos($session->user_account_id);
                                    $found_app_request = App_request::find_by_menu_and_customer($found_extension->menu_id, $found_userinfos->user_info_id);

                                    if (strpos($_SERVER['HTTP_REFERER'], "app.php") !== false) {
                                        $url = "app.php";
                                    } elseif (strpos($_SERVER['HTTP_REFERER'], "appex.php") !== false) {
                                        $url = "appex.php";
                                    }
                                    
                                    if (strpos($found_extension->menu_link, "app.php") !== false && $found_typeuser->type_user_id > 2) {
                                        $menu_link = str_replace("app.php", "appex.php", $found_extension->menu_link);
                                    } 
                                    elseif (strpos($found_extension->menu_link, "appex.php") !== false && $found_typeuser->type_user_id < 3) {
                                        $menu_link = str_replace("appex.php", "app.php", $found_extension->menu_link);
                                    } else {
                                        $menu_link = $found_extension->menu_link;
                                    }
                                    if (App_request::count_by_menu_and_customer($found_extension->menu_id, $found_userinfos->user_info_id) > 0) {
                                        if ($found_app_request->app_request_active == 1) {
                                            echo '<h3><span class="label label-success">Extension ajoutée</span></h3>';
                                            echo '<a href="' . $menu_link . '" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary">Utiliser</a>';
                                        } else {
                                            echo '<a href="' . $menu_link . '" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary">Essayer</a>';
                                            echo '<h3><span class="label label-warning">Demande en cours<br/> traitement</span></h3>';
                                        }
                                    } else {
                                        echo '<a href="' . $menu_link . '" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary">Essayer</a>';
                                        echo '<a href="saveapp_request.php?mid=' . $found_extension->menu_id . '&new=ok&url=' . $url . '" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary">Ajouter l\'extension</a>';
                                    }
                                }
                                echo '</div>';
                                echo '</div>';
                                echo '</div>';
                            }
                            ?>

                            <?php echo '<br/><b> </b>'; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    </div>
</div>