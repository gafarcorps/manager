$(function () {
  $(".btn-sweetalert button").on("click", function () {
    var type = $(this).data("type");
    var id_value = $(this).data("id"); //$(this).data('id');
    if (type === "basic") {
      showBasicMessage();
    } else if (type === "with-title") {
      showWithTitleMessage();
    } else if (type === "success") {
      showSuccessMessage();
    } else if (type === "confirm") {
      showConfirmMessage();
    } else if (type === "cancel") {
      showCancelMessage();
    } else if (type === "with-custom-icon") {
      showWithCustomIconMessage();
    } else if (type === "html-message") {
      showHtmlMessage();
    } else if (type === "autoclose-timer") {
      showAutoCloseTimerMessage();
    } else if (type === "prompt") {
      showPromptMessage();
    } else if (type === "ajax-loader") {
      showAjaxLoaderMessageDelete(id_value);
    } else if (type === "ajax-loader-categorie-delete") {
      showAjaxLoaderCategorieDelete(id_value);
    } else if (type === "ajax-loader-categorie-restor") {
      showAjaxLoaderCategorieRestor(id_value);
    } else if (type === "ajax-loader-article-delete") {
      showAjaxLoaderArticleDelete(id_value);
    } else if (type === "ajax-loader-article-restor") {
      showAjaxLoaderArticleRestor(id_value);
    } else if (type === "ajax-loader-comment-delete") {
      showAjaxLoaderCommentDelete(id_value);
    } else if (type === "ajax-loader-delete") {
      showAjaxLoaderMessageDelete(id_value);
    }
  });
});

//These codes takes from http://t4t5.github.io/sweetalert/
function showBasicMessage() {
  swal("Good Morning!");
}

function showAjaxLoaderMessageDelete(id_value) {
  swal(
    {
      title: "Voulez-vous vraiment supprimer ?",
      text: "",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
    },
    function (isConfirm) {
      if (!isConfirm) return;
      $.ajax({
        url: "savead.php?id=" + id_value + "&option=delete",
        type: "GET",
        /*data: {
                id: 5 ,// get value of id
                option: 'delete'
            },*/
        dataType: "html",
        success: function () {
          swal("Fait!", "Supprimer avec succès!", "success");
          location.reload();
        },
        error: function (xhr, ajaxOptions, thrownError) {
          swal("Erreur de suppression!", "Veuillez réessayer", "erreur");
        },
      });
    }
  );
}

function showAjaxLoaderCategorieDelete(id_value) {
  swal(
    {
      title: "Voulez-vous vraiment supprimer ?",
      text: "",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
    },
    function (isConfirm) {
      if (!isConfirm) return;
      $.ajax({
        url: "savecategorie.php?cid=" + id_value + "&option=delete",
        type: "GET",
        /*data: {
                id: 5 ,// get value of id
                option: 'delete'
            },*/
        dataType: "html",
        success: function () {
          swal("Fait!", "Supprimer avec succès!", "success");
          location.reload();
        },
        error: function (xhr, ajaxOptions, thrownError) {
          swal("Erreur de suppression!", "Veuillez réessayer", "erreur");
        },
      });
    }
  );
}

function showAjaxLoaderCategorieRestor(id_value) {
  swal(
    {
      title: "Voulez-vous vraiment restaurer ?",
      text: "",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
    },
    function (isConfirm) {
      if (!isConfirm) return;
      $.ajax({
        url: "savecategorie.php?cid=" + id_value + "&option=recover",
        type: "GET",
        /*data: {
                id: 5 ,// get value of id
                option: 'delete'
            },*/
        dataType: "html",
        success: function () {
          swal("Fait!", "Supprimer avec succès!", "success");
          location.reload();
        },
        error: function (xhr, ajaxOptions, thrownError) {
          swal("Erreur de suppression!", "Veuillez réessayer", "erreur");
        },
      });
    }
  );
}

function showAjaxLoaderArticleDelete(id_value) {
  swal(
    {
      title: "Voulez-vous vraiment supprimer ?",
      text: "",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
    },
    function (isConfirm) {
      if (!isConfirm) return;
      $.ajax({
        url: "savetopic.php?pid=" + id_value + "&option=delete",
        type: "GET",
        /*data: {
                id: 5 ,// get value of id
                option: 'delete'
            },*/
        dataType: "html",
        success: function () {
          swal("Fait!", "Supprimer avec succès!", "success");
          location.reload();
        },
        error: function (xhr, ajaxOptions, thrownError) {
          swal("Erreur de suppression!", "Veuillez réessayer", "erreur");
        },
      });
    }
  );
}

function showAjaxLoaderArticleRestor(id_value) {
  swal(
    {
      title: "Voulez-vous vraiment restaurer ?",
      text: "",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
    },
    function (isConfirm) {
      if (!isConfirm) return;
      $.ajax({
        url: "savetopic.php?pid=" + id_value + "&option=recover",
        type: "GET",
        /*data: {
                id: 5 ,// get value of id
                option: 'delete'
            },*/
        dataType: "html",
        success: function () {
          swal("Fait!", "Supprimer avec succès!", "success");
          location.reload();
        },
        error: function (xhr, ajaxOptions, thrownError) {
          swal("Erreur de suppression!", "Veuillez réessayer", "erreur");
        },
      });
    }
  );
}

function showAjaxLoaderCommentDelete(id_value) {
  swal(
    {
      title: "Voulez-vous vraiment supprimer ?",
      text: "",
      type: "warning",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
    },
    function (isConfirm) {
      if (!isConfirm) return;
      $.ajax({
        url: "savetopic_comment.php?comment_id=" + id_value + "&option=delete",
        type: "GET",
        /*data: {
                id: 5 ,// get value of id
                option: 'delete'
            },*/
        dataType: "html",
        success: function () {
          swal("Fait!", "Supprimer avec succès!", "success");
          location.reload();
        },
        error: function (xhr, ajaxOptions, thrownError) {
          swal("Erreur de suppression!", "Veuillez réessayer", "erreur");
        },
      });
    }
  );
}

function showWithTitleMessage() {
  swal("Here's a message!", "How Are You?");
}

function showSuccessMessage() {
  swal("Good job!", "You clicked the button!", "success");
}

function showConfirmMessage() {
  swal(
    {
      title: "Are you sure?",
      text: "You will not be able to recover this imaginary file!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",
      closeOnConfirm: false,
    },
    function () {
      swal("Deleted!", "Your imaginary file has been deleted.", "success");
    }
  );
}

function showCancelMessage() {
  swal(
    {
      title: "Are you sure?",
      text: "You will not be able to recover this imaginary file!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#DD6B55",
      confirmButtonText: "Yes, delete it!",
      cancelButtonText: "No, cancel plx!",
      closeOnConfirm: false,
      closeOnCancel: false,
    },
    function (isConfirm) {
      if (isConfirm) {
        swal("Deleted!", "Your imaginary file has been deleted.", "success");
      } else {
        swal("Cancelled", "Your imaginary file is safe :)", "error");
      }
    }
  );
}

function showWithCustomIconMessage() {
  swal({
    title: "Sweet!",
    text: "Here's a custom image.",
    imageUrl: "../assets/sweet-alert/thumbs_up.png",
  });
}

function showHtmlMessage() {
  swal({
    title: "HTML <small>Title</small>!",
    text: 'A custom <span style="color: #CC0000">html<span> message.',
    html: true,
  });
}

function showAutoCloseTimerMessage() {
  swal({
    title: "Auto close alert!",
    text: "I will close in 2 seconds.",
    timer: 2000,
    showConfirmButton: false,
  });
}

function showPromptMessage() {
  swal(
    {
      title: "An input!",
      text: "Write something interesting:",
      type: "input",
      showCancelButton: true,
      closeOnConfirm: false,
      animation: "slide-from-top",
      inputPlaceholder: "Write something",
    },
    function (inputValue) {
      if (inputValue === false) return false;
      if (inputValue === "") {
        swal.showInputError("You need to write something!");
        return false;
      }
      swal("Nice!", "You wrote: " + inputValue, "success");
    }
  );
}

function showAjaxLoaderMessage() {
  swal(
    {
      title: "Ajax request example",
      text: "Submit to run ajax request",
      type: "info",
      showCancelButton: true,
      closeOnConfirm: false,
      showLoaderOnConfirm: true,
    },
    function () {
      setTimeout(function () {
        swal("Ajax request finished!");
      }, 2000);
    }
  );
}
