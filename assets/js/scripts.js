function reach_profile_tab(){
    if(location.href.split('/').pop() == 'app.php?item=home'){
        $('#profile').trigger('click')
    }
}

$('.checkboxes').click(function (){
    let check = false
    if($(this).prop('checked')){
        $('#delete-all').show()
    }else{
        $('.checkboxes').each(function(){
            if($(this).prop('checked'))
                check = true
        })
        if(!check)
            $('#delete-all').hide()
    }
})

$('.delete').click(function (){
    let href = $(this).attr('data-href')
    let valid = confirm('Etes vous sûr ?')

    if(valid)
        window.location.href = href
})

$('#delete-all').click(function (){
    let valid = confirm('Etes vous sûr ?')
    let target_page = $(this).attr('data-target')
    if(valid){
        let ids = '';
        $('.checkboxes').each(function(){
            if($(this).prop('checked')){
                ids += $(this).val()+';'
            }
        })
        window.location.href= target_page+'.php?ids='+ids+'&option=delete'
    }
})

// gallery toggle buttons
$('#form-bloc').on('click', '.btn-photo', function(){
    $(this).parents('.one-form').find('.photo-bloc').show()
    $(this).parents('.one-form').find('.video-bloc').hide()
})
$('#form-bloc').on('click', '.btn-video', function(){
    $(this).parents('.one-form').find('.photo-bloc').hide()
    $(this).parents('.one-form').find('.video-bloc').show()
})

