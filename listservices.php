<!-- start service content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="app.php?item=listservices">services</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Liste des services</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Liste des services</header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row p-b-20">
                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group">
                                    <a href="app.php?item=formservice&service_id=11&option=new" id="addRow" class="btn btn-info">
                                        Nouveau service <i class="fa fa-plus"></i>
                                    </a>

                                </div>
                                <div class="btn-group">
                                    <a href="app.php?item=disabledservices&list=disabled" id="addRow" class="btn btn-danger"><b>Corbeille </b></a> 
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group pull-right">
                                    <?php
                                    if (isset($_GET['service_id'])) {
                                        $service_id = $_GET['service_id'];
                                        echo '<a href="app.php?item=listservices" class="btn deepPink-bgcolor">Retourner à liste précédente';
                                        echo '</a>';
                                    } // enf if
                                    ?>

                                </div>
                            </div>
                        </div>

                        <?php
                        if (isset($_GET['msg']) && $_GET['msg'] == 'success') {
                            echo '<span class="clsAvailable"> Enregistrement réussi. </span>';
                        }
                        ?>

                        <div class="table-scrollable">
                            <table class="table table-hover table-checkable order-column full-width"
                                   id="example4">
                                <thead>
                                    <tr>  
                                        <!--<th class="center"></th>-->
                                        <th class="center">Titre </th>
                                        <th class="center"> Description </th>   
                                        <th class="center"></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    if (empty($_GET['list']))
                                        $found_services = Service :: find_all_not_deleted();
                                    else
                                        $found_services = Service :: find_all_deleted();
                                    foreach ($found_services as $found_service) {
                                        echo '<tr class="odd gradeX">';
//                                        echo '<td class="center"></td>';
                                        echo '<td class="center">' . $found_service->service_title . '</td>';
                                        echo '<td class="center">' . $found_service->service_desc . '</td>';
                                        echo '<td class="center">';
                                        if ($found_service->service_active == 1) {
                                            echo '<a href="app.php?item=formservice&service_id=' . $found_service->service_id . '&option=edit"><span class="label label-info">Modifier</span></a>';
                                        } else {
                                            echo '<a href="app.php?item=formservice&service_id=' . $found_service->service_id . '&option=edit"><span class="label label-danger">Non publiée</span></a>';
                                        }
                                        echo '</td>';

                                        echo '</tr>';
                                    } // end foreach
                                    ?>

                                </tbody>
                            </table>
                            <?php echo '<br/><b> </b>'; ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end service content -->
