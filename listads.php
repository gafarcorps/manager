<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                            href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="#">Catégories</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Liste des pubs en ligne</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Liste des pubs en ligne</header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row p-b-20">
                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group">
                                    <a href="app.php?item=formadvertisecustomer&option=new" id="addRow"
                                        class="btn btn-info">
                                        Nouvelle pub <i class="fa fa-plus"></i>
                                    </a>

                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group pull-right">
                                </div>
                            </div>
                        </div>

                        <?php
                        if(isset($_GET['msg']) && $_GET['msg']=='success') {
                            echo '<span class="clsAvailable"> Enregistrement réussi. </span>';
                        }
                        ?>
                        <script>

                        </script>


                        <div class="table-scrollable">
                            <table id="example1" class="display full-width btn-sweetalert">
                                <thead>
                                    <tr>
                                        <th>Image</th>
                                        <th>Titre </th>
                                        <th>Bannière </th>
                                        <th>Date début </th>
                                        <th>Date fin </th>
                                        <!--<th>Client </th>-->
                                        <!--<th>Payée</th>-->
                                        <th>Active</th>
                                        <!--<th>Facture</th>-->
                                        <th style="width:100px">Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php

                                                $found_ads = Advertise :: find_all();

                                                foreach ($found_ads as $found_ad) {

                                                    $found_typebanner = Type_banner :: find_by_id($found_ad->advertise_typebanner_id);

                                                    echo '<tr class="table-flag-blue">';

                                                    echo '<td><img src="./pub/'.$found_ad->image_path.'" alt="" height="30px"></td>';
                                                    echo '<td style="width:300px">'.($found_ad->advertise_title).'</td>';
                                                    echo '<td>'.($found_typebanner->type_banner_desc).'</td>';
                                                    echo '<td>' . $found_ad->advertise_datestart . '</td>';
                                                    echo '<td>' . $found_ad->advertise_dateend . '</td>';
                                                    //echo '<td>' . changedateusfr($found_ad->advertise_datestart) . '</td>';
                                                    //echo '<td>' . changedateusfr($found_ad->advertise_dateend) . '</td>';
                                                   // echo '<td>' . $found_ad->advertise_customer_id . '</td>';

                                                    /*if ($found_ad->advertise_payed) { // Show the statut of the page
                                                        echo '<td><span class="label label-success"><i class="fa fa-check"></i></span></td>';
                                                    }// end if
                                                    else {
                                                        echo '<td><span class="label label-warning"><i class="fa fa-times"></i></span></td>';
                                                    }// end else
                                                    */
                                                    if ($found_ad->advertise_active) { // Show the statut of the page
                                                        echo '<td><span class="label label-success"><i class="fa fa-check"></i></span></td>';
                                                    }// end if
                                                    else {
                                                        echo '<td><span class="label label-warning"><i class="fa fa-times"></i></span></td>';
                                                    }// en

                                                    //echo '<td><div class="btn-group"><a href="tcpdf/examples/pdf_ads_invoice.php?id=' . $found_ad->advertise_id . '" class="btn btn-small show-tooltip"><i class="fa fa-file-pdf-o"></i></a></div></td>';


                                                    echo '<td>';
                                                    echo '<div class="btn-group">'; // Update and delete buttons
                                                    echo '<a class="btn btn-small show-tooltip" title="Edit" href="app.php?item=formadvertisecustomer&id=' . $found_ad->advertise_id . '&option=edit"><i class="fa fa-edit"></i></a>';
                                                    // echo '<a class="btn btn-small btn-danger show-tooltip" title="Delete" href="savead.php?aid=' . $found_ad->advertise_id . '&option=delete"><i class="icon-trash"></i></a>';
                                                     echo '<button class="btn btn-small btn-danger show-tooltip btn-sweetalert" type="button" data-type="ajax-loader" data-id="'.$found_ad->advertise_id.'"><i class="icon-trash"></i></button>';
                              
                                                    echo '</div>';
                                                    echo '</td>';
                                                    echo '</tr>';
                                                }// end foreach
                                                ?>
                                </tbody>
                            </table>
                        </div>


                    </div>
                </div>
            </div>
            <!-- end page content -->