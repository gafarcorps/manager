<?php 
	
    require_once("includes/initialize.php");
    $abonnement = new Abonnement();	
	if(isset($_GET['aid'])){
        $aid = $_GET['aid'];
    }
    $found_ab = $abonnement->find_by_id($aid);
    if(!empty($found_ab)){
        $abonnement->abonne_id = $found_ab->abonne_id;
        $abonnement->abonne_reader_id = $found_ab->abonne_reader_id;
        $abonnement->abonne_type_abonnement_id = $found_ab->abonne_type_abonnement_id;
        $abonnement->abonne_code_ref = $found_ab->abonne_code_ref;
        $abonnement->abonne_date_paie = $found_ab->abonne_date_paie;
        $abonnement->abonne_date_start = $found_ab->abonne_date_start;
        $abonnement->abonne_date_end = $found_ab->abonne_date_end;
        $abonnement->abonne_date_creation = $found_ab->abonne_date_creation;
        $abonnement->abonne_active = 1;
        $abonnement->save();
        redirect_to("app.php?item=listdemandeabonnement&msg=success");

    }

?>
 