<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Welcome to your website's manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="dashboard.php">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="app.php?item=listpages">Pages</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Edit page</li>
                </ol>
            </div>
        </div>

        <form action="savepage.php" class="form-horizontal" method="POST" enctype="multipart/form-data">

            <?php
            if (isset($_GET['pid'])) {
                $pid = $_GET['pid'];
            }


            if (isset($_GET['option']) && $_GET['option'] == 'new') {

                echo '<div class="row">';
                echo '<div class="col-md-12">';
                //<!-- BEGIN PROFILE SIDEBAR -->
                echo '<div class="profile-sidebar">';
                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Attributes </div>';
                echo '<div class="profile-usertitle-job"> of page </div>';
                echo '</div>';

                echo '<div id="displayed" class="card-body no-padding height-9">';
                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b>Etat </b>';
                echo '<div class="profile-desc-item pull-right">';
                echo '<i class="fa fa-eye"></i> published';
                echo '</div>';
                echo '</li>';

                echo '</ul><br/>';

                echo '</div>';

                echo '</div>';

                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Image de couverture </div>';
                echo '</div>';

                echo '<div id="displayed" class="card-body no-padding height-9">';
                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<div class="profile-desc-item pull-left">';
                echo '<input type="file" name="file_upload">';
                echo '</div>';
                echo '</li>';

                echo '</ul><br/>';
                echo '</div>';
                echo '</div>';
                echo '<div class="profile-usertitle">';
                echo '</div>';
                echo '<div class="card">';
                echo '<div class="card-body no-padding height-9">';
                echo '<div class="row">';
                echo '</div>';

                echo '<ul class="list-group list-group-unbordered">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Parent </div>';
                echo '<div class="form-group">';
                echo '<select name="parent_id" id="parent" class="form-control" >';
                echo '<option>Choisissez une page</option>';
                $found_pages = Page::find_all();
                foreach ($found_pages as $found_page) {
                    $found_children_menus = Page::find_page_children($found_page->page_id);
                    if (!empty($found_children_menus)) {
                        echo '<optgroup label="' . $found_page->page_title . '">';

                        foreach ($found_children_menus as $found_children_menu) {
                            if (!empty($found_children_menu)) {
                                echo '<option value="' . $found_children_menu->page_parent_id . '">' . $found_children_menu->page_title . '</option>';
                            } // end if
                        } // end foreach
                        echo '</optgroup>';
                    } // end if found_children_menus
                    echo '<option value="' . $found_page->page_id . '">' . $found_page->page_title . '</option>';
                }
                echo '</select>';
                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Menu alignment</div>';
                echo '</div>';
                echo '<li class="list-group-item">';
                //echo '<b>Section de page</b> <a class="pull-right"></a>';
                echo ' <select id="multiple" name="section_id[]" class="form-control select2-multiple" multiple>';
                $found_sections = Section::find_elements_alignment();
                foreach ($found_sections as $found_section) {
                    echo '<option value="' . $found_section->section_id . '">' . $found_section->section_desc . '</option>';
                }
                echo '</select>';
                echo '</li>';
                echo '</div>';

                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Order</div>';
                echo '</div>';
                echo '<div class="card-body no-padding height-9">';

                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                //	echo '<b>Position</b> <a class="pull-right"></a>';
                echo '<select name="position" class="form-control">';
                echo '<option value="1">position 1</option>';
                echo '<option value="2">position 2</option>';
                echo '<option value="3">position 3</option>';
                echo '<option value="4">position 4</option>';
                echo '<option value="5">position 5</option>';
                echo '<option value="6">position 6</option>';
                echo '<option value="7">position 7</option>';
                echo '<option value="8">position 8</option>';
                echo '<option value="9">position 9</option>';
                echo '<option value="10">position 10</option>';
                echo '</select>';
                echo '</li>';

                echo '</ul>';
                echo '</div>';
                echo '</div>';

                //<!-- END SIDEBAR USER TITLE -->
                echo '<br/>';
                echo '<div class="control-group">';
                echo '<label class="control-label" for="active"> Publish ?</label>';
                echo '<div class="controls">';
                echo '<div class="form-group">';
                echo '<div class="checkbox checkbox-icon-black">';
                echo '<input id="rememberChk1" name="active" type="checkbox" checked="checked">';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                echo '<label class="control-label" for="active"> Accueil ?</label>';
                echo '<div class="controls">';
                echo '<div class="form-group">';
                echo '<div class="checkbox checkbox-icon-black">';
                echo '<input id="rememberChk1" name="home" type="checkbox">';
                echo '</div>';
                echo '</div>';
                echo '</div>';
                

                //<!-- SIDEBAR BUTTONS -->
                echo '</div>';
                echo '</div>';

                //<!-- END BEGIN PROFILE SIDEBAR -->
                //<!-- BEGIN PROFILE CONTENT -->
                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';

                echo '<h4> Créer une nouvelle page </h4>';

                echo '</div>';
                echo '</div>';
                echo '<div class="white-box">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Title:</label>';
                echo '<input name="title" type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Description:</label>';
                echo '<input name="desc1" type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Content:</label>';
                echo '<textarea name="desc" id="description" class="span12" rows="8"></textarea>';
                echo '</div>';

                // Add By Boris
                echo '<div class="form-group">';
                echo '<label for="subject" class="">Link:</label>';
                echo '<input name="link" id="title"  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Description : &nbsp;</label>';
                echo '<input name="only_desc" id="title" type="checkbox" tabindex="1" id="subject">';
                echo '</div>';
                //
                echo '</div>';
                echo '</div>';

                echo '</div>';
                echo '<br>';
                echo '&nbsp;&nbsp;';

                if (isset($_GET['option']) && $_GET['option'] == 'new') {
                    echo '<input type="submit" name="new" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="Save">';
                } else {
                    echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="Save">';
                }

                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->
                echo '</div>';
            } //end if
            ?>


            <?php
            if (isset($_GET['option']) && $_GET['option'] == 'edit' && isset($_GET['pid'])) {

                $pid = $_GET['pid'];
                echo '<input type="hidden" name="pid" value="' . $pid . '" />';
                $found_page = Page::find_by_id($pid);

                echo '<div class="row">';
                echo '<div class="col-md-12">';
                //<!-- BEGIN PROFILE SIDEBAR -->
                echo '<div class="profile-sidebar">';
                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Attributes</div>';
                echo '<div class="profile-usertitle-job"> of page ' . $found_page->page_title . ' </div>';
                echo '</div>';
                echo '<div class="card-body no-padding height-9">';

                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b>Etat </b>';
                echo '<div class="profile-desc-item pull-right">';
                if ($found_page->page_active == 1) {
                    echo '<i class="fa fa-eye"></i> published';
                } else {
                    echo '<span class="label label-danger"><i class="fa fa-eye-slash"></i> not published</span>';
                }

                echo '</div>';
                echo '</li>';

                echo '<li class="list-group-item">';
                echo '<b>published on </b>';
                echo '<div class="profile-desc-item pull-right">';
                echo '<i class="fa fa-clock-o"> </i> ' . $found_page->page_date . ' ';
                echo '</div>';
                echo '</li>';

                echo '<li class="list-group-item">';
                if ($found_page->page_active == 0) {

                    echo '<b>Move to trash </b>';
                    echo '<div class="profile-desc-item pull-right"> <a href="savepage.php?pid=' . $pid . '&option=delete"><i class="fa fa-trash-o "></i></a> </div>';
                } else {
                    echo '<b>Restore</b>';
                    echo '<div class="profile-desc-item pull-right"> <a href="savepage.php?pid=' . $pid . '&option=recover"><i class="fa  fa-recycle "></i></a> </div>';
                }

                echo '</li>';
                echo '<br/><b> <a href="app.php?item=disabledpages"><b>Corbeille </b></a></b>';

                echo '</ul>';

                echo '</div>';
                echo '</div>';

                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Cover image </div>';
                echo '</div>';

                echo '<div id="displayed" class="card-body no-padding height-9">';
                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<div class="profile-desc-item pull-left">';
                echo '<input type="file" name="file_upload">';
                echo '</div>';
                echo '</li>';

                echo '</ul><br/>';
                echo '</div>';
                echo '</div>';

                echo '<div class="card card-topline-aqua">';
                echo '<div class="card-body no-padding height-9">';
                echo '<div class="row">';
                echo '<div class="profile-userpic">';
                echo '</div>';
                echo '</div>';

                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b>Parent </b> <a class="pull-right"></a>';
                echo '<div class="form-group">';
                echo '<select name="parent_id" id="parent" class="form-control" >';
                echo '<option value=0></option>';
                $found_parent = Page::find_the_parent($found_page->page_parent_id);
                echo '<option value="' . $found_parent->page_id . '" selected>' . $found_parent->page_title . '</option>';
                $found_children_menus = Page::find_page_children($found_page->page_id);
                if (!empty($found_children_menus)) {
                    echo '<optgroup label="' . $found_parent->page_title . '">';

                    foreach ($found_children_menus as $found_children_menu) {
                        if (!empty($found_children_menu)) {
                            echo '<option value="' . $found_children_menu->page_parent_id . '">' . $found_children_menu->page_title . '</option>';
                        } // end if	   
                    } // end foreach
                    echo '</optgroup>';
                } // end if found_children_menus


                $found_pages = Page::list_others($pid);
                foreach ($found_pages as $found_page) {
                    $found_children_menus = Page::find_page_children($found_page->page_id);
                    if (!empty($found_children_menus)) {
                        echo '<optgroup label="' . $found_page->page_title . '">';

                        foreach ($found_children_menus as $found_children_menu) {
                            if (!empty($found_children_menu)) {
                                echo '<option value="' . $found_children_menu->page_parent_id . '">' . $found_children_menu->page_title . '</option>';
                            } // end if	   
                        } // end foreach
                        echo '</optgroup>';
                    } // end if found_children_menus

                    echo '<option value="' . $found_page->page_id . '">' . $found_page->page_title . '</option>';
                }

                echo '</select>';

                echo '</div>';
                echo '</li>';

                //echo'<div id="children">';
                //echo '</div>';


                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Menu alignment</div>';
                echo '</div>';
                echo '<li class="list-group-item">';
                //echo '<b>Section de page</b> <a class="pull-right"></a>';
                $found_page_sections = Page_section::find_by_page($pid);
                if (!empty($found_page_sections)) {
                    foreach ($found_page_sections as $found_page_section) {
                        if (!empty($found_page_section)) {
                            $found_section = Section::find_by_id($found_page_section->page_section_sid);
                            echo '<input id="checkboxbg4" type="checkbox" checked="checked"> ' . $found_section->section_desc . ' <br/>';
                        }
                    }
                } //end if

                echo ' <select id="multiple" name="section_id[]" class="form-control select2-multiple" multiple>';
                $found_sections = Section::find_elements_alignment();
                foreach ($found_sections as $found_section) {
                    echo '<option value="' . $found_section->section_id . '">' . $found_section->section_desc . '</option>';
                }

                echo '</select>';
                echo '</li>';
                echo '</div>';

                echo '<li class="list-group-item">';
                echo '<b>Order</b> <a class="pull-right"></a>';
                echo '<select name="position" class="form-control">';
                $found_page = $page->find_by_id($pid);
                echo '<option value="' . $found_page->page_position . '">Position ' . $found_page->page_position . '</option>';
                $found_items = Filter::list_others($found_page->page_position);
                foreach ($found_items as $found_item) {
                    echo '<option value="' . $found_item->filter_id . '">Position ' . $found_item->filter_desc . '</option>';
                }

                echo '</select>';
                echo '</li>';

                echo '</ul>';

                echo '<div class="controls">';
                echo '<div class="form-group">';
                echo '<div class="checkbox checkbox-icon-black">';
                $found_page = $page->find_by_id($pid);
                if ($found_page->page_active == 1) {
                    echo '<input id="rememberChk1" name="active" type="checkbox" checked="checked"> published';
                } else {
                    echo '<input id="rememberChk1" name="active" type="checkbox"> not published';
                }

                echo '</div>';

                echo '<div class="checkbox checkbox-icon-black">';
                $found_page = $page->find_by_id($pid);
                if ($found_page->page_home == 1) {
                    echo '<input id="rememberChk1" name="home" type="checkbox" checked="checked"> Home';
                } else {
                    echo '<input id="rememberChk1" name="home" type="checkbox"> not home';
                }

                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END SIDEBAR BUTTONS -->
                echo '</div>';
                echo '</div>';

                echo '</div>';

                //<!-- END BEGIN PROFILE SIDEBAR -->
                //<!-- BEGIN PROFILE CONTENT -->

                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';
                $found_page = $page->find_by_id($pid);
                echo '<h4> Edit page : ' . $found_page->page_title . ' </h4>';

                if (isset($_GET['r']) && $_GET['r'] == 0) {
                    echo '<span class="clsNotAvailable"> Moved to trash. </span>';
                } else if (isset($_GET['r']) && $_GET['r'] == 1) {
                    echo '<span class="clsAvailable"> Restored. </span>';
                }



                echo '<div class="btn-group">';
                echo '<a href="app.php?item=formpage&pid=' . $pid . '&option=new" id="addRow" class="btn btn-info">Nouveau page <i class="fa fa-plus"></i>';
                echo '</a>';
                echo '</div>';

                echo '</div>';
                echo '</div>';

                echo '<div class="white-box">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Title:</label>';
                echo '<input name="title" id="title" value="' . $found_page->page_title . '"  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Description:</label>';
                echo '<input name="desc1" id="title" value="' . $found_page->page_desc . '"  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Content:</label>';
                echo '<textarea name="desc" id="description" class="span12" rows="8">' . $found_page->page_desc . '</textarea>';

                echo '</div>';

                // Add By Boris
                echo '<div class="form-group">';
                echo '<label for="subject" class="">Link:</label>';
                echo '<input name="link" id="title" value="' . $found_page->page_link . '"  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Description : &nbsp;</label>';
                echo ($found_page->page_only_desc) ?
                        '<input name="only_desc" id="title" type="checkbox" tabindex="1" id="subject" checked>' :
                        '<input name="only_desc" id="title" type="checkbox" tabindex="1" id="subject">';
                echo '</div>';
                //


                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '<br>';
                echo '&nbsp;&nbsp;';
                echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="Save">';

                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->

                echo '</div>';
            } // end if
            ?>

        </form>


        <!-- end page container -->