<?php
$customer_id = (!empty($_GET['customer_id'])) ? $_GET['customer_id'] : null;
if (!empty($customer_id))
    $found_customer = User_info::find_by_id($customer_id);

$type = (empty($_GET['type'])) ? 1 : $_GET['type'];
?>
<!-- start contrat content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <?php if (!empty($customer_id)) { ?>
                    <li><a class="parent-item" href="app.php?item=listcustomers">Clients</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <?php } ?>
                    <li><a class="parent-item" href="#">Demande d'application</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Liste des demandes d'application</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Liste des demandes d'application </header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row p-b-20">
                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group">
                                    <?Php if(!empty($_GET['type']) && $_GET['type'] == 2) { ?>
                                    <a href="app.php?item=formapp_request&option=new" id="addRow" class="btn btn-info">
                                        Nouvelle demande <i class="fa fa-plus"></i>
                                    </a>
                                    <?php } ?>
                                </div>
                                <div class="btn-group">
                                    <a href="app.php?item=disabledapps_requests&list=disabled" id="addRow" class="btn btn-danger"><i class="fa fa-trash"></i> <b>Corbeille </b></a> 
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group pull-right">
                                    <?php
                                    if (isset($_GET['app_request_id'])) {
                                        $app_request_id = $_GET['app_request_id'];
                                        echo '<a href="app.php?item=listapps_requests" class="btn deepPink-bgcolor">Retourner à liste précédente';
                                        echo '</a>';
                                    } // enf if
                                    ?>

                                </div>
                            </div>
                        </div>

                        <?php
                        if (isset($_GET['msg']) && $_GET['msg'] == 'success') {
                            echo '<span class="clsAvailable"> Enregistrement réussi. </span>';
                        }
                        ?>

                        <div class="table-scrollable">
                            <table class="table table-hover table-checkable order-column full-width"
                                   id="example4">
                                <thead>
                                    <tr>  
                                        <!--<th class="center"></th>-->

                                        <th class="center">Client </th>
                                        <th class="center">Menu </th>
                                        <th class="center"> Date de la demande </th>   
                                        <th class="center"> Statut de la demande </th>   
                                        <th class="center"></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $found_apps_requests = App_request :: find_all_not_deleted_by_type($type);

                                    foreach ($found_apps_requests as $found_app_request) {
//                                        echo 'jer';
                                        echo '<tr class="odd gradeX">';
//                                        echo '<td class="center"></td>';
                                        $found_customer = User_info::find_by_id($found_app_request->app_request_customer_id);
                                        echo '<td class="center"><span class="label label-info">' . $found_customer->user_info_firstname . '</span></td>';
                                        $found_menu = Menu::find_by_id($found_app_request->app_request_menu_id);
                                        echo '<td class="center">' . $found_menu->menu_title . '</td>';
                                        echo '<td class="center">' . (($found_app_request->app_request_date != "0000-00-00") ? $found_app_request->app_request_date : "Inconnue") . '</td>';
                                        echo '<td class="center">';
                                        echo '<span class="label label-' . (($found_app_request->app_request_active == 1) ? "success" : "warning") . '">' . (($found_app_request->app_request_active == 0) ? "Non traitée" : "Traitée") . '</span>';
//                                        echo '<a target="_blank" href="saveapp_request.php?customer_id=' . $found_customer->user_info_id . '&app_request_id=' . $found_app_request->app_request_id . '&option=break"><span class="btn btn-danger"><i class="fa fa-trash"></i> Rompre</span></a>';
                                        ?>


                                        <?php
                                        echo '</td>';
                                        echo '<td class="center">';
                                        if ($type == 1) {
                                            $url = !empty($found_customer->user_info_customer_url) ? $found_customer->user_info_customer_url : "http://creativesweb.info/rt";
//                                        echo '<a href="'.$found_customer->user_info_customer_url.'/manager/connexionLogin_root.php?menu_title=' . $found_menu->menu_title. '&menu_link=' . explode("item=", $found_menu->menu_link)[1] . '"><span class="btn btn-info"><i class="fa fa-pencil"></i> Traiter</span></a>';
                                            echo '<a href="saveapp_request.php?app_request_id=' . $found_app_request->app_request_id . '&option=customer_manager_connexion&url=' . $url . '&menu_title=' . $found_menu->menu_title . '&menu_link=' . explode("item=", $found_menu->menu_link)[1] . '"><span class="btn btn-info"><i class="fa fa-pencil"></i> Traiter</span></a>';
                                        } else {
                                            $url = !empty($found_customer->user_info_customer_url) ? $found_customer->user_info_customer_url : "http://creativesweb.info/rt";
//                                        echo '<a href="'.$found_customer->user_info_customer_url.'/manager/connexionLogin_root.php?menu_title=' . $found_menu->menu_title. '&menu_link=' . explode("item=", $found_menu->menu_link)[1] . '"><span class="btn btn-info"><i class="fa fa-pencil"></i> Traiter</span></a>';
                                            echo '<a href="app.php?item=formapp_request&app_request_id=' . $found_app_request->app_request_id . '&option=edit"><span class="btn btn-info"><i class="fa fa-pencil"></i> Modifier</span></a>';
                                            echo '<a href="saveapp_request.php?app_request_id=' . $found_app_request->app_request_id . '&option=active"><span class="btn btn-inverse"><i class="fa fa-ok"></i> Traiter</span></a>';
                                        }
                                        echo '</td>';

                                        echo '</tr>';
                                    } // end foreach
                                    ?>

                                </tbody>
                            </table>
                            <?php echo '<br/><b> </b>'; ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end contrat content -->
