<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Liste des utilisateurs</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="index.html">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="#"></a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active"> Mes utilisateurs</li>
                </ol>
            </div>
        </div>
        <!--        <ul class="nav nav-pills nav-pills-rose">
                    <li class="nav-item tab-all"><a class="nav-link active show" href="#tab1" data-toggle="tab">Vue Liste</a></li>
                    <li class="nav-item tab-all"><a class="nav-link" href="#tab2" data-toggle="tab">Vue Grille</a>
                    </li>
                </ul>-->
        <div class="tab-content tab-space">
            <div class="tab-pane active show" id="tab1">
                <div class="row">
                    <div class="col-md-12">

                        <div class="card-box">

                            <div class="card-head">
                                <div class="btn-group">
                                    <button class="btn btn-success  btn-outline dropdown-toggle"
                                            data-toggle="dropdown">Paramétrage avancé
                                        <i class="fa fa-angle-down"></i>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="<?php echo $app_or_appex ?>.php?item=listusers_db&extension_id=<?php echo $_GET['extension_id'] ?>" id="addRow">
                                                Mes utilisateurs <i class="fa fa-cogs"></i>
                                            </a>

                                        </li>
                                        <li>
                                            <a href="<?php echo $app_or_appex ?>.php?item=listdb_privileges&extension_id=<?php echo $_GET['extension_id'] ?>" id="addRow">
                                                Privilèges <i class="fa fa-cogs"></i>
                                            </a>

                                        </li>
                                        <li>
                                            <a href="<?php echo $app_or_appex ?>.php?item=listform_builders_db&list=root" id="addRow">
                                                Paramétrages <i class="fa fa-cogs"></i>
                                            </a>

                                        </li>
                                        <li>
                                            <a href="<?php echo $app_or_appex ?>.php?item=listform_builders_db" id="addRow">
                                                Bases de données <i class="fa fa-files-o"></i>
                                            </a>

                                        </li>
                                        <?php if (!empty($_GET['list'])) { ?>

                                            <li>
                                                <a href="<?php echo $app_or_appex ?>.php?item=disabledform_builders_db&customer_id=<?php echo $found_customer->user_info_id; ?>" id="addRow"><b> Corbeille </b><i class="fa fa-trash"></i></a> 

                                            </li>

                                        <?php } ?>
                                    </ul>
                                </div>
                                <button id="panel-button"
                                        class="mdl-button mdl-js-button mdl-button--icon pull-right"
                                        data-upgraded=",MaterialButton">
                                    <i class="material-icons"></i>
                                </button>
                                <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                                    data-mdl-for="panel-button">
                                    <li class="mdl-menu__item"><i
                                            class="material-icons"></i>Action</li>
                                    <li class="mdl-menu__item"><i class="material-icons"></i>
                                        action</li>
                                    <li class="mdl-menu__item"><i
                                            class="material-icons"></i></li>
                                </ul>
                            </div>

                            <?php
                            if (isset($_GET['msg']) && $_GET['msg'] == 'success') {
                                echo '<span class="clsAvailable"> Enregistrement réussi. </span>';
                            }
                            ?>
                            <div class="card-body ">

                                <div class="table-scrollable">
                                    <table class="table table-hover table-checkable order-column full-width"
                                           id="example4">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th class="center"> Email </th>
                                                <th class="center"> Type d'utilisateur </th>
                                                <th class="center"> Date de création</th>
                                                <th class="center"> Action </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
//                                            $user_account = new User_account();
//                                            $found_user_accounts = $user_account->find_by_typeuser_and_extension(3, $session->extension_id);
//                                            foreach ($found_user_accounts as $found_user_account) {
                                            $user_info = new User_info();
                                            $user_account = new User_account();
                                            $found_users = $user_info->find_all_active();
                                            foreach ($found_users as $found_user) {
                                                $found_user_account = $user_account->find_by_id($found_user->user_info_user_account_id);
                                                if ($found_user_account->user_account_typeuser_id == 3 || $found_user_account->user_account_typeuser_id == 5) { //Guest root ou Guest Guest
                                                    if (($found_user_account->user_account_typeuser_id == 5 && $found_user_account->user_account_extension == $session->extension_id) || ($found_user_account->user_account_typeuser_id == 3 && !empty(App_request::find_by_menu_and_customer($session->extension_id, $found_user->user_info_id)))) {
                                                        $found_typeuser = Typeuser::find_by_id($found_user_account->user_account_typeuser_id);
                                                        ?>
                                                        <tr class="odd gradeX">
                                                            <td></td>

                                                            <td class="center"><?php echo $found_user_account->user_account_email; ?> </a></td>
                                                            <td class="center"><strong><?php echo $found_typeuser->type_user_desc; ?></strong></td>
                                                            <td class="center"><?php echo $found_user_account->user_account_date_last_connexion; ?></td>
                                                            <td class="center">
                                                                <a href="appex.php?item=formusers_db&option=edit&uid=<?php echo $found_user->user_info_id; ?>"
                                                                   class="btn btn-tbl-edit btn-xs">
                                                                    <i class="fa fa-pencil"></i>
                                                                </a>
                                                                <!--<a class="btn btn-tbl-delete btn-xs">
                                                                        <i class="fa fa-trash-o "></i>
                                                                </a> -->
                                                            </td>
                                                        </tr>

                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>