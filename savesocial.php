<?php
require_once("includes/initialize.php");

if(isset($_POST['submit']))   		{

    if(isset($_POST['social_id']))		{ $social_id  	    =	$_POST['social_id'];	    	}
    if(isset($_POST['name']))		{ $name 		= 	$_POST['name'];			}
    if(isset($_POST['icon']))		{ $icon 		= 	$_POST['icon'];   	        }
    if(isset($_POST['link']))		{ $link 		= 	$_POST['link'];   	        }
    if(isset($_POST['active']))		{ $active		= 	($_POST['active']) ? true : false;			}
    if(isset($_POST['position']))	{$position		    =	$_POST['position']; 	}



    if (empty($_POST['social_id'])){ // NEW ENTRY


        $social->social_name 		= 	$name;
        $social->social_icon		=	$icon;
        $social->social_link		=	$link;
        $social->social_position  	    = 	$position;
        $social->social_active 		= 	$active;

        $social->save();
    } // END IF NEW ENTRY

    else{ // UPDATE ENTRY
        $found_social = Social::find_by_id($social_id);
        $social->social_id = $found_social->social_id;
        $social->social_name 		= 	isset($name) ? $name : $found_social->social_name;
        $social->social_icon 		= 	isset($icon) ? $icon : $found_social->social_icon;
        $social->social_link 		= 	isset($link) ? $link : $found_social->social_link;
        $social->social_position 		= 	$position;
        $social->social_active 		= 	$active;

        $social->save();

    }  // END IF UPDATE
    redirect_to('app.php?item=listsocials&msg=success');
} // END IF SUBMIT

if ($_GET['option']=='delete'){  // DELETE
    $id  = $_GET['id'];
    $social = Social::find_by_id($id);
    $del  = $social->delete();
    redirect_to('app.php?item=listsocials&msg=del');
}  // END IF DELETE


?>
