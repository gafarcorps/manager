<!DOCTYPE html>
<html>
    <link rel="stylesheet" href="assets/css/pages/login.css">

    <head>
        <meta charset="utf-8" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta name="description" content="Manager 3.0" />
        <meta name="author" content="CreativesWeb" />
        <title>Manager | Login</title>
        <!-- icons -->
        <link href="assets/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="assets/plugins/iconic/css/material-design-iconic-font.min.css">
        <!-- bootstrap -->
        <link href="assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <!-- style -->
        <link rel="stylesheet" href="assets/css/pages/extra_pages.css">
        <!-- favicon -->
        <link rel="shortcut icon" href="assets/img/favicon.ico" />
    </head>
    <?php
    // Afficher l'image de fond
//    $url = "https://creativesweb.info/rt/manager/rss_pub.php?sid=27&bid=2"; //2 pour les bannière 300x250 | 6 pour bannière 900 / 301
//
//    $feeds = simplexml_load_file($url);
//    $image_path = "";
//    foreach ($feeds->channel->item as $item) {
//        $image_path = $item->image->url;
//    }
    ?>
    <!--<body style="background: url('assets/img/build.jpg') right center; background-size: cover">-->
    <body style="background: url('<?php echo $image_path; ?>') right center; background-size: cover">
        <!--<body style="background: url('assets/img/aneho.jpg') right center; background-size: cover">-->
        <div class="login-box">
            <div class="user-box">
                <i class="fas fa-user-tie"></i>
            </div>
            <h2 style="text-align: center">MANAGER</h2> <br/>

            <?php
            if (!empty($_GET['msg']) && $_GET['msg'] == 'good') {
                echo '<h4 style="color: green">Votre mot de passe a été réinitialisé avec succ&egrave;. Vous pouvez vous connecter &agrave; pr&eacute;sent!</h4>';
            }
            ?>

            <form action="connexionLogin.php" method="post" class="login100-form validate-form">
                <div class="wrap-input100 validate-input" data-validate="Entrer votre Identifiant">
                    <input class="input100" type="text" name="username" placeholder="Votre Identifiant" value="<?php echo (empty($_GET['email'])) ? "" : $_GET['email'] ?>">
                    <span class="focus-input100" data-placeholder="&#xf207;"></span>
                </div>
                <div class="wrap-input100 validate-input" data-validate="Entrer votre mot de passe">
                    <input class="input100" type="password" name="password" placeholder="Entrer votre mot de passe">
                    <span class="focus-input100" data-placeholder="&#xf191;"></span>
                </div>

                <div class="container-login100-form-btn">
                    <input type="submit" name="submit" class="login100-form-btn" value="Valider" />
                </div>
                <div class="text-center p-t-30">
                    <a class="txt1" href="forgot_password.php">
                        Mot de passe oublié?
                    </a>
                </div>
            </form>


        </div>





        <div class="text-box">
            <h4> Manager <br/> </h4> <div align="justify">est une plateforme de gestion de contenu vous offrant la liberté de gérer avec grande facilité votre site. Ce logiciel écrit en PHP repose sur une base de données MySQL et est distribué par l'entreprise togolaise CreativesWeb. </div><br />  <br/>
        </div>




        <!-- start js include path -->
        <script src="assets/plugins/jquery/jquery.min.js"></script>
        <!-- bootstrap -->
        <script src="assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="assets/js/pages/extra_pages/login.js"></script>
        <!-- end js include path -->
    </body>

</html>
