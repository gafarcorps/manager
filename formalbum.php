<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="app.php?item=listcategories">Gestionnaire des catégories</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Editer un album</li>
                </ol>
            </div>
        </div>


        <form action="savegallery.php" class="form-horizontal" method="POST" enctype="multipart/form-data">

            <?php
            if(isset($_GET['option']) && $_GET['option'] == 'new'){
                echo '<div class="row">';
                echo '<div class="col-md-12">';

                //<!-- BEGIN PROFILE CONTENT -->
                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';
                echo '<h4> Créer un album </h4>';
                echo '</div>';
                echo '</div>';
                echo '<div class="white-box col-lg-12" style="padding: 50px">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Titre de l\'album</label>';
                echo '<input name="album_name" type="text" tabindex="1" id="subject" class="form-control" required>';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Album activé</label>';
                echo '&nbsp; <input name="album_active" type="checkbox" tabindex="1" id="subject">';
                echo '</div>';

                echo '</div>';
                echo '</div>';
                ?>


                <div id="form-bloc">
                    <!-- First Form -->
                    <hr>
                    <div class="form-group">
                        <label for="">Uploader la photo *</label>
                        <input type="file" class="form-control" name="image[]" required>
                    </div>
                    <div class="form-group">
                        <label for="">Titre de la photo</label>
                        <input type="text" class="form-control" name="name[]">
                    </div>
                    <div class="form-group">
                        <label for="">Description de la photo</label>
                        <textarea class="form-control" name="desc[]"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Position de la photo</label>
                        <input type="number" class="form-control" value="1" name="position[]">
                    </div>
                    <div class="form-group">
                        <label for="">Activé</label>&nbsp;
                        <input type="checkbox" name="active[]" checked>
                    </div>
                </div>
                <button type="button" id="add" class="btn btn-outline-primary">+ Ajouter un formulaire</button>


                <template id="another-form">
                    <!-- Another Form -->
                    <hr>
                    <div class="form-group">
                        <label for="">Uploader la photo *</label>
                        <input type="file" class="form-control" name="image[]" required>
                    </div>
                    <div class="form-group">
                        <label for="">Titre de la photo</label>
                        <input type="text" class="form-control" name="name[]">
                    </div>
                    <div class="form-group">
                        <label for="">Description de la photo</label>
                        <textarea class="form-control" name="desc[]"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="">Position de la photo</label>
                        <input type="number" class="form-control" value="1" name="position[]">
                    </div>
                    <div class="form-group">
                        <label for="">Activé</label>&nbsp;
                        <input type="checkbox" name="active[]" checked>
                    </div>
                </template>

                <?php
                echo '</div>';
                echo '<br>';
                echo '&nbsp;&nbsp;';
                echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';


                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->
                echo '</div>';


            } //end if
            ?>


            <?php
            if(isset($_GET['option']) && $_GET['option'] == 'edit' && isset($_GET['id'])) {
            $album = Gallery::find_by_id($_GET['id']);
            echo '<div class="row">';
            echo '<div class="col-md-12">';

            //<!-- BEGIN PROFILE CONTENT -->
            echo '<div class="profile-content">';
            echo '<div class="row">';
            echo '<div class="profile-tab-box">';
            echo '<div class="p-l-20">';
            echo '<h4> Editer l\'album </h4>';
            echo '</div>';
            echo '</div>';
            echo '<div class="white-box col-lg-12" style="padding: 50px">';
            //<!-- Tab panes -->
            echo '<div class="tab-content">';
            echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

            echo '<div class="inbox-body no-pad">';
            echo '<div class="mail-list">';
            echo '<div class="compose-mail">';

            echo '<div class="form-group">';
            echo '<label for="subject" class="">Titre de l\'album</label>';
            echo '<input name="album_name" value="'.$album->gallery_name.'" type="text" tabindex="1" id="subject" class="form-control" required>';
            echo '</div>';

            echo '<div class="form-group">';
            echo '<label for="subject" class="">Album activé</label>';
            $checked = ($album->gallery_active) ? 'checked' : '';
            echo '&nbsp; <input name="album_active" type="checkbox" tabindex="1" id="subject" '.$checked.'>';
            echo '</div>';

            echo '</div>';
            echo '</div>';
            ?>

            <input type="hidden" name="album_id" value="<?php echo $album->gallery_id ?>">

            <div id="form-bloc">
                <?php
                    $found_photos = Gallery::find_all_photos($album->gallery_id);
                    foreach($found_photos as $found_photo){
                ?>
                <hr>
                <div class="form-group">
                    <label for="">Uploader la photo *</label><br>
                    <label for="">(<?php echo $found_photo->gallery_image_path ?>)</label>
                    <input type="file" class="form-control" name="image[]">
                    <?php echo ' <img src="manager/media/files/"'.$found_slider->gallery_image_path.'" width="190" '; ?>
                    <input type="hidden" name="old_image[]" value="<?php echo $found_photo->gallery_image_path ?>">
                </div>
                <div class="form-group">
                    <label for="">Titre de la photo</label>
                    <input value="<?php echo $found_photo->gallery_name ?>" type="text" class="form-control" name="name[]">
                </div>
                <div class="form-group">
                    <label for="">Description de la photo</label>
                    <textarea class="form-control" name="desc[]"><?php echo $found_photo->gallery_desc ?></textarea>
                </div>
                <div class="form-group">
                    <label for="">Position de la photo</label>
                    <input value="<?php echo $found_photo->gallery_position ?>" type="number" class="form-control" value="1" name="position[]">
                </div>
                <div class="form-group">
                    <label for="">Activé</label>&nbsp;
                    <input type="checkbox" name="active[]" <?php if($found_photo->gallery_active) echo 'checked' ?>>
                </div>
                <?php } ?>
            </div>
            <button type="button" id="add" class="btn btn-outline-primary">+ Ajouter un formulaire</button>


            <template id="another-form">
                <!-- Another Form -->
                <hr>
                <div class="form-group">
                    <label for="">Uploader la photo *</label>
                    <input type="file" class="form-control" name="image[]" required>
                </div>
                <div class="form-group">
                    <label for="">Titre de la photo</label>
                    <input type="text" class="form-control" name="name[]">
                </div>
                <div class="form-group">
                    <label for="">Description de la photo</label>
                    <textarea class="form-control" name="desc[]"></textarea>
                </div>
                <div class="form-group">
                    <label for="">Position de la photo</label>
                    <input type="number" class="form-control" value="1" name="position[]">
                </div>
                <div class="form-group">
                    <label for="">Activé</label>&nbsp;
                    <input type="checkbox" name="active[]" checked>
                </div>
            </template>

            <?php
            echo '</div>';
            echo '<br>';
            echo '&nbsp;&nbsp;';
            echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';


            echo '</div>';
            echo '</div>';
            echo '</div>';
            echo '</div>';

            //<!-- END PROFILE CONTENT -->
            echo '</div>';
            echo '</div>';
            echo '</div>';
            //<!-- end page content -->
            echo '</div>';


            } //end if





            ?>


        </form>


        <!-- end page container -->

        <script src="assets/plugins/jquery/jquery.min.js"></script>

        <script>
            $('#add').click(function(){
                $('#form-bloc').append($('#another-form').html())
            })
        </script>
