


<?php
$found_userinfos = User_info::find_user_infos($session->user_account_id);
$found_useraccount = User_account::find_by_id($session->user_account_id);
$found_typeuser  = Typeuser::find_by_id($found_useraccount->user_account_typeuser_id);
?>
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="app.php?item=listpages">Pub</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Editer une pub</li>
                </ol>
            </div>
        </div>

        <form action="savead.php" class="form-horizontal" method="POST" enctype="multipart/form-data">

                <script>
                    function changeit(){
                        if(document.getElementById("cli").checked == true){
                            document.getElementById("divcustomer").hidden = false;
                            document.getElementById("divgroupe").hidden = true;
                        } else if (document.getElementById("group").checked == true){
                            document.getElementById("divcustomer").hidden = true;
                            document.getElementById("divgroupe").hidden = false;
                        } else if (document.getElementById("group").checked == false && document.getElementById("cli").checked == false){
                            document.getElementById("divcustomer").hidden = true;
                            document.getElementById("divgroupe").hidden = true;
                        }
                    }  
                </script>

            <?php
            $found_type_banners = Type_banner::find_all();
            //$found_clients = Customer::find_all();
            $found_clients = User_info::find_all();
            $found_groups = Group::find_all();
            if(isset($_GET['id'])) { $id  = $_GET['id']; }


            if(isset($_GET['option']) && $_GET['option'] == 'new'){
        
                //$found_ads = $advertise->find_by_id($id);

                echo '<div class="row">';
                echo '<div class="col-md-12">';
                //<!-- BEGIN PROFILE SIDEBAR -->
                echo '<div class="profile-sidebar">';
                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Attributs de la pub </div>';
                echo '<div class="profile-usertitle-job"> de la nouvelle pub </div>';
                echo '</div>';


                echo '<div id="displayed" class="card-body no-padding height-9">';
                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b>Etat </b>';
                echo '<div class="profile-desc-item pull-right">';
                echo '<i class="fa fa-eye"></i> publié';
                echo '</div>';
                echo '</li>';


                echo '</ul><br/>';

                echo '</div>';


                echo '</div>';


                echo '<div class="card card-topline-aqua">';
                echo '<div class="card-body no-padding height-9">';
                echo '<div class="row">';
                echo '</div>';
                echo '<br/>';



                //<!-- END SIDEBAR USER TITLE -->
                echo '<br/>';
                echo '<div class="control-group">';
                echo '<label class="control-label" for="active"> Publier cette pub?</label>';
                echo '<div class="controls">';
                echo '<div class="form-group">';
                echo '<div class="checkbox checkbox-icon-black">';
                echo '<input id="rememberChk1" name="active" type="checkbox" checked="checked">';
                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- SIDEBAR BUTTONS -->
                echo '</div>';
                echo '</div>';

                echo '</div>';


                //<!-- END BEGIN PROFILE SIDEBAR -->
                //<!-- BEGIN PROFILE CONTENT -->
                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';

                echo '<h4> Créer une nouvelle pub </h4>';

                echo '</div>';
                echo '</div>';
                echo '<div class="white-box">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Titre de la pub:</label>';
                echo '<input name="title" id="title"   type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Date de début:</label>';
                echo '<input name="datestart" id="title"   type="date" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Date de fin:</label>';
                echo '<input name="dateend" id="title"   type="date" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Type bannière:</label>';
                echo '<select name="type_banner_id" class="form-control">';
                foreach($found_type_banners as $found_type_banner){
                    echo "<option value='$found_type_banner->type_banner_id'>$found_type_banner->type_banner_desc</option>";
                }
                echo '</select>';
                echo '</div>';
                
                /*echo '<div class="form-group">';
                echo '<label for="subject" class="">groupe</label>';
                echo '<input type="checkbox" id="group" name="group"  onclick="changeit()">';
                echo '<label for="subject" class="">client</label>';
                echo '<input type="checkbox" id="cli" name="cli" onclick="changeit()" >';
                echo '</div>';*/

                echo '<div class="btn-group" data-toggle="buttons">
                            <label class="btn btn-primary deepPink-bgcolor">
                                <input type="checkbox" id="group" name="group"  onclick="changeit()"> Groupe
                            </label>
                            <label class="btn btn-primary deepPink-bgcolor">
                                <input type="checkbox" id="cli" name="cli" onclick="changeit()"> Client
                            </label>
                        
                        </div>
                        <br>
                        
                    </div>';

                

                echo '<div class="form-group" id="divcustomer" hidden>';
                echo '<label for="subject" class="">Client:</label>';
                echo '<select name="customer[]" multiple class=" select2-multiple select2-search" >';
                foreach($found_clients as $found_client){
                    //echo "<option value='$found_client->customer_id'>$found_client->customer_firstname</option>";
                    echo "<option value='$found_client->user_info_id'>$found_client->user_info_firstname</option>";
                }
                echo '</select>';
                echo '</div>';

                echo '<div class="form-group" id="divgroupe" hidden>';
                echo '<label for="subject" class="">Groupe:</label>';
                echo '<select name="groupe[]" multiple class=" select2-multiple select2-search">';
                foreach($found_groups as $found_group){
                    //echo "<option value='$found_client->customer_id'>$found_client->customer_firstname</option>";
                    echo "<option value='$found_group->group_id'>$found_group->group_libelle</option>";
                }
                echo '</select>';
                echo '</div>';

                /*echo '<div class="form-group">';
                echo '<label for="subject" class="">Client</label>';
                echo '<input name="customer" id="customer  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';*/

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Url</label>';
                echo '<input name="link" id="link"  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Position</label>';
                echo '<input name="position" value="1" id="position"  type="number" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Image</label>';
                echo '<input name="image" id="image" type="file" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

               /* echo '<div class="form-group">';
                echo '<label for="subject" class="">Choisissez votre Pack</label>';
                $found_packs = Pack::find_all();
                $current_name = '';
                $current_position = '';

                foreach($found_packs as $found_pack){
                    $found_pack->pack_position = ($found_pack->pack_position == 1) ? "Page Principale" : "Page Secondaire";
                    if($current_name != $found_pack->pack_name){
                        echo "<h3>$found_pack->pack_name</h3>";
                    }
                    if($current_position != $found_pack->pack_position){
                        echo "<h5>$found_pack->pack_position</h5>";
                    }
                    echo "<input type='radio' name='pack_id' value='$found_pack->pack_id'> $found_pack->pack_type : <b>$found_pack->pack_amount</b><br>";
                    $current_name = $found_pack->pack_name;
                    $current_position = $found_pack->pack_position;
                }

                echo '</div>';*/

                echo '</div>';
                echo '<br>';
                echo '&nbsp;&nbsp;';

                if(isset($_GET['id'])){
                    $id = $_GET['id'];
                    echo '<input name="id" type="hidden" value="'.$id.'" >';
                }

                echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';

                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->
                echo '</div>';


            } //end if
            ?>


            <?php
            if(isset($_GET['option']) && $_GET['option'] == 'edit' && isset($_GET['id'])) {

                echo '<input type="hidden" name="id" value="'.$id.'" />';
                $found_ads = $advertise->find_by_id($id);


                echo '<div class="row">';
                echo '<div class="col-md-12">';
                //<!-- BEGIN PROFILE SIDEBAR -->
                echo '<div class="profile-sidebar">';
                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Attributs de la pub </div>';
                echo '<div class="profile-usertitle-job"> de la pub '.$found_ads->advertise_title.' </div>';
                echo '</div>';
                echo '<div class="card-body no-padding height-9">';

                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b>Etat </b>';
                echo '<div class="profile-desc-item pull-right">';

                if($found_ads->advertise_active==1) {
                    echo '<i class="fa fa-eye"></i> publié';
                }
                else {
                    echo '<i class="fa fa-eye-slash"></i> non publiée';
                }

                echo '</div>';
                echo '</li>';

                echo '<li class="list-group-item">';
                if($found_ads->advertise_active==0) {

                    echo '<b>Déplacer dans la corbeille </b>';
                    echo '<div class="profile-desc-item pull-right"> <a href="savead.php?pid='.$id.'&option=delete"><i class="fa fa-trash-o "></i></a> </div>';
                }
                else {
                    echo '<b>Restaurer cette page</b>';
                    echo '<div class="profile-desc-item pull-right"> <a href="savead.php?pid='.$id.'&option=recover"><i class="fa  fa-recycle "></i></a> </div>';
                }

                echo '</li>';

                echo '</ul><br/>';

                echo '</div>';
                echo '</div>';


                echo '<div class="card card-topline-aqua">';
                echo '<div class="card-body no-padding height-9">';
                echo '<div class="row">';
                echo '<div class="profile-userpic">';
                echo '</div>';
                echo '</div>';

                echo '<div class="controls">';
                echo '<div class="form-group">';
                echo '<div class="checkbox checkbox-icon-black">';


                if($found_ads->advertise_active==1) {
                    echo '<input id="rememberChk1" name="active" type="checkbox" checked="checked"> publié';
                }
                else {
                    echo '<input id="rememberChk1" name="active" type="checkbox"> non publiée';
                }

                echo '</div>';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<div class="checkbox checkbox-icon-black">';

                //Payed à voir
                /*if($found_typeuser->type_user_desc == 'root'){
                    $checked = $found_ads->advertise_payed ? 'checked' : '';
                    echo '<input id="rememberChk1" name="payed" type="checkbox" '.$checked.'> Payée ?';
                }*/

                echo '</div>';
                echo '</div>';

                echo '</div>';

                //<!-- END SIDEBAR BUTTONS -->
                echo '</div>';
                echo '</div>';

                echo '</div>';


                //<!-- END BEGIN PROFILE SIDEBAR -->
                //<!-- BEGIN PROFILE CONTENT -->

                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';
                echo '<h4> Modifier la pub : '.$found_ads->advertise_title.' </h4>';

                if(isset($_GET['r']) && $_GET['r']==0) {
                    echo '<span class="clsNotAvailable"> Cette pub est placée dans la corbeille. </span>';
                }
                else if(isset($_GET['r']) && $_GET['r']==1) {
                    echo '<span class="clsAvailable"> Cette pub a été restaurée. </span>';
                }

                echo '</div>';
                echo '</div>';


                echo '<div class="white-box">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';


                echo '<div class="form-group">';
                echo '<label for="subject" class="">Titre de la pub:</label>';
                echo '<input name="title" id="title" value="'.$found_ads->advertise_title.'"  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Date de début:</label>';
                echo '<input name="datestart" id="title"  value="'.$found_ads->advertise_datestart.'"  type="date" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Date de fin:</label>';
                echo '<input name="dateend" id="title" value="'.$found_ads->advertise_dateend.'"  type="date" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Type bannière:</label>';
                echo '<select name="type_banner_id" class="form-control">';
                    foreach($found_type_banners as $found_type_banner){
                        $selected = $found_type_banner->type_banner_id == $found_ads->advertise_typebanner_id ? 'selected' : '';
                        echo "<option value='$found_type_banner->type_banner_id' $selected>$found_type_banner->type_banner_desc</option>";
                    }
                echo '</select>';
                echo '</div>';

                
                echo '<div class="form-group">';
                echo '<label for="subject" class="">groupe</label>';
                echo '<input type="checkbox" id="group" name="group"  onclick="changeit()">';
                echo '<label for="subject" class="">client</label>';
                echo '<input type="checkbox" id="cli" name="cli" onclick="changeit()" >';
                echo '</div>';


                if($found_ads->advertise_grouped == 0){
                    echo '<div class="form-group" id="divcustomer" >';
                }else{
                    echo '<div class="form-group" id="divcustomer" hidden>';
                }
                echo '<label for="subject" class="">Client:</label>';
                echo '<select name="customer[]" multiple class=" select2-multiple">';
                    //explode ids
                    //on part sur les clients présent dans le linkads

                    $links =  Linkads :: find_by_id($id);
                    $string = "";
                    echo sizeof($links);
                    if(sizeof($links) != 0 && $found_ads->advertise_grouped == 0){
                    /*foreach($links as $link){
                        $customer_ids = User_info::find_by_id($link->linkads_customerid);
                        echo "<option value='$customer_ids->user_info_id' selected >$customer_ids->user_info_firstname</option>";
                        $string.=$customer_ids->user_info_id.",";
                    }
                    $string = substr($string, 0, -1);
                    $listcustomers =  User_info::list_multiple_others($string);
                    foreach($listcustomers as $listcustomer){
                        echo "<option value='$listcustomer->user_info_id' >$listcustomer->user_info_firstname</option>";
                    }*/
                    }else{
                        $listcustomers =  User_info::find_all();
                        foreach($listcustomers as $listcustomer){
                            echo "<option value='$listcustomer->user_info_id' >$listcustomer->user_info_firstname</option>";
                        }

                    }

                    
                echo '</select>';
                echo '</div>';



                if($found_ads->advertise_grouped == 1){
                    echo '<div class="form-group" id="divgroupe" >';
                }else{
                    echo '<div class="form-group" id="divgroupe" hidden>';
                }
                echo '<label for="subject" class="">Groupe:</label>';
                echo '<select name="groupe[]" multiple class=" select2-multiple">';
                    //explode ids
                    //on part sur les clients présent dans le linkads
                    $links =  Linkads :: find_by_id($id);
                    $string = "";
                    $tmp_int = 0;
                    if(sizeof($links) != 0 && $found_ads->advertise_grouped != 0){
                    foreach($links as $link){
                        if($tmp_int != $link->linkads_group_id){
                            $group_ids = Group::find_by_id($link->linkads_group_id);
                            echo "<option value='$group_ids->group_id' selected >$group_ids->group_libelle</option>";
                            $string.=$group_ids->group_id.",";
                            $tmp_int=$link->linkads_group_id;
                        }
                    }
                        if($tmp_int != 0){
                            $string = substr($string, 0, -1);
                            $listgroups =  Group::list_multiple_others($string);
                            foreach($listgroups as $listgroup){
                                echo "<option value='$listgroup->group_id' >$listgroup->group_libelle</option>";
                            }
                        }
                    }else if($tmp_int == 0){

                        $listgroups =  Group::find_all();
                        foreach($listgroups as $listgroup){
                            echo "<option value='$listgroup->group_id' >$listgroup->group_libelle</option>";
                        }

                    }

                    
                echo '</select>';
                echo '</div>';

                /*echo '<div class="form-group">';
                echo '<label for="subject" class="">Client</label>';
                echo '<input name="customer" id="customer" value="'.$found_ads->advertise_customer.'"  type="text" class="form-control">';
                echo '</div>';*/

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Url</label>';
                echo '<input name="link" id="link" value="'.$found_ads->advertise_link.'"  type="text" tabindex="1" 	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Position</label>';
                echo '<input name="position" id="position" value="'.$found_ads->advertise_position.'"  type="number" tabindex="1" class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Image</label>';
                echo '<input name="image" type="file"  tabindex="1" id="image" class="form-control">';
                echo '</div>';

                /*echo '<div class="form-group">';
                echo '<label for="subject" class="">Choisissez votre Pack</label>';
                $found_packs = Pack::find_all();
                $current_name = '';
                $current_position = '';

                foreach($found_packs as $found_pack){
                    $found_pack->pack_position = ($found_pack->pack_position == 1) ? "Page Principale" : "Page Secondaire";
                    if($current_name != $found_pack->pack_name){
                        echo "<h3>$found_pack->pack_name</h3>";
                    }
                    if($current_position != $found_pack->pack_position){
                        echo "<h5>$found_pack->pack_position</h5>";
                    }
                    $checked = ($found_ads->advertise_pack_id == $found_pack->pack_id) ? 'checked' : '';

                    echo "<input type='radio' name='pack_id' value='$found_pack->pack_id' $checked> $found_pack->pack_type : <b>$found_pack->pack_amount</b><br>";
                    $current_name = $found_pack->pack_name;
                    $current_position = $found_pack->pack_position;
                }

                echo '</div>';*/



                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '<br>';
                echo '&nbsp;&nbsp;';
                echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';

                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->

                echo '</div>';


            } // end if
            ?>

        </form>


        <!-- end page container -->

