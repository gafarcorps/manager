


<?php
$found_userinfos = User_info::find_user_infos($session->user_account_id);
$found_useraccount = User_account::find_by_id($session->user_account_id);
$found_typeuser  = Typeuser::find_by_id($found_useraccount->user_account_typeuser_id);
?>
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Editer une activité</li>
                </ol>
            </div>
        </div>

        <form action="saveactivity.php" class="form-horizontal" method="POST" enctype="multipart/form-data">

            <?php
            $found_type_banners = Type_banner::find_all();
            //$found_clients = Customer::find_all();
            $found_clients = User_info::find_all();
            if(isset($_GET['id'])) { $id  = $_GET['id']; }


            if(isset($_GET['option']) && $_GET['option'] == 'new'){
        
                //$found_ads = $advertise->find_by_id($id);

                echo '<div class="row">';
                echo '<div class="col-md-12">';
                //<!-- BEGIN PROFILE SIDEBAR -->
                echo '<div class="profile-sidebar">';
                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Attributs de l\'activité </div>';
                echo '<div class="profile-usertitle-job"> de la nouvelle activité </div>';
                echo '</div>';


                echo '<div id="displayed" class="card-body no-padding height-9">';
                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b>Etat </b>';
                echo '<div class="profile-desc-item pull-right">';
                echo '<i class="fa fa-eye"></i> publié';
                echo '</div>';
                echo '</li>';


                echo '</ul><br/>';

                echo '</div>';


                echo '</div>';


                echo '<div class="card card-topline-aqua">';
                echo '<div class="card-body no-padding height-9">';
                echo '<div class="row">';
                echo '</div>';
                echo '<br/>';



                //<!-- END SIDEBAR USER TITLE -->
                echo '<br/>';
                /*echo '<div class="control-group">';
                echo '<label class="control-label" for="active"> Publier cette pub?</label>';
                echo '<div class="controls">';
                echo '<div class="form-group">';
                echo '<div class="checkbox checkbox-icon-black">';
                echo '<input id="rememberChk1" name="active" type="checkbox" checked="checked">';
                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';*/

                //<!-- SIDEBAR BUTTONS -->
                echo '</div>';
                echo '</div>';

                echo '</div>';


                //<!-- END BEGIN PROFILE SIDEBAR -->
                //<!-- BEGIN PROFILE CONTENT -->
                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';

                echo '<h4> Créer une nouvelle activité </h4>';

                echo '</div>';
                echo '</div>';
                echo '<div class="white-box">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Nom de l\'activité:</label>';
                echo '<input name="title" id="title"   type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';


               /*  PARTIE CLIENT EN COMMENTAIRE
               echo '<div class="form-group">';
                echo '<label for="subject" class="">Client:</label>';
                echo '<select name="customer[]" multiple class="form-control select2-multiple">';
                foreach($found_clients as $found_client){
                    //echo "<option value='$found_client->customer_id'>$found_client->customer_firstname</option>";
                    echo "<option value='$found_client->user_info_id'>$found_client->user_info_firstname</option>";
                }
                echo '</select>';
                echo '</div>';*/

                /*echo '<div class="form-group">';
                echo '<label for="subject" class="">Client</label>';
                echo '<input name="customer" id="customer  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';*/

                
               /* echo '<div class="form-group">';
                echo '<label for="subject" class="">Choisissez votre Pack</label>';
                $found_packs = Pack::find_all();
                $current_name = '';
                $current_position = '';

                foreach($found_packs as $found_pack){
                    $found_pack->pack_position = ($found_pack->pack_position == 1) ? "Page Principale" : "Page Secondaire";
                    if($current_name != $found_pack->pack_name){
                        echo "<h3>$found_pack->pack_name</h3>";
                    }
                    if($current_position != $found_pack->pack_position){
                        echo "<h5>$found_pack->pack_position</h5>";
                    }
                    echo "<input type='radio' name='pack_id' value='$found_pack->pack_id'> $found_pack->pack_type : <b>$found_pack->pack_amount</b><br>";
                    $current_name = $found_pack->pack_name;
                    $current_position = $found_pack->pack_position;
                }

                echo '</div>';*/

                echo '</div>';
                echo '<br>';
                echo '&nbsp;&nbsp;';

                if(isset($_GET['id'])){
                    $id = $_GET['id'];
                    echo '<input name="id" type="hidden" value="'.$id.'" >';
                }

                echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';

                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->
                echo '</div>';


            } //end if
            ?>


            <?php
            if(isset($_GET['option']) && $_GET['option'] == 'edit' && isset($_GET['id'])) {

                echo '<input type="hidden" name="id" value="'.$id.'" />';
                $found_groupe = $activity->find_by_id($id);


                echo '<div class="row">';
                echo '<div class="col-md-12">';
                //<!-- BEGIN PROFILE SIDEBAR -->
                

                //<!-- END BEGIN PROFILE SIDEBAR -->
                //<!-- BEGIN PROFILE CONTENT -->

                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';
                echo '<h4> Modifier le groupe : '.$found_groupe->activity_libelle.' </h4>';

                /*if(isset($_GET['r']) && $_GET['r']==0) {
                    echo '<span class="clsNotAvailable"> Cette pub est placée dans la corbeille. </span>';
                }
                else if(isset($_GET['r']) && $_GET['r']==1) {
                    echo '<span class="clsAvailable"> Cette pub a été restaurée. </span>';
                }*/

                echo '</div>';
                echo '</div>';


                echo '<div class="white-box">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';


                echo '<div class="form-group">';
                echo '<label for="subject" class="">Nom du groupe:</label>';
                echo '<input name="title" id="title" value="'.$found_groupe->activity_libelle.'"  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                // COMMENTAIRE
                /*echo '<div class="form-group">';
                echo '<label for="subject" class="">Client:</label>';
                echo '<select name="customer[]" multiple class="form-control select2-multiple">';*/
                    //explode ids
                    //on part sur les clients présent dans le linkads
                    /*$links =  Linkads :: find_by_id($id);
                    $string = "";
                    if(sizeof($links) != 0){
                    foreach($links as $link){
                        $customer_ids = Customer::find_by_id($link->linkads_customerid);
                        echo "<option value='$customer_ids->customer_id' selected >$customer_ids->customer_firstname</option>";
                        $string+=$customer_ids->customer_id+",";
                    }
                    $string = substr($string, 0, -1);
                    $listcustomers =  Customer::list_multiple_others($string);
                    foreach($listcustomers as $listcustomer){
                        echo "<option value='$listcustomer->customer_id' >$listcustomer->customer_firstname</option>";
                    }
                    }else{
                        $listcustomers =  Customer::find_all();
                        foreach($listcustomers as $listcustomer){
                            echo "<option value='$listcustomer->customer_id' >$listcustomer->customer_firstname</option>";
                        }

                    }*/

                   /* $links =  Link_groupe :: find_by_id($id); // use of groupe
                    $string = "";
                    if(sizeof($links) != 0){
                    foreach($links as $link){
                        $customer_ids = User_info::find_by_id($link->user_id);
                        echo "<option value='$customer_ids->user_info_id' selected >$customer_ids->user_info_firstname</option>";
                        $string.=$customer_ids->user_info_id.",";
                    }
                    echo $string;
                    $string = substr($string, 0, -1);
                    
                    $listcustomers =  User_info::list_multiple_others($string);
                    foreach($listcustomers as $listcustomer){
                        echo "<option value='$listcustomer->user_info_id' >$listcustomer->user_info_firstname</option>";
                    }
                    }else{
                        $listcustomers =  User_info::find_all();
                        foreach($listcustomers as $listcustomer){
                            echo "<option value='$listcustomer->user_info_id' >$listcustomer->user_info_firstname</option>";
                        }

                    }*/

                    /*if(isset($found_ads->advertise_customer_id)){
                        $explodes = explode(",",$found_ads->advertise_customer_id);
                        
                       
                            foreach($explodes as $explode){
                                $customer_ids = Customer::find_by_id($explode);
                                echo "<option value='$customer_ids->customer_id' selected >$customer_ids->customer_firstname</option>";
                            }
                       
                        //other ids
                        $listcustomers =  Customer::list_multiple_others($found_ads->advertise_customer_id);
                        foreach($listcustomers as $listcustomer){
                            echo "<option value='$listcustomer->customer_id' >$listcustomer->customer_firstname</option>";
                        }

                    }else{
                        foreach($found_clients as $found_client){
                            echo "<option value='$found_client->customer_id'  >$found_client->customer_firstname</option>";
                        }
                    }*/
                    /*foreach($found_clients as $found_client){

                        $selected = $found_client->customer_id == $found_ads->advertise_customer ? 'selected' : '';
                        echo "<option value='$found_type_banner->type_banner_id' $selected>$found_type_banner->type_banner_desc</option>";
                    }*/
                /*echo '</select>';
                echo '</div>';*/

                /*echo '<div class="form-group">';
                echo '<label for="subject" class="">Client</label>';
                echo '<input name="customer" id="customer" value="'.$found_ads->advertise_customer.'"  type="text" class="form-control">';
                echo '</div>';*/

                

                

                /*echo '<div class="form-group">';
                echo '<label for="subject" class="">Choisissez votre Pack</label>';
                $found_packs = Pack::find_all();
                $current_name = '';
                $current_position = '';

                foreach($found_packs as $found_pack){
                    $found_pack->pack_position = ($found_pack->pack_position == 1) ? "Page Principale" : "Page Secondaire";
                    if($current_name != $found_pack->pack_name){
                        echo "<h3>$found_pack->pack_name</h3>";
                    }
                    if($current_position != $found_pack->pack_position){
                        echo "<h5>$found_pack->pack_position</h5>";
                    }
                    $checked = ($found_ads->advertise_pack_id == $found_pack->pack_id) ? 'checked' : '';

                    echo "<input type='radio' name='pack_id' value='$found_pack->pack_id' $checked> $found_pack->pack_type : <b>$found_pack->pack_amount</b><br>";
                    $current_name = $found_pack->pack_name;
                    $current_position = $found_pack->pack_position;
                }

                echo '</div>';*/



                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '<br>';
                echo '&nbsp;&nbsp;';
                echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';

                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->

                echo '</div>';


            } // end if
            ?>

        </form>


        <!-- end page container -->

