<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="app.php?item=home">Accueil</a>&nbsp;<i
                                class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="#">Pages</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Liste des archives</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Liste des archives</header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row p-b-20">
                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group">
                                <a href="app.php?item=formtopic&pid=11&option=new" id="addRow" class="btn    btn-info">
                                               Nouvel article <i class="fa fa-plus"></i>
                                           </a>
                                             &nbsp;&nbsp;
                                            <a href="app.php?item=listtopics" id="addRow" class="btn btn-success">
												Articles publiés 
												</a> &nbsp;&nbsp;
                                                <a href="app.php?item=archivetopics" id="addRow" class="btn btn-warning">
												Archives 
												</a> &nbsp;&nbsp;
												<a href="app.php?item=disabledtopics" id="addRow" class="btn btn-danger"><b>Corbeille </b>
                                            </a> 
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group pull-right">
                                    <?php
                                    if (isset($_GET['pid'])) {
                                        $pid = $_GET['pid'];
                                        echo '<a href="app.php?item=listtopics" class="btn deepPink-bgcolor">Retourner à liste précédente';
                                        echo '</a>';
                                    } // enf if
                                    ?>

                                </div>
                            </div>
                        </div>

                                              <?php
												 if(isset($_GET['msg']) && $_GET['msg']=='success') {?>
													 <script>
													  Swal.fire(
														  "Parfait", 
														  "Enregistrement réussi.",
														  "success"
														  )
												  </script>
												  <?php
												 	} 
													?>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-topline-aqua">
                                    <div class="card-head">
                                        <?php
                                        if (isset($_GET['msg']) && $_GET['msg'] == 'delete') {
                                            echo '<span class="clsAvailable"> Article supprimé. </span>';
                                        }
                                        ?>
                                        <div class="tools">
                                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                                        </div>
                                    </div>
                                    <div class="card-body ">
                                        <div class="table-scrollable">
                                            <table id="example1" class="display full-width">
                                                <thead>
                                                <tr>
                                                    <!--																	<th> </th> -->
                                                    <th> Date création</th>
                                                    <th>Titre</th>
                                                    <th> Catégorie</th>
                                                    <th> Status</th>
                                                    <th> Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <?php
                                                if (isset($_GET['pid'])) {
                                                    $pid = $_GET['pid'];
                                                    $found_topics = Topics:: find_topic_archive($pid);
                                                    foreach ($found_topics as $found_topic) {
                                                        $found_type_topic = Type_topic::find_by_id($found_topic->topic_typetopic_id);
                                                        $verify = Topics::verify_topic($found_topic->topic_image_path,$found_topic->topic_title,$found_topic->topic_text);
                                                        echo '<tr class="odd gradeX">';

                                                        //echo '<td class="user-circle-img">';
                                                        //echo '<img src="media/files/'.$found_topic->topic_image_path.'" width="50" height="50" alt="'.$found_topic->topic_title.'">';
                                                        //echo '</td>';
                                                        //echo '<td></td>';
                                                        echo '<td>' . $found_topic->topic_date_creation . '</td>';
                                                        echo '<td><a href="app.php?item=formtopic&pid=' . $found_topic->page_id . '&option=edit">' . $found_topic->topic_title . '</td>';
                                                        echo '<td>' . $found_type_topic->type_topic_desc . '</td>';
                                                        echo '<td>'.$verify.'</td>';
                                                        echo '<td>';
                                                        echo ' <a href="app.php?item=formtopic&pid=' . $found_topic->topic_id . '&option=edit" class="btn btn-tbl-edit btn-xs">';
                                                        echo '<i class="fa fa-pencil"></i>';
                                                        echo '</a>';
                                                        echo '</td>';
                                                        echo '</tr>';
                                                    } // end foreach
                                                } else {
                                                    $found_topics = Topics:: find_all_archive();
                                                    foreach ($found_topics as $found_topic) {
                                                        $found_type_topic = Type_topic::find_by_id($found_topic->topic_typetopic_id);
                                                        $verify = Topics::verify_topic($found_topic->topic_image_path,$found_topic->topic_title,$found_topic->topic_text);
                                                        echo '<tr class="odd gradeX">';
                                                        //echo '<td class="user-circle-img">';
                                                        //echo '<img src="media/files/'.$found_topic->topic_image_path.'" width="50" height="50" alt="'.$found_topic->topic_title.'">';
                                                        //echo '</td>';
                                                        echo '<td>' . $found_topic->topic_date_creation . '</td>';
                                                        echo '<td><a href="app.php?item=formtopic&pid=' . $found_topic->topic_id . '&option=edit">' . neatest_trim($found_topic->topic_title, 50) . '</td>';
                                                        echo '<td>' . $found_type_topic->type_topic_desc . '</td>';
                                                        echo '<td>'.$verify.'</td>';
                                                        echo '<td>';
                                                        echo ' <a href="app.php?item=formtopic&pid=' . $found_topic->topic_id . '&option=edit" class="btn btn-tbl-edit btn-xs">';
                                                        echo '<i class="fa fa-pencil"></i>';
                                                        echo '</a>';
                                                        echo '</td>';

                                                        echo '</tr>';
                                                    } // end foreach

                                                } // else

                                                ?>

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end page content -->