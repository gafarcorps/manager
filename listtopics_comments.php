
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans le gestionnaire de votre site Web</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="app.php?item=listtopics_comments">Commentaies</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Liste des commentaies</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Liste des commentaies</header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row p-b-20">
                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group">
                                    <!--                                    <a href="app.php?item=formbookc?option=new" id="addRow" class="btn btn-info">
                                                                            New book <i class="fa fa-plus"></i>
                                                                        </a>-->

                                </div>
                                <div class="btn-group">
                                    <a href="app.php?item=disabledtopics_comments" id="addRow" class="btn btn-danger"><b> Corbeille </b></a> 
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group pull-right">
                                    <?php
                                    if (isset($_GET['comment_id'])) {
                                        $pid = $_GET['comment_id'];
                                        echo '<a href="app.php?item=listtopics_comments" class="btn deepPink-bgcolor">Return to previous list ';
                                        echo '</a>';
                                    } // enf if
                                    ?>

                                </div>
                            </div>
                        </div>

                        <?php
                        if (isset($_GET['msg']) && $_GET['msg'] == 'success') {
                            echo '<span class="clsAvailable"> Success. </span>';
                        }
                        ?>

                        <div class="table-scrollable">
                            <table class="table table-hover table-checkable order-column full-width"
                                   id="example4">
                                <thead>
                                    <tr>  
                                        <th class="center"></th>
                                        <th class="center">Date </th>
                                        <th class="center"> Auteur </th> 
                                        <!--<th class="center"> Email </th>--> 
                                        <th class="center"> Message </th>
                                        <th class="center"> Article </th>
                                        <th class="center"> Statut </th>
                                        <th class="center"> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $found_topics_comments = Comment_topic :: find_all_active();
                                    foreach ($found_topics_comments as $found_topic_comment) {
//                                        echo ">>".$found_topic_comment->comment_topic_active;
                                        $found_topic = Topics::find_by_id($found_topic_comment->comment_topic_topicid);
                                        echo '<tr class="odd gradeX">';
                                        echo '<td class="center"></td>';
                                        echo '<td class="center">' . $found_topic_comment->comment_topic_date . '</td>';
                                        echo '<td class="center">' . $found_topic_comment->comment_topic_author . '</td>';
//                                        echo '<td class="center">' . $found_topic_comment->comment_topic_author_email . '</td>';
                                        echo '<td class="center">' . $found_topic_comment->comment_topic_text . '<br><a href="app.php?item=formtopic_comment&comment_id='.$found_topic_comment->comment_topic_id.'&option=edit">Modifier</a></td>';
                                        echo '<td class="center">' . $found_topic->topic_title . '</td>';
                                        echo '<td class="center">';
                                        echo '<strong style="color:' . (($found_topic_comment->comment_topic_published == 1) ? "green" : "red") . '">' . (($found_topic_comment->comment_topic_published == 1) ? "Publié" : "Non publié") . '</strong>';
                                        echo '</td>';
                                        echo '<td class="center">';
                                        echo '<a href="savetopic_comment.php?comment_id=' . $found_topic_comment->comment_topic_id . '&option=' . (($found_topic_comment->comment_topic_published == 1) ? "disable" : "activate") . '"><span class="label label-' . (($found_topic_comment->comment_topic_published == 1) ? "warning" : "success") . '">' . (($found_topic_comment->comment_topic_published == 1) ? "Désactiver" : "Publier") . '</span></a>';
                                        echo '<a href="savetopic_comment.php?comment_id=' . $found_topic_comment->comment_topic_id . '&option=' . (($found_topic_comment->comment_topic_active == 1) ? "delete" : "recover") . '"><span class="label label-' . (($found_topic_comment->comment_topic_active == 1) ? "danger" : "info") . '">' . (($found_topic_comment->comment_topic_active == 1) ? "Supprimer" : "Restaurer") . '</span></a>';
                                        echo '</td>';
//                                            echo '<td class="center">';
//                                            if ($found_page->page_active == 1) {
//                                                echo '<a href="app.php?item=formpage&pid=' . $found_page->page_id . '&option=edit"><span class="label label-info">Update</span></a>';
//                                            } else {
//                                                echo '<a href="app.php?item=formpage&pid=' . $found_page->page_id . '&option=edit"><span class="label label-danger">Not published</span></a>';
//                                            }
//                                            echo '</td>';

                                        echo '</tr>';
                                    } // end foreach
                                    ?>

                                </tbody>
                            </table>
                            <?php echo '<br/><b> </b>'; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end page content -->
