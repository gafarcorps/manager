<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item" href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="app.php?item=listpages">Pages</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Editer un article</li>
                </ol>
            </div>
        </div>

        <form action="savetopic.php" class="form-horizontal" method="POST" enctype="multipart/form-data">

            <?php
            if (isset($_GET['pid'])) {
                $pid = $_GET['pid'];
            }


            if (isset($_GET['option']) && $_GET['option'] == 'new') {

                echo '<div class="row">';
                echo '<div class="col-md-12">';
                //<!-- BEGIN PROFILE SIDEBAR -->
                echo '<div class="profile-sidebar">';
                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Attributs de la page </div>';
                echo '<div class="profile-usertitle-job"> de la nouvelle page </div>';
                echo '</div>';


                echo '<div id="displayed" class="card-body no-padding height-9">';
                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b>&Agrave; la une</b>';
                echo '<div class="profile-desc-item pull-right">';
                echo '<input id="rememberChk1" name="breakingnew" type="checkbox">';
                echo '</div>';
                echo '</li>';

                echo '<li class="list-group-item">';
                echo '<b>Choix de la rédaction</b>';
                echo '<div class="profile-desc-item pull-right">';
                echo '<input id="rememberChk1" name="breakingnew1" type="checkbox">';
                echo '</div>';
                echo '</li>';

                echo '<li class="list-group-item">';
                echo '<b>Pour les abonnés</b>';
                echo '<div class="profile-desc-item pull-right">';
                echo '<input id="rememberChk2" name="breakingnew2" type="checkbox">';
                echo '</div>';
                echo '</li>';

                echo '<li class="list-group-item">';
                echo '<b>Etat </b>';
                echo '<div class="profile-desc-item pull-right">';
                echo '<i class="fa fa-eye"></i> publi&eacute;';
                echo '</div>';
                echo '</li>';

                echo '<li class="list-group-item">';
                echo '<b> Date de cr&eacute;ation </b>';
                echo '<div class="profile-desc-item pull-right">';
                echo '<i class="fa fa-clock-o"> </i>' . date('Y-m-d H:i:s') . '';
                echo '</div>';
                echo '</li>';

                echo '</ul><br/>';
                echo '</div>';
                echo '</div>';


                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Photo de couverture </div>';
                echo '</div>';

                echo '<div id="displayed" class="card-body no-padding height-9">';
                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<div class="profile-desc-item pull-left">';
                echo '<input type="file" name="file_upload">';
                echo '</div>';
                echo '</li>';
                echo '</ul><br/>';
                echo '</div>';
                echo '</div>';

                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Alignement du menu</div>';
                echo '</div>';
                echo '<li class="list-group-item">';
                //echo '<b>Section de page</b> <a class="pull-right"></a>';
                echo ' <select id="multiple" name="section_id[]" class="form-control select2-multiple" multiple>';
                $found_sections = Section::find_elements_alignment();
                foreach ($found_sections as $found_section) {
                    echo '<option value="' . $found_section->section_id . '">' . $found_section->section_desc . '</option>';
                }
                echo '</select>';
                echo '</li>';
                echo '</div>';


                echo '<div class="card card-topline-aqua">';
                echo '<div class="card-body no-padding height-9">';
                echo '<div class="row">';
                echo '</div>';
                echo '<br/>';


                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b>Cat&eacute;gorie </b> <a class="pull-right"></a>';
                echo '<div class="form-group">';
                echo '<select name="typetopic" id="parent" class="form-control" >';

                $found_typetopics = Type_topic::find_all();
                foreach ($found_typetopics as $found_typetopic) {
                    echo '<option value="' . $found_typetopic->type_topic_id . '">' . $found_typetopic->type_topic_desc . '</option>';
                } // end  foreach 
                echo '</select>';

                echo '</div>';
                echo '</li>';

                echo '<b>Etiquette </b> <a class="pull-right"></a>';
                echo '<select name="etiquette[]" id="multiple" class="form-control select2-multiple" multiple="multiple">';
                echo '<option value="1">S&eacute;lectionner une &eacute;tiquette</option>';
                $found_etiquettes = $etiquette->find_all();
                foreach ($found_etiquettes as $found_etiquette) {
                    echo '<option value="' . $found_etiquette->etiquette_id . '">' . $found_etiquette->etiquette_desc . '</option>';
                } // end  foreach
                echo '</optgroup>';
                echo '</select>';
            ?>


                <?php
                echo '</ul>';
                //<!-- END SIDEBAR USER TITLE -->
                echo '<br/>';
                echo '<div class="control-group">';
                echo '<label class="control-label" for="active"> Publier cette page?</label>';
                echo '<div class="controls">';
                echo '<div class="form-group">';
                echo '<div class="checkbox checkbox-icon-black">';
                echo '<input id="rememberChk1" name="active" type="checkbox" checked="checked">';
                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- SIDEBAR BUTTONS -->
                echo '</div>';
                echo '</div>';

                echo '</div>';


                //<!-- END BEGIN PROFILE SIDEBAR -->
                //<!-- BEGIN PROFILE CONTENT -->
                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';

                echo '<h4> Ecrire un nouveau article </h4>';

                echo '</div>';
                echo '</div>';
                echo '<div class="white-box">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Titre de l\'article:</label>';
                echo '<input name="title" type="text" tabindex="1" id="subject" class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Contenu de l\'article:</label>';
                echo '<textarea name="topic_text" id="description" class="span12" rows="8"></textarea>';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Mots clés de l\'article:</label>';
                echo '<input name="icon" placeholder="Mots clés" type="text" tabindex="1" id="subject" class="form-control">';
                echo '</div>';

                echo '</div>';

                echo '</div>';
                echo '<br>';
                echo '&nbsp;&nbsp;';
                // echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';
                ?>

            <?php
                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';


                //@Abraham
                echo '<div class="row">';
                echo '<div class="col-sm-12">';
                echo '<div class="card-box">';
                echo '<div class="card-head">';
                echo  '<header>Autres photos </header>';
                echo '<button id="panel-button" class="mdl-button mdl-js-button mdl-button--icon pull-right" data-upgraded=",MaterialButton"><i class="material-icons">more_vert</i></button>';

                echo '</div>';

                echo '<div class="row p-b-20 mb-10">';
                echo '<div class="col-md-6 col-sm-6 col-6">';
                echo '<div class="btn-group">';

                // echo '<a href="#" class="btn btn-info"><i class="fa fa-plus"></i> Ajouter des photos</a>';
                // echo '&nbsp; &nbsp;';
                // echo '<label for="" class="pull-left" id="label-img-names"></label>';

                echo '</div>';
                echo '</div>';
                echo '</div>';
                 ?>
                <center>
                <form action="savetopic.php" method="POST" enctype="multipart/form-data">
                <!-- <label for="files" class="profile-usertitle-name">Selectionner des images :</label> -->
                <input type="file"  id="files" name="files[]" multiple  class="" style="font-size:20px"><br><br><br>
                 <!-- <input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 ml-4 btn-circle btn-primary" value="Valider"> -->
                 
                </center>
                <?php
                echo '<div class="card-body row">';
                echo '<div id="aniimated-thumbnials" class="list-unstyled row clearfix">';

                // echo '<div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20"> <a href="media/files/image-test1.jpg" data-href="savetopic.php?option=delete&id=image-test1.jpg" data-sub-html="Demo Description"><img class="img-fluid img-thumbnail" src="media/files/image-test1.jpg" alt="" > </a> </div>';
                // echo '<div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20"> <a href="media/files/image-test1.jpg" data-href="savetopic.php?option=delete&id=image-test1.jpg" data-sub-html="Demo Description"><img class="img-fluid img-thumbnail" src="media/files/image-test1.jpg" alt="" > </a> </div>';

                echo '</div>';
                echo '</div>';
                echo '&nbsp;&nbsp;';
               
                echo '</div>';
                echo '</div>';
                echo'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;';
                echo '<center><input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 ml-4 btn-circle btn-primary" value="Enregistrer" style="margin-right:50px"></center>';
                echo '</div>';
                //@Abraham
                echo'</form>';
                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->
                echo '</div>';
            } //end if
            ?>


            <?php
            if (isset($_GET['option']) && $_GET['option'] == 'edit' && isset($_GET['pid'])) {
                $pid = $_GET['pid'];
                echo '<input name="topicid" type="hidden" value="' . $pid . '" >';
                $found_topic = $topic->find_by_id($pid);


                echo '<div class="row">';
                echo '<div class="col-md-12">';
                //<!-- BEGIN PROFILE SIDEBAR -->
                echo '<div class="profile-sidebar">';
                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Attributs de la page </div>';
                echo '<div class="profile-usertitle-job"> de la page ' . $found_topic->topic_title . ' </div>';
                echo '</div>';
                echo '<div class="card-body no-padding height-9">';

                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b>Etat </b>';
                echo '<div class="profile-desc-item pull-right">';
                if ($found_topic->topic_active == 1) {
                    echo '<i class="fa fa-eye"></i> publi&eacute;';
                } else {
                    echo '<i class="fa fa-eye-slash"></i> non publi&eacute;e';
                }

                echo '</div>';
                echo '</li>';

                echo '<li class="list-group-item">';
                echo '<b>&Agrave; la une </b>';
                echo '<div class="profile-desc-item pull-right">';
                if ($found_topic->topic_top_slider == 1) {
                    echo '<input id="rememberChk1" name="breakingnew" type="checkbox" checked="checked"> oui';
                } else {
                    echo '<input id="rememberChk1" name="breakingnew" type="checkbox">';
                }

                echo '</div>';
                echo '</li>';

                echo '<li class="list-group-item">';
                echo '<b>Choix de la rédaction </b>';
                echo '<div class="profile-desc-item pull-right">';
                if ($found_topic->topic_top_small == 1) {
                    echo '<input id="rememberChk1" name="breakingnew1" type="checkbox" checked="checked"> oui';
                } else {
                    echo '<input id="rememberChk1" name="breakingnew1" type="checkbox">';
                }

                echo '</div>';
                echo '</li>';


                echo '<li class="list-group-item">';
                echo '<b>Pour les abonnés </b>';
                echo '<div class="profile-desc-item pull-right">';
                if ($found_topic->topic_statut == 1) {
                    echo '<input id="rememberChk2" name="breakingnew2" type="checkbox" checked="checked"> oui';
                } else {
                    echo '<input id="rememberChk2" name="breakingnew2" type="checkbox">';
                }

                echo '</div>';
                echo '</li>';


                echo '<li class="list-group-item">';
                echo '<b>publi&eacute; le </b>';
                echo '<div class="profile-desc-item pull-right">';
                echo '<i class="fa fa-clock-o"> </i> ' . $found_topic->topic_date_creation . ' ';
                echo '</div>';
                echo '</li>';

                echo '<li class="list-group-item btn-sweetalert">';
                if ($found_topic->topic_active == 1) {

                    echo '<b>D&eacute;placer dans la corbeille </b>';
                    echo '<button class="btn btn-small btn-danger show-tooltip btn-sweetalert" type="button" data-type="ajax-loader-article-delete" data-id="'.$pid.'"><i class="fa fa-trash-o "></i></button>';
                } else {
                    echo '<b>Restaurer cette page</b>';
                    echo '<button class="btn btn-small btn-warning show-tooltip btn-sweetalert" type="button" data-type="ajax-loader-article-restor" data-id="'.$pid.'"><i class="fa  fa-recycle "></i></button>';
                }

                echo '</li>';


                echo '</ul><br/>';
                echo '</div>';
                echo '</div>';

                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Photo de couverture </div>';
                echo '</div>';
                echo '<div id="displayed" class="card-body no-padding height-9">';
                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<div class="profile-desc-item pull-left">';
                echo '<input type="file" name="file_upload">';
                echo '</div>';
                $galeries= explode(",", $found_topic->topic_image_path, 2);
                echo '<img src="media/files/' . $galeries[0]. '" width="200"/>';
                echo '</li>';
                echo '</ul><br/>';
                echo '</div>';
                echo '</div>';

                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Antidater l\'article </div>';
                echo '</div>';
                echo '<div id="displayed" class="card-body no-padding height-9">';
                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<div class="profile-desc-item pull-left">';
                echo '<input type="date" name="date_creation" value="' . $found_topic->topic_date_creation . '">';
                echo '</div>';
                echo '</li>';
                echo '</ul><br/>';
                echo '</div>';
                echo '</div>';

                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Alignement du menu</div>';
                echo '</div>';
                echo '<li class="list-group-item">';
                //echo '<b>Section de page</b> <a class="pull-right"></a>';
                $found_topic_sections = Topic_section::find_by_topic($pid);
                if (!empty($found_topic_sections)) {
                    foreach ($found_topic_sections as $found_topic_section) {
                        if (!empty($found_topic_section)) {
                            $found_section = Section::find_by_id($found_topic_section->topic_section_id);
                            echo '<input id="checkboxbg4" type="checkbox" checked="checked"> ' . $found_section->section_desc . '<br/>';
                        }
                    }
                } //end if

                echo ' <select id="multiple" name="section_id[]" class="form-control select2-multiple" multiple>';

                $found_sections = Section::find_elements_alignment();
                foreach ($found_sections as $found_section) {
                    echo '<option value="' . $found_section->section_id . '">' . $found_section->section_desc . '</option>';
                }
                echo '</select>';
                echo '</li>';
                echo '</div>';



                echo '<div class="card card-topline-aqua">';
                echo '<div class="card-body no-padding height-9">';
                echo '<div class="row">';
                echo '<div class="profile-userpic">';
                echo '</div>';
                echo '</div>';

                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b> Cat&eacute;gories </b> <a class="pull-right"></a>';
                echo '<div class="form-group">';


                echo '<select name="typetopic" id="parent" class="form-control" >';
                if (isset($pid)) {

                    $found_typetopic = Type_topic::find_by_id($found_topic->topic_typetopic_id);
                    echo '<option value="' . $found_typetopic->type_topic_id . '">' . $found_typetopic->type_topic_desc . '</option>';

                    $found_typetopics = Type_topic::list_others($found_topic->topic_typetopic_id);
                    foreach ($found_typetopics as $found_typetopic) {
                        echo '<option value="' . $found_typetopic->type_topic_id . '">' . $found_typetopic->type_topic_desc . '</option>';
                    } // end  foreach
                } // end  if
                else {

                    $found_typetopics = Type_topic::find_all();
                    foreach ($found_typetopics as $found_typetopic) {
                        echo '<option value="' . $found_typetopic->type_topic_id . '">' . $found_typetopic->type_topic_desc . '</option>';
                    } // end  foreach
                } // end  else


                echo '</select>';


                echo '</div>';
                echo '</li>';

                echo '<div id="children">';
                echo '</div>';


                echo '<li class="list-group-item">';
                echo '<b>Etiquettes </b> <a class="pull-right"></a> <br/>';

                $found_tags = $press_tag->find_by_topic_id($pid);
                foreach ($found_tags as $found_tag) {
                    $found_etiquette = $etiquette->find_by_id($found_tag->press_tag_etiquette_id);
                    echo '<strong>' . $found_etiquette->etiquette_desc . ',  </strong>';
                } // end  foreach

                echo '<select name="etiquette[]" id="multiple" class="form-control select2-multiple" multiple="multiple">';
                echo '<option value="1">S&eacute;lectionner une &eacute;tiquette</option>';
                $found_tags = $press_tag->find_by_topic_id($pid);
                if (empty($found_tags)) {
                    $found_etiquettes = $etiquette->find_all();
                    foreach ($found_etiquettes as $found_etiquette) {
                        echo '<option value="' . $found_etiquette->etiquette_id . '">' . $found_etiquette->etiquette_desc . '</option>';
                    } // end  foreach
                } else {
                    foreach ($found_tags as $found_tag) {
                        $found_etiquettes = $etiquette->list_others($found_tag->press_tag_etiquette_id);
                        foreach ($found_etiquettes as $found_etiquette) {
                            echo '<option value="' . $found_etiquette->etiquette_id . '">' . $found_etiquette->etiquette_desc . '</option>';
                        } // end  foreach
                    } // end  foreach
                } // emd else
                echo '</optgroup>';
                echo '</select>';

                echo '</li>';

                echo '</ul>';




                echo '<div class="controls">';
                echo '<div class="form-group">';
                echo '<div class="checkbox checkbox-icon-black">';
                $found_topic = $topic->find_by_id($pid);
                if ($found_topic->topic_active == 1) {
                    echo '<input id="rememberChk1" name="active" type="checkbox" checked="checked"> publi&eacute;';
                } else {
                    echo '<input id="rememberChk1" name="active" type="checkbox"> non publi&eacute;e';
                }

                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END SIDEBAR BUTTONS -->
                echo '</div>';
                echo '</div>';

                echo '</div>';


                //<!-- END BEGIN PROFILE SIDEBAR -->
                //<!-- BEGIN PROFILE CONTENT -->

                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';
                echo '<h4> Modifier l\'article : ' . $found_topic->topic_title . ' </h4>';

                if (isset($_GET['r']) && $_GET['r'] == 0) {
                    echo '<span class="clsNotAvailable"> Cette page est plac&eacute;e dans la corbeille. </span>';
                } else if (isset($_GET['r']) && $_GET['r'] == 1) {
                    echo '<span class="clsAvailable"> Cette page a &eacute;t&eacute; restaur&eacute;e. </span>';
                }
                if(!empty($found_topic->topic_url)){
                    $url=$found_topic->topic_url;
                }else{
                    $url='u';
                }  
                $found_topic_type = Type_topic::find_by_id($found_topic->topic_typetopic_id); //type topic correspondant     
                echo '<div class="btn-group">';
                echo '<a href="../singlepost-' . $url . '-' . $found_topic_type->type_topic_id  . '-' . $found_topic->topic_id . '" id="addRow" class="btn btn-info" target="_blank">Aperçu de l\'article <i class="fa fa-eye"></i>';
                echo '</a>';
                echo '</div>';


                echo '</div>';
                echo '</div>';


                echo '<div class="white-box">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';


                echo '<div class="form-group">';
                echo '<label for="subject" class="">Titre de l\'article :</label>';
                echo '<input name="title" id="title" value="' . $found_topic->topic_title . '"  type="text" tabindex="1" id="subject" class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Contenu de  l\'article :</label>';
                echo '<textarea name="topic_text" id="description" class="span12" rows="8">' . $found_topic->topic_text . '</textarea>';

                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Mots clés de l\'article:</label>';
                echo '<input name="icon" value="' . $found_topic->topic_icon . '" type="text" tabindex="1" id="subject" class="form-control">';
                echo '</div>';


                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '<br>';
                echo '&nbsp;&nbsp;';
                echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';

                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //@Abraham
                echo '<div class="row">';
                echo '<div class="col-sm-12">';
                echo '<div class="card-box">';
                echo '<div class="card-head">';
                echo  '<header>Autres photos </header>';
                echo '<button id="panel-button" class="mdl-button mdl-js-button mdl-button--icon pull-right" data-upgraded=",MaterialButton"><i class="material-icons">more_vert</i></button>';

                echo '</div>';

                echo '<div class="row p-b-20 mb-10">';
                echo '<div class="col-md-6 col-sm-6 col-6">';
                echo '<div class="btn-group">';

                // echo '<a href="" class="btn btn-info"> Ajouter des photos</a>';
                // echo '&nbsp; &nbsp;';
                // echo '<label for="" class="pull-left" id="label-img-names"></label>';

                echo '</div>';
                echo '</div>';
                echo '</div>';

                ?>
                <center>
                <form action="savetopic.php" method="POST" enctype="multipart/form-data">
                <?php
                  $urlr = 'https://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                ?>
                 <input type="hidden" id="urlr"  name="urlr"  value="<?php echo $urlr ?>">
                <input name="topicid" type="hidden" value="<?php echo $pid ?>">
                <!-- <label for="files" class="profile-usertitle-name">Selectionner des images :</label> -->
                <input type="file"  id="files" name="files[]" multiple  class="" style="font-size:20px"><br><br><br>
                 <input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 ml-4 btn-circle btn-primary" value="Valider">
                 </form>
                </center>
                </br><br>
               <?php
                echo '<div class="card-body row">';
                echo '<div idd="aniimated-thumbnials" class="list-unstyled row clearfix">';
                $galeries= explode(",", $found_topic->topic_image_path);
                foreach($galeries as $key => $gal){
                    if($key!=0){
                    echo '<div class="col-xl-3 col-lg-4 col-md-6 col-sm-12 m-b-20">
                     <a href="media/files/' .  $gal . '" data-href="savetopic.php?option=delete&id=' .  $gal . '" data-sub-html="Demo Description"><img class="img-fluid img-thumbnail" src="media/files/' .  $gal . '" alt=""> </a> 
                    <center> <a id="delete' . $key . '" onclick="return deleteImage(' . $key. ')" href="savetopic.php?pid=' . $pid . '&indice='.$key.'&option=deleteimage"><p style="color:red"> <i class="fa fa-trash-o "></i></p></a> </center>
                     </div>';
                    }
                }
                echo '</div>';
                echo '</div>';
                // echo '&nbsp;&nbsp;';
                // echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 ml-4 btn-circle btn-primary" value="valider">';
                echo '</div>';
                echo '</div>';
                echo '</div>';


                //@Abraham

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->

                echo '</div>';
            } // end if edit option
            ?>
        </form>

        <!-- end page container -->


        <!-- start page content -->

        <!-- delete confirm -->
        <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
        <script>
            function deleteImage(id) {
                event.preventDefault();
                Swal.fire({
                    title: 'Voulez-vous vraiment supprimer?',
                    text: "",
                    icon: 'warning',
                    showCancelButton: true,
                    closeOnConfirm: false,
                    showLoaderOnConfirm: true,

                    // showCancelButton: true,
                    // confirmButtonColor: '#D63030',
                    // cancelButtonColor: '#797979E3',
                    confirmButtonText: 'Oui, Supprimer',
                    cancelButtonText: 'Annuler'
                }).then((result) => {
                    if (result.isConfirmed) {
                        var href = $('#delete' + id).attr('href');
                        window.location.href = href;
                        Swal.fire(
                            'Image supprimée!',
                            'Supprimer avec succès!.',
                            'success'
                        )
                    }

                })
            }
        </script>
