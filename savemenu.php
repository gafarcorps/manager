<?php

require_once("includes/initialize.php");


if (isset($_POST['mid'])) {
    $mid = $_POST['mid'];
} //$mid is the id of current menu

if (isset($_POST['parent_id'])) {
    $parent_id = $_POST['parent_id'];
}
if (empty($parent_id)) {
    $parent_id = 11;
}

if (isset($_POST['menu_title'])) {
    $menu_title = $_POST['menu_title'];
}

if (isset($_POST['menu_title_en'])) {
    $menu_title_en = $_POST['menu_title_en'];
}

if (isset($_POST['menu_title_es'])) {
    $menu_title_es = $_POST['menu_title_es'];
}

if (isset($_POST['menu_desc'])) {
    $menu_desc = $_POST['menu_desc'];
}
if (isset($_POST['menu_desc_infos'])) {
    $menu_desc_infos = $_POST['menu_desc_infos'];
}
if (isset($_POST['menu_url'])) {
    $menu_link = $_POST['menu_url'];
}
if (isset($_POST['position'])) {
    $menu_position = $_POST['position'];
}

if (isset($_POST['menu_active'])) {
    $menu_active = $_POST['menu_active'];
}

if (isset($_POST['is_extension'])) {
    $menu_extension = $_POST['is_extension'];
}

if (isset($_POST['is_star_extension'])) {
    $menu_star_extension = $_POST['is_star_extension'];
}

if (!empty($menu_extension)) {
    $menu_extension = 1;
} else {
    $menu_extension = 0;
}

if (!empty($menu_star_extension)) {
    $menu_star_extension = 1;
} else {
    $menu_star_extension = 0;
}


if (!empty($_FILES['file_upload']['name'])) {
    $file_upload = basename($_FILES['file_upload']['name']);
}


if (!empty($menu_active)) {
    $menu_active = 1;
} else {
    $menu_active = 0;
}

echo '>>'.$menu_active;
$menu = new Menu();

if (isset($_POST['submit']) && empty($mid) && !empty($session->user_account_id)) {    // New menu
    // Fonctionnel ... this
    $menu_date = date('Y-m-d H:i:s');
    $repository = 'media/files/';
    $menu->menu_parent_id = $parent_id;
    $menu->menu_title = $menu_title;
    $menu->menu_title_en = $menu_title_en;
    $menu->menu_title_es = $menu_title_es;
    $menu->menu_desc = $menu_desc;
    $menu->menu_desc_infos = $menu_desc_infos;
    $menu->menu_link = $menu_link;
    $menu->menu_active = $menu_active;
    $menu->menu_position = $menu_position;
    $menu->is_extension = $menu_extension;
    $menu->menu_picture = $file_upload;
    $menu->menu_extension_vedette = $menu_star_extension;
    $menu_created_at = date('Y-m-d H:i:s');
    $menu->menu_created_at = $menu_created_at;

    if (isset($_FILES['file_upload'])) {
        move_uploaded_file($_FILES['file_upload']['tmp_name'], $repository . $file_upload);
        $menu->menu_picture = $file_upload;
    }

    $menu->save();
    $found_last_menu = Menu::find_last();
    $found_typeusers = Typeuser::find_all_active();
    foreach ($found_typeusers as $found_typeuser) {
        $privilege = new Privileges();
        $privilege->privilege_type_user_id = $found_typeuser->type_user_id;
        $privilege->privilege_menu_id = $found_last_menu->menu_id;
        $privilege->privilege_active = ($found_typeuser->type_user_id == 1) ? 1 : 0;
        $privilege->save();
    }
    if (!empty($menu->is_extension)) {
        redirect_to('app.php?item=listextensions&msg=success');
    } else {
        redirect_to('app.php?item=listmenus&msg=success');
    }
}


if (isset($_POST['new']) && empty($mid) && !empty($session->user_account_id)) {    // New menu menu
    // This a real menu
    $menu_date = date('Y-m-d H:i:s');
    $repository = 'media/files/';
    $menu->menu_parent_id = $parent_id;
    $menu->menu_desc = $menu_desc;
    $menu->menu_desc_infos = $menu_desc_infos;
    $menu->menu_title = $menu_title;
    $menu->menu_title_en = $menu_title_en;
    $menu->menu_title_es = $menu_title_es;
    $menu->menu_link = $menu_link;
    $menu->menu_active = $menu_active;
    $menu->menu_position = $menu_position;
    $menu->menu_is_extension = $menu_extension;
    $menu->menu_picture = $file_upload;
    $menu->menu_extension_vedette = $menu_star_extension;
    $menu_created_at = date('Y-m-d H:i:s');
    $menu->menu_created_at = $menu_created_at;

    if (isset($_FILES['file_upload'])) {
        move_uploaded_file($_FILES['file_upload']['tmp_name'], $repository . $file_upload);
        $menu->menu_picture = $file_upload;
    }

    $menu->save();
    $found_last_menu = Menu::find_last();
    $found_typeusers = Typeuser::find_all_active();
    foreach ($found_typeusers as $found_typeuser) {
        $privilege = new Privileges();
        $privilege->privilege_type_user_id = $found_typeuser->type_user_id;
        $privilege->privilege_menu_id = $found_last_menu->menu_id;
        $privilege->privilege_active = ($found_typeuser->type_user_id == 1) ? 1 : 0;
        $privilege->save();
    }
    if (!empty($menu->menu_is_extension)) {
        redirect_to('app.php?item=listextensions&mid=' . $menu->menu_id . '&msg=success');
    } else {
        redirect_to('app.php?item=listmenus&msg=success');
    }
} // END IF NEW ENTRY


if (isset($_POST['submit']) && !empty($mid) && !empty($session->user_account_id)) {    // Updating a menu
    $found_menu = Menu::find_by_id($mid);

    $menu->menu_id = $found_menu->menu_id;

    if (!empty($parent_id)) { // UPDATE IF THERE ANY POSTED PARENT_ID VARIABLE
        $menu->menu_parent_id = $parent_id;
    } else {  // REPLACE THE ASSET VALUE
        $menu->menu_parent_id = $found_menu->menu_parent_id;
    }


    if (!empty($menu_title)) {  // UPDATE IF THERE ANY POSTED TITLE VARIABLE
        $menu->menu_title = $menu_title;
    } else { // REPLACE THE ASSET VALUE
        $menu->menu_title = $found_menu->menu_title;
    }

    if (!empty($menu_title_en)) {  // UPDATE IF THERE ANY POSTED TITLE VARIABLE
        $menu->menu_title_en = $menu_title_en;
    } else { // REPLACE THE ASSET VALUE
        $menu->menu_title_en = $found_menu->menu_title_en;
    }

    if (!empty($menu_title_es)) {  // UPDATE IF THERE ANY POSTED TITLE VARIABLE
        $menu->menu_title_es = $menu_title_es;
    } else { // REPLACE THE ASSET VALUE
        $menu->menu_title_es = $found_menu->menu_title_es;
    }


    if (!empty($menu_desc)) {  // UPDATE IF THERE ANY POSTED DESC VARIABLE
        $menu->menu_desc = $menu_desc;
    } else { // REPLACE THE ASSET VALUE
        $menu->menu_desc = $found_menu->menu_desc;
    }



    if (!empty($menu_link)) { // UPDATE IF THERE ANY POSTED LINK VARIABLE
        $menu->menu_link = $menu_link;
    } else { // REPLACE THE ASSET VALUE
        $menu->menu_link = $found_menu->menu_link;
    }

    if (!empty($menu_position)) { // UPDATE IF THERE ANY POSTED ACTIVE VARIABLE
        $menu->menu_position = $menu_position;
    } else { // REPLACE THE ASSET VALUE
        $menu->menu_position = $found_menu->menu_position;
    }

    if (!empty($menu_extension)) {
        $menu->menu_is_extension = 1;
    } else {
        $menu->menu_is_extension = 0;
    }

    if (!empty($menu_star_extension)) {
        $menu->menu_extension_vedette = 1;
    } else {
        $menu->menu_extension_vedette = 0;
    }

    $repository = 'media/files/';
    if (isset($file_upload)) {
        move_uploaded_file($_FILES['file_upload']['tmp_name'], $repository . $file_upload);
        $menu->menu_picture = $file_upload;
    } else {
        $menu->menu_picture = $found_menu->menu_picture;
    }

    if (!empty($menu_active)) {
        $menu->menu_active = 1;
    } else {
        $menu->menu_active = 0;
    }

    $menu->menu_active = $menu_active;
    $menu->save();

    if (!empty($menu_extension)) {
    redirect_to('app.php?item=listmenus&mid=' . $menu->menu_parent_id . '&msg=success');
    } else {
        redirect_to('app.php?item=listmenus&msg=success');
    }
}  // END IF UPDATE


if (!empty($_GET['mid']) && !empty($session->user_account_id) && $_GET['option'] == 'delete') {  // DELETE ENTRIE
    $mid = $_GET['mid'];
    $menu = $menu->find_by_id($mid);
    $del = $menu->unable();
    redirect_to('app.php?item=formmenu&mid=' . $mid . '&option=edit&r=0');
}  // END IF DELETE

if (!empty($_GET['mid']) && !empty($session->user_account_id) && $_GET['option'] == 'recover') {  // DELETE ENTRIE
    $mid = $_GET['mid'];
    $menu = $menu->find_by_id($mid);
    $del = $menu->recover();
    redirect_to('app.php?item=formmenu&mid=' . $mid . '&option=edit&r=1');
}  // END IF DELETE
if (!empty($_GET['mid']) && !empty($session->user_account_id) && $_GET['option'] == 'new_automatic_menu') {  // DELETE ENTRIE
    $mid = $_GET['mid'];
    $found_menu = $menu->find_by_id($mid);
    $menu->menu_parent_id = 11;
    $menu->menu_title =  $found_menu->menu_title;
    $menu->menu_title_en =  $found_menu->menu_title_en;
    $menu->menu_title_es =  $found_menu->menu_title_es;
    $menu->menu_desc = $found_menu->menu_desc;
    $menu->menu_link =  $found_menu->menu_link;
    $menu->menu_active = 1;
    $menu->menu_position = 10;
    
    $menu_created_at = date('Y-m-d H:i:s');
    $menu->menu_created_at = $menu_created_at;

    $menu->save();
    $found_last_menu = Menu::find_last();
    $found_typeusers = Typeuser::find_all_active();
    foreach ($found_typeusers as $found_typeuser) {
        $privilege = new Privileges();
        $privilege->privilege_type_user_id = $found_typeuser->type_user_id;
        $privilege->privilege_menu_id = $found_last_menu->menu_id;
        $privilege->privilege_active = ($found_typeuser->type_user_id == 1) ? 1 : 0;
        $privilege->save();
    }
    if (!empty($menu->menu_is_extension)) {
        redirect_to('app.php?item=listextensions&mid=' . $menu->menu_id . '&msg=success');
    } else {
        redirect_to('app.php?item=listmenus&msg=success');
    }
}  // END IF DELETE
?>
 