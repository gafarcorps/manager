
		 	<!-- start page content -->
			<div class="page-content-wrapper">
				<div class="page-content">
					<div class="page-bar">
						<div class="page-title-breadcrumb">
							<div class=" pull-left">
								<div class="page-title">Bienvenue dans Manager</div>
							</div>
							<ol class="breadcrumb page-breadcrumb pull-right">
								<li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
										href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li><a class="parent-item" href="#">Section</a>&nbsp;<i class="fa fa-angle-right"></i>
								</li>
								<li class="active">Nouvelle section</li>
							</ol>
						</div>
					</div>

	 <form action="savesection.php" class="form-horizontal" method="POST" enctype="multipart/form-data"> 
		
			  <?php 
	     				if(isset($_GET['option']) && $_GET['option'] == 'new'){
						echo '<div class="row">';
						echo '<div class="col-md-12">';
							//<!-- BEGIN PROFILE SIDEBAR -->
								echo '<div class="profile-sidebar">';
									echo '<div class="card">';
												echo '<div class="profile-usertitle">';
												echo '<div class="profile-usertitle-name"> Attributs </div>';
												echo '<div class="profile-usertitle-job"> de la nouvelle section </div>';
												echo '</div>';
												
											
												echo '<div id="displayed" class="card-body no-padding height-9">';										 
												echo '<ul class="list-group list-group-unbordered">';
												echo '<li class="list-group-item">';
												echo '<b>Etat </b>';
												echo '<div class="profile-desc-item pull-right">';
												echo '<i class="fa fa-eye"></i> ';
										    	echo '</div>';
												echo '</li>';

												echo '<li class="list-group-item">';
												echo '<b>Date de création</b>';
												echo '<div class="profile-desc-item pull-right">';
												echo '<i class="fa fa-clock-o"> </i> '.date('Y-m-d h:i:s').' ';
												echo '</div>';
												echo '</li>';
												
												echo 'Activer cette section?';
												echo '<div class="controls">';
												echo '<div class="form-group">';
												echo '<div class="checkbox checkbox-icon-black">';
												echo '<input id="rememberChk1" name="section_active" type="checkbox" checked="checked">';
											echo '</div>';
												 
											echo '</ul><br/>';
										 	 						 
										echo '</div>'; 
									
									 
									echo '</div>';

 
							 echo '</div>';

					
							//<!-- END BEGIN PROFILE SIDEBAR -->
						
							//<!-- BEGIN PROFILE CONTENT -->
						 	echo '<div class="profile-content">';
								echo '<div class="row">';
									echo '<div class="profile-tab-box">';
										echo '<div class="p-l-20">';
										     echo '<h4> Créer une nouvelle section </h4>';
							    		echo '</div>';
									echo '</div>';
									echo '<div class="white-box">';
										//<!-- Tab panes -->
										echo '<div class="tab-content">';
											echo '<div class="tab-pane active fontawesome-demo" id="tab1">';
											
 												echo '<div class="inbox-body no-pad">';
														echo '<div class="mail-list">';
															echo '<div class="compose-mail">'; 
																	echo '<div class="form-group">';
																	echo '<label for="subject" class="">Titre de la section:</label>';
																	echo '<input name="section_desc" type="text" tabindex="1" id="subject" class="form-control">';
																	echo '</div>';  		
														    	echo '</div>';
														    echo '</div>';

													echo '</div>';
													echo '<br>'; 
													echo '&nbsp;&nbsp;'; 
												    echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';
							      				    
																		 
												  echo '</div>';
											echo '</div>';
								  echo '</div>';
							echo '</div>';
											 
							//<!-- END PROFILE CONTENT -->
						echo '</div>';
					echo '</div>';
				echo '</div>';
				//<!-- end page content -->
			echo '</div>';
						
									
	} //end if
?>
 

            <?php 
					if(isset($_GET['option']) && $_GET['option'] == 'edit' && isset($_GET['sid'])) {

						if(isset($_GET['sid'])) { $sid  = $_GET['sid'];   }								 	
						 
						 echo '<input type="hidden" name="sid" value="'.$sid.'" />';

						 $found_section = $section->find_by_id($sid);	
										 	
				      
				    	echo '<div class="row">';
						echo '<div class="col-md-12">';
							//<!-- BEGIN PROFILE SIDEBAR -->
								echo '<div class="profile-sidebar">';
									echo '<div class="card">';
										echo '<div class="profile-usertitle">';
										echo '<div class="profile-usertitle-name"> Attributs de la section </div>';
										echo '<div class="profile-usertitle-job"> de la page '.$found_section->section_active.' </div>';
										echo '</div>';
										echo '<div class="card-body no-padding height-9">';
											
											echo '<ul class="list-group list-group-unbordered">';
												echo '<li class="list-group-item">';
												echo '<b>Etat </b>';
												echo '<div class="profile-desc-item pull-right">';
												if($found_section->section_active==1) {
													echo '<i class="fa fa-eye"></i> activé';
												 } 
												else { echo '<i class="fa fa-eye-slash"></i> désactivé';
												}
												
										    	echo '</div>';
												echo '</li>';
 
										      
										      	echo '<li class="list-group-item">';
										        if($found_section->section_active==1) {
													
														echo '<b>Déplacer dans la corbeille </b>'; 
														echo '<div class="profile-desc-item pull-right"> <a href="savesection.php?sid='.$sid.'&option=delete"><i class="fa fa-trash-o "></i></a> </div>';
												}
												else {
														echo '<b>Restaurer cette page</b>'; 
														echo '<div class="profile-desc-item pull-right"> <a href="savesection.php?sid='.$sid.'&option=recover"><i class="fa  fa-recycle "></i></a> </div>';	
												} 
												
												echo '</li>';
												 
											echo '</ul><br/>';
										 										 
									
										 
											echo '<div class="controls">';
												echo '<div class="form-group">';
												echo '<div class="checkbox checkbox-icon-black">';  
														$found_section  = $section->find_by_id($sid);	
														if($found_section->section_active==1) {
															echo '<input id="rememberChk1" name="section_active" type="checkbox" checked="checked"> activé';
														 } 
														else { echo '<input id="rememberChk1" name="section_active" type="checkbox"> désactivé';
														}
											
								     			echo '</div>';
										 
										//<!-- END SIDEBAR BUTTONS -->
									echo '</div>';
								echo '</div>'; 
							echo '</div>'; 
				
					echo '</div>';
			   echo '</div>';


							//<!-- END BEGIN PROFILE SIDEBAR -->
							//<!-- BEGIN PROFILE CONTENT -->
				
							echo '<div class="profile-content">';
								echo '<div class="row">';
									echo '<div class="profile-tab-box">';
										echo '<div class="p-l-20">';
							      				echo '<h4> Modifier la section : '.$found_section->section_desc.' </h4>';

							      				if(isset($_GET['r']) && $_GET['r']==0) {
							      					echo '<span class="clsNotAvailable"> Cette section a été déplacée dans la corbeille. </span> <br/>';
							      				}
							      				else if(isset($_GET['r']) && $_GET['r']==1) {
							      					 echo '<span class="clsAvailable"> Cette section a été restaurée. </span> <br/>';
							      				}


									 
											echo '<div class="btn-group">'; 
													echo '<a href="app.php?item=formsection&sid='.$sid.'&option=new" id="addRow" class="btn btn-info">Nouvelle section <i class="fa fa-plus"></i></a>';  
											echo '</div>';
									 

										echo '</div>';
									echo '</div>';


									echo '<div class="white-box">';
										//<!-- Tab panes -->
										echo '<div class="tab-content">';
											echo '<div class="tab-pane active fontawesome-demo" id="tab1">';
											
 												echo '<div class="inbox-body no-pad">';
														echo '<div class="mail-list">';
															echo '<div class="compose-mail">';
															
																 
																	echo '<div class="form-group">';
																		echo '<label for="subject" class="">Titre du section:</label>';
																		echo '<input name="section_desc" id="title" value="'.$found_section->section_desc.'"  type="text" tabindex="1" id="subject"	class="form-control">';
																	echo '</div>'; 

															echo '</div>';
														echo '</div>';
													echo '</div>';
													echo '<br>'; 
													echo '&nbsp;&nbsp;'; 
													echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';
																		 
												  echo '</div>';
											echo '</div>';
								  echo '</div>';
							echo '</div>';
							 	 
							//<!-- END PROFILE CONTENT -->
						echo '</div>';
					echo '</div>';
				echo '</div>';
				//<!-- end page content -->
			  
			echo '</div>';
 
			  } // end if

									 


			?>
 

 </form>


<!-- end page container -->
			
 