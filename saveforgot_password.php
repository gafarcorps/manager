<?php
require_once("includes/initialize.php");
if (isset($_POST['email'])) {
    
    $email = $_POST['email'];
    $found_user_account = User_account::find_by_email($email);
}

if (empty($found_user_account)) {
    redirect_to('forgot_password.php?msg=error');
} else {
    $found_copyright = Copyright::find_by_id(1);
    
    $title = "Manager " . $found_copyright->copyright_username . " | Réinitialisation de mot de passe";
    $desc = "Vous avez demandé a réinitialiser votre mot de passe sur le Manager " . $found_copyright->mention_legale_nom . ".";
//    $desc .= "Pour le faire, il suffit de cliquer sur le lien suivant: " . $found_mention->mention_legale_website_manager_link . "/reinitialize_password.php?user_account_id=" . $found_user_account->user_account_id;
    $desc .= "Pour le faire, il suffit de cliquer sur le lien suivant: " . $found_copyright->copyright_manager_url . "/reinitialize_password.php?user_account_id=" . $found_user_account->user_account_id;
    $headers = $found_copyright->copyright_username;

    $mailok = mail($email, $title, $desc, $headers);
    
    redirect_to('forgot_password.php?msg=good&email=' . $email);
}
?>
 