<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                            href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="app.php?item=listcategories">Gestionnaire des
                            catégories</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Mentions légales </li>
                </ol>
            </div>
        </div>


        <form action="savecopyright.php" class="form-horizontal" method="POST" enctype="multipart/form-data">

            <?php
$found_copyright = Copyright::find_by_id(1);

//<!-- BEGIN PROFILE CONTENT -->

echo '<div class="profile-content">';
echo '<div class="row">';

echo '<div class="profile-tab-box">';
echo '<div class="p-l-20">';
echo '<h4> Modifier le Copyright: ' . $found_copyright->copyright_username . ' </h4>';
echo '</div>';
echo '</div>';

echo '<div class="white-box">';
//<!-- Tab panes -->
echo '<div class="tab-content">';
echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

echo '<div class="inbox-body no-pad">';
echo '<div class="mail-list">';
echo '<div class="compose-mail">';

echo '<div class="form-group">';
echo '<label for="subject" class="">Nom:</label>';
echo '<input name="nom" id="title" value="' . $found_copyright->copyright_username . '"  type="text" tabindex="1" id="subject"	class="form-control">';
echo '</div>';

echo '<div class="form-group">';
echo '<label for="subject" class="">Numéro togocel:</label>';
echo '<input name="tel1" id="title" value="' . $found_copyright->copyright_tel1 . '"  type="text" tabindex="1" id="subject"	class="form-control">';
echo '</div>';

echo '<div class="form-group">';
echo '<label for="subject" class="">Numéro moov:</label>';
echo '<input name="tel2" id="title" value="' . $found_copyright->copyright_tel2 . '"  type="text" tabindex="1" id="subject"	class="form-control">';
echo '</div>';

echo '<div class="form-group">';
echo '<label for="subject" class="">Email:</label>';
echo '<input name="email" id="title" value="' . $found_copyright->copyright_email . '"  type="text" tabindex="1" id="subject"	class="form-control">';
echo '</div>';

echo '<div class="form-group">';
echo '<label for="subject" class="">Adresse:</label>';
echo '<input name="address" id="title" value="' . $found_copyright->copyright_address . '"  type="text" tabindex="1" id="subject"	class="form-control">';
echo '</div>';

echo '<div class="form-group">';
echo '<label for="subject" class="">Copyright manager url:</label>';
echo '<input name="copyright_manager_url" id="title" value="' . $found_copyright->copyright_manager_url . '"  type="text" tabindex="1" id="subject"	class="form-control">';
echo '</div>';

echo '<div class="form-group">';
echo '<label for="subject" class="">Copyright web site url:</label>';
echo '<input name="copyright_website_url" id="title" value="' . $found_copyright->copyright_website_url . '"  type="text" tabindex="1" id="subject"	class="form-control">';
echo '</div>';

echo '<div class="form-group">';
echo '<label for="subject" class="">Logo:</label>';
echo '<input name="logo" id="title" value="' . $found_copyright->copyright_logo . '"  type="file" tabindex="1" id="subject"	class="form-control">';
echo '</div>';

// echo '<div class="form-group">';
// echo '<label for="subject" class="">Numr:</label>';
// echo '<textarea name="copyright_text" id="title" tabindex="1" id="subject" class="form-control">'.$found_copyright->copyright_userdesc.'</texarea>';
// echo '</div>';

echo '</div>';
echo '</div>';
echo '</div>';
echo '<br>';
echo '&nbsp;&nbsp;';
echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';

echo '</div>';
echo '</div>';
echo '</div>';
echo '</div>';

//<!-- END PROFILE CONTENT -->
echo '</div>';
echo '</div>';
echo '</div>';
//<!-- end page content -->

echo '</div>';

// end if

?>


        </form>


        <!-- end page container -->