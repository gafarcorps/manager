<!-- start page container -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans le Workspace</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;
                        <a class="parent-item" href="#">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                </ol>
            </div>
        </div> 
        <!-- start widget -->
        <div class="row">
            <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="row clearfix">
                    <?php
//                    $found_apps_requests = App_request::find_all_active_by_customer_and_type($found_userinfos->user_info_id, 2);
//                    foreach ($found_apps_requests as $found_app_request) {
//                        $found_menu = Menu::find_by_id($found_app_request->app_request_menu_id);
//                        if (strpos($found_menu->menu_link, "app.php") !== false && $found_typeuser->type_user_id > 2) {
//                            $menu_link = str_replace("app.php", "appex.php", $found_menu->menu_link);
//                        } elseif (strpos($found_menu->menu_link, "appex.php") !== false && $found_typeuser->type_user_id < 3) {
//                            $menu_link = str_replace("appex.php", "app.php", $found_menu->menu_link);
//                        } else {
//                            $menu_link = $found_menu->menu_link;
//                        }
//
//                        echo '<div class="col-md-6 col-sm-6 col-6">';
//                        echo '<div class="card">';
//                        echo '<div class="panel-body">';
//                        echo '<div class="symbol">';
//                        echo '<i class="fa fa-users usr-clr"></i>';
//                        echo '</div>';
//                        echo '<h3>' . $found_menu->menu_title_en . '</h3>';
//                        echo '<div
//												class="progressbar-xs progress-rounded progress-striped progress ng-isolate-scope active">';
//                        echo '<div class="progress-bar progress-bar-success width-100"
//													role="progressbar" aria-valuenow="65" aria-valuemin="0"
//													aria-valuemax="100"></div>';
//                        echo '</div>';
//                        echo '</div>';
//                        echo '<a href="' . $menu_link . '" class="nav-link "><span class="text-small margin-top-10 full-width"> Launch the app </a></span>';
//                        echo '</div>';
//                        echo '</div>';
//                    }
                    ?> 

                    <?php
                    $found_db_menus = Db_menu :: list_menus_active($session->extension_id);
                    $i = 0;
                    foreach ($found_db_menus as $found_db_menu) {

                        $found_userinfos = User_info::find_user_infos($session->user_account_id);
                        $found_useraccount = User_account::find_by_id($session->user_account_id);
                        $found_typeuser = Typeuser::find_by_id($found_useraccount->user_account_typeuser_id);
                        $found_db_privilege = Db_privilege :: find_by_ids($found_typeuser->type_user_id, $found_db_menu->menu_id, $session->extension_id);
                        echo '>>'.$found_db_privilege->privilege_id;
                        echo '>>'.$found_db_privilege->privilege_active;
                        if ($found_db_privilege->privilege_active == 1) {
                            $i++;
                            if ($i == 1)
                                $color = "info";
                            elseif ($i == 2)
                                $color = "danger";
                            elseif ($i == 3)
                                $color = "success";
                            elseif ($i == 4)
                                $color = "warning";
                            elseif ($i == 5)
                                $color = "inverse";
                            echo '<div class="col-md-6 col-sm-6 col-6">';
                            echo '<div class="card">';
                            echo '<div class="panel-body">';
                            echo '<div class="symbol">';
//                            echo '<i class="fa fa-users usr-clr"></i> '.$form_builder_object->count_all_active();
                            echo '</div>';
                            echo '<h3>' . $found_db_menu->menu_title . '</h3>';
                            echo '<div class="progressbar-xs progress-rounded progress-striped progress ng-isolate-scope active">';
                            echo '<div class="progress-bar progress-bar-' . $color . ' width-100" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>';
                            echo '</div>';
                            $menu_link = $found_db_menu->menu_link;
                            $found_builder_commission = Form_builder::find_one_active_by_type_and_extension(4, $session->extension_id); //Commissions
                            if ($found_builder_commission->form_builder_type == 4) //Groupes Utilisteurs/Commissions
                                $menu_link = str_replace("xx", $found_builder_commission->form_builder_id, $menu_link);


                            echo '<a href="' . $menu_link . '" class="nav-link "><span class="text-small margin-top-10 full-width"> Lancer </a></span>';
//                            echo '<a href="appex.php?item=listform_builder_objects_db&form_builder_id=' . $found_form_builder->form_builder_id . '" class="nav-link "><span class="text-small margin-top-10 full-width"> Lancer the app </a></span>';
                            echo '</div>';
                            echo '</div>';
                            echo '</div>';
                        }
                    }
                    ?>
                    <?php
//                    $found_form_builders = Form_builder :: find_all_not_deleted_by_form_builder_extension($session->extension_id);
//                    foreach ($found_form_builders as $found_form_builder) {
//                        $form_builder_object = new Form_builder_object($found_form_builder->form_builder_name);
//                        if ($found_form_builder->form_builder_type == 4) { //Groupes Utilisteurs
//                            echo '<div class="col-md-6 col-sm-6 col-6">';
//                            echo '<div class="card">';
//                            echo '<div class="panel-body">';
//                            echo '<div class="symbol">';
//                            echo '<i class="fa fa-users usr-clr"></i> ' . $form_builder_object->count_all_active();
//                            echo '</div>';
//                            echo '<h3>' . $found_form_builder->form_builder_desc . '</h3>';
//                            echo '<div class="progressbar-xs progress-rounded progress-striped progress ng-isolate-scope active">';
//                            echo '<div class="progress-bar progress-bar-inverse width-100" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>';
//                            echo '</div>';
//                            echo '<a href="appex.php?item=listform_builder_objects_db&form_builder_id=' . $found_form_builder->form_builder_id . '" class="nav-link "><span class="text-small margin-top-10 full-width"> Lancer </a></span>';
////                            echo '<a href="appex.php?item=listform_builder_objects_db&form_builder_id=' . $found_form_builder->form_builder_id . '" class="nav-link "><span class="text-small margin-top-10 full-width"> Lancer the app </a></span>';
//                            echo '</div>';
//                            echo '</div>';
//                            echo '</div>';
//                        }
//                    }
                    ?>
                    <?php
//                    $found_form_builders = Form_builder :: find_all_not_deleted_by_form_builder_extension($session->extension_id);
//                    foreach ($found_form_builders as $found_form_builder) {
//                        $form_builder_object = new Form_builder_object($found_form_builder->form_builder_name);
//                        if ($found_form_builder->form_builder_type == 4) { //Groupes Utilisteurs
//                            echo '<div class="col-md-6 col-sm-6 col-6">';
//                            echo '<div class="card">';
//                            echo '<div class="panel-body">';
//                            echo '<div class="symbol">';
//                            echo '<i class="fa fa-users usr-clr"></i> ' . $form_builder_object->count_all_active();
//                            echo '</div>';
//                            echo '<h3>' . $found_form_builder->form_builder_desc . '</h3>';
//                            echo '<div class="progressbar-xs progress-rounded progress-striped progress ng-isolate-scope active">';
//                            echo '<div class="progress-bar progress-bar-inverse width-100" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>';
//                            echo '</div>';
//                            echo '<a href="appex.php?item=listform_builder_objects_db&form_builder_id=' . $found_form_builder->form_builder_id . '" class="nav-link "><span class="text-small margin-top-10 full-width"> Lancer </a></span>';
////                            echo '<a href="appex.php?item=listform_builder_objects_db&form_builder_id=' . $found_form_builder->form_builder_id . '" class="nav-link "><span class="text-small margin-top-10 full-width"> Lancer the app </a></span>';
//                            echo '</div>';
//                            echo '</div>';
//                            echo '</div>';
//                        }
//                    }
                    ?>
                    <?php
//                    $found_form_builders = Form_builder :: find_all_not_deleted_by_form_builder_extension($session->extension_id);
//                    foreach ($found_form_builders as $found_form_builder) {
//                        $form_builder_object = new Form_builder_object($found_form_builder->form_builder_name);
//                        if ($found_form_builder->form_builder_type == 4) { //Groupes Utilisteurs
//                            echo '<div class="col-md-6 col-sm-6 col-6">';
//                            echo '<div class="card">';
//                            echo '<div class="panel-body">';
//                            echo '<div class="symbol">';
//                            echo '<i class="fa fa-users usr-clr"></i> ' . $form_builder_object->count_all_active();
//                            echo '</div>';
//                            echo '<h3>' . $found_form_builder->form_builder_desc . '</h3>';
//                            echo '<div class="progressbar-xs progress-rounded progress-striped progress ng-isolate-scope active">';
//                            echo '<div class="progress-bar progress-bar-inverse width-100" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>';
//                            echo '</div>';
//                            echo '<a href="appex.php?item=listform_builder_objects_db&form_builder_id=' . $found_form_builder->form_builder_id . '" class="nav-link "><span class="text-small margin-top-10 full-width"> Lancer </a></span>';
////                            echo '<a href="appex.php?item=listform_builder_objects_db&form_builder_id=' . $found_form_builder->form_builder_id . '" class="nav-link "><span class="text-small margin-top-10 full-width"> Lancer the app </a></span>';
//                            echo '</div>';
//                            echo '</div>';
//                            echo '</div>';
//                        }
//                    }
                    ?>
                    <?php
//                    $found_form_builders = Form_builder :: find_all_not_deleted_by_form_builder_extension($session->extension_id);
//                    foreach ($found_form_builders as $found_form_builder) {
//                        $form_builder_object = new Form_builder_object($found_form_builder->form_builder_name);
//                        if ($found_form_builder->form_builder_type == 2) { //Utilisateur
//                            echo '<div class="col-md-6 col-sm-6 col-6">';
//                            echo '<div class="card">';
//                            echo '<div class="panel-body">';
//                            echo '<div class="symbol">';
//                            echo '<i class="fa fa-users usr-clr"></i> ' . $form_builder_object->count_all_active();
//                            echo '</div>';
//                            echo '<h3>' . $found_form_builder->form_builder_desc . '</h3>';
//                            echo '<div class="progressbar-xs progress-rounded progress-striped progress ng-isolate-scope active">';
//                            echo '<div class="progress-bar progress-bar-success width-100" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>';
//                            echo '</div>';
//                            echo '<a href="appex.php?item=listform_builder_objects_db&form_builder_id=' . $found_form_builder->form_builder_id . '" class="nav-link "><span class="text-small margin-top-10 full-width"> Lancer </a></span>';
////                            echo '<a href="appex.php?item=listform_builder_objects_db&form_builder_id=' . $found_form_builder->form_builder_id . '" class="nav-link "><span class="text-small margin-top-10 full-width"> Lancer the app </a></span>';
//                            echo '</div>';
//                            echo '</div>';
//                            echo '</div>';
//                        }
//                    }
                    ?>
                    <?php
//                    $found_form_builders = Form_builder :: find_all_not_deleted_by_form_builder_extension($session->extension_id);
//                    foreach ($found_form_builders as $found_form_builder) {
//                        $form_builder_object = new Form_builder_object($found_form_builder->form_builder_name);
//                        if ($found_form_builder->form_builder_type == 3) { //Communiqué
//                            echo '<div class="col-md-6 col-sm-6 col-6">';
//                            echo '<div class="card">';
//                            echo '<div class="panel-body">';
//                            echo '<div class="symbol">';
//                            echo '<i class="fa fa-file-archive-o usr-clr"></i> ' . $form_builder_object->count_all_active();
//
//                            echo '</div>';
//                            echo '<h3>' . $found_form_builder->form_builder_desc . '</h3>';
//                            echo '<div class="progressbar-xs progress-rounded progress-striped progress ng-isolate-scope active">';
//                            echo '<div class="progress-bar progress-bar-danger width-100" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>';
//                            echo '</div>';
//                            echo '<a href="appex.php?item=view_form_builder_objects_db" class="nav-link "><span class="text-small margin-top-10 full-width"> Lancer </a></span>';
////                            echo '<a href="appex.php?item=listform_builder_objects_db&form_builder_id=' . $found_form_builder->form_builder_id . '" class="nav-link "><span class="text-small margin-top-10 full-width"> Lancer the app </a></span>';
//                            echo '</div>';
//                            echo '</div>';
//                            echo '</div>';
//                        }
//                    }
                    ?>
                    <?php
//                    $found_form_builders = Form_builder :: find_all_not_deleted_by_form_builder_extension($session->extension_id);
//                    foreach ($found_form_builders as $found_form_builder) {
//                        $form_builder_object = new Form_builder_object($found_form_builder->form_builder_name);
//                        if ($found_form_builder->form_builder_type == 6) { //Upcoming events
//                            echo '<div class="col-md-6 col-sm-6 col-6">';
//                            echo '<div class="card">';
//                            echo '<div class="panel-body">';
//                            echo '<div class="symbol">';
//                            echo '<i class="fa fa-calendar-times-o usr-clr"></i> ' . $form_builder_object->count_all_active();
//
//                            echo '</div>';
//                            echo '<h3>' . $found_form_builder->form_builder_desc . '</h3>';
//                            echo '<div class="progressbar-xs progress-rounded progress-striped progress ng-isolate-scope active">';
//                            echo '<div class="progress-bar progress-bar-inverse width-100" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>';
//                            echo '</div>';
//                            echo '<a href="appex.php?item=view_form_builder_objects_db#upcoming" class="nav-link "><span class="text-small margin-top-10 full-width"> Lancer </a></span>';
////                            echo '<a href="appex.php?item=listform_builder_objects_db&form_builder_id=' . $found_form_builder->form_builder_id . '" class="nav-link "><span class="text-small margin-top-10 full-width"> Lancer the app </a></span>';
//                            echo '</div>';
//                            echo '</div>';
//                            echo '</div>';
//                        }
//                    }
                    ?>
                    <?php
//                    $found_form_builders = Form_builder :: find_all_not_deleted_by_form_builder_extension($session->extension_id);
//                    foreach ($found_form_builders as $found_form_builder) {
//                        $form_builder_object = new Form_builder_object($found_form_builder->form_builder_name);
//                        if ($found_form_builder->form_builder_type == 5) { //Contributions
//                            echo '<div class="col-md-6 col-sm-6 col-6">';
//                            echo '<div class="card">';
//                            echo '<div class="panel-body">';
//                            echo '<div class="symbol">';
//                            echo '<i class="fa fa-money usr-clr"></i> ' . $form_builder_object->count_all_active();
//
//                            echo '</div>';
//                            echo '<h3>' . $found_form_builder->form_builder_desc . '</h3>';
//                            echo '<div class="progressbar-xs progress-rounded progress-striped progress ng-isolate-scope active">';
//                            echo '<div class="progress-bar progress-bar-warning width-100" role="progressbar" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100"></div>';
//                            echo '</div>';
//                            echo '<a href="appex.php?item=view_form_builder_objects_db#contributions" class="nav-link "><span class="text-small margin-top-10 full-width"> Lancer </a></span>';
////                            echo '<a href="appex.php?item=listform_builder_objects_db&form_builder_id=' . $found_form_builder->form_builder_id . '" class="nav-link "><span class="text-small margin-top-10 full-width"> Lancer the app </a></span>';
//                            echo '</div>';
//                            echo '</div>';
//                            echo '</div>';
//                        }
//                    }
                    ?>


                </div>
            </div>		   

            <div class="col-lg-6 col-md-12 col-sm-12 col-12">
                <div class="row1 clearfix top-report">
                    <div class="col-12-1">
                        <div class="card">
                            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                                <!-- Indicators -->
                                <ol class="carousel-indicators">
                                    <li data-target="#carousel-example-generic" data-slide-to="0"
                                        class="active"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="1" class="">
                                    </li>
                                    <li data-target="#carousel-example-generic" data-slide-to="2" class="">
                                    </li>
                                </ol>
                                <!-- Wrapper for slides -->


                                <div class="carousel-inner owl-carousel" role="listbox" id="owl-demo">
                                    <?php
                                    // Afficher l'image de fond
                                    $url = "https://creativesweb.info/dev/en/rss_pub.php?sid=75&bid=7";

                                    $feeds = simplexml_load_file($url);
                                    $image_path = "";
                                    $count = 0;
                                    foreach ($feeds->channel->item as $item) {
                                        $image_path = $item->image->url;

                                        $count++;
                                        if (empty($image_path)) {
                                            echo '';
                                        } else {
                                            if ($count == 1) {
                                                echo '<div class="item active"> <img src="' . $image_path . '" alt=""></div>';
                                            } else {
                                                echo '<div class="item"> <img src="' . $image_path . '" alt=""></div>';
                                            }
                                        }
                                    }
                                    ?>
                                    <!--<div class="item active"> <img src="assets/img/slider/slider1.jpg"
                                                                   alt=""> </div>
                                    <div class="item"> <img src="assets/img/slider/slider2.jpg" alt="">
                                    </div>
                                    <div class="item"> <img src="assets/img/slider/slider3.jpg" alt="">-->
                                </div>
                            </div>
                            <!-- Controls -->
                            <!--<a class="left carousel-control" href="#carousel-example-generic"
                               role="button" data-slide="prev"> <span
                                    class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                                <span class="sr-only">Previous</span> </a> <a
                                class="right carousel-control" href="#carousel-example-generic"
                                role="button" data-slide="next"> <span
                                    class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                                <span class="sr-only">Next</span> </a>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="profile-content">
            <div class="row">


            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
