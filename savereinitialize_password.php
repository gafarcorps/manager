<?php

require_once("includes/initialize.php");
$found_user_account = User_account::find_by_id($_GET['user_account_id']);
if (isset($_POST['password'])) {
    $password = $_POST['password'];
}
if (isset($_POST['confirm_password'])) {
    $confirm_password = $_POST['confirm_password'];
}
if ($password != $confirm_password) {
    redirect_to('reinitialize_password.php?msg=error&user_account_id='.$_GET['user_account_id']);
} else {   

    //update user account
    $found_user_account->user_account_password = password_hash($password, PASSWORD_BCRYPT);
    $found_user_account->save();

    redirect_to('login.php?msg=good&email=' . $found_user_account->user_account_email);
}
?>
 