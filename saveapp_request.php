<?php

require_once("includes/initialize.php");

if (isset($_GET['url'])) {
    $url = $_GET['url'];
}
$customer_id = (!empty($_GET['customer_id'])) ? $_GET['customer_id'] : null;
if (!empty($customer_id))
    $found_customer = User_info::find_by_id($customer_id);

if (isset($_POST['app_request_id'])) {
    $app_request_id = $_POST['app_request_id'];
} //$app_request_id is the id of current app_request

if (isset($_POST['app_request_customer_id'])) {
    $app_request_customer_id = $_POST['app_request_customer_id'];
    $found_userinfo = User_info::find_by_id($app_request_customer_id);
}
if (isset($_POST['app_request_menu_id'])) {
    $app_request_menu_id = $_POST['app_request_menu_id'];
    $found_menu = Menu::find_by_id($app_request_menu_id);
}
if (isset($_GET['mid'])) {
    $mid = $_GET['mid'];
    $found_menu = Menu::find_by_id($mid);
}
if (isset($_POST['app_request_title'])) {
    $app_request_title = $_POST['app_request_title'];
}
if (isset($_POST['app_request_desc'])) {
    $app_request_desc = $_POST['app_request_desc'];
}
if (isset($_POST['app_request_date'])) {
    $app_request_date = $_POST['app_request_date'];
}
if (isset($_POST['app_request_type'])) {
    $app_request_type = $_POST['app_request_type'];
}
if (isset($_POST['app_request_active'])) {
    $app_request_active = $_POST['app_request_active'];
}
if (!empty($_FILES['app_request_image']['name'])) {
    $app_request_image = basename($_FILES['app_request_image']['name']);
}
$repository = '../../images/app_requests/';
if (isset($_POST['app_request_installation_date'])) {
    $app_request_installation_date = $_POST['app_request_installation_date'];
}

if ((isset($_POST['new']) || (isset($_GET['new']))) && !empty($session->user_account_id)) { // New app_request menu 
    $app_request->app_request_customer_id = $found_userinfo->user_info_id;
    $app_request->app_request_menu_id = $found_menu->menu_id;

    $app_request->app_request_title = "Demande du client " . $found_userinfo->user_info_firstname . ": Menu (" . $found_menu->menu_title . ")";
    $app_request->app_request_desc = $app_request_desc;
    $app_request->app_request_date = date('Y-m-d');
    $app_request->app_request_type = 2;
    if (isset($_FILES['app_request_image'])) {
        move_uploaded_file($_FILES['app_request_image']['tmp_name'], $repository . $app_request_image);
        $app_request->app_request_image = $app_request_image;
    }
    $app_request->app_request_installation_date = $app_request_installation_date;
    $app_request->app_request_date_creation = date('Y-m-d');

    $app_request->app_request_date_last_modification = date('Y-m-d');

    $app_request->app_request_user_creation = $session->user_account_id;
    $app_request->app_request_user_last_modification = $session->user_account_id;
    $app_request->app_request_deleted = 0;

    $app_request->save();

    redirect_to('app.php?item=listapps_requests&type=2&msg=success');
} // END IF NEW ENTRY  


if (isset($_POST['submit']) && !empty($app_request_id) && !empty($session->user_account_id)) { // Updating a app_request	
    $found_app_request = $app_request->find_by_id($app_request_id);

    $app_request->app_request_id = $found_app_request->app_request_id;

    $app_request->app_request_customer_id = $found_userinfo->user_info_id;
    $app_request->app_request_menu_id = $found_menu->menu_id;

    $app_request->app_request_title = "Demande du client " . $found_userinfo->user_info_firstname . ": Menu (" . $found_menu->menu_title . ")";
    $app_request->app_request_desc = $app_request_desc;
    $app_request->app_request_date = $found_app_request->app_request_date;
    $app_request->app_request_type = 2;

    if (isset($app_request_image)) {
        move_uploaded_file($_FILES['app_request_image']['tmp_name'], $repository . $app_request_image);
        $app_request->app_request_image = $app_request_image;
    } else {
        $app_request->app_request_image = $found_app_request->app_request_image;
    }
    $app_request->app_request_installation_date = $app_request_installation_date;
    $app_request->app_request_date_creation = $found_app_request->app_request_date_creation;

    $app_request->app_request_date_last_modification = date('Y-m-d');

    $app_request->app_request_user_creation = $found_app_request->app_request_user_creation;
    $app_request->app_request_user_last_modification = $session->user_account_id;
    $app_request->app_request_active = $app_request_active;
    $app_request->app_request_deleted = 0;

    $app_request->save();

    $found_apps_requests_b = App_request::find_all_by_menu($found_menu->menu_id);
    foreach ($found_apps_requests_b as $found_app_request_b) {
        $found_app_request_b->app_request_image = "";
        if (isset($app_request_image)) {
            $found_app_request_b->app_request_image = $app_request_image;
        }
        $found_app_request_b->save();
    }

    $found_user = User_info::find_by_id($app_request_customer_id);
    $found_user_account = User_account::find_by_id($found_user->user_info_user_account_id);
    $found_typeuser = Typeuser::find_by_id($found_user_account->user_account_typeuser_id);
    $found_menus = Db_menu:: find_all();
    foreach ($found_menus as $found_db_menu) {
        $db_menu_id = $found_db_menu->menu_id;
        $db_privilege = empty(Db_privilege::find_by_ids($found_typeuser->type_user_id, $db_menu_id, $app_request_menu_id)) ? new Db_privilege() : Db_privilege::find_by_ids($found_typeuser->type_user_id, $db_menu_id, $app_request_menu_id);
        $db_privilege->privilege_typeuser_id = $found_typeuser->type_user_id;
        $db_privilege->privilege_db_menu_id = $found_db_menu->menu_id;
        $db_privilege->privilege_active = ($found_typeuser->type_user_id == 4 || $found_typeuser->type_user_id == 5) ? 0 : 1;
        $db_privilege->save();
    }


//    redirect_to('app.php?item=listapp_requests&customer_id=' . $customer_id . '&msg=success');
    redirect_to('app.php?item=listapps_requests&type=2&msg=success');
}  // END IF UPDATE 

if (!empty($_GET['app_request_id']) && !empty($session->user_account_id) && $_GET['option'] == 'delete') {  // UNABLE ENTRIE
    $app_request_id = $_GET['app_request_id'];
    $app_request = $app_request->find_by_id($app_request_id);
    $del = $app_request->unable();
    redirect_to('app.php?item=formapp_request&app_request_id=' . $app_request_id . '&customer_id=' . $customer_id . '&option=edit&r=0');
}  // END IF UNABLE 

if (!empty($_GET['app_request_id']) && !empty($session->user_account_id) && $_GET['option'] == 'recover') {  // DELETE ENTRIE
    $app_request_id = $_GET['app_request_id'];
    $app_request = $app_request->find_by_id($app_request_id);
    $del = $app_request->recover();
    redirect_to('app.php?item=formapp_request&app_request_id=' . $app_request_id . '&customer_id=' . $customer_id . '&option=edit&r=1');
}  // END IF DELETE 
if (!empty($_GET['app_request_id']) && !empty($session->user_account_id) && $_GET['option'] == 'active') {  // DELETE ACTIVE
    $app_request_id = $_GET['app_request_id'];
//    echo '>>'.$_GET['app_request_id'];
    $app_request = $app_request->find_by_id($app_request_id);
    $app_request->app_request_active = 1;
    $app_request->save();

    $found_user = User_info::find_by_id($app_request->app_request_customer_id);
    $found_user_account = User_account::find_by_id($found_user->user_info_user_account_id);
    $found_typeuser = Typeuser::find_by_id($found_user_account->user_account_typeuser_id);
    $found_menus = Db_menu:: find_all();
    foreach ($found_menus as $found_db_menu) {
        $db_menu_id = $found_db_menu->menu_id;
        $db_privilege = empty(Db_privilege::find_by_ids($found_typeuser->type_user_id, $db_menu_id, $app_request->app_request_menu_id)) ? new Db_privilege() : Db_privilege::find_by_ids($found_typeuser->type_user_id, $db_menu_id, $app_request->app_request_menu_id);
        $db_privilege->privilege_typeuser_id = $found_typeuser->type_user_id;
        $db_privilege->privilege_db_menu_id = $db_menu_id;
        $db_privilege->privilege_extension_id = $app_request->app_request_menu_id;
        $db_privilege->privilege_active = ($found_typeuser->type_user_id == 4 || $found_typeuser->type_user_id == 5) ? 0 : 1;
        $db_privilege->save();
    }

    redirect_to('app.php?item=listapps_requests&type=2&msg=success');
}  // END IF ACTIVE 
if (!empty($_GET['app_request_id']) && !empty($session->user_account_id) && $_GET['option'] == 'break') {  // DELETE ACTIVE
    $app_request_id = $_GET['app_request_id'];
    $customer_id = $_GET['customer_id'];
    $app_request = $app_request->find_by_id($app_request_id);
    $app_request->app_request_active = 0;
    $app_request->save();

    redirect_to('app.php?item=listapp_requests&customer_id=' . $customer_id . '&msg=success');
}  // END IF ACTIVE 

if (!empty($_GET['app_request_id']) && !empty($session->user_account_id) && $_GET['option'] == 'delete2') {  // DELETE ENTRIE
    $app_request_id = $_GET['app_request_id'];
    $app_request = $app_request->find_by_id($app_request_id);
    $del = $app_request->delete();
    redirect_to('app.php?item=formapp_request&app_request_id=' . $app_request_id . '&customer_id=' . $customer_id . '&option=edit&r=3');
}  // END IF DELETE 

if (!empty($_GET['app_request_id']) && !empty($session->user_account_id) && $_GET['option'] == 'customer_manager_connexion') {  // DELETE ENTRIE
    $app_request_id = $_GET['app_request_id'];
    $app_request = $app_request->find_by_id($app_request_id);
    $app_request->app_request_active = 1;
    $app_request->save();

    $url = $_GET['url'];
    $menu_title = $_GET['menu_title'];
    $menu_link = $_GET['menu_link'];
    redirect_to($url . '/manager/connexionLogin_root.php?menu_title=' . $menu_title . '&menu_link=' . $menu_link);
}  // END IF DELETE 
?>
 