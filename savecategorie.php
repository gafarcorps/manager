<?php

require_once("includes/initialize.php");


if (isset($_POST['cid'])) {
	$cid      	         	=	$_POST['cid'];
} //$cid is the id of current categorie 
if (isset($_POST['categorie_desc'])) {
	$categorie_desc 		=	$_POST['categorie_desc'];
}
if (isset($_POST['categorie_active'])) {
	$categorie_active		=	($_POST['categorie_active']) ? true : false;
}
if (isset($_POST['position'])) {
	$position 		=	$_POST['position'];
}


if (isset($_POST['submit']) && empty($cid) && !empty($session->user_account_id)) {	// New categorie


	$type_topic->type_topic_desc    	     = $categorie_desc;
	$type_topic->type_topic_date             = date('Y-m-d- h:i:s');

	$type_topic->type_topic_position  		 = $position;
	$type_topic->type_topic_active  		 = $categorie_active;

	$type_topic->save();
	redirect_to('app.php?item=listcategories&msg=success');
}



if (isset($_POST['submit'])  && !empty($cid) && !empty($session->user_account_id)) {	// Updating a categorie			   	

	$found_typetopic                     =  $type_topic->find_by_id($cid);

	$type_topic->type_topic_id           =  $found_typetopic->type_topic_id;

	if (!empty($categorie_desc)) {  // UPDATE IF THERE ANY POSTED TITLE VARIABLE
		$type_topic->type_topic_desc =  $categorie_desc;
	} else { // REPLACE THE ASSET VALUE
		$type_topic->type_topic_desc  = $found_typetopic->type_topic_desc;
	}
	if (!empty($position)) {  // UPDATE IF THERE ANY POSTED POSITION VARIABLE
		$type_topic->type_topic_position =  $position;
	} else { // REPLACE THE ASSET VALUE
		$type_topic->type_topic_position  = $found_typetopic->type_topic_position;
	}


	if (!empty($categorie_active)) { // UPDATE IF THERE ANY POSTED ACTIVE VARIABLE
		$type_topic->type_topic_active =  $found_typetopic->type_topic_active;
	} else { // REPLACE THE ASSET VALUE
		$type_topic->type_topic_active  = $found_typetopic->type_topic_active;
	}
	$type_topic->type_topic_date             = date('Y-m-d- h:i:s');
	$type_topic->save();

	redirect_to('app.php?item=listcategories&msg=success');
}  // END IF UPDATE 	





if (!empty($_GET['cid'])  && !empty($session->user_account_id) && $_GET['option'] == 'delete') {  // DELETE ENTRIE

	$cid        = $_GET['cid'];
	$type_topic = $type_topic->find_by_id($cid);
	$del        = $type_topic->unable();
	redirect_to('app.php?item=formcategorie&cid=' . $cid . '&option=edit&r=0');
}  // END IF DELETE 

if (!empty($_GET['cid'])  && !empty($session->user_account_id) && $_GET['option'] == 'recover') {  // DELETE ENTRIE

	$cid        = $_GET['cid'];
	$type_topic = $type_topic->find_by_id($cid);
	$del        = $type_topic->recover();
	redirect_to('app.php?item=formcategorie&cid=' . $cid . '&option=edit&r=1');
}  // END IF DELETE 
