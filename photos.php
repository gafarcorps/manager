<?php

//This class will use the database so wil need the database class included
require_once(LIB_PATH . DS . 'database.php');

class Photo {

    public $table_name = "photos";
    public $id;
    public $filename;
    public $type;
    public $size;
    public $caption;
    public $all_photos = array();
    private $temp_path;
    protected $upload_dir = "images";
    public $errors = array();
    protected $upload_errors = array(
        UPLOAD_ERR_OK => "No errors.",
        UPLOAD_ERR_INI_SIZE => "Larger than upload_max_filesize.",
        UPLOAD_ERR_FORM_SIZE => "Larger than form MAX_FILE_SIZE.",
        UPLOAD_ERR_PARTIAL => "Partial Upload.",
        UPLOAD_ERR_NO_FILE => "No File",
        UPLOAD_ERR_NO_TMP_DIR => "No Temp Directory.",
        UPLOAD_ERR_CANT_WRITE => "Cant write file to disk.",
        UPLOAD_ERR_EXTENSION => "File uploaded stopped by extension."
    );

//Pass in $_FILE['uploaded_file'] as an argument
    public function attach_file($file) {
        //Perform error checking on the form params
        if (!$file || empty($file) || !is_array($file)) {
            //error: nothing uploaded or wrong argument
            $this->errors[] = "No file was uploaded.";
            return false;
        } elseif ($file['error'] != 0) {
            //error: report what php says went wrong
            $this->errors[] = $this->upload_errors[$file['error']];
            return false;
        } else {
            //Set object attributes to the forms params
            $this->temp_path = $file['tmp_name'];
            $this->filename = basename($file['name']);
            $this->type = $file['type'];
            $this->size = $file['size'];
            //Dont worry about saving anything to database yet
            return true;
        }
    }

    public function save() {
        //A new record wont have an id yet
        if (isset($this->id)) {
            $this->update();
            //Really just to update the caption
        } else {
            //Make sure there are no errors
            //Cant save if there are pre existing errors
            if (!empty($this->errors)) {
                return false;
            }

            //Make sure the caption is not to long for the database
            if (strlen($this->caption) >= 255) {
                $this->errors[] = "The caption can only be 255 characters long.";
                return false;
            }

            //Cant save without the filename and temp location
            if (empty($this->filename) || empty($this->temp_path)) {
                $this->errors[] = "The file location was not available.";
                return false;
            }

            //Determine the target path
            $target_path = $this->upload_dir . DS . $this->filename;

            //Make sure that the file doesn't already exist in that location
            if (file_exists($target_path)) {
                $this->errors[] = "The file {$this->filename} already exists.";
                return false;
            }


            //Attempt to move the file

            if (move_uploaded_file($this->temp_path, $target_path)) {
                //Success
                //Save a corresponding entry to the database
                if ($this->create()) {
                    //Were done with the temp path variable, The file isn't there any more
                    unset($this->temp_path);
                    return true;
                }
            } else {
                //Error File was not moved
                $this->errors[] = "The file upload failed, Possibly due to an incorrect permissions on upload folder.";
                return false;
            }
        }
    }

    public function destroy($id, $filename) {
        //First remove the database entry
        if ($this->delete($id)) {
            //Then remove the file
            $target_path = $this->upload_dir . DS . $filename;
            return unlink($target_path) ? true : false;
        } else {
            //Database delete failed
            return false;
        }
    }

    public function image_path() {
        return $this->upload_dir . DS;
    }

    public function size_as_text($file_size) {
        if ($file_size < 1024) {
            return "{$file_size} bytes";
        } elseif ($file_size < 1048576) {

            $size_kb = round($file_size / 1024);
            return "{$size_kb} KB";
        } else {
            $size_mb = round($file_size / 1048576, 1);
            return "{size_mb} MB";
        }
    }

    public function find_all() {
        global $database;
        $result = $this->find_by_sql("SELECT * FROM " . $this->table_name);
        return $result;
    }

    public function find_all_photos() {
        global $database;
        $query = "SELECT * FROM {$this->table_name}";
        $result = $database->query($query);

        return $result;
    }

    public function find_by_id($id = 0) {
        global $database;

        $result_array = $this->find_by_sql("SELECT * FROM " . $this->table_name . " WHERE id={$database->escape_value($id)} LIMIT 1");
        return !empty($result_array) ? $result_array : false;
    }

    public function find_by_sql($sql = "") {
        global $database;
        $result = $database->query($sql);
        $object_array = $database->fetch_array($result);
        $object = $this->instantiate($object_array);
        //while($row = $database->fetch_array($result)) {
        //$object_array[] = $this->instantiate($row);
        //}
        return $object_array;
    }

    private function instantiate($record) {

        //Could check if $record exists and is an array
        //Simple long form approach
        //$object = new self;
        $this->id = $record['id'];
        $this->filename = $record['filename'];
        $this->type = $record['type'];
        $this->size = $record['size'];
        $this->caption = $record['caption'];

        //More dynamic, Short form approach
        //foreach($record as $attribute=>$value) {
        //if($object->has_attribute($attribute)) {
        //$object->$attribute = $value;
        //}
        //}
        //return $object;
    }

    private function has_attribute($attribute) {
        //get_object_vars returns an assocative array with all attributes
        //Incl. pribate ones as the keys and their current values as the value
        $object_vars = get_object_vars($this);
        //We dont care about the value, we just want to know if the key exists
        //Will return true or false
        return array_key_exists($attribute, $object_vars);
    }

    public function create() {
        //This is the create method
        global $database;
        //DOnt forget your SQL syntax and good habits
        //INSERT INTO table (key,key) VALUES ('value','value')
        //SIngle-quotes around all values
        //Escape all values to prevent sql injection

        $query = "INSERT INTO {$this->table_name} (";
        $query .= "filename, type, size, caption";
        $query .= ") VALUES ('";
        $query .= $database->escape_value($this->filename) . "', '";
        $query .= $database->escape_value($this->type) . "', '";
        $query .= $database->escape_value($this->size) . "', '";
        $query .= $database->escape_value($this->caption) . "')";

        if ($database->query($query)) {
            $this->id = $database->insert_id();
            return true;
        } else {
            return false;
        }
    }

    public function update() {
        global $database;
        //Dont forget your sql good habits
        //UPDATE table SET key='value', key='value' WHERE condition
        //single quotes around all values
        //Escape all values to prevent sql injection

        $query = "UPDATE {$this->table_name} SET ";
        $query .= "filename='" . $database->escape_value($this->filename) . "', ";
        $query .= "type='" . $database->escape_value($this->type) . "', ";
        $query .= "size='" . $database->escape_value($this->size) . "', ";
        $query .= "caption='" . $database->escape_value($this->caption) . "'";
        $query .= " WHERE id=" . $database->escape_value($this->id);
        $database->query($query);
        return ($database->affected_rows() == 1) ? true : false;
    }

    public function delete($id = 0) {
        global $database;
        //Dont forget good sql habits
        //DELETE FROM table WHERE condition LIMIT 1
        //Escape all values to prevent sql injection 
        //Use limit 1

        $query = "DELETE FROM {$this->table_name} ";
        $query .= "WHERE id=" . $database->escape_value($id);
        $query .= " LIMIT 1";
        $database->query($query);
        return ($database->affected_rows() == 1) ? true : false;
    }

}

?>