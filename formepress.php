 


<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="app.php?item=listepress">Liste des fichiers numériques</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Editer un fichier </li>
                </ol>
            </div>
        </div>


        <form action="saveepress.php" class="form-horizontal" method="POST" enctype="multipart/form-data"> 

            <?php
            if (isset($_GET['option']) && $_GET['option'] == 'new') {
                echo '<div class="row">';
                echo '<div class="col-md-12">';
                //<!-- BEGIN PROFILE SIDEBAR -->
                echo '<div class="profile-sidebar">';
                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Attributs </div>';
                echo '<div class="profile-usertitle-job"> du nouveau fichier </div>';
                echo '</div>';


                echo '<div id="displayed" class="card-body no-padding height-9">';
                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b>Etat </b>';
                echo '<div class="profile-desc-item pull-right">';
                echo '<i class="fa fa-eye"></i> ';
                echo '</div>';
                echo '</li>';

                echo '<li class="list-group-item">';
                echo '<b>Dernière mise jour</b>';
                echo '<div class="profile-desc-item pull-right">';
                echo '<i class="fa fa-clock-o"> </i> ' . date('Y-m-d h:i:s') . ' ';
                echo '</div>';
                echo '</li>';

                echo 'Activer ce fichier?';
                echo '<div class="controls">';
                echo '<div class="form-group">';
                echo '<div class="checkbox checkbox-icon-black">';
                echo '<input id="rememberChk1" name="epress_active" type="checkbox">';
                echo '</div>';

                echo '</ul><br/>';

                echo '</div>';


                echo '</div>';


                echo '</div>';


                //<!-- END BEGIN PROFILE SIDEBAR -->
                //<!-- BEGIN PROFILE CONTENT -->
                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';
                echo '<h4> Créer un nouveau fichier </h4>';
                echo '</div>';
                echo '</div>';
                echo '<div class="white-box">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';
                echo '<div class="form-group">';
                echo '<label for="subject" class="">Titre:</label>';
                echo '<input name="epress_title" type="text" tabindex="1" id="subject1" class="form-control">';
                echo '<label for="subject" class="">Fichier:</label>';
                echo '<input name="epress_file_path" type="file" tabindex="1" id="subject1" class="form-control">';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                echo '</div>';
                echo '<br>';
                echo '&nbsp;&nbsp;';
                echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';


                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->
                echo '</div>';
            } //end if
            ?>


            <?php
            $epress = new Epress();
            
            if (isset($_GET['option']) && $_GET['option'] == 'edit' && isset($_GET['eaid'])) {

                if (isset($_GET['eaid'])) {
                    $eaid = $_GET['eaid'];
                }
               
                echo '<input type="hidden" name="eaid" value="' . $eaid . '" />';

                $found_epress = $epress->find_by_id($eaid);


                echo '<div class="row">';
                echo '<div class="col-md-12">';
                //<!-- BEGIN PROFILE SIDEBAR -->
                echo '<div class="profile-sidebar">';
                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Attributs de la page </div>';
                echo '<div class="profile-usertitle-job"> de la page ' . $found_epress->epress_title . ' </div>';
                echo '</div>';
                echo '<div class="card-body no-padding height-9">';

                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b>Etat </b>';
                echo '<div class="profile-desc-item pull-right">';

                if ($found_epress->epress_active == 1) {
                    echo '<i class="fa fa-eye"></i> activé';
                } else {
                    echo '<span class="label label-danger"><i class="fa fa-eye-slash"></i> désactivé</span>';
                }


                echo '</div>';
                echo '</li>';


                echo '<li class="list-group-item">';
                if ($found_epress->epress_active == 1) {

                    echo '<b>Déplacer dans la corbeille </b>';
                    echo '<div class="profile-desc-item pull-right"> <a href="saveepress.php?eaid=' . $eaid . '&option=delete"><i class="fa fa-trash-o "></i></a> </div>';
                } else {
                    echo '<b>Restaurer ce type </b>';
                    echo '<div class="profile-desc-item pull-right"> <a href="saveepress.php?eaid=' . $eaid . '&option=recover"><i class="fa  fa-recycle "></i></a> </div>';
                }

                echo '</li>';

                echo '</ul><br/>';



                echo '<div class="controls">';
                echo '<div class="form-group">';
                echo '<div class="checkbox checkbox-icon-black">';
                //echo '<br/><b> <a href="app.php?item=disabledtags"><b>Corbeille </b></a></b>';
                echo '</div>';

                //<!-- END SIDEBAR BUTTONS -->
                echo '</div>';
                echo '</div>';
                echo '</div>';

                echo '</div>';
                echo '</div>';


                //<!-- END BEGIN PROFILE SIDEBAR -->
                //<!-- BEGIN PROFILE CONTENT -->

                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';
                echo '<h4> Modifier le contenu : ' . $found_epress->epress_title . ' </h4>';

                if (isset($_GET['r']) && $_GET['r'] == 0) {
                    echo '<span class="clsNotAvailable"> Ce fichier a été déplacé dans la corbeille. </span> <br/>';
                } else if (isset($_GET['r']) && $_GET['r'] == 1) {
                    echo '<span class="clsAvailable"> Ce fichier a été restauré. </span> <br/>';
                }



                echo '<div class="btn-group">';
                echo '<a href="app.php?item=formepress&eaid=' . $eaid . '&option=new" id="addRow" class="btn btn-info">Nouveau fichier <i class="fa fa-plus"></i></a>';
                echo '</div>';


                echo '</div>';
                echo '</div>';


                echo '<div class="white-box">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';


                echo '<div class="form-group">';
                echo '<label for="subject" class="">Titre:</label>';
                echo '<input name="epress_title" id="title" value="' . $found_epress->epress_title . '"  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '<label for="subject" class="">Fichier:</label>';
                echo '<input name="epress_file_path" id="title" value=""  type="file" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '<br>';
                echo '&nbsp;&nbsp;';
                echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';

                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->

                echo '</div>';
            } // end if
            ?>


        </form>


        <!-- end page container -->

