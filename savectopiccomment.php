<?php

    require_once("includes/initialize.php");
    
      if (isset($_POST['submit'])) {
    
        if (isset($_POST['auteur'])) {
            $author = $_POST['auteur'];
        }
        if (isset($_POST['email'])) {
            $email = $_POST['email'];
        }
        if (isset($_POST['topic_id'])) {
            $topic_id = $_POST['topic_id'];
        }
        if (isset($_POST['comment'])) {
            $comment = trim($_POST['comment']);
        }
        
        $comment_date = date('Y-m-d H:i:s');
        // NEW ENTRY 
        $comment_topic->comment_topic_author = $author;
        $comment_topic->comment_topic_author_email = $email;
        $comment_topic->comment_topic_text = $comment;
        $comment_topic->comment_topic_topicid = $topic_id;
        $comment_topic->comment_topic_active = 1;
        $comment_topic->comment_topic_date = $comment_date;
        
        $comment_topic->save();
        // END IF NEW ENTRY  
    
        redirect_to('singlepost.php?msg=success');
        
    } // END IF SUBMIT

?>