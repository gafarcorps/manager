<?php
$customer_id = (!empty($_GET['customer_id'])) ? $_GET['customer_id'] : null;
if (!empty($customer_id))
    $found_customer = User_info::find_by_id($customer_id);
?>
<!-- start contrat content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="appex.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="appex.php?item=listcustomers">Clients</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="#">Contrats</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Liste des contrats</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Liste des contrats du client: <i style="color: blue"><?php echo $found_customer->user_info_firstname . ' (' . $found_customer->user_info_lastname . ')' ?></i></header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row p-b-20">
                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group">
                                    <a href="appex.php?item=formcontract&customer_id=<?php echo $found_customer->user_info_id; ?>&option=new" id="addRow" class="btn btn-info">
                                        Nouveau contrat <i class="fa fa-plus"></i>
                                    </a>

                                </div>
                                <div class="btn-group">
                                    <a href="appex.php?item=disabledcontracts&customer_id=<?php echo $found_customer->user_info_id; ?>&list=disabled" id="addRow" class="btn btn-danger"><i class="fa fa-trash"></i> <b>Corbeille </b></a> 
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group pull-right">
                                    <?php
                                    if (isset($_GET['contract_id'])) {
                                        $contract_id = $_GET['contract_id'];
                                        echo '<a href="appex.php?item=listcontracts" class="btn deepPink-bgcolor">Retourner à liste précédente';
                                        echo '</a>';
                                    } // enf if
                                    ?>

                                </div>
                            </div>
                        </div>

                        <?php
                        if (isset($_GET['msg']) && $_GET['msg'] == 'success') {
                            echo '<span class="clsAvailable"> Enregistrement réussi. </span>';
                        }
                        ?>

                        <div class="table-scrollable">
                            <table class="table table-hover table-checkable order-column full-width"
                                   id="example4">
                                <thead>
                                    <tr>  
                                        <!--<th class="center"></th>-->
                                        <th class="center">Titre </th>
                                        <th class="center">Service </th>
                                        <th class="center"> Date signature </th>   
                                        <th class="center"> Date début </th>   
                                        <!--<th class="center"> Date fin </th>-->   
                                        <th class="center"> Date rupture </th>   
                                        <th class="center"> En cours/Terminé </th>   
                                        <th class="center"></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    if (empty($_GET['list']))
                                        $found_contracts = Contract :: find_all_not_deleted($_GET['customer_id']);
                                    else
                                        $found_contracts = Contract :: find_all_deleted($_GET['customer_id']);
                                    foreach ($found_contracts as $found_contract) {
                                        echo '<tr class="odd gradeX">';
//                                        echo '<td class="center"></td>';
                                        echo '<td class="center">' . $found_contract->contract_title . '</td>';
                                        $found_service = Service::find_by_id($found_contract->contract_service_id);
                                        echo '<td class="center"><span class="label label-success">' . $found_service->service_title . '</span></td>';
                                        echo '<td class="center">' . (($found_contract->contract_date != "0000-00-00") ? $found_contract->contract_date : "Inconnue") . '</td>';
                                        echo '<td class="center">' . (($found_contract->contract_datestart != "0000-00-00") ? $found_contract->contract_datestart : "Inconnue") . '</td>';
//                                        echo '<td class="center">' . (($found_contract->contract_dateend != "0000-00-00") ? $found_contract->contract_dateend : "Inconnue") . '</td>';
                                        echo '<td class="center">' . (($found_contract->contract_date_breaking != "0000-00-00") ? $found_contract->contract_date_breaking : "Inconnue") . '</td>';
                                        echo '<td class="center">';
                                        echo '<span class="label label-' . (($found_contract->contract_active == 1) ? "success" : "warning") . '">' . (($found_contract->contract_active == 1) ? "En cours" : "Terminée") . '</span>';
//                                        echo '<a target="_blank" href="savecontract.php?customer_id=' . $found_customer->user_info_id . '&contract_id=' . $found_contract->contract_id . '&option=break"><span class="btn btn-danger"><i class="fa fa-trash"></i> Rompre</span></a>';
                                        ?>

                                    <div class="panel-group accordion" id="accordion<?php echo $found_contract->contract_id ?>">

                                        <div class="panel panel-default">
                                            <div class="panel-heading panel-heading-gray">
                                                <h5 class="panel-title">
                                                    <a style="color: red; font-weight: bold" class="accordion-toggle accordion-toggle-styled collapsed"
                                                       data-toggle="collapse" data-parent="#accordion<?php echo $found_contract->contract_id ?>"
                                                       href="#collapse_3_2"> Rompre </a>
                                                </h5>
                                            </div>
                                            <div id="collapse_3_2" class="panel-collapse collapse">
                                                <div class="panel-body">
                                                    <?php
                                                    echo '<form action="savecontract.php?customer_id=' . $found_customer->user_info_id . '&contract_id=' . $found_contract->contract_id . '&option=break" class="form-horizontal" method="POST" enctype="multipart/form-data">';
                                                    echo '<div class="form-group">';
                                                    echo '<label for="contract_date_breaking" class="">Date rupture contrat</label>';
                                                    echo '<input name="contract_date_breaking" type="date" tabindex="1" id="contract_date_breaking" class="form-control" value="'.date("Y-m-d").'">';
                                                    echo '<input type="submit" name="break" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-danger" value="Rompre">';
                                                    echo '</div>';
                                                    echo '</form>';
                                                    ?>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <?php
                                    echo '</td>';
                                    echo '<td class="center">';

                                    echo '<a href="appex.php?item=formcontract&contract_id=' . $found_contract->contract_id . '&customer_id=' . $found_customer->user_info_id . '&option=edit"><span class="btn btn-info"><i class="fa fa-pencil"></i> Modifier</span></a>';

                                    echo '</td>';

                                    echo '</tr>';
                                } // end foreach
                                ?>

                                </tbody>
                            </table>
                            <?php echo '<br/><b> </b>'; ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end contrat content -->
