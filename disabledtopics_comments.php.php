
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Welcome to your website's manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="app.php?item=home">Home</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="app.php?item=listpages">Pages</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Disabled condoleances</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Disabled books condoleances</header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row p-b-20">
                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group">
                                    <a href="app.php?item=listbookscs" id="addRow" class="btn btn-success">
                                        Published books condoleances 
                                    </a>

                                </div>
<!--                                <div class="btn-group">
                                    <a href="app.php?item=disabledbookscs" id="addRow" class="btn btn-danger"><b> Bin </b></a> 
                                </div>-->
                            </div>

                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group pull-right">
                                    
                                </div>
                            </div>
                        </div>

                        <?php
                        if (isset($_GET['msg']) && $_GET['msg'] == 'success') {
                            echo '<span class="clsAvailable"> Success. </span>';
                        }
                        ?>

                        <div class="table-scrollable">
                            <table class="table table-hover table-checkable order-column full-width"
                                   id="example4">
                                <thead>
                                    <tr>  
                                        <th class="center"></th>
                                        <th class="center">Date </th>
                                        <th class="center"> Author </th> 
                                        <th class="center"> Email </th> 
                                        <th class="center"> Testimony </th>
                                        <th class="center"> Actions </th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    $comment_id = $_GET['comment_id'];
                                    $found_bookscs = Comment_topic :: find_unabled_by_topic(34);
                                    foreach ($found_bookscs as $found_bookc) {
                                        echo '<tr class="odd gradeX">';
                                        echo '<td class="center"></td>';
                                        echo '<td class="center">' . $found_bookc->comment_topic_date . '</td>';
                                        echo '<td class="center">' . $found_bookc->comment_topic_author . '</td>';
                                        echo '<td class="center">' . $found_bookc->comment_topic_author_email . '</td>';
                                        echo '<td class="center">' . $found_bookc->comment_topic_text . '</td>';
                                        echo '<td class="center">';
                                        echo '<a href="savebookc.php?comment_id=' . $found_bookc->comment_topic_id . '&option=' . (($found_bookc->comment_topic_published == 1) ? "disable" : "activate") . '"><span class="label label-' . (($found_bookc->comment_topic_published == 1) ? "success" : "warning") . '">'.(($found_bookc->comment_topic_published == 1) ? "Disable" : "Activate") .'</span></a>';
                                        echo '<a href="savebookc.php?comment_id=' . $found_bookc->comment_topic_id . '&option=' . (($found_bookc->comment_topic_active == 1) ? "delete" : "recover") . '"><span class="label label-' . (($found_bookc->comment_topic_active == 1) ? "info" : "danger") . '">'.(($found_bookc->comment_topic_published == 1) ? "Delete" : "Recover") .'</span></a>';
                                        echo '</td>';
//                                            echo '<td class="center">';
//                                            if ($found_page->page_active == 1) {
//                                                echo '<a href="app.php?item=formpage&pid=' . $found_page->page_id . '&option=edit"><span class="label label-info">Update</span></a>';
//                                            } else {
//                                                echo '<a href="app.php?item=formpage&pid=' . $found_page->page_id . '&option=edit"><span class="label label-danger">Not published</span></a>';
//                                            }
//                                            echo '</td>';

                                        echo '</tr>';
                                    } // end foreach
                                    ?>

                                </tbody>
                            </table>
                            <?php echo '<br/><b> </b>'; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end page content -->
