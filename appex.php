<!-- start header -->
<?php
include('./header.php');
if (!empty($session->login($found_user_account))) {
    $session->logout();
}
if (!empty($_GET['customer_number'])) {
    $found_user_account = User_account::find_by_customer_number($_GET['customer_number']);
    $session->login($found_user_account);
}
?>
<!-- end header -->


<?php
if (!empty($_GET['item']) && $_GET['item'] == 'formpageelement' && $_GET['id'] == '2') {
    redirect_to('app.php?item=footer&pid=2');
}
?> 
<?php
if (!empty($_GET['item']) && $_GET['item'] == 'formpageelement' && $_GET['id'] == '3') {
    redirect_to('app.php?item=listsocials&pid=3');
}
?> 

<?php
$found_userinfos = User_info::find_user_infos($session->user_account_id);
$found_useraccount = User_account::find_by_id($session->user_account_id);
$found_typeuser = Typeuser::find_by_id($found_useraccount->user_account_typeuser_id);
$app_or_appex = ($found_useraccount->user_account_typeuser_id > 2) ? "appex" : "app";
?>

<!-- start page container -->
<div class="page-container">

    <!-- start sidebar menu -->
    <?php
    include('sidebarex.php');
    ?> 
    <!-- end sidebar menu -->


    <!-- start page content  --> 

    <?php
    if (isset($_GET['item'])) {

        if (!empty($_GET['item'])) {

            switch ($_GET['item']) {

                case 'home': include('home.php');
                    break;
              
            }// end switch
            
        } // end if
    } // end if isset
    include 'app_settings.php';
    include 'app_extensions.php'; #D9
    ?>

    <!-- end chat sidebar -->

</div>		
<!-- end page container  -->


<?php
include('footer.php');
?>


