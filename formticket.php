<?php
$customer_id = (!empty($_GET['customer_id'])) ? $_GET['customer_id'] : null;
if (!empty($customer_id))
    $found_customer = User_info::find_by_id($customer_id);
?>
<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="<?php echo $app_or_appex ?>.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="<?php echo $app_or_appex ?>.php?item=listtickets&customer_id=<?php echo $found_customer->user_info_id; ?>">Gestionnaire des tickets</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Editer un ticket</li>
                </ol>
            </div>
        </div>


        <form action="saveticket.php?customer_id=<?php echo $found_customer->user_info_id; ?>" class="form-horizontal" method="POST" enctype="multipart/form-data">

            <?php
            if (isset($_GET['option']) && $_GET['option'] == 'new') {
                echo '<div class="row">';
                echo '<div class="col-md-12">';

                //<!-- BEGIN PROFILE CONTENT -->
                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';
                echo '<h4> Créer un ticket : <strong>' . $found_customer->user_info_firstname . ' (' . $found_customer->user_info_lastname . ')</strong></h4>';
                echo '</div>';
                echo '</div>';
                echo '<div class="white-box col-lg-12" style="padding: 50px">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Nom du ticket</label>';
                echo '<input name="ticket_title" type="text" tabindex="1" id="ticket_title" class="form-control" required>';
                echo '</div>';

                echo '<div class="col-lg-6 p-t-20">';
                echo '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">';
                echo '<label for="sample2" class="mdl-textfield__label">Service</label>';
                echo '<select class="mdl-textfield__input" name="ticket_service_id">';

                $service = new Service();
                $found_services = $service->find_all_active();
                foreach ($found_services as $found_service) {

                    echo '<option class="mdl-menu__item" value="' . $found_service->service_id . '">' . $found_service->service_title . '</option>';
                }

                echo '</select>';
                echo '</div>';
                echo '</div>';
                echo '<input name="ticket_customer_id" type="hidden" tabindex="1" id="ticket_customer_id" class="form-control" value="' . $customer_id . '">';

                echo '<div class="form-group">';
                echo '<label for="ticket_datestart" class="">Date initialisation ticket</label>';
                echo '<input name="ticket_date" type="date" tabindex="1" id="ticket_date" value="" class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="ticket_datestart" class="">Date début ex�cution ticket</label>';
                echo '<input name="ticket_datestart" type="date" tabindex="1" id="ticket_datestart" class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="ticket_dateend" class="">Date fin ex�cution ticket</label>';
                echo '<input name="ticket_dateend" type="date" tabindex="1" id="ticket_dateend" class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="">Description du ticket</label>';
                echo '<textarea class="form-control" name="ticket_desc" id="ticket_desc"></textarea>';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="">Fichier</label>';
                echo '<input type="file" class="form-control" name="image">';
                echo '</div>';

//                echo '<div class="form-group">';
//                echo '<label for="subject" class="">Contrat actif?</label>';
//                echo '&nbsp; <input name="ticket_active" type="checkbox" tabindex="1" id="ticket_active">';
//                echo '</div>';

                echo '</div>';
                echo '</div>';

                echo '</div>';
                echo '<br>';
                echo '&nbsp;&nbsp;';
                echo '<input type="submit" name="new" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';


                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->
                echo '</div>';
            } //end if
            ?>


            <?php
            if (isset($_GET['option']) && $_GET['option'] == 'edit' && isset($_GET['ticket_id'])) {

                $ticket_id = $_GET['ticket_id'];
                $found_ticket = Ticket::find_by_id($_GET['ticket_id']);
                echo '<input type="hidden" name="ticket_id" value="' . $found_ticket->ticket_id . '">';

                echo '<div class="row">';
                echo '<div class="col-md-12">';
                //<!-- BEGIN PROFILE SIDEBAR -->
                echo '<div class="profile-sidebar">';
                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
                echo '<div class="profile-usertitle-name"> Attributs </div>';
                echo '<div class="profile-usertitle-job"> du ticket </div>';
                echo '</div>';
                echo '<div class="card-body no-padding height-9">';

                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b>Etat </b>';
                echo '<div class="profile-desc-item pull-right">';

                if ($found_ticket->ticket_deleted == 0) {
                    echo '<i class="fa fa-eye"></i> Activé';
                } else {
                    echo '<span class="label label-danger"><i class="fa fa-eye-slash"></i> Supprimé</span>';
                }


                echo '</div>';
                echo '</li>';


                echo '<li class="list-group-item">';
                if ($found_ticket->ticket_deleted == 0) {

                    echo '<b>Déplacer dans la corbeille </b>';
                    echo '<div class="profile-desc-item pull-right"> <a href="saveticket.php?ticket_id=' . $ticket_id . '&customer_id=' . $found_customer->user_info_id . '&option=delete"><i class="fa fa-trash-o "></i></a> </div>';
                } else {
                    echo '<b>Restaurer ce ticket </b>';
                    echo '<div class="profile-desc-item pull-right"> <a href="saveticket.php?ticket_id=' . $ticket_id . '&customer_id=' . $found_customer->user_info_id . '&option=recover"><i class="fa  fa-recycle "></i></a> </div>';
                }

                echo '</li>';

                echo '</ul><br/>';



                echo '<div class="controls">';
                echo '<div class="form-group">';
                echo '<div class="checkbox checkbox-icon-black">';
                echo '<br/><b> <a href="'.$app_or_appex .'.php?item=disabledtickets&list=disabled"><b>Corbeille </b></a></b>';
                echo '</div>';

                //<!-- END SIDEBAR BUTTONS -->
                echo '</div>';
                echo '</div>';
                echo '</div>';

                echo '</div>';
                echo '</div>';


                //<!-- END BEGIN PROFILE SIDEBAR -->

                echo '<div class="row">';
                echo '<div class="col-md-12">';

                //<!-- BEGIN PROFILE CONTENT -->
                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';
                echo '<h4> Editer le ticket <strong>' . $found_customer->user_info_firstname . ' (' . $found_customer->user_info_lastname . ')</strong> </h4>';
                echo '</div>';
                echo '</div>';
                echo '<div class="white-box col-lg-12" style="padding: 50px">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Nom du ticket</label>';
                echo '<input name="ticket_title" value="' . $found_ticket->ticket_title . '" type="text" tabindex="1" id="subject" class="form-control" required>';
                echo '</div>';

                echo '<div class="col-lg-6 p-t-20">';
                echo '<div class="mdl-textfield mdl-js-textfield mdl-textfield--floating-label getmdl-select getmdl-select__fix-height txt-full-width">';
                echo '<label for="sample2" class="mdl-textfield__label">Service</label>';
                echo '<select class="mdl-textfield__input" name="ticket_service_id">';

                $service = new Service();
                $found_service = $service->find_by_id($found_ticket->ticket_service_id);
                echo '<option class="mdl-menu__item" value="' . $found_service->service_id . '">' . $found_service->service_title . '</option>';

                $found_services = $service->list_others($found_ticket->ticket_service_id);
                foreach ($found_services as $found_service) {

                    echo '<option class="mdl-menu__item" value="' . $found_service->service_id . '">' . $found_service->service_title . '</option>';
                }

                echo '</select>';
                echo '</div>';
                echo '</div>';
                echo '<input name="ticket_customer_id" type="hidden" tabindex="1" id="ticket_customer_id" class="form-control" value="' . $customer_id . '">';

                echo '<div class="form-group">';
                echo '<label for="ticket_date" class="">Date initialisation ticket</label>';
                echo '<input name="ticket_date" type="date" tabindex="1" id="ticket_date" value="' . $found_ticket->ticket_date . '" class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="ticket_datestart" class="">Date début ex�cution ticket</label>';
                echo '<input name="ticket_datestart" type="date" tabindex="1" id="ticket_datestart" value="' . $found_ticket->ticket_datestart . '" class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="ticket_dateend" class="">Date fin ex�cution ticket</label>';
                echo '<input name="ticket_dateend" type="date" tabindex="1" id="ticket_dateend" value="' . $found_ticket->ticket_dateend . '" class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="">Description du ticket</label>';
                echo '<textarea class="form-control" name="ticket_desc" id="ticket_desc">' . $found_ticket->ticket_desc . '</textarea>';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="">Fichier</label>';
                echo '<input type="file" class="form-control" name="image">';
                if (!empty($found_ticket->ticket_file_path))
                    echo '<a href="media/files/' . $found_ticket->ticket_file_path . '">"<img src="media/files/' . $found_ticket->ticket_file_path . '" width="500"/> </a>';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Contrat actif?</label>';
                $checked = ($found_ticket->ticket_active) ? 'checked' : '';
                echo '&nbsp; <input name="ticket_active" type="checkbox" tabindex="1" id="ticket_active" ' . $checked . '>';
                echo '</div>';

                echo '</div>';
                echo '</div>';
                ?>


                <?php
                echo '</div>';
                echo '<br>';
                echo '&nbsp;&nbsp;';
                echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="valider">';


                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->
                echo '</div>';
            } //end if
            ?>


        </form>


        <!-- end page container -->

        <script src="assets/plugins/jquery/jquery.min.js"></script>

        <script>
            $('#add').click(function () {
                $('#form-bloc').append($('#another-form').html())
            })
        </script>
