<?php
$customer_id = (!empty($_GET['customer_id'])) ? $_GET['customer_id'] : null;
if (!empty($customer_id))
    $found_customer = User_info::find_by_id($customer_id);
?>
<!-- start ticket content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans Manager</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="app.php?item=listcustomers">Clients</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="#">tickets</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Liste des tickets</li>
                </ol>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-box">
                    <div class="card-head">
                        <header>Liste des tickets du client: <i style="color: blue"><?php echo $found_customer->user_info_firstname . ' (' . $found_customer->user_info_lastname . ')' ?></i></header>
                        <div class="tools">
                            <a class="fa fa-repeat btn-color box-refresh" href="javascript:;"></a>
                            <a class="t-collapse btn-color fa fa-chevron-down" href="javascript:;"></a>
                            <a class="t-close btn-color fa fa-times" href="javascript:;"></a>
                        </div>
                    </div>
                    <div class="card-body ">
                        <div class="row p-b-20">
                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group">
                                    <a href="app.php?item=formticket&customer_id=<?php echo $found_customer->user_info_id; ?>&option=new" id="addRow" class="btn btn-info">
                                        Nouveau ticket <i class="fa fa-plus"></i>
                                    </a>

                                </div>
                                <div class="btn-group">
                                    <a href="app.php?item=disabledtickets&customer_id=<?php echo $found_customer->user_info_id; ?>&list=disabled" id="addRow" class="btn btn-danger"><b>Corbeille </b></a> 
                                </div>
                            </div>

                            <div class="col-md-6 col-sm-6 col-6">
                                <div class="btn-group pull-right">
                                    <?php
                                    if (isset($_GET['ticket_id'])) {
                                        $ticket_id = $_GET['ticket_id'];
                                        echo '<a href="app.php?item=listtickets" class="btn deepPink-bgcolor">Retourner à liste précédente';
                                        echo '</a>';
                                    } // enf if
                                    ?>

                                </div>
                            </div>
                        </div>

                        <?php
                        if (isset($_GET['msg']) && $_GET['msg'] == 'success') {
                            echo '<span class="clsAvailable"> Enregistrement réussi. </span>';
                        }
                        ?>

                        <div class="table-scrollable">
                            <table class="table table-hover table-checkable order-column full-width"
                                   id="example4">
                                <thead>
                                    <tr>  
                                        <!--<th class="center"></th>-->
                                        <th class="center">Titre </th>
                                        <th class="center">Service </th>
                                        <th class="center"> Date initialisation </th>   
                                        <th class="center"> Date début </th>   
                                        <th class="center"> Date fin </th>   
                                        <th class="center"> En cours/Terminé </th>   
                                        <th class="center"></th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <?php
                                    if (empty($_GET['list']))
                                        $found_tickets = Ticket :: find_all_not_deleted();
                                    else
                                        $found_tickets = Ticket :: find_all_deleted();
                                    foreach ($found_tickets as $found_ticket) {
                                        echo '<tr class="odd gradeX">';
//                                        echo '<td class="center"></td>';
                                        echo '<td class="center">' . $found_ticket->ticket_title . '</td>';
                                        $found_service = Service::find_by_id($found_ticket->ticket_service_id);
                                        echo '<td class="center"><span class="label label-inverse">' . $found_service->service_title . '</span></td>';
                                        echo '<td class="center">' . (($found_ticket->ticket_date != "0000-00-00") ? $found_ticket->ticket_date : "Inconnue") . '</td>';
                                        echo '<td class="center">' . (($found_ticket->ticket_datestart != "0000-00-00") ? $found_ticket->ticket_datestart : "Inconnue") . '</td>';
                                        echo '<td class="center">' . (($found_ticket->ticket_dateend != "0000-00-00") ? $found_ticket->ticket_dateend : "Inconnue") . '</td>';
                                        echo '<td class="center">';
                                        echo '<span class="label label-' . (($found_ticket->ticket_active == 1) ? "success" : "danger") . '">' . (($found_ticket->ticket_active == 1) ? "En cours" : "Terminée") . '</span>';
                                        echo '</td>';
                                        echo '<td class="center">';

                                        echo '<a href="app.php?item=formticket&ticket_id=' . $found_ticket->ticket_id . '&customer_id=' . $found_customer->user_info_id . '&option=edit"><span class="label label-info">Modifier</span></a>';

                                        echo '</td>';

                                        echo '</tr>';
                                    } // end foreach
                                    ?>

                                </tbody>
                            </table>
                            <?php echo '<br/><b> </b>'; ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- end ticket content -->
