<!-- start page content -->
<div class="page-content-wrapper">
    <div class="page-content">
        <div class="page-bar">
            <div class="page-title-breadcrumb">
                <!--                <div class=" pull-left">
                                    <div class="page-title">Welcome to your website's manager</div>
                                </div>-->
                <div class=" pull-left">
                    <div class="page-title">Bienvenue dans le gestionnaire de votre site Web</div>
                </div>
                <ol class="breadcrumb page-breadcrumb pull-right">
                    <li><i class="fa fa-home"></i>&nbsp;<a class="parent-item"
                                                           href="app.php?item=home">Accueil</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li><a class="parent-item" href="app.php?item=listtopics_momments">Commentaires</a>&nbsp;<i class="fa fa-angle-right"></i>
                    </li>
                    <li class="active">Editer un commentaire</li>
                </ol>
            </div>
        </div>

        <form action="savetopic_comment.php" class="form-horizontal" method="POST" enctype="multipart/form-data"> 

            <?php
            if (isset($_GET['comment_id'])) {
                $comment_id = $_GET['comment_id'];
            }


            if (isset($_GET['option']) && $_GET['option'] == 'new') {

                echo '<div class="row">';
                echo '<div class="col-md-12">';
                //<!-- BEGIN PROFILE SIDEBAR -->
                echo '<div class="profile-sidebar">';
                echo '<div class="card">';
                echo '<div class="profile-usertitle">';
//                echo '<div class="profile-usertitle-name"> Attributes </div>';
//                echo '<div class="profile-usertitle-job"> of the page </div>';
                echo '<div class="profile-usertitle-name"> Attributs </div>';
                echo '<div class="profile-usertitle-job"> du commentaire </div>';
                echo '</div>';


                echo '<div id="displayed" class="card-body no-padding height-9">';
                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b>Status  </b>';
                echo '<div class="profile-desc-item pull-right">';
//                echo '<i class="fa fa-eye"></i> published';
                echo '<i class="fa fa-eye"></i> Publié';
                echo '</div>';
                echo '</li>';


                echo '</ul><br/>';

                echo '</div>';


                echo '</div>';



                //<!-- END SIDEBAR USER TITLE -->
                echo '<br/>';
                echo '<div class="control-group">';
//                echo '<label class="control-label" for="active">Publish this page ?</label>';
                echo '<label class="control-label" for="active">Publier le commentaire ?</label>';
                echo '<div class="controls">';
                echo '<div class="form-group">';
                echo '<div class="checkbox checkbox-icon-black">';
                echo '<input id="rememberChk1" name="published" type="checkbox" checked="checked">';
                echo '</div>';
                echo '</div>';
                echo '</div>';


                //<!-- SIDEBAR BUTTONS -->
                echo '</div>';
                echo '</div>';




                //<!-- END BEGIN PROFILE SIDEBAR -->
                //<!-- BEGIN PROFILE CONTENT -->
                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';

                echo '<h4> New page </h4>';

                echo '</div>';
                echo '</div>';
                echo '<div class="white-box">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Date du commentaire:</label>';
                echo '<input name="author" id="title" value="' . date("Y-m-d") . '"  type="date" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Auteur du commentaire:</label>';
                echo '<input name="author" id="title" value=""  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Email de l\'Auteur du commentaire:</label>';
                echo '<input name="author_email" id="title" value=""  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

//                echo '<div class="form-group">';
//                echo '<label for="subject" class="">Page description: &nbsp;</label>';
//                echo '<input name="only_desc" id="title" type="checkbox" tabindex="1" id="subject">';
//                echo '</div>';
                echo '<div class="form-group">';
                echo '<label for="subject" class="">Comentaire:</label>';
                echo '<textarea name="desc" id="description" class="span12" rows="8"></textarea>';

                //
                echo '</div>';
                echo '</div>';

                echo '</div>';
                echo '<br>';
                echo '&nbsp;&nbsp;';

//                if (isset($_GET['option']) && $_GET['option'] == 'new') {
//                    echo '<input type="submit" name="new" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="Submit">';
//                } else {
//                    echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="Submit">';
//                }
                if (isset($_GET['option']) && $_GET['option'] == 'new') {
                    echo '<input type="submit" name="new" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="Valider">';
                } else {
                    echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="Valider">';
                }

                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->
                echo '</div>';
            } //end if
            ?>


            <?php
            if (isset($_GET['option']) && $_GET['option'] == 'edit' && isset($_GET['comment_id'])) {

                $comment_id = $_GET['comment_id'];
                echo '<input type="hidden" name="comment_id" value="' . $comment_id . '" />';
                $found_comment = Comment_topic::find_by_id($comment_id);
                $found_topic = Topics::find_by_id($found_comment->comment_topic_topicid);
                echo '<input type="hidden" name="topicid" value="' . $found_topic->topic_id . '" />';
                echo '<div class="row">';
                echo '<div class="col-md-12">';
                //<!-- BEGIN PROFILE SIDEBAR -->
                echo '<div class="profile-sidebar">';
                echo '<div class="card">';

                echo '<div class="profile-usertitle">';
//                echo '<div class="profile-usertitle-name"> Page Attributes</div>';
//                echo '<div class="profile-usertitle-job">  ' . $found_page->page_title . ' </div>';
                echo '<div class="profile-usertitle-name"> Détails du commentaire de l\'article</div>';

                echo '<div class="profile-usertitle-job">  ' . $found_topic->topic_title . ' </div>';

                echo '</div>';

                echo '<div class="card-body no-padding height-9">';

                echo '<ul class="list-group list-group-unbordered">';
                echo '<li class="list-group-item">';
                echo '<b>Etat </b>';
                echo '<div class="profile-desc-item pull-right">';
//                if ($found_comment->comment_topic_published == 1) {
//                    echo '<i class="fa fa-eye"></i> published';
//                } else {
//                    echo '<span class="label label-danger"><i class="fa fa-eye-slash"></i> Not published</span>';
//                }
                if ($found_comment->comment_topic_published == 1) {
                    echo '<i class="fa fa-eye"></i> Publié';
                } else {
                    echo '<span class="label label-danger"><i class="fa fa-eye-slash"></i> Non publié</span>';
                }

                echo '</div>';
                echo '</li>';

                echo '<li class="list-group-item">';
                echo '<b>Publié le </b>';
                echo '<div class="profile-desc-item pull-right">';
                echo '<i class="fa fa-clock-o"> </i> ' . $found_comment->comment_topic_date . ' ';
                echo '</div>';
                echo '</li>';

                echo '<li class="list-group-item">';
                if (isset($_GET['option']) && isset($_GET['r']) && $_GET['option'] == 'edit' && $_GET['r'] == 0 || $found_comment->comment_topic_active == 0) {

                    echo '<b>Restaurer ce commentaire</b>';
                    echo '<div class="profile-desc-item pull-right"> <a href="savetopic_comment.php?comment_id=' . $comment_id . '&option=recover"><i class="fa  fa-recycle "></i></a> </div>';
                } else {
                    echo '<b>Déplacer dans la corbeille </b>';
                    echo '<div class="profile-desc-item pull-right"> <a href="savetopic_comment.php?comment_id=' . $comment_id . '&option=delete"><i class="fa fa-trash-o "></i></a> </div>';
                }

                echo '</li>';
                echo '<br/><b> <a href="app.php?item=disabledtopics_comments"><b>Corbeille </b></a></b>';

                echo '</ul>';

                echo '</div>';
//                echo '</div>';

                echo '<div class="controls">';
                echo '<div class="form-group">';
                echo '<div class="checkbox checkbox-icon-black">';
                $found_comment = $comment_topic->find_by_id($comment_id);
//                if ($found_comment->comment_topic_published == 1) {
//                    echo '<input id="rememberChk1" name="active" type="checkbox" checked="checked"> published';
//                } else {
//                    echo '<input id="rememberChk1" name="active" type="checkbox"> not published';
//                }
                if ($found_comment->comment_topic_published == 1) {
                    echo '<input id="rememberChk1" name="published" type="checkbox" checked="checked"> Publié';
                } else {
                    echo '<input id="rememberChk1" name="published" type="checkbox"> Non publié';
                }

//                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END SIDEBAR BUTTONS -->
                echo '</div>';
                echo '</div>';

                echo '</div>';


                //<!-- END BEGIN PROFILE SIDEBAR -->
                //<!-- BEGIN PROFILE CONTENT -->

                echo '<div class="profile-content">';
                echo '<div class="row">';
                echo '<div class="profile-tab-box">';
                echo '<div class="p-l-20">';
//                echo '<h4> Update the page : ' . $found_topic->topic_title . ' </h4>';
                echo '<h4> Mettre à jour un commentaire de l\'article : ' . $found_topic->topic_title . ' </h4>';

//                if (isset($_GET['r']) && $_GET['r'] == 0) {
//                    echo '<span class="clsNotAvailable"> This page is placed in the recycle bin.  </span><br/>';
//                } else if (isset($_GET['r']) && $_GET['r'] == 1) {
//                    echo '<span class="clsAvailable"> This page has been restored. </span><br/>';
//                }
                if (isset($_GET['r']) && $_GET['r'] == 0) {
                    echo '<span class="clsNotAvailable"> Ce commentaire est placé dans la corbeille.  </span><br/>';
                } else if (isset($_GET['r']) && $_GET['r'] == 1) {
                    echo '<span class="clsAvailable"> Ce commentaire a été restauré. </span><br/>';
                }



                echo '<div class="btn-group">';
//                echo '<a href="app.php?item=formtopic_comment&comment_id=' . $comment_id . '&option=new" id="addRow" class="btn btn-info"> New coment <i class="fa fa-plus"></i>';
                echo '<a href="app.php?item=formtopic_comment&comment_id=' . $comment_id . '&option=new" id="addRow" class="btn btn-info"> Nouveau commentaire <i class="fa fa-plus"></i>';
                echo '</a>';
                echo '</div>';


                echo '</div>';
                echo '</div>';

                echo '<div class="white-box">';
                //<!-- Tab panes -->
                echo '<div class="tab-content">';
                echo '<div class="tab-pane active fontawesome-demo" id="tab1">';

                echo '<div class="inbox-body no-pad">';
                echo '<div class="mail-list">';
                echo '<div class="compose-mail">';


                echo '<div class="form-group">';
                echo '<label for="subject" class="">Date du commentaire:</label>';
                echo '<input name="author" id="title" value="' . $found_comment->comment_topic_date . '"  type="date" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Auteur du commentaire:</label>';
                echo '<input name="author" id="title" value="' . $found_comment->comment_topic_author . '"  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="form-group">';
                echo '<label for="subject" class="">Email de l\'Auteur du commentaire:</label>';
                echo '<input name="author_email" id="title" value="' . $found_comment->comment_topic_author_email . '"  type="text" tabindex="1" id="subject"	class="form-control">';
                echo '</div>';

                echo '<div class="col-lg-12 p-t-20">';
                echo '<div class="mdl-textfield mdl-js-textfield txt-full-width">';
                echo '<label for="subject" class="">Comentaire:</label>';
                echo '<textarea class="mdl-textfield__input" rows="4" name="desc" id="description">' . $found_comment->comment_topic_text . '</textarea>';

                echo '</div>';
                echo '</div>';


                echo '<br>';
                echo '&nbsp;&nbsp;';
                echo '<input type="submit" name="submit" class="mdl-button mdl-js-button mdl-button--raised mdl-js-ripple-effect m-b-10 btn-circle btn-primary" value="Valider">';

                echo '</div>';
                echo '</div>';
                echo '</div>';
                echo '</div>';

                //<!-- END PROFILE CONTENT -->
                echo '</div>';
                echo '</div>';
                echo '</div>';
                //<!-- end page content -->

                echo '</div>';
            } // end if
            ?>

        </form>


        <!-- end page container -->

