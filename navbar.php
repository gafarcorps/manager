<?php
require_once("includes/initialize.php");
require_once("includes/verifsession.php");
?>
<?php
$user_account = new User_account();
if (!empty($session->user_account_id)) {
    $found_user_account = $user_account->find_by_id($session->user_account_id);
    $found_user_info = User_info::find_by_user_account($found_user_account->user_account_id);
}
?>

<!DOCTYPE html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title>MANAGER 1.2</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width">

        <!-- Place favicon.png and apple-touch-icon.png in the root directory -->

        <!--base css styles-->
        <link rel="stylesheet" href="assets/bootstrap/bootstrap.min.css">
        <link rel="stylesheet" href="assets/bootstrap/bootstrap-responsive.min.css">
        <link rel="stylesheet" href="assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="assets/normalize/normalize.css">

        <!--page specific css styles-->
        <link rel="stylesheet" type="text/css" href="assets/chosen-bootstrap/chosen.min.css" />
        <link rel="stylesheet" type="text/css" href="assets/jquery-tags-input/jquery.tagsinput.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-fileupload/bootstrap-fileupload.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-colorpicker/css/colorpicker.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-timepicker/compiled/timepicker.css" />
        <link rel="stylesheet" type="text/css" href="assets/clockface/css/clockface.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-datepicker/css/datepicker.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-daterangepicker/daterangepicker.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-switch/static/stylesheets/bootstrap-switch.css" />
        <link rel="stylesheet" type="text/css" href="assets/bootstrap-wysihtml5/bootstrap-wysihtml5.css" />
        <script type="text/javascript" src="tiny_mce/js/tinymce/tinymce.min.js"></script>
        <script type="text/javascript">
            /*
             tinymce.init({
             selector: "textarea",
             automatic_uploads: true,
             plugins: [
             "advlist autolink lists link image charmap print preview anchor",
             "searchreplace visualblocks code fullscreen",
             "insertdatetime media table contextmenu paste"
             ],
             toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image"
             });
             */


            tinymce.init({
                selector: "textarea",
                automatic_uploads: true,
                theme: "modern",
                width: 1400,
                height: 300,
                subfolder: "",
                plugins: [
                    "advlist autolink link image lists charmap print preview hr anchor pagebreak",
                    "searchreplace wordcount visualblocks visualchars code insertdatetime media nonbreaking",
                    "table contextmenu directionality emoticons paste textcolor filemanager"
                ],
                image_advtab: true,
                toolbar: "undo redo | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | styleselect forecolor backcolor | link unlink anchor | image media | print preview code"
            });

        </script>


        <!--flaty css styles-->
        <link rel="stylesheet" href="css/flaty.css">
        <link rel="stylesheet" href="css/flaty-responsive.css">

        <link rel="shortcut icon" href="img/favicon.html">

        <script src="assets/modernizr/modernizr-2.6.2.min.js"></script>

    </head>
    <body>



        <!-- BEGIN Navbar -->
        <div id="navbar" class="navbar">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <!-- BEGIN Brand -->
                    <a href="app.php?item=home" class="brand">
                        <small>
                            <i class="icon-desktop"></i>
                            MANAGER 1.2
                        </small>
                    </a>
                    <!-- END Brand -->

                    <!-- BEGIN Responsive Sidebar Collapse -->
                    <a href="#" class="btn-navbar collapsed" data-toggle="collapse" data-target=".nav-collapse">
                        <i class="icon-reorder"></i>
                    </a>
                    <!-- END Responsive Sidebar Collapse -->

                    <!-- BEGIN Navbar Buttons -->
                    <ul class="nav flaty-nav pull-right">
                        <!-- BEGIN Button Tasks -->

                        <!-- BEGIN Button Notifications -->
                        <!--                        <li class="hidden-phone">
                                                    <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                                        <i class="icon-envelope"></i>
                                                        <span class="badge badge-important">  <?php //  $found_emails = Message :: count_newmails();  if(empty($found_emails)) {$found_emails = 0; }  echo $found_emails;        ?> </span>
                                                    </a>-->

                        <!-- BEGIN Notifications Dropdown -->
                        <!--                            <ul class="dropdown-navbar dropdown-menu">
                                                        <li class="nav-header"> 
                        <?php // echo $found_emails;   ?>  message(s)
                                                        </li> 
                         
                        <?php
                        //  $found_emails = Message :: find_newmails();  
//												foreach($found_emails as $found_email){ 
//														echo '<li class="msg">';
//                                   						echo '<a href="#">';
//														echo '<img src="img/demo/avatar/avatar1.jpg" alt="creativesweb message" />';
//														echo '<div>';
//														echo '<span class="msg-title">'.$found_email->message_subject.'</span>';
//														echo '<span class="msg-time">';
//														echo '<i class="icon-time"></i> ';
//														echo '<span> '.changedateusfr($found_email->message_date).'</span>';
//														echo '</span>';
//														echo '</div>';
//														echo '<p'.neatest_trim($found_email->message_text, 10).'</p>';
//														echo '</a>';
//														echo '<br/>';
//                                						echo '</li>';
//											
//											}
                        ?>
                                                            
                                                           
                        
                         
                        
                                                        <li class="more">
                                                            <a href="listmessages.php">Voir tous les messages.</a>
                                                        </li>
                                                    </ul>-->


                        <!-- END Notifications Dropdown -->
                        <!--</li>-->
                        <!-- END Button Notifications -->

                        <!-- BEGIN Button User -->
                        <li class="user-profile">
                            <a data-toggle="dropdown" href="#" class="user-menu dropdown-toggle">
                                <?php
                                if (!empty($found_user_info->user_info_image)) {
                                    echo '<img class="nav-user-photo" src="../images/' . $found_user_info->user_info_image . '" height="31" alt="' . $found_user_info->user_info_firstname . '" />';
                                } else {
                                    echo '<img class="nav-user_-photo" src="img/demo/avatar/avatar1.jpg" alt="' . $found_user_info->user_info_firstname . '" />';
                                }
                                ?>

                                <span class="hidden-phone" id="user_info">
                                    <?php
                                    $user_account = new User_account();
                                    $found_user_account = $user_account->find_by_id($session->user_account_id);
                                    echo $found_user_info->user_info_firstname;
                                    ?>
                                </span>
                                <i class="icon-caret-down"></i>
                            </a>

                            <!-- BEGIN User Dropdown -->
                            <ul class="dropdown-menu dropdown-navbar" id="user_menu">
                                <li>
                                    <a href="formuser.php?opt=me">
                                        <i class="icon-user"></i>
                                        Editer le profil
                                    </a>

                                </li>
<!--                                <li>
                                    <a href="formuser.php?opt=me&password=change">
                                        <i class="icon-user"></i>
                                        Editer mon mot de passe
                                    </a>

                                </li>-->

                                <!--                                <li>
                                                                    <a href="formhelp.php">
                                                                        <i class="icon-question"></i>
                                                                        Aide
                                                                    </a>
                                                                </li>-->

                                <!--                                <li class="divider visible-phone"></li>
                                
                                                                <li class="visible-phone">
                                                                    <a href="#">
                                                                        <i class="icon-tasks"></i>
                                                                        Tasks
                                                                        <span class="badge badge-warning">4</span>
                                                                    </a>
                                                                </li>
                                                                <li class="visible-phone">
                                                                    <a href="#">
                                                                        <i class="icon-bell-alt"></i>
                                                                        Notifications
                                                                        <span class="badge badge-important">8</span>
                                                                    </a>
                                                                </li>
                                                                <li class="visible-phone">
                                                                    <a href="#">
                                                                        <i class="icon-envelope"></i>
                                                                        Messages
                                                                        <span class="badge badge-success">5</span>
                                                                    </a>
                                                                </li>-->

                                <!--<li class="divider"></li>-->

                                <li>
                                    <?php echo '<a href="logout.php">'; ?>
                                    <i class="icon-off"></i>
                                    Déconnexion
                                    </a>
                                </li>
                            </ul>
                            <!-- BEGIN User_account Dropdown -->
                        </li>
                        <!-- END Button User_account -->
                        </li>
                        <!-- END Button User_account -->
                    </ul>
                    <!-- END Navbar Buttons -->
                </div><!--/.container-fluid-->
            </div><!--/.navbar-inner-->
        </div>						