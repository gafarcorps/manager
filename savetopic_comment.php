<?php

require_once("includes/initialize.php");


if (isset($_POST['comment_id'])) {
    $comment_id = $_POST['comment_id'];
} //$comment_id is the id of current comment
if (isset($_POST['topicid'])) {
    $comment_topic_topicid = $_POST['topicid'];
}
//if (isset($_POST['title'])) {
//    $comment_title = $_POST['title'];
//}
if (isset($_POST['desc'])) {
    $comment_desc = $_POST['desc'];
}
if (isset($_POST['author'])) {
    $comment_topic_author = $_POST['author'];
}
if (isset($_POST['author_email'])) {
    $comment_topic_author_email = $_POST['author_email'];
}
if (isset($_POST['published'])) {
    $comment_topic_published = $_POST['published'];
}
if (isset($_POST['active'])) {
    $comment_topic_active = $_POST['active'];
}

//if (!empty($_FILES['file_upload']['name'])) {
//    $repository = 'media/files/';
//    $file_upload = basename($_FILES['file_upload']['name']);
//}
//
if (!empty($comment_topic_published)) {
    $comment_topic_published = 1;
} else {
    $comment_topic_published = 0;
}
if (!empty($comment_topic_active)) {
    $comment_topic_active = 1;
} else {
    $comment_topic_active = 0;
}

$comment_date = date('Y-m-d H:i:s');

if (isset($_POST['new']) && !empty($session->user_account_id)) { // New comment menu 
    $comment_topic->comment_topic_topicid = $comment_topic_topicid;
//    $comment_topic->comment_title = $comment_title;
    $comment_topic->comment_topic_text = $comment_desc;
    $comment_topic->comment_topic_author = $comment_topic_author;
    $comment_topic->comment_topic_author_email = $comment_topic_author_email;
    $comment_topic->comment_topic_date = $comment_date;
    $comment_topic->comment_topic_published = $comment_topic_published;
    $comment_topic->comment_topic_active = $comment_topic_active;

//    if (!empty($_FILES['file_upload'])) {
//        move_uploaded_file($_FILES['file_upload']['tmp_name'], $repository . $file_upload);
//        $comment_topic->comment_image_path = $file_upload;
//    }

    $comment_topic->save();


    redirect_to('app.php?item=listtopics_comments&msg=success');
} // END IF NEW ENTRY  






if (isset($_POST['submit']) && !empty($comment_id) && !empty($session->user_account_id)) { // Updating a comment			   	
    $found_comment = $comment_topic->find_by_id($comment_id);

    $comment_topic->comment_topic_id = $found_comment->comment_topic_id;

    $comment_topic->comment_topic_topicid = $comment_topic_topicid;
//    $comment_topic->comment_title = $comment_title;
    $comment_topic->comment_topic_text = $comment_desc;
    $comment_topic->comment_topic_author = $comment_topic_author;
    $comment_topic->comment_topic_author_email = $comment_topic_author_email;
    $comment_topic->comment_topic_date = $comment_date;
    $comment_topic->comment_topic_published = $comment_topic_published;
    $comment_topic->comment_topic_active = $comment_topic_active;

//    if (!empty($_FILES['file_upload'])) {
//        move_uploaded_file($_FILES['file_upload']['tmp_name'], $repository . $file_upload);
//        $comment_topic->comment_image_path = $file_upload;
//    }

    $comment_topic->save();
    redirect_to('app.php?item=listtopics_comments&msg=success');
}  // END IF UPDATE 	





if (!empty($_GET['comment_id']) && !empty($session->user_account_id) && $_GET['option'] == 'delete') {  // DELETE ENTRIE
    $comment_id = $_GET['comment_id'];
    $comment_topic = $comment_topic->find_by_id($comment_id);
    $del = $comment_topic->unable();
}  // END IF DELETE 

if (!empty($_GET['comment_id']) && !empty($session->user_account_id) && $_GET['option'] == 'recover') {  // DELETE ENTRIE
    $comment_id = $_GET['comment_id'];
    $comment_topic = $comment_topic->find_by_id($comment_id);
    $del = $comment_topic->recover();
}  // END IF DELETE 

if (!empty($_GET['comment_id']) && !empty($session->user_account_id) && $_GET['option'] == 'disable') {  // DELETE ENTRIE
    $comment_id = $_GET['comment_id'];
    $comment_topic = $comment_topic->find_by_id($comment_id);
    $comment_topic->comment_topic_published = 0;
    $comment_topic->save();
}  // END IF DELETE 

if (!empty($_GET['comment_id']) && !empty($session->user_account_id) && $_GET['option'] == 'activate') {  // DELETE ENTRIE
    $comment_id = $_GET['comment_id'];
    $comment_topic = $comment_topic->find_by_id($comment_id);
    $comment_topic->comment_topic_published = 1;
    $comment_topic->save();
}  // END IF DELETE 
redirect_to('app.php?item=listtopics_comments&msg=success');
?>
 